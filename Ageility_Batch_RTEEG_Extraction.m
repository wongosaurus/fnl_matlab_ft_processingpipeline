%% Age-ility Processing Pipeline
%  Adapted by Patrick Cooper and Aaron Wong
%  May - 2014, Functional Neuroimaging Laboratory
%% Set-up working parameters
close all
clear all
clc
warning off;
wpms       = [];%working parameters
wpms.dirs  = struct('CWD',                  'F:\FNL\Joint_Modelling\Pipeline_Extraction\',...
                    'packages',             'PACKAGES', ...
                    'FUNCTIONS',            'FUNCTIONS\',...
                    'RAW',                  'RAW\',...
                    'preproc',              'PREPROC_OUTPUT\', ...
                    'DATA_DIR',             'EEGLAB_FORMAT\',...
                    'WAVELET_OUTPUT_DIR',   'WAVELET_OUTPUT_DIR\', ...
                    'COHERENCE_DIR',        'IMAGCOH_OUTPUT\',...
                    'JOINT_IN_DIR',         'JOINT_MODEL_INPUT\');
                
%wpms.names = {'AGE002','AGE003','AGE005','AGE007','AGE008','AGE012','AGE013','AGE014','AGE015','AGE017','AGE018','AGE019','AGE020','AGE021','AGE022','AGE023','AGE024','AGE026','AGE027','AGE028','AGE030','AGE032','AGE033','AGE035','AGE036','AGE038','AGE043','AGE046','AGE047','AGE048','AGE050','AGE051','AGE052','AGE053','AGE058','AGE059','AGE061','AGE062','AGE063','AGE064','AGE066','AGE067','AGE068','AGE069','AGE070','AGE072','AGE075','AGE077','AGE079','AGE081','AGE083','AGE084','AGE085','AGE086','AGE088','AGE089','AGE090','AGE092','AGE093','AGE094','AGE095','AGE096','AGE097','AGE098','AGE100','AGE102','AGE103','AGE104','AGE106','AGE107','AGE108','AGE109','AGE114','AGE115','AGE116','AGE117','AGE118','AGE119','AGE120','AGE121','AGE122','AGE123','AGE124','AGE127','AGE128','AGE129','AGE130','AGE131','AGE133','AGE134','AGE135','AGE136','AGE138','AGE141','AGE143','AGE145','AGE146','AGE147','AGE148','AGE149','AGE150','AGE151','AGE152','AGE153','AGE155','AGE156','AGE158','AGE159','AGE161','AGE162','AGE163','AGE164','AGE165','AGE166','AGE167','AGE168','AGE169','AGE170','AGE172','AGE175','AGE176','AGE177','AGE178','AGE179','AGE180','AGE181','AGE182','AGE183','AGE184','AGE185','AGE186','AGE187','AGE189','AGE190','AGE192','AGE195','AGE197','AGE198','AGE199','AGE200','AGE201','AGE202','AGE203','AGE204','AGE205','AGE206','AGE207','AGE208','AGE209','AGE210','AGE212','AGE216','AGE217','AGE218','AGE219','AGE220','AGE222','AGE224','AGE225','AGE226','AGE227','AGE228','AGE229','AGE230','AGE231','AGE233','AGE234','AGE236','AGE237','AGE238','AGE239','AGE240','AGE241','AGE243','AGE244','AGE245','AGE246','AGE247','AGE248','AGE249','AGE251','AGE252','AGE253','AGE254','AGE255','AGE256','AGE257','AGE258','AGE259','AGE260','AGE261','AGE264','AGE265','AGE266','AGE267','AGE268','AGE273','AGE276','AGE277','AGE279'};%AGE079 still has problems with empty files
wpms.names = {  %'AGE004_TSWT_A','AGE004_TSWT_B',...
                %'AGE009_TSWT_A','AGE009_TSWT_B', ...
                %'AGE034_TSWT_A','AGE034_TSWT_B', ...
                'AGE005_TSWT', ...
                'AGE007_TSWT', ...
                'AGE008_TSWT', ...
                'AGE012_TSWT', ...
                'AGE013_TSWT', ...
                'AGE014_TSWT', ...
                'AGE015_TSWT', ...
                'AGE017_TSWT', ...
                'AGE018_TSWT', ...
                'AGE019_TSWT', ...
                'AGE020_TSWT', ...
                'AGE021_TSWT', ...
                'AGE022_TSWT', ...
                'AGE023_TSWT', ...
                'AGE024_TSWT', ...
                'AGE026_TSWT', ...
                'AGE027_TSWT', ...
                'AGE028_TSWT', ...
                'AGE030_TSWT', ...
                'AGE032_TSWT', ...
                'AGE033_TSWT', ...
                'AGE035_TSWT', ...
                'AGE036_TSWT', ...
                'AGE038_TSWT', ...
                'AGE043_TSWT', ...
                'AGE046_TSWT', ...
                'AGE047_TSWT'};

% add path
wpms.sampling_frequency = 512;
cd([wpms.dirs.CWD]);
addpath(genpath([wpms.dirs.CWD,wpms.dirs.FUNCTIONS]));
addpath(genpath([wpms.dirs.CWD,wpms.dirs.packages '\fieldtrip']));
%% Preprocessing I
%  Import, downsample and re-reference
for name_i = 1:length(wpms.names)

    [dat] = fnl_importeeg_and_downsample(wpms,'bdf',name_i,wpms.sampling_frequency);
    [refdat] = fnl_rereference(dat,48);
    
    clear dat
    
    [data] = fnl_preproc_filter(refdat,'yes',[48 52],'yes',0.1,'onepass',1,'but');
    
    save([wpms.dirs.CWD wpms.dirs.preproc wpms.names{name_i} '_REFnFILT'],'data','-v7.3');
    clear refdat data cfg %tidying
end

%% Visual Inspection of Data:
for name_i = 1:length(wpms.names)   
    fnl_bad_channel_inspection(wpms,name_i);
end

%% Remove Bad Channels out from Data:
for name_i = 1:length(wpms.names)   
    fnl_remove_bad_channels(wpms,name_i);
end

%% Trial Definition and ICA
% ------ SPECIAL CASE: SPLIT FILE ------------------
%'AGE004_TSWT_A','AGE004_TSWT_B',...
%'AGE009_TSWT_A','AGE009_TSWT_B', ...
%'AGE034_TSWT_A','AGE034_TSWT_B', ...
wpms.names = {  'AGE034_TSWT_A','AGE034_TSWT_B' };
%for name_i = 1:length(wpms.names)   
    pre_trial = 1.0;
    post_trial = 3.5;
    trialfunction = 'Agility_ALL'; 
    trdat = fnl_trial_definition(wpms,1,trialfunction,pre_trial,post_trial);
    trdat2 = fnl_trial_definition(wpms,2,trialfunction,pre_trial,post_trial);
    
    % Join Both Together:
    trdat.trial = [trdat.trial,trdat2.trial];
    trdat.time =  [trdat.time,trdat2.time];
    trdat.sampleinfo =  [trdat.sampleinfo;trdat2.sampleinfo];
    trdat.trialinfo =  [trdat.trialinfo;trdat2.trialinfo];
    trdat.trl =        [trdat.trl;trdat2.trl];
    
    
    clear post_trial pre_trial trialfunction trdat2
    wpms.names = {wpms.names{1}(1:end-2)};
    name_i = 1;
    fnl_ICA(wpms,name_i,trdat);        
%end
%% Standard Trial Definition and ICA:

for name_i = 23:length(wpms.names)   
    pre_trial = 1.0;
    post_trial = 3.5;
    trialfunction = 'Agility_ALL'; 
    trdat = fnl_trial_definition(wpms,name_i,trialfunction,pre_trial,post_trial);

    fnl_ICA(wpms,name_i,trdat);        
end

%% Remove EOG Components from ICA
wpms.names = {  'AGE004_TSWT','AGE009_TSWT','AGE034_TSWT'};
for name_i = 1:length(wpms.names)
    fnl_ICA_inspection(wpms,name_i);
end

%% Apply artifact rejection
for name_i = 1:length(wpms.names)
    fnl_artifact_rejection_auto(wpms,name_i,true,30,-120,120)%Olivia try this with -120 and 120 as the last values instead -Patrick
end
%% Apply scalp current density montage
% addpath(genpath([wpms.dirs.CWD,wpms.dirs.packages '\CSDtoolbox']));
% for name_i = 1:length(wpms.names)
%     fnl_csd_transformation(wpms,name_i);
% end
%% Reinterpolate bad channels
for name_i = 1:length(wpms.names)
    fnl_reinterpolate(wpms,name_i);
end
%% CONVERT FROM FIELDTRIP TO EEG STRUCT %NAT

conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninf','noninfswitch','noninfrepeat'};
cond = {'ar','mr','st','sa','ni','ns','nr'};
for name_i =1:length(wpms.names)
    try
        fnl_saveOffIndividualConditions(wpms,name_i);
    catch error
        fnl_saveOffIndividualConditions_altbiosemicodes(wpms,name_i);
    end
    fnl_setup_eegstructure(wpms,conditions,cond,name_i)
end

%% PRODUCE TIMELOCK ERP - Not for Coherency analysis

for name_i = 1:length(wpms.names)
    conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninf','noninfswitch','noninfrepeat'};
    %baseline is from -200 to 0 (cue)
    % We know that Data starts is 1 second before Cue. (512)
    baseline_start = floor(wpms.sampling_frequency*0.8);
    baseline_end = floor(wpms.sampling_frequency*1.0);
    for cond_i = 1:length(conditions)
        timelock.(conditions{cond_i}) = fnl_timelockanalysis(wpms,name_i,conditions{cond_i},baseline_start,baseline_end);
    end;
    save([wpms.dirs.CWD wpms.dirs.preproc wpms.names{name_i} '_TIMELOCK.mat'],'timelock*','-v7.3');
    clear timelock*
end

%% Draw TIMELOCK ERP:
close all;
for name_i = 1%:length(wpms.names)
    isreverse_ydir = true; %true or false
    channel = 30;
    fnl_plotERP(wpms,name_i,channel,isreverse_ydir)
    fnl_plotERP_AllRepeatRemoved(wpms,name_i,channel,isreverse_ydir)
end

%% START AGAIN: But process with the Ageility_ALL function:
%But this time we include all trials.


% Trial Definition and ICA
for name_i = 1:length(wpms.names)   
    pre_trial = 1.0;
    post_trial = 3.5;
    trialfunction = 'Agility_ALL'; 
    trdat = fnl_trial_definition(wpms,name_i,trialfunction,pre_trial,post_trial);
    %trdat2 = fnl_trial_definition(wpms,2,trialfunction,pre_trial,post_trial);
    
    % Join Both Together:
    %trdat.trial = [trdat.trial,trdat2.trial];
    %trdat.time =  [trdat.time,trdat2.time];
    %trdat.sampleinfo =  [trdat.sampleinfo;trdat2.sampleinfo];
    %trdat.trialinfo =  [trdat.trialinfo;trdat2.trialinfo];
    %trdat.trl =        [trdat.trl;trdat2.trl];
    
    
    clear post_trial pre_trial trialfunction trdat2
    %wpms.names = {'AGE004'};
    %name_i = 1;
    
    fnl_ICA(wpms,name_i,trdat);        
end

%% SPLIT FILES: START AGAIN: But process with the Ageility_ALL function:
%But this time we include all trials.
wpms.splitfile_names = {  'AGE004_TSWT_A','AGE004_TSWT_B',...
                'AGE009_TSWT_A','AGE009_TSWT_B', ...
                'AGE034_TSWT_A','AGE034_TSWT_B', ...
                };
            
wpms.names = {  'AGE004_TSWT', ...%'AGE004_TSWT_A','AGE004_TSWT_B',...
                'AGE009_TSWT', ...%'AGE009_TSWT_A','AGE009_TSWT_B', ...
                'AGE034_TSWT', ...%'AGE034_TSWT_A','AGE034_TSWT_B', ...
                'AGE005_TSWT', ...
                'AGE007_TSWT', ...
                'AGE008_TSWT', ...
                'AGE012_TSWT', ...
                'AGE013_TSWT', ...
                'AGE014_TSWT', ...
                'AGE015_TSWT', ...
                'AGE017_TSWT', ...
                'AGE018_TSWT', ...
                'AGE019_TSWT', ...
                'AGE020_TSWT', ...
                'AGE021_TSWT', ...
                'AGE022_TSWT', ...
                'AGE023_TSWT', ...
                'AGE024_TSWT', ...
                'AGE026_TSWT', ...
                'AGE027_TSWT', ...
                'AGE028_TSWT', ...
                'AGE030_TSWT', ...
                'AGE032_TSWT', ...
                'AGE033_TSWT', ...
                'AGE035_TSWT', ...
                'AGE036_TSWT', ...
                'AGE038_TSWT', ...
                'AGE043_TSWT', ...
                'AGE046_TSWT', ...
                'AGE047_TSWT'};
% Trial Definition and ICA
count = 1;
for name_i = 1:2:length(wpms.splitfile_names)   
    pre_trial = 1.0;
    post_trial = 3.5;
    trialfunction = 'Agility_ALL';
    temp_wpms = wpms;
    temp_wpms.names = wpms.splitfile_names;
    
    trdat = fnl_trial_definition(temp_wpms,name_i,trialfunction,pre_trial,post_trial);
    trdat2 = fnl_trial_definition(temp_wpms,name_i+1,trialfunction,pre_trial,post_trial);
    
    % Join Both Together:
    trdat.trial =       [trdat.trial,trdat2.trial];
    trdat.time =        [trdat.time,trdat2.time];
    trdat.sampleinfo =  [trdat.sampleinfo;trdat2.sampleinfo];
    trdat.trialinfo =   [trdat.trialinfo;trdat2.trialinfo];
    trdat.trl =         [trdat.trl;trdat2.trl];
    
    
    clear post_trial pre_trial trialfunction trdat2
    %wpms.names = {'AGE004'};
    %count = 1;
    
    fnl_ICA(wpms,count,trdat);   
    count = count+1;
end


            
%% Remove EOG Components from ICA
for name_i = 1:3%length(wpms.names)
    fnl_ICA_inspection(wpms,name_i);
end
%% Apply artifact rejection
for name_i = 1:3%length(wpms.names)
    fnl_artifact_rejection_auto(wpms,name_i,true,30,-120,120)%Olivia try this with -120 and 120 as the last values instead -Patrick
end
%% Reinterpolate bad channels
for name_i = 1:3%length(wpms.names)
    fnl_reinterpolate(wpms,name_i);
end
%% Convert and save Final Cleaned data into text file:

wpms.names = {  'AGE004_TSWT', ...%'AGE004_TSWT_A','AGE004_TSWT_B',...
                'AGE009_TSWT', ...%'AGE009_TSWT_A','AGE009_TSWT_B', ...
                'AGE034_TSWT', ...%'AGE034_TSWT_A','AGE034_TSWT_B', ...
                'AGE005_TSWT', ...
                'AGE007_TSWT', ...
                'AGE008_TSWT', ...
                'AGE012_TSWT', ...
                'AGE013_TSWT', ...
                'AGE014_TSWT', ...
                'AGE015_TSWT', ...
                'AGE017_TSWT', ...
                'AGE018_TSWT', ...
                'AGE019_TSWT', ...
                'AGE020_TSWT', ...
                'AGE021_TSWT', ...
                'AGE022_TSWT', ...
                'AGE023_TSWT', ...
                'AGE024_TSWT', ...
                'AGE026_TSWT', ...
                'AGE027_TSWT', ...
                'AGE028_TSWT', ...
                'AGE030_TSWT', ...
                'AGE032_TSWT', ...
                'AGE033_TSWT', ...
                'AGE035_TSWT', ...
                'AGE036_TSWT', ...
                'AGE038_TSWT', ...
                'AGE043_TSWT', ...
                'AGE046_TSWT', ...
                'AGE047_TSWT'};

%CODES AND MEANINGS:
AR = [65411 65421 65431 65412 65422 65432 131 132 141 142 151 152];
MR = [65441 65442 65451 65452 65461 65462 161 162 171 172 181 182];
ST = [65443 65444 65453 65454 65463 65464 163 164 173 174 183 184];
SA = [65445 65446 65455 65456 65465 65466 165 166 175 176 185 186];
NI = [65447 65448 65449 65450 65457 65458 65459 65460 65467 65468 65469 65470 167 168 169 170 177 178 179 180 187 188 189 190];
NR = [65447 65448 65457 65458 65467 65468 167 168  177 178  187 188];%[167 168  177 178  187 188 ];   %[169 170 179 180 189 190]
NS = [65449 65450 65459 65460  65469 65470 169 170 179 180 189 190];%[169 170 179 180 189 190];

Letter_TaskCodes = [161:170 (161:170)+65280 131:132 (131:132)+65280];
Digit_TaskCodes  = [171:180 (171:180)+65280 141:142 (141:142)+65280];
Colour_TaskCodes = [181:190 (181:190)+65280 151:152 (151:152)+65280];

ConditionsShort1 = {'AR','MR','ST','SA','NI'};
ConditionsShort2 = {'AR','MR','ST','SA','NR','NS'};
ConditionsShortALL = {'AR','MR','ST','SA','NI','NR','NS'};

TASK = {'Letter','Digit','Colour'};

%Instantiate New array for data storage of allparticipant data:
AllExportData = [];
AllPlots = [];
ALL_TRL_CELL = {};
for name_i = 1:length(wpms.names)
    fprintf('-----------------------------\n 1. Loading Data from %s\n',wpms.names{name_i});
    load([wpms.dirs.CWD wpms.dirs.preproc wpms.names{name_i} '_REPAIRED_AND_REFERENCED']);
    TRL = refdat.trl;
    TrlCell = num2cell(refdat.trl);
    [refdat] = fnl_rereference(refdat,[65 66]);
    refdat.trl = TRL;
    newCol1 = 7;%size(refdat.trl,2)+1;
    newCol2 = 8;%size(refdat.trl,2)+2;
    newCol3 = 9;%size(refdat.trl,2)+3;
    
    fprintf('2a. Sorting Data into Conditions %s\n',wpms.names{name_i});
    for trl_i = 1:size(refdat.trl,1)
        %insert into afterlast column:
        %Now we have to compare against the CODES: with col4
        if any(ismember(AR,refdat.trl(trl_i,4)))
            TrlCell{trl_i,newCol1} = 'AR';
        end
        if any(ismember(MR,refdat.trl(trl_i,4)))
            TrlCell{trl_i,newCol1} = 'MR';
        end
        if any(ismember(ST,refdat.trl(trl_i,4)))
            TrlCell{trl_i,newCol1} = 'ST';
        end    
        if any(ismember(SA,refdat.trl(trl_i,4)))
            TrlCell{trl_i,newCol1} = 'SA';
        end    
        if any(ismember(NI,refdat.trl(trl_i,4)))
            TrlCell{trl_i,newCol1} = 'NI';
        end
    end
    fprintf('2b. Sorting Data into Tasks %s\n',wpms.names{name_i});
    for trl_i = 1:size(refdat.trl,1)
        %insert into afterlast column:
        %Now we have to compare against the CODES: with col4
        if any(ismember(Letter_TaskCodes,refdat.trl(trl_i,4)))
            TrlCell{trl_i,newCol3} = 'Letter';
        end
        if any(ismember(Digit_TaskCodes,refdat.trl(trl_i,4)))
            TrlCell{trl_i,newCol3} = 'Digit';
        end
        if any(ismember(Colour_TaskCodes,refdat.trl(trl_i,4)))
            TrlCell{trl_i,newCol3} = 'Colour';
        end    
    end
    fprintf('3. Inserting Conditions into Data variable %s\n',wpms.names{name_i});
    for trl_i = 1:size(refdat.trl,1)
        %insert into afterlast column:
        %Now we have to compare against the CODES: with col4
        if any(ismember(AR,refdat.trl(trl_i,4)))
            TrlCell{trl_i,newCol2} = 'AR';
        end
        if any(ismember(MR,refdat.trl(trl_i,4)))
            TrlCell{trl_i,newCol2} = 'MR';
        end
        if any(ismember(ST,refdat.trl(trl_i,4)))
            TrlCell{trl_i,newCol2} = 'ST';
        end    
        if any(ismember(SA,refdat.trl(trl_i,4)))
            TrlCell{trl_i,newCol2} = 'SA';
        end    
        if any(ismember(NR,refdat.trl(trl_i,4)))
            TrlCell{trl_i,newCol2} = 'NR';
        end
        if any(ismember(NS,refdat.trl(trl_i,4)))
            TrlCell{trl_i,newCol2} = 'NS';
        end
    end    

    % Bin every 100 ms
    fprintf('4. Begin Exporting 100ms Timebins for %s\n',wpms.names{name_i});
    ExportData = zeros(length(refdat.trial),10);
    %Baseline: Calculate baselines for each trial type:
    %baseline is from -200 to 0 (cue)
    % We know that Data starts is 1 second before Cue. (512)
    baseline_start = floor(wpms.sampling_frequency*0.8);    %-200ms
    baseline_end = floor(wpms.sampling_frequency*1.0);      %0ms
    plot_start = floor(wpms.sampling_frequency*0.6);    %-200ms
    plot_end = floor(wpms.sampling_frequency*2.2);      %0ms
    fprintf('\t4A. AR Basline Calculation for %s\n',wpms.names{name_i});
    ARIndices = find(strcmpi(TrlCell(:,7),'AR'));
    AR_baseline = 0;
    AR_PLOT = zeros(1,length(plot_start:plot_end));
    MR_PLOT = zeros(1,length(plot_start:plot_end));
    ST_PLOT = zeros(1,length(plot_start:plot_end));
    SA_PLOT = zeros(1,length(plot_start:plot_end));
    NI_PLOT = zeros(1,length(plot_start:plot_end));
    NR_PLOT = zeros(1,length(plot_start:plot_end));
    NS_PLOT = zeros(1,length(plot_start:plot_end));
    
    for ARIndices_i = 1:length(ARIndices)
        AR_baseline = AR_baseline+mean(refdat.trial{1,ARIndices(ARIndices_i)}(30,baseline_start:baseline_end));
        AR_PLOT = AR_PLOT + refdat.trial{1,ARIndices(ARIndices_i)}(30,plot_start:plot_end);
    end
    AR_baseline = AR_baseline/length(ARIndices);
    AR_PLOT = AR_PLOT/length(ARIndices)-AR_baseline;
    
    fprintf('\t4B. MR Basline Calculation for %s\n',wpms.names{name_i});
    MRIndices = find(strcmpi(TrlCell(:,7),'MR'));
    MR_baseline = 0;
    for MRIndices_i = 1:length(MRIndices)
        MR_baseline = MR_baseline+mean(refdat.trial{1,MRIndices(MRIndices_i)}(30,baseline_start:baseline_end));
        MR_PLOT = MR_PLOT +refdat.trial{1,MRIndices(MRIndices_i)}(30,plot_start:plot_end);
    end
    MR_baseline = MR_baseline/length(MRIndices);
    MR_PLOT = MR_PLOT/length(MRIndices)-MR_baseline;
    
    fprintf('\t4C. ST Basline Calculation for %s\n',wpms.names{name_i});
    STIndices = find(strcmpi(TrlCell(:,7),'ST'));
    ST_baseline = 0;
    for STIndices_i = 1:length(STIndices)
        ST_baseline = ST_baseline+mean(refdat.trial{1,STIndices(STIndices_i)}(30,baseline_start:baseline_end));
        ST_PLOT = ST_PLOT + refdat.trial{1,STIndices(STIndices_i)}(30,plot_start:plot_end);
    end
    ST_baseline = ST_baseline/length(STIndices);
    ST_PLOT = ST_PLOT/length(STIndices)-ST_baseline;
    
    fprintf('\t4D. SA Basline Calculation for %s\n',wpms.names{name_i});
    SAIndices = find(strcmpi(TrlCell(:,7),'SA'));
    SA_baseline = 0;
    for SAIndices_i = 1:length(SAIndices)
        SA_baseline = SA_baseline+mean(refdat.trial{1,SAIndices(SAIndices_i)}(30,baseline_start:baseline_end));
        SA_PLOT = SA_PLOT + refdat.trial{1,SAIndices(SAIndices_i)}(30,plot_start:plot_end);
    end
    SA_baseline = SA_baseline/length(SAIndices);
    SA_PLOT = SA_PLOT/length(SAIndices)-SA_baseline;
    
    fprintf('\t4E. NI Basline Calculation for %s\n',wpms.names{name_i});
    NIIndices = find(strcmpi(TrlCell(:,7),'NI'));
    NI_baseline = 0;
    for NIIndices_i = 1:length(NIIndices)
        NI_baseline = NI_baseline+mean(refdat.trial{1,NIIndices(NIIndices_i)}(30,baseline_start:baseline_end));
        NI_PLOT = NI_PLOT + refdat.trial{1,NIIndices(NIIndices_i)}(30,plot_start:plot_end);
    end
    NI_baseline = NI_baseline/length(NIIndices);
    NI_PLOT = NI_PLOT/length(NIIndices)-NI_baseline;
    
    fprintf('\t4F. NR Basline Calculation for %s\n',wpms.names{name_i});
    NRIndices = find(strcmpi(TrlCell(:,8),'NR'));
    NR_baseline = 0;
    for NRIndices_i = 1:length(NRIndices)
        NR_baseline = NR_baseline+mean(refdat.trial{1,NRIndices(NRIndices_i)}(30,baseline_start:baseline_end));
        NR_PLOT = NR_PLOT + refdat.trial{1,NRIndices(NRIndices_i)}(30,plot_start:plot_end);
    end
    NR_baseline = NR_baseline/length(NRIndices);
    NR_PLOT = NR_PLOT/length(NRIndices)-NR_baseline;
    
    NSIndices = find(strcmpi(TrlCell(:,8),'NS'));
    NS_baseline = 0;
    for NSIndices_i = 1:length(NSIndices)
        NS_baseline = NS_baseline+mean(refdat.trial{1,NSIndices(NSIndices_i)}(30,baseline_start:baseline_end));
        NS_PLOT = NS_PLOT + refdat.trial{1,NSIndices(NSIndices_i)}(30,plot_start:plot_end);
    end
    NS_baseline = NS_baseline/length(NSIndices);
    NS_PLOT = NS_PLOT/length(NSIndices)-NS_baseline;
    
    fprintf('5. 100ms Time bins Extraction %s\n',wpms.names{name_i});
    %Extract 100ms bins of POz for each trial: Ch30
    for trial_i = 1:length(refdat.trial)
        for time_i = 0:0.1:0.9
            [~,start_index]=min(abs(refdat.time{1,1}-time_i));
            [~,end_index]=min(abs(refdat.time{1,1}-(time_i+0.1)));
            %fprintf('StartIndex = %i, %3.2f s\n',start_index,refdat.time{1,1}(start_index));
            %fprintf('  EndIndex = %i, %3.2f s\n',end_index,refdat.time{1,1}(end_index));
            ExportData(trial_i,round(time_i*10+1))=mean(refdat.trial{1,trial_i}(30,start_index:end_index));
        end
    end
    
    %Apply baselineson export data:
    fprintf('6. Apply basline to extracted data %s\n',wpms.names{name_i});
    for trial_i = size(ExportData,1)
       if strcmpi(TrlCell(trial_i,7),'AR')
           ExportData(trial_i,:) = ExportData(trial_i,:)-AR_baseline;
       end
       if strcmpi(TrlCell(trial_i,7),'MR')
           ExportData(trial_i,:) = ExportData(trial_i,:)-MR_baseline;
       end
       if strcmpi(TrlCell(trial_i,7),'ST')
           ExportData(trial_i,:) = ExportData(trial_i,:)-ST_baseline;
       end
       if strcmpi(TrlCell(trial_i,7),'SA')
           ExportData(trial_i,:) = ExportData(trial_i,:)-SA_baseline;
       end
       if strcmpi(TrlCell(trial_i,7),'NI')
           ExportData(trial_i,:) = ExportData(trial_i,:)-NI_baseline;
       end
       
    end
    
    %Plot:
    fprintf('7. Plot Box plots for %s\n',wpms.names{name_i});
    figure('Position',[100 100 1200 800]);
    for i = 1:10
        subplot(2,5,i)
        boxplot(ExportData(:,i)',TrlCell(:,7),...
                 'notch','on',...
                 'labels',ConditionsShort1);
        ylim([-10 10]);
        title([wpms.names{name_i}(1:6),' Starting Time: ',num2str(100*(i-1)),' ms'])
    end
    
      
    saveas(gcf,[wpms.dirs.CWD wpms.dirs.JOINT_IN_DIR wpms.names{name_i} '_EEGDATA.jpg'])
    
    figure('Position',[100 100 1200 800]);
    times_plot = refdat.time{1,1}(plot_start:plot_end);
    plot(times_plot,MR_PLOT,'-k');hold on;
    plot(times_plot,SA_PLOT,'-r');
    plot(times_plot,ST_PLOT,'-b');
    plot(times_plot,NI_PLOT,'-g');
    saveas(gcf,[wpms.dirs.CWD wpms.dirs.JOINT_IN_DIR wpms.names{name_i} '_ConditionERP.jpg'])
    
    % Write Export Data to TEXT File:
    fprintf('8. Write Text Files %s\n',wpms.names{name_i});
    fid = fopen([wpms.dirs.CWD wpms.dirs.JOINT_IN_DIR wpms.names{name_i} '_EEGDATA.txt'],'w');
    for i = 1:size(ExportData,1)
        for j = 1:size(ExportData,2)
            fprintf(fid,'%s\t',num2str(ExportData(i,j)));
        end
        fprintf(fid,'\n');
    end
    fclose(fid);
    fprintf('9. Write TRL Files %s\n',wpms.names{name_i});
    % Write TRL to text file:
    fid = fopen([wpms.dirs.CWD wpms.dirs.JOINT_IN_DIR wpms.names{name_i} '_TRL.txt'],'w');
    for i = 1:size(TrlCell,1)
        for j = 1:size(TrlCell,2)
            fprintf(fid,'%s\t',num2str(TrlCell{i,j}));
        end
        fprintf(fid,'\n');
    end
    fclose(fid);
    fprintf('10. Concatenate and append participant code for joint modelling for %s\n',wpms.names{name_i});
    %Save ExportData to concatenated file:
    subject_number = str2double(wpms.names{name_i}(4:6));
    subject_number_column = ones(size(ExportData,1),1)*subject_number;
    AllExportData=[AllExportData;[ExportData,subject_number_column]];
    AllPlots(name_i,:,:) = [AR_PLOT;MR_PLOT;SA_PLOT;ST_PLOT;NI_PLOT;NR_PLOT;NS_PLOT];
    ALL_TRL_CELL = [ALL_TRL_CELL;TrlCell];
    close all;
end

%Plot ERP of Dataset:
ERP_DATA = squeeze(mean(AllPlots,1));
plot(times_plot,ERP_DATA(2,:),'-b');hold on;
plot(times_plot,ERP_DATA(3,:),'-m');
plot(times_plot,ERP_DATA(4,:),'-r');
plot(times_plot,ERP_DATA(5,:),'-g');
legend({'MR','SA','ST','NI'},'Location','southeast');
set(gca,'ydir','reverse');
title('POz ERP for n=30 AGE-ILTTY');
saveas(gcf,[wpms.dirs.CWD wpms.dirs.JOINT_IN_DIR 'GRAND_AVERAGE_ERP.jpg']);

% Format the data so it reads straight into R:
% Variable: MA.0-1 MA.1-2 MA.2-3 ... MA.9-10, snum??, TN, RT, Correct.

JointModelFormat = num2cell(AllExportData);
JointModelFormat = [JointModelFormat,ALL_TRL_CELL(:,7),ALL_TRL_CELL(:,8),ALL_TRL_CELL(:,5),ALL_TRL_CELL(:,6),ALL_TRL_CELL(:,9)];
fid = fopen([wpms.dirs.CWD wpms.dirs.JOINT_IN_DIR 'ALL_JointModelData_170721.txt'],'w');
%Header:
fprintf(fid,'ma.0-1\tma.1-2\tma.2-3\tma.3-4\tma.4-5\tma.5-6\tma.6-7\tma.7-8\tma.8-9\tma.9-10\tsnum\ttnum\tTN\tRT\tcrct\tTask\n',num2str(JointModelFormat{i,j}));
for i = 1:size(JointModelFormat,1)
    for j = 1:size(JointModelFormat,2)
        fprintf(fid,'%s\t',num2str(JointModelFormat{i,j}));
    end
    fprintf(fid,'\n');
end
fclose(fid);
