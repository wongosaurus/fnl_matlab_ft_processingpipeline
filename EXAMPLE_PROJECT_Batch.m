%% Age-ility Processing Pipeline
%  Adapted by Patrick Cooper and Aaron Wong
%  May - 2014, Functional Neuroimaging Laboratory
%% Set-up working parameters
close all
clear all
clc
warning off;
wpms       = [];%working parameters
wpms.dirs  = struct('CWD',                  'F:\Joint_Modelling\Pipeline_Extraction\',...
                    'packages',             'PACKAGES', ...
                    'FUNCTIONS',            'FUNCTIONS\',...
                    'RAW',                  'RAW\',...
                    'preproc',              'PREPROC_OUTPUT\', ...
                    'DATA_DIR',             'EEGLAB_FORMAT\',...
                    'WAVELET_OUTPUT_DIR',   'WAVELET_OUTPUT_DIR\', ...
                    'COHERENCE_DIR',        'IMAGCOH_OUTPUT\');
%wpms.names = {'AGE002','AGE003','AGE005','AGE007','AGE008','AGE012','AGE013','AGE014','AGE015','AGE017','AGE018','AGE019','AGE020','AGE021','AGE022','AGE023','AGE024','AGE026','AGE027','AGE028','AGE030','AGE032','AGE033','AGE035','AGE036','AGE038','AGE043','AGE046','AGE047','AGE048','AGE050','AGE051','AGE052','AGE053','AGE058','AGE059','AGE061','AGE062','AGE063','AGE064','AGE066','AGE067','AGE068','AGE069','AGE070','AGE072','AGE075','AGE077','AGE079','AGE081','AGE083','AGE084','AGE085','AGE086','AGE088','AGE089','AGE090','AGE092','AGE093','AGE094','AGE095','AGE096','AGE097','AGE098','AGE100','AGE102','AGE103','AGE104','AGE106','AGE107','AGE108','AGE109','AGE114','AGE115','AGE116','AGE117','AGE118','AGE119','AGE120','AGE121','AGE122','AGE123','AGE124','AGE127','AGE128','AGE129','AGE130','AGE131','AGE133','AGE134','AGE135','AGE136','AGE138','AGE141','AGE143','AGE145','AGE146','AGE147','AGE148','AGE149','AGE150','AGE151','AGE152','AGE153','AGE155','AGE156','AGE158','AGE159','AGE161','AGE162','AGE163','AGE164','AGE165','AGE166','AGE167','AGE168','AGE169','AGE170','AGE172','AGE175','AGE176','AGE177','AGE178','AGE179','AGE180','AGE181','AGE182','AGE183','AGE184','AGE185','AGE186','AGE187','AGE189','AGE190','AGE192','AGE195','AGE197','AGE198','AGE199','AGE200','AGE201','AGE202','AGE203','AGE204','AGE205','AGE206','AGE207','AGE208','AGE209','AGE210','AGE212','AGE216','AGE217','AGE218','AGE219','AGE220','AGE222','AGE224','AGE225','AGE226','AGE227','AGE228','AGE229','AGE230','AGE231','AGE233','AGE234','AGE236','AGE237','AGE238','AGE239','AGE240','AGE241','AGE243','AGE244','AGE245','AGE246','AGE247','AGE248','AGE249','AGE251','AGE252','AGE253','AGE254','AGE255','AGE256','AGE257','AGE258','AGE259','AGE260','AGE261','AGE264','AGE265','AGE266','AGE267','AGE268','AGE273','AGE276','AGE277','AGE279'};%AGE079 still has problems with empty files
wpms.names = {'AGE004_TSWT_A','AGE004_TSWT_B'};
% add path

cd([wpms.dirs.CWD]);
addpath(genpath([wpms.dirs.CWD,wpms.dirs.FUNCTIONS]));
addpath(genpath([wpms.dirs.CWD,wpms.dirs.packages '\fieldtrip']));
%% Preprocessing I
%  Import, downsample and re-reference
for name_i = 1:length(wpms.names)
    sampling_frequency = 512; %hz
    [dat] = fnl_importeeg_and_downsample(wpms,'bdf',name_i,sampling_frequency);
    [refdat] = fnl_rereference(dat,48);
    
    clear dat
    
    [data] = fnl_preproc_filter(refdat,'yes',[48 52],'yes',0.1,'onepass',1,'but');
    
    save([wpms.dirs.CWD wpms.dirs.preproc wpms.names{name_i} '_REFnFILT'],'data','-v7.3');
    clear refdat data cfg %tidying
end

%% Visual Inspection of Data:
for name_i = 1:length(wpms.names)   
    fnl_bad_channel_inspection(wpms,name_i);
end

%% Remove Bad Channels out from Data:
for name_i = 1:length(wpms.names)   
    fnl_remove_bad_channels(wpms,name_i);
end

%% Trial Definition and ICA
%for name_i = 1:length(wpms.names)   
    pre_trial = 1.0;
    post_trial = 3.5;
    trialfunction = 'Agility'; 
    trdat = fnl_trial_definition(wpms,1,trialfunction,pre_trial,post_trial);
    trdat2 = fnl_trial_definition(wpms,2,trialfunction,pre_trial,post_trial);
    
    % Join Both Together:
    trdat.trial = [trdat.trial,trdat2.trial];
    trdat.time =  [trdat.time,trdat2.time];
    trdat.sampleinfo =  [trdat.sampleinfo;trdat2.sampleinfo];
    trdat.trialinfo =  [trdat.trialinfo;trdat2.trialinfo];
    trdat.trl =        [trdat.trl;trdat2.trl];
    
    
    clear post_trial pre_trial trialfunction trdat2
    wpms.names = {'AGE004'};
    fnl_ICA(wpms,name_i,trdat);        
%end
%% Remove EOG Components from ICA
for name_i = 1:length(wpms.names)
    fnl_ICA_inspection(wpms,name_i);
end

%% Apply artifact rejection
for name_i = 1:length(wpms.names)
    fnl_artifact_rejection_auto(wpms,name_i,true,30,-120,120)%Olivia try this with -120 and 120 as the last values instead -Patrick
end
%% Apply scalp current density montage
addpath(genpath([wpms.dirs.CWD,wpms.dirs.packages '\CSDtoolbox']));
for name_i = 1:length(wpms.names)
    fnl_csd_transformation(wpms,name_i);
end
%% Reinterpolate bad channels
for name_i = 1:length(wpms.names)
    fnl_reinterpolate(wpms,name_i);
end
%% CONVERT FROM FIELDTRIP TO EEG STRUCT %NAT

conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninf','noninfswitch','noninfrepeat'};
cond = {'ar','mr','st','sa','ni','ns','nr'};
for name_i =100:length(wpms.names)
    try
        fnl_saveOffIndividualConditions(wpms,name_i);
    catch error
        fnl_saveOffIndividualConditions_altbiosemicodes(wpms,name_i);
    end
    %fnl_setup_eegstructure(wpms,conditions,cond,name_i)
end
% Convert to EEG Display:
for name_i = 1:length(wpms.names)
    for cond_i = 1:length(conditions)
        load([wpms.names{name_i},'_CSD_',conditions{cond_i},'.mat']);
        cfg = [];
        mkdir ([wpms.dirs.CWD, wpms.dirs.preproc, 'EEGDisplay_Output\']);
        mkdir ([wpms.dirs.CWD, wpms.dirs.preproc, 'EEGDisplay_Output\',conditions{cond_i},'\']);
        if strcmp(conditions{cond_i}, 'allrepeat') %variable should be named ardata
            FT2EEGDisplay(cfg,ardata,[wpms.dirs.CWD, wpms.dirs.preproc, 'EEGDisplay_Output\',conditions{cond_i},'\', wpms.names{name_i},'_CSD_',conditions{cond_i},'.erp']);
            clear ardata
        elseif strcmp(conditions{cond_i}, 'mixrepeat') %variable should be named mrdata
            FT2EEGDisplay(cfg,mrdata,[wpms.dirs.CWD, wpms.dirs.preproc, 'EEGDisplay_Output\',conditions{cond_i},'\', wpms.names{name_i},'_CSD_',conditions{cond_i},'.erp']);
            clear mrdata
        elseif strcmp(conditions{cond_i}, 'switchto') %variable should be named stdata
            FT2EEGDisplay(cfg,stdata,[wpms.dirs.CWD, wpms.dirs.preproc, 'EEGDisplay_Output\',conditions{cond_i},'\', wpms.names{name_i},'_CSD_',conditions{cond_i},'.erp']);
            clear stdata
        elseif strcmp(conditions{cond_i}, 'switchaway') %variable should be named sadata
            FT2EEGDisplay(cfg,sadata,[wpms.dirs.CWD, wpms.dirs.preproc, 'EEGDisplay_Output\',conditions{cond_i},'\', wpms.names{name_i},'_CSD_',conditions{cond_i},'.erp']);
            clear sadata
        elseif strcmp(conditions{cond_i}, 'noninf') %variable should be named sadata
            FT2EEGDisplay(cfg,nidata,[wpms.dirs.CWD, wpms.dirs.preproc, 'EEGDisplay_Output\',conditions{cond_i},'\', wpms.names{name_i},'_CSD_',conditions{cond_i},'.erp']);
            clear nidata
        elseif strcmp(conditions{cond_i}, 'noninfswitch') %variable should be named sadata
            FT2EEGDisplay(cfg,nsdata,[wpms.dirs.CWD, wpms.dirs.preproc, 'EEGDisplay_Output\',conditions{cond_i},'\', wpms.names{name_i},'_CSD_',conditions{cond_i},'.erp']);
            clear nsdata
        elseif strcmp(conditions{cond_i}, 'noninfrepeat') %variable should be named sadata
            FT2EEGDisplay(cfg,nrdata,[wpms.dirs.CWD, wpms.dirs.preproc, 'EEGDisplay_Output\',conditions{cond_i},'\', wpms.names{name_i},'_CSD_',conditions{cond_i},'.erp']);
            clear nrdata
        end
        
        
    end
end
%% Begin FFT
% add eeglab to path
addpath(genpath([wpms.dirs.CWD,wpms.dirs.packages '\eeglab']));
times = -400:0.5:2200;
windowsize = 20;
bins = 80;
freqrange = [2 50];
channels = 1:64;
conditions = [{'switchto'},{'switchaway'},{'noninf'},{'mixrepeat'}];
for name_i = 1:length(wpms.names)
    fnl_wavelet_analysis(wpms,times,windowsize,bins,freqrange,channels,conditions,name_i);
end
%% IMAGINARY COHERENCE ANALYSIS
channels = 1:64;
conditions = {'switchto','switchaway','noninf','mixrepeat'};
for name_i = 1:length(wpms.names)
    fnl_imaginarycoherence_analysis(wpms,channels,conditions,name_i)
end
%% GENERATE CONNECTIVITY MATRICES
channelcount = 64;
freq_names = {'delta','theta','loweralpha','upperalpha','beta'};
freq = [{1:18},{19:31},{35:40},{41:47},{48:68}];
conditions = {'switchto','switchaway','noninf','mixrepeat'};
for name_i = 1:length(wpms.names)
    starttimes = 400:200:3600;
    endtime = 600;
    fnl_genconnmatrix(wpms,channelcount,conditions,starttimes,endtime,freq_names,freq,name_i);
end
%% PERFORM STATISTICAL ANALYSES
addpath(genpath([wpms.dirs.CWD,wpms.dirs.packages '\massunivariate']));
conditions = {'switchto','switchaway','noninf','mixrepeat'};
frequencies = {'delta','theta','loweralpha','upperalpha','beta'};
times = {};
starttime = -200:100:1400;
endtime   = starttime+200;
for time_i = 1:length(starttime)
    times{time_i} = strcat(num2str(starttime(time_i)),'to',num2str(endtime(time_i)));
end
% generate list of files to be read in
filelist{1,(length(names)*length(conditions)*length(times))} = [];
count = 0;
for name_i = 1:length(wpms.names)
    for cond_i = 1:length(conditions)
        for time_i = 1:length(times)
            count = count+1;
            filelist(count) = [wpms.dirs.CWD,wpms.dirs.COHERENCE_DIR,'\',wpms.names{name_i},'\',conditions{cond_i},'\',conditions{cond_i},times{time_i},'_CONECTIVITY_IMAG.mat'];
        end%time_i loop
    end%cond_i loop
end%name_i loop
clear count name_i cond_i time_i freqstruct condstruct

%% PRODUCE TIMELOCK ERP - Not for Coherency analysis

for name_i = 1:length(wpms.names)
    conditions = {'mixrepeat','switchto','switchaway','noninf'};
    %baseline is from -200 to 0 (cue)
    % We know that Data starts is 1 second before Cue. (512)
    baseline_start = floor(sampling_frequency*0.8);
    baseline_end = floor(sampling_frequency*1.0);
    for cond_i = 1:length(conditions)
        timelock.(conditions{cond_i}) = fnl_timelockanalysis(wpms,name_i,conditions{cond_i},baseline_start,baseline_end);
    end;
    save([wpms.dirs.CWD wpms.dirs.preproc wpms.names{name_i} '_TIMELOCK.mat'],'timelock*','-v7.3');
    clear timelock*
end

%% Draw TIMELOCK ERP:
close all;
for name_i = 1%:length(wpms.names)
    isreverse_ydir = true; %true or false
    channel = 30;
    fnl_plotERP(wpms,name_i,channel,isreverse_ydir)
end