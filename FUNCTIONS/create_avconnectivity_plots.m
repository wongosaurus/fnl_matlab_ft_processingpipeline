clc;clear all;close all;
names = {{'AGE023','AGE026','AGE032','AGE043','AGE047','AGE050','AGE051', ...
    'AGE059','AGE081','AGE092','AGE095','AGE102','AGE104','AGE109','AGE116', ...
    'AGE117','AGE118','AGE119','AGE123','AGE124','AGE134','AGE145','AGE155', ...
    'AGE161','AGE162','AGE166','AGE167','AGE170','AGE172','AGE177','AGE178', ...
    'AGE179','AGE184','AGE187','AGE195','AGE198','AGE199','AGE202','AGE204', ...
    'AGE226','AGE231','AGE233','AGE236','AGE241','AGE244','AGE254','AGE257','AGE267','AGE276',...
    'AGE027','AGE033','AGE038','AGE052','AGE053','AGE061','AGE072','AGE088', ...
    'AGE093','AGE108','AGE114','AGE127','AGE128','AGE131','AGE133','AGE143', ...
    'AGE150','AGE158','AGE164','AGE165','AGE180','AGE181','AGE182','AGE189', ...
    'AGE190','AGE197','AGE205','AGE222','AGE248','AGE261','AGE264'},...
    {'AGE012','AGE013','AGE014','AGE017','AGE018','AGE019','AGE021', ...
    'AGE024','AGE028','AGE030','AGE036','AGE058','AGE067','AGE069', ...
    'AGE070','AGE075','AGE077','AGE083','AGE085','AGE086','AGE089', ...
    'AGE090','AGE094','AGE100','AGE120','AGE129','AGE135','AGE138', ...
    'AGE147','AGE149','AGE151','AGE153','AGE159','AGE168','AGE169', ...
    'AGE175','AGE183','AGE185','AGE186','AGE200','AGE201','AGE203', ...
    'AGE208','AGE209','AGE210','AGE217','AGE218','AGE219','AGE220', ...
    'AGE224','AGE225','AGE227','AGE228','AGE229','AGE230','AGE243', ...
    'AGE245','AGE246','AGE251','AGE252','AGE253','AGE258','AGE266', ...
    'AGE268','AGE273','AGE279', ...
    'AGE008','AGE062','AGE068','AGE130','AGE156','AGE176','AGE192', ...
    'AGE237','AGE238','AGE247','AGE249','AGE259','AGE277'}};
conditions = {'mixrepeat','switchto','switchaway','noninf'};
frequencies = {'delta','theta','loweralpha','upperalpha','beta'};
starttime = -200:100:1400;
endtime = starttime+200;
times{1,length(starttime)} = [];
for time_i = 1:length(starttime)
    times{1,time_i} = [num2str(starttime(time_i)) 'to' num2str(endtime(time_i))];
end
channels = 64;
CWD = 'F:\FNL_EEG_TOOLBOX\';
DATAIN = [CWD 'IMAGCOH_OUTPUT\'];
DATAOUT = [CWD 'ANALYSES\CFI\'];
%% load data
for group_i = 1:length(names)
    fprintf('\n%s %s','Processing group:',num2str(group_i));
    DATA = zeros(length(names{group_i}),length(conditions),length(frequencies),length(times),channels,channels);
    for name_i = 1:length(names{group_i})
        fprintf('\n%s %s %s %s','Subject:',num2str(name_i),'of',num2str(length(names{group_i})));
        for cond_i = 1:length(conditions)
            fprintf('.');
            for time_i = 1:length(times)
                filename = [DATAIN names{group_i}{name_i} filesep conditions{cond_i} filesep ...
                    conditions{cond_i} times{time_i} '_CONNECTIVITY_IMAG.mat'];
                if exist(filename,'file')==1;
                    dat = load(filename);
                elseif exist(filename,'file')==0;
                    filename = [DATAIN names{group_i}{name_i} filesep conditions{cond_i} filesep ...
                        conditions{cond_i} times{time_i} '_CONECTIVITY_IMAG.mat'];%alternate spelling on some files
                    dat = load(filename);
                elseif exist(filename,'file')==2;
                    dat = load(filename);
                else
                    error(['Cannot find file for subject ' names{group_i}{name_i}]);
                end
                datnams = fieldnames(dat);
                for field_i = 1:length(datnams)
                    ind = find(datnams{field_i}=='_')+1;
                    for freq_i = 1:length(frequencies)
                        if strcmp(datnams{field_i}(ind:end),frequencies{freq_i})==1;
                            DATA(name_i,cond_i,freq_i,time_i,:,:) = dat.(datnams{field_i})(:,:);
                        end
                    end
                end
                clear dat
            end
        end
    end
    save([DATAOUT 'GROUP_' num2str(group_i) 'ALLDATA.mat'],'DATA','-v7.3');
end
%% compute average connectivity
addpath(genpath([CWD 'FUNCTIONS']));
for group_i = 1:length(names)
    fprintf('\n%s %s','Processing group:',num2str(group_i));
    filename = [DATAOUT 'GROUP_' num2str(group_i) 'ALLDATA.mat'];
    load(filename);
    AVDATA = zeros(length(names{group_i}),length(conditions),length(frequencies),length(times),channels);
    for name_i = 1:length(names{group_i})
        fprintf('\n%s %s %s %s','Subject:',num2str(name_i),'of',num2str(length(names{group_i})));
        for cond_i = 1:length(conditions)
            fprintf('.');
            for time_i = 1:length(times)
                for freq_i = 1:length(frequencies)
                    dat = squeeze(abs(DATA(name_i,cond_i,freq_i,time_i,:,:)));
                    dat = fisherz(dat); 
                    dat(isinf(dat))=NaN;%remove Inf that appear on diagnonal and replace with NaN
                    for chann_i = 1:channels
                        AVDATA(name_i,cond_i,freq_i,time_i,chann_i) = nanmean(dat(chann_i,:),2);
                    end
                end%frequency loop
            end
        end
    end
    save([DATAOUT 'GROUP_' num2str(group_i) 'AVCONNDATA.mat'],'AVDATA','-v7.3');
end
%% average over time to create smaller amount of comparisons
cue1  = 3:8;
cue2  = 8:13;
targ1 = 13:17;
avtimes = {cue1,cue2,targ1};
for group_i = 1:length(names)
    fprintf('\n%s %s','Processing group:',num2str(group_i));
    filename = [DATAOUT 'GROUP_' num2str(group_i) 'AVCONNDATA.mat'];
    load(filename);
    AVTIMEDATA = zeros(length(names{group_i}),length(conditions),length(frequencies),length(times),channels);
    for name_i = 1:length(names{group_i})
        fprintf('\n%s %s %s %s','Subject:',num2str(name_i),'of',num2str(length(names{group_i})));
        for cond_i = 1:length(conditions)
            fprintf('.');
            for time_i = 1:length(avtimes)
                for freq_i = 1:length(frequencies)
                    AVTIMEDATA(name_i,cond_i,freq_i,time_i,:)= mean(squeeze(AVDATA(name_i,cond_i,freq_i,avtimes{time_i},:)),1);
%                     avdat = mean(squeeze(AVDATA(name_i,cond_i,freq_i,avtimes{time_i},:)),1);
%                     for chann_i =1:length(avdat)
%                         AVTIMEDATA(name_i,cond_i,freq_i,time_i,chann_i) = avdat(chann_i);
%                     end
                end%frequency loop
            end
        end
    end
    save([DATAOUT 'GROUP_' num2str(group_i) 'AVCONNTIMEDATA.mat'],'AVTIMEDATA','-v7.3');
end
%% run statistics
groupnames = {'LOW','HIGH'};
for group_i = 1:length(names)
    fprintf('\n%s %s','Processing group:',num2str(group_i));
    filename = [DATAOUT 'GROUP_' num2str(group_i) 'AVCONNTIMEDATA.mat'];
    eval([groupnames{group_i} '= load(''' filename ''');']);
end

STATDATA.p = zeros(length(conditions),length(frequencies),length(times),channels);
STATDATA.t = zeros(length(conditions),length(frequencies),length(times),channels);
datnams = fieldnames(LOW);
% ALLDATA = [LOW.(datnams{1})(:,:,:,:,:);HIGH.(datnams{1})(:,:,:,:,:)];
for cond_i = 1:length(conditions)
    fprintf('\n%s %s','Running statistics for condition:',conditions{cond_i});
    for time_i = 1:length(avtimes)
        fprintf('.');
        for freq_i = 1:length(frequencies)
            for chann_i = 1:channels
                x = squeeze(LOW.(datnams{1})(:,cond_i,freq_i,time_i,chann_i));
                y = squeeze(HIGH.(datnams{1})(:,cond_i,freq_i,time_i,chann_i));
%                 x = squeeze(ALLDATA(:,cond_i,freq_i,time_i,chann_i));
%                 y = squeeze(HIGH.(datnams{1})(:,cond_i,freq_i,time_i,chann_i));
                [~,STATDATA.p(cond_i,freq_i,time_i,chann_i),~,stats] = ttest2(y,x);
                STATDATA.t(cond_i,freq_i,time_i,chann_i) = stats.tstat;
            end%channel loop
        end%frequency loop
    end
end
save([DATAOUT 'STATDATA.mat'],'STATDATA','-v7.3');
%% apply corrections
addpath(genpath([CWD 'PACKAGES' filesep 'mass_uni_toolbox']));
alpha = 0.05;
% significance = alpha;
significance = alpha;
buffer = alpha+alpha*0.25;
load([DATAOUT 'STATDATA.mat']);
STATDATA.adjt = STATDATA.t;%used for plotting significant clusters
STATDATA.plott = STATDATA.t;%used for plotting uncorrected values
fprintf('\n%s%s%s\n','Applying correction at ',num2str(significance),' level');
[~,~,STATDATA.adjp] = fdr_bh(STATDATA.p,significance,'pdep');%apply fdr correction
for cond_i = 1:length(conditions)
    fprintf('.');
    for time_i = 1:length(avtimes)
        for freq_i = 1:length(frequencies)
            for chann_i = 1:channels
                if STATDATA.p(cond_i,freq_i,time_i,chann_i)>buffer;
                    STATDATA.plott(cond_i,freq_i,time_i,chann_i) = 0;%10% buffer range
                end
                if STATDATA.adjp(cond_i,freq_i,time_i,chann_i)>significance;
                    STATDATA.adjt(cond_i,freq_i,time_i,chann_i) = 0;%fdr correction
                end
%                 if STATDATA.adjp(cond_i,freq_i,time_i,chann_i)>significance;
%                     STATDATA.adjt(cond_i,freq_i,time_i,chann_i) = 0;
%                 end
            end
        end%frequency loop
    end
end
save([DATAOUT 'STATDATA.mat'],'STATDATA','-v7.3');
%% plot data
addpath(genpath([CWD 'PACKAGES' filesep 'fieldtrip']));
addpath(genpath([CWD 'PACKAGES' filesep 'tightsubplot']));
addpath(genpath([CWD 'PACKAGES' filesep 'export_fig']));
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','Afz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
%set plotting params
fsize = 16;%fontsize
cfg =[];
cfg.layout = 'biosemi64.lay';
cfg.marker = 'off';
cfg.parameter = 'avg';
cfg.comment = 'no';
cfg.contournum = 2;
cfg.gridscale = 400;
cfg.shading = 'interp';
cfg.interactive = 'no';
dat.var = zeros(64,1);
dat.label = labels(:,1);
dat.time = 1;
dat.dimord = 'chan_time';
cfg.zlim = [-2.5 2.5];
cfg.highlightcolor = [0 0 0];
cfg.highlight = 'on';
load([DATAOUT 'STATDATA.mat']);
groupnames = {'LOW','HIGH'};
time_labels = {'Early Cue','Late Cue','Post Target'};
for freq_i = 2%1:length(frequencies)
    count = 0;
    figure();set(gcf,'OuterPosition',[0 0 1920 1080],'Color',[1 1 1]);ha = tight_subplot(length(conditions),length(avtimes),0,[0 0.1]);
    for cond_i = 1:length(conditions)
        for time_i = 1:length(avtimes)
            count = count+1;
            dat.avg = squeeze(STATDATA.plott(cond_i,freq_i,time_i,:));
            %search for sig electrodes
            siglabs = {};
            for i = 1:size(STATDATA.adjt,4);
                if abs(STATDATA.adjt(cond_i,freq_i,time_i,i)) >0;
                    siglabs = [siglabs;labels(i)];
                end
            end
            cfg.highlightchannel = siglabs;
            axes(ha(count));ft_topoplotER(cfg,dat);
%             if count < 4
%                 time_string = strcat(num2str((avtimes{time_i}(1)-3)*100),...
%                     '-',num2str((avtimes{time_i}(end)-3)*100));
%                 formatted_string = sprintf('%s\n%s',time_labels{time_i},time_string);
%                 title(formatted_string,'FontSize',fsize);
%             end
        end
    end
    saveas(gcf,[DATAOUT 'TMAPS_' frequencies{freq_i} '.pdf'],'pdf');
    close;
end
figure()
ft_topoplotER(cfg,dat)
%% use averaged over time data to explore differences 
%  between average connectivity for conditions
groupnames = {'LOW','HIGH'};
for group_i = 1:length(names)
    fprintf('\n%s %s','Processing group:',num2str(group_i));
    filename = [DATAOUT 'GROUP_' num2str(group_i) 'AVCONNTIMEDATA.mat'];
    eval([groupnames{group_i} '= load(''' filename ''');']);
end
freqofinterest = 2;%theta
for time_i = 1:length(avtimes)
    figure();%set(gcf,'OuterPosition',[0 0 1920 1080],'Color',[1 1 1]);
    for cond_i = 1:length(conditions)
        currentstat = squeeze(STATDATA.plott(cond_i,freqofinterest,time_i,:));
%         currentmask = find(currentstat~=0);
        currentmask = 1:size(STATDATA.plott,4);
        x(cond_i) = squeeze(mean(mean(LOW.AVTIMEDATA(:,cond_i,freqofinterest,time_i,currentmask),1)));
        y(cond_i) = squeeze(mean(mean(HIGH.AVTIMEDATA(:,cond_i,freqofinterest,time_i,currentmask),1)));
    end%cond_i loop
    plotdat = zeros(1,length(conditions)*length(groupnames));
    count= -1;
    for cond_i = 1:length(conditions)
        count = count+2;
        plotdat(1,count) = x(cond_i);
        plotdat(1,(count+1)) = y(cond_i);
    end
    plotdat = [plotdat(1:2);plotdat(3:4);plotdat(5:6);plotdat(7:8)];%hard coded
    bar(plotdat);set(gcf,'Color',[1 1 1]);colormap gray
    set(gca,'YLim',[0 0.55]);
    set(gca,'YTick',[0:0.05:0.2,0.4:0.05:0.55]);
    ylabel('Global Average Connectivity (z)','FontSize',12);    
    set(gca,'XTickLabel',{'R','ST','SA','NI'},...
        'FontSize',12);legend({'Low CFI','High CFI'});
    breakyaxis([0.05 0.41],0.015);
    saveas(gcf,[DATAOUT 'Theta_time_' num2str(time_i) '_AV_connectivity.pdf'],'pdf');
    close;
end%time_i loop