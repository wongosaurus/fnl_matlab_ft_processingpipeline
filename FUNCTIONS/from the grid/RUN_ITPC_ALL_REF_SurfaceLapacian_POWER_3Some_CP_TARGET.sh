#!/bin/bash

source /etc/profile.d/modules.sh
module load pbspro


#for ((i=1; i<=215; i+=1))
index_subject=(10 14 15 16 17 47 62 80 108 136 145 175 )
#index_subject=(26 27 33 100)
for i in "${index_subject[@]}"
do
jobname=ItCpT_$i

cat << EOF | qsub
######  Select resources #####
#PBS -N $jobname
#PBS -l select=1:ncpus=4:mem=2GB
#PBS -l walltime=40:00:00
#PBS -k oe
#PBS -m bea
#PBS -M aw946@newcastle.edu.au
########## Output File ##########
#PBS -o $jobname.txt
########## Error File ##########
#PBS -e $jobname.err
##### Change to current working directory #####
cd \$PBS_O_WORKDIR
##### Execute Program #####
#/usr/local/matlab/2015b/bin/matlab -nodisplay -nodesktop -nosplash -nojvm -r 'RUN_ITPC_SUBJECT_REF_SurfaceLapacian_POWER_3Some($i)'

source /etc/profile.d/modules.sh
module load matlab/R2016b

matlab -nodisplay -nodesktop -nosplash -r 'RUN_ITPC_SUBJECT_REF_SurfaceLapacian_ISPC_CLUSTER_CP_TARGET($i)'
EOF
sleep 30
done
