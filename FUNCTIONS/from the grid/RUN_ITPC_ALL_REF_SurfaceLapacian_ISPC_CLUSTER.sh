#!/bin/bash

#for ((i=1; i<=215; i+=1))
#index_subject=(115 116 119 121 125 128 133 154 158 162 174 177 179 180 182 198 201 202 203 204 205 206 207 208 209 210 211 212 213 214)
index_subject=(26 27 33 100)
for i in "${index_subject[@]}"
do
jobname=ItpcSL_$i
sleep 5
cat << EOF | qsub
######  Select resources #####
#PBS -N $jobname
#PBS -l select=1:ncpus=4:mem=2GB
#PBS -l walltime=40:00:00
#PBS -k oe
#PBS -m bea
#PBS -M aw946@newcastle.edu.au
########## Output File ##########
#PBS -o $jobname.txt
########## Error File ##########
#PBS -e $jobname.err
##### Change to current working directory #####
cd \$PBS_O_WORKDIR
##### Execute Program #####
/usr/local/matlab/2015b/bin/matlab -nodisplay -nodesktop -nosplash -r 'RUN_ITPC_SUBJECT_REF_SurfaceLapacian_ISPC_CLUSTER($i)'
EOF
done