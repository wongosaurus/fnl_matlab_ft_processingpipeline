#!/bin/bash
for ((i=1; i<=215; i+=1))
do
jobname=ItpcCA_$i
cat << EOF | qsub
######  Select resources #####
#PBS -N $jobname
#PBS -l select=1:ncpus=4:mem=2GB
#PBS -l walltime=4:00:00
#PBS -k oe
#PBS -m bea
#PBS -M aw946@newcastle.edu.au
########## Output File ##########
#PBS -o $jobname.txt
########## Error File ##########
#PBS -e $jobname.err
##### Change to current working directory #####
cd \$PBS_O_WORKDIR
##### Execute Program #####
/usr/local/matlab/2015a/bin/matlab -nodisplay -nodesktop -nosplash -r 'RUN_ITPC_SUBJECT_REF_CommonAverage($i)'
EOF
done