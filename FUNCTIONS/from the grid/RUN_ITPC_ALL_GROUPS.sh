#!/bin/bash
for ((i=1; i<=9; i+=1))
do
jobname=ITPC_$i
cat << EOF | qsub
######  Select resources #####
#PBS -N $jobname
#PBS -l select=1:ncpus=4:mem=2GB
#PBS -l walltime=40:00:00
#PBS -k oe
#PBS -m bea
#PBS -M aw946@newcastle.edu.au
########## Output File ##########
#PBS -o \$PBS_O_WORKDIR/$jobname.txt
########## Error File ##########
#PBS -e \$PBS_O_WORKDIR/$jobname.err
##### Change to current working directory #####
cd \$PBS_O_WORKDIR
##### Execute Program #####
/usr/local/matlab/2014a/bin/matlab -nodisplay -nodesktop -nosplash -r 'RUN_ALL_$i'
EOF
done