#!/bin/bash
array=( 1 2 3 4 5 6 7 8 9 10 11 12 13 14 16 17 19 20 21 22 23 24 25 26 27 28 29 31 32 33 34 35 36 38 39 40 41 42 44 45 47 49 51 52 53 54 55 56 60 61 62 63 64 65 66 67 68 69 70 71 73 74 76 80 81 82 83 84 85 86 87 89 90 93 97 101 103 110 114 115 119 121 122 123 126 127 131 132 133 134 135 137 141 144 145 148 150 151 152 153 155 156 157 158 159 160 161 163 164 165 168 169 170 171 173 174 176 177 178 179 180 181 182 183 184 185 186 189 190 191 192 194 195 196 197 198 199 200 201 203 204 205 206 210 211 213 215 )

for i in "${array[@]}"
do

jobname=ITPC_SL_$i

cat << EOF | qsub
######  Select resources #####
#PBS -N $jobname
#PBS -l select=1:ncpus=4:mem=2GB
#PBS -l walltime=10:00:00
#PBS -k oe
#PBS -m bea
#PBS -M aw946@newcastle.edu.au
########## Output File ##########
#PBS -o $jobname.txt
########## Error File ##########
#PBS -e $jobname.err
##### Change to current working directory #####
cd \$PBS_O_WORKDIR
##### Execute Program #####
/usr/local/matlab/2015a/bin/matlab -nodisplay -nodesktop -nosplash -r 'RUN_ITPC_SUBJECT_REF_SurfaceLapacian($i)'
EOF
done