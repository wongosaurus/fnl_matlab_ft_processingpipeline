function RUN_ITPC_SUBJECT_REF_SurfaceLapacian_ISPC_CLUSTER_MF_TARGET(name_i)

%Electrode Labels:
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
CWD = ['.',filesep,'ReferenceOutput',filesep,'SurfaceLapacian',filesep];
warning off;
listings = dir([CWD]);
NAMES = {listings(3:end).name};
disp(NAMES);
SAVE_DIR = ['.',filesep,'PROCESSED',filesep,'SurfaceLapacian',filesep];
%mkdir(SAVE_DIR);
% listings = dir([CWD,NAMES{1}]);
%cd(SAVE_DIR);
%listings = dir('AGE002_*');
%CONDITIONS = {'mixrepeat','switchto'};%Comparision Pipeline has only mixrepeat and switchto
CONDITIONS  = {'allrepeat','mixrepeat','noninfrepeat','switchto','switchaway','noninfswitch','noninf'};
%remove AGE212 <- manually from NAMES
fprintf('Loading: %s\n',[CWD,NAMES{name_i},filesep,CONDITIONS{1},filesep,num2str(1),'.mat'])
frequencies = logspace(log10(2),log10(30),80);
s = logspace(log10(3),log10(10),length(frequencies))./(2*pi*frequencies); %3-10 cycles varying window = window size
load([CWD,NAMES{name_i},filesep,CONDITIONS{1},filesep,num2str(1),'.mat']);

time    = -EEG.pnts/EEG.srate/2:1/EEG.srate:EEG.pnts/EEG.srate/2-1/EEG.srate;
mycluster=parcluster('local');

if strcmp(getenv('PBS_ENVIRONMENT'),'PBS_BATCH')
    pooltemp=getenv('TMPDIR');
    mycluster.JobStorageLocation = pooltemp;
    randWaitTime = 1+60*rand();
    pause(randWaitTime);
    
    fprintf('Setting Parallel Pool Temporary Directory to be %s\n',pooltemp);
end

parpool(mycluster,4);

%for name_i = 1:length(NAMES)%fell over after first subj
fprintf('Name: %s\n',NAMES{name_i});
name_t = 0;
for cond_i = 1:length(CONDITIONS)
    sum_t = 0;
    fprintf('\tCondition: %s\n',CONDITIONS{cond_i});
    load([CWD,NAMES{name_i},filesep,CONDITIONS{cond_i},filesep,num2str(1),'.mat']);
    %n_wavelet     = EEG.pnts;
    %n_data        = EEG.pnts*EEG.trials;
    %n_convolution = n_wavelet+n_data-1;
    %n_conv_pow2   = pow2(nextpow2(n_convolution));
    
    itpc_all = zeros(64,length(frequencies),length(time),'single');
    eegpower_all = itpc_all;
    ispc_all = itpc_all;
    %ClusterChannels = {'CP1','CPz','CP2','P1','Pz','P2'};
    ClusterChannels = {'FC1','FCz','FC2','C1','Cz','C2'};
    sig1_all = zeros(length(frequencies),EEG.pnts,EEG.trials);
    
    for chan_num = 1:length(ClusterChannels); %FCz
        %find the Channel Number:
        chan_i = find(ismember(labels,ClusterChannels{chan_num}));
        
        load([CWD,NAMES{name_i},filesep,CONDITIONS{cond_i},filesep,num2str(chan_i),'.mat']);
        itpc = zeros(length(frequencies),length(time),'single');
        eegpower = itpc;
        fprintf('\t\tChannel: %02i \t',chan_i);
        n_wavelet     = length(time);%EEG.pnts;
        n_data        = EEG.pnts*EEG.trials;
        n_convolution = n_wavelet+n_data-1;
        n_conv_pow2   = pow2(nextpow2(n_convolution));
        eegfft = fft(reshape(EEG.data,1,[]),n_conv_pow2);
        %data_fft1 = fft(reshape(EEG.data,1,n_data),n_convolution);
        data_fft1 = fft(reshape(EEG.data,1,[]),n_convolution);
        tic;
        
        half_wavelet  = (length(time)-1)/2;
        sig1_temp = zeros(length(frequencies),EEG.pnts,EEG.trials);
        
        npnts = EEG.pnts;
        ntrials = EEG.trials;
        parfor fi=1:length(frequencies)
            % create wavelet
            %fprintf('.');
            wavelet = exp(2*1i*pi*frequencies(fi).*time) .* exp(-time.^2./(2*(s(fi)^2)))/frequencies(fi);
            
            % convolution
            eegconv = ifft(fft(wavelet,n_conv_pow2).*eegfft);
            eegconv = eegconv(1:n_convolution);
            eegconv = reshape(eegconv(floor((npnts-1)/2):end-1-ceil((npnts-1)/2)),npnts,ntrials);
            
            % extract ITPC
            itpc(fi,:) = abs(mean(exp(1i*angle(eegconv)),2));
            eegpower(fi,:) = abs(mean(eegconv,2)).^2;
            
            s1 = frequencies(fi)/(2*pi*frequencies(fi));
            wavelet_fft = fft( exp(2*1i*pi*frequencies(fi).*time) .* exp(-time.^2./(2*(s1^2))) ,n_convolution);
            
            % phase angles from channel 1 via convolution
            convolution_result_fft = ifft(wavelet_fft.*data_fft1,n_convolution);
            convolution_result_fft = convolution_result_fft(half_wavelet+1:end-half_wavelet);
            sig1_temp(fi,:,:) = reshape(convolution_result_fft,npnts,ntrials);
            
        end
        
        t= toc;
        fprintf('\t%3.2f secs\n',t);
        itpc_all(chan_i,:,:) = itpc;
        eegpower_all(chan_i,:,:) = eegpower;
        sum_t = sum_t +t;
        sig1_all = sig1_all +sig1_temp;
        
    end
    
    sig1_all = sig1_all/length(ClusterChannels);
    
    for chan_i = 1:64
        load([CWD,NAMES{name_i},filesep,CONDITIONS{cond_i},filesep,num2str(chan_i),'.mat']);
        n_wavelet     =length(time);
        n_data        = EEG.pnts*EEG.trials;
        n_convolution = n_wavelet+n_data-1;
        n_conv_pow2   = pow2(nextpow2(n_convolution));
        itpc = zeros(length(frequencies),length(time),'single');
        eegpower = itpc;
        fprintf('\t\tChannel: %02i \t',chan_i);
        
        eegfft = fft(reshape(EEG.data,1,[]),n_conv_pow2);
        %data_fft1 = fft(reshape(EEG.data,1,n_data),n_convolution);
        data_fft1 = fft(reshape(EEG.data,1,[]),n_convolution);
        tic;
        
        npnts = EEG.pnts;
        ntrials = EEG.trials;
        parfor fi=1:length(frequencies)
            % create wavelet
            %fprintf('.');
            wavelet = exp(2*1i*pi*frequencies(fi).*time) .* exp(-time.^2./(2*(s(fi)^2)))/frequencies(fi);
            
            % convolution
            eegconv = ifft(fft(wavelet,n_conv_pow2).*eegfft);
            eegconv = eegconv(1:n_convolution);
            eegconv = reshape(eegconv(floor((npnts-1)/2):end-1-ceil((npnts-1)/2)),npnts,ntrials);
            
            % extract ITPC
            itpc(fi,:) = abs(mean(exp(1i*angle(eegconv)),2));
            eegpower(fi,:) = abs(mean(eegconv,2)).^2;
            
            s1 = frequencies(fi)/(2*pi*frequencies(fi));
            wavelet_fft = fft( exp(2*1i*pi*frequencies(fi).*time) .* exp(-time.^2./(2*(s1^2))) ,n_convolution);
            
            % phase angles from channel 1 via convolution
            convolution_result_fft = ifft(wavelet_fft.*data_fft1,n_convolution);
            convolution_result_fft = convolution_result_fft(half_wavelet+1:end-half_wavelet);
            sig2 = double(reshape(convolution_result_fft,npnts,ntrials));
            sig1 = double(squeeze(sig1_all(fi,:,:)));
            % cross-spectral density
            cdd = sig1 .* conj(sig2);
            
            % ISPC
            ispc_all(chan_i,fi,:) = abs(mean(exp(1i*angle(cdd)),2)); % note: equivalent to ispc(fi,:) = abs(mean(exp(1i*(angle(sig1)-angle(sig2))),2));
            
            
            
        end
        t= toc;
        fprintf('\t%3.2f secs\n',t);
        itpc_all(chan_i,:,:) = itpc;
        eegpower_all(chan_i,:,:) = eegpower;
        sum_t = sum_t +t;
    end
    fprintf('\tCondition %s Finished in %3.2f secs\n',CONDITIONS{cond_i},sum_t);
    save([SAVE_DIR,NAMES{name_i},'_',CONDITIONS{cond_i},'_ISPC_MF_TARGET.mat'],'ispc_all','-v7.3');
    name_t = name_t+ sum_t;
end
delete(gcp);
fprintf('Subject %s Finished in %3.2f secs\n',NAMES{name_i},name_t);
end

