
%% This file checks for missing files in the grid.
%
% For the ITPC/ISPC:
%
%   Looks for missing files from the AllRepeat Condition,
%   If all repeat condition is missing then the processing must begin
%   again for that subject.
%   Prints out the index to be copy and pasted into the shell script index
%   array.
%
%
%   Aaron Wong (20/5/16)

listings= dir('z:\ITPC\ReferenceOutput\SurfaceLapacian\');
orig_names = {listings().name}
orig_names = orig_names(3:end);

type = 'ISPC_CP_TARGET';
listings= dir(['z:\ITPC\PROCESSED\SurfaceLapacian\*allrepeat_' type '*']);
proc_names = {listings().name};
for i=1:length(proc_names)
    proc_names{i}=proc_names{i}(1:6);
end

missing_indices = find(~ismember(orig_names,proc_names));
fprintf('There are %i missing subjects.\n',sum(missing_indices>0));
for i = 1:length(missing_indices)
    fprintf('%i ',missing_indices(i));
end

%% Other Sanity Checks:
fprintf('\n');
conditions = {'switchto','switchaway','noninf','mixrepeat','allrepeat','noninfrepeat','noninfswitch'}; 
missing_inds = [];
for cond_i = 1:length(conditions)
    
    listings= dir(['z:\ITPC\PROCESSED\SurfaceLapacian\*',conditions{cond_i},'_',type,'*']);
    proc_names = {listings().name};
    for i=1:length(proc_names)
        proc_names{i}=proc_names{i}(1:6);
    end
    fprintf('Number of %s: \t %i.\n',conditions{cond_i},length(proc_names));
    missing_indices = find(~ismember(orig_names,proc_names));
    missing_inds = [missing_inds,missing_indices];
end
fprintf('Total Number of Subjects: \t %i.\n',length(orig_names));
unique_missing = unique(missing_inds);
fprintf('Conjucntion of Uniquely missing indices: \n')
for i = 1:length(unique_missing)
    fprintf('%i ',unique_missing(i));
end
fprintf('\n')
