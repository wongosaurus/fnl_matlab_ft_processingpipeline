#!/bin/bash
array=( 53 130 132 133 134 135 136 137 138 140 141 142 143 144 145 146 147 148 149 150 151 152 153 154 155 156 157 158 159 160 161 162 163 164 165 167 168 169 170 171 172 173 174 175 176 177 178 179 180 181 182 184 185 186 187 191 192 196 197 199 200 201 202 203 204 205 206 207 208 209 )


for i in "${array[@]}"
do

jobname=ITPC_CA_$i

cat << EOF | qsub
######  Select resources #####
#PBS -N $jobname
#PBS -l select=1:ncpus=4:mem=2GB
#PBS -l walltime=10:00:00
#PBS -k oe
#PBS -m bea
#PBS -M aw946@newcastle.edu.au
########## Output File ##########
#PBS -o $jobname.txt
########## Error File ##########
#PBS -e $jobname.err
##### Change to current working directory #####
cd \$PBS_O_WORKDIR
##### Execute Program #####
/usr/local/matlab/2015a/bin/matlab -nodisplay -nodesktop -nosplash -r 'RUN_ITPC_SUBJECT_REF_CommonAverage($i)'
EOF
done