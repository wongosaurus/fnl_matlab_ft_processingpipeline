#!/bin/bash
array=( 53 78 79 80 81 82 83 84 85 86 87 88 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 116 117 118 119 120 121 122 123 124 125 126 127 128 129 130 131 132 133 134 135 136 137 138 139 141 142 143 144 146 149 150 153 158 159 160 161 162 163 164 165 166 167 168 169 170 171 172 173 174 175 176 177 178 179 180 181 182 183 184 185 186 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 203 204 205 206 207 208 209 210 211 212 213 214 215 )

for i in "${array[@]}"
do

jobname=ITPC_AM_$i

cat << EOF | qsub
######  Select resources #####
#PBS -N $jobname
#PBS -l select=1:ncpus=4:mem=2GB
#PBS -l walltime=10:00:00
#PBS -k oe
#PBS -m bea
#PBS -M aw946@newcastle.edu.au
########## Output File ##########
#PBS -o $jobname.txt
########## Error File ##########
#PBS -e $jobname.err
##### Change to current working directory #####
cd \$PBS_O_WORKDIR
##### Execute Program #####
/usr/local/matlab/2015a/bin/matlab -nodisplay -nodesktop -nosplash -r 'RUN_ITPC_SUBJECT_REF_AverageMastoids($i)'
EOF
done