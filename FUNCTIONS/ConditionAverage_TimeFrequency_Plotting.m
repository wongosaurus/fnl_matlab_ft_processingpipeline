%% 1. Determine study specific working parameters.
clc; clear all; close all;
disp('1. Determine study specific working parameters.');
% set up data directories
workingvolume = 'E:\'; %this would vary per operating system.
wpms          = [];       %working parameters
wpms.dirs     = struct('CWD',[workingvolume 'UIB_DATA' filesep]);
wpms.dirs     = struct('CWD',[workingvolume 'UIB_DATA' filesep], ...
    'PACKAGES',[wpms.dirs.CWD 'PACKAGES' filesep], ...
    'FUNCTIONS',[wpms.dirs.CWD 'FUNCTIONS' filesep], ...
    'PREPROC', [wpms.dirs.CWD 'MTV3 data' filesep], ...
    'DATA_DIR',[wpms.dirs.CWD 'CSD_DATAIN' filesep], ...
    'WAVELET_OUTPUT_DIR',[wpms.dirs.CWD 'WAVELET_OUTPUT' filesep],...
    'COHERENCE_DIR',[wpms.dirs.CWD 'IMAGCOH_OUTPUT' filesep],...
    'ERP_DIR',[wpms.dirs.CWD 'ERP' filesep]);
% set up study conditions
listing                              = dir(wpms.dirs.PREPROC);
wpms.conditions{1,length(listing)-3}           = [];%preallocate,exclude 3 irrevlevant listings
wpms.conditionshort{1,length(wpms.conditions)} = [];
for list_i = 4:length(listing)
    wpms.conditions{1,(list_i-3)} = listing(list_i).name;
    space_index = strfind(wpms.conditions{list_i-3},' ');
    wpms.conditionshort{1,list_i-3} = [wpms.conditions{list_i-3}(1) '-' wpms.conditions{list_i-3}(space_index+1:space_index+3)];
    if wpms.conditionshort{1,list_i-3}(1) == 'G';%go/no go trials labelled as N not G
        wpms.conditionshort{1,list_i-3}(1) = 'N';
    end
end%list_i loop
%set blue gabor short labels
wpms.conditionshort{4} = 'O-blue-BL1';
wpms.conditionshort{5} = 'O-blue-BL3';
% set up participant list
cd([wpms.dirs.PREPROC filesep wpms.conditions{1}])
listing                       = dir('*.set');
participant_index             = find(listing(1).name=='_')-1;%determine location in listing,name where subject string ends
temp_names{1,length(listing)} = [];%preallocate
for list_i = 1:length(listing)
    temp_names{1,list_i} = listing(list_i).name(1:participant_index);
end%list_i loop
wpms.names = unique(temp_names);
wpms.responsetype   = {'R','S'};
clear workingvolume listing temp_names list_i participant_index space_index%tidy as we travel
% add relevant paths
fprintf('\t%s\t%s %s\n','Adding',wpms.dirs.FUNCTIONS,'to MATLAB path');
addpath(genpath(wpms.dirs.FUNCTIONS))%functions
fprintf('\t%s\t%s %s\n','Adding',[wpms.dirs.PACKAGES 'eeglab12_0_1_0b'],'to MATLAB path');
addpath(genpath([wpms.dirs.PACKAGES 'eeglab12_0_1_0b']));%eeglab
%
% %add blue oddballs
% wpms.conditions = [wpms.conditions 'ODDBALL BLUE BL1' 'ODDBALL BLUE BL3'];
% wpms.conditionshort = [wpms.conditionshort 'O-blue-BL1' 'O-blue-BL3'];
%% create condition average time-frequency plots
clearvars -except wpms
disp('Setting up working time-frequency-specific parameters')
EEG       = pop_loadset([wpms.dirs.PREPROC wpms.conditions{1} filesep wpms.names{1} '_N-CUE-R-C-f.set']);
labels    = {EEG.chanlocs(:).labels};%extract electrode labels
times     = -1000:0.5:3500;
frequency = logspace(log10(2),log10(30),80);%frequency bins
clear EEG;
disp('Beginning condition averaging')
% cluster_name = {'Frontal','Frontocentral','Centroparietal','Parietoccipital'};
cluster_name = {'Frontal','Frontocentral','Central',...
    'Parietal','Parietoccipital'};
% electrode_clusters = {{'AF3','FpZ','AF4','F1','FZ','F2'},...
%     {'FC1','FCZ','FC2','C1','CZ','C2'},...
%     {'CP1','CPZ','CP2','P1','PZ','P2'},...
%     {'PO3','POZ','PO4','O1','OZ','O2'}};
electrode_clusters = {{'AF3','FpZ','AF4'},...
    {'FC1','FCZ','FC2'},...
    {'C3','CZ','C4'},...
    {'P1','PZ','P2'},...
    {'PO3','POZ','PO4'}};
% electrode = find(strcmpi('FCz',labels));
% electrode = find(ismember(labels,electrode_clusters));
for cluster_i = 1:length(cluster_name)
    condavg_power = zeros(length(wpms.names),length(labels),length(frequency),length(times),'single');
    for electrode = find(ismember(labels,electrode_clusters{cluster_i})); %= 1:length(labels)
        fprintf('\n%s %s\t','Working on:',labels{electrode});
        for name_i = 1:length(wpms.names)
            fprintf('\n%s %s\t','Working on:',wpms.names{name_i});
            tic;
            count=0;
            for cond_i = 1:length(wpms.conditions)
                fprintf('.');
                for response_i = 1:length(wpms.responsetype)
                    count=count+1;
                    filename = [wpms.dirs.WAVELET_OUTPUT_DIR wpms.names{name_i} filesep ...
                        wpms.conditions{cond_i} filesep wpms.conditions{cond_i} ...
                        wpms.responsetype{response_i} '_' num2str(electrode) ...
                        '_imagcoh_mwtf.mat'];
                    load(filename,'mw_tf');
                    %                 matobj = matfile(filename);
                    condavg_power(name_i,electrode,:,:) = squeeze(condavg_power(name_i,electrode,:,:))+mw_tf;
                end%response_i loop
            end%cond_i loop
            condavg_power(name_i,electrode,:,:) = condavg_power(name_i,electrode,:,:)./count;
            t=toc;fprintf('\t%3.2f %s',t,'s');
            fprintf('\t%s %3.2f %s','estimated:',((length(wpms.names)-name_i)*t),'s time remaining');
        end%name_i loop
    end
    fprintf('\n');
    disp('Performing t-tests')
    p=zeros(length(frequency),length(times));
    for freq_i=1:size(condavg_power,3)
        [~,p(freq_i,:)] = ttest(squeeze(mean(condavg_power(:,electrode,freq_i,:),2)));
    end
    % FDR correction
    plot_times = find(times==-200):find(times==1200);%times to show for t-f
    addpath(genpath([wpms.dirs.PACKAGES 'mass_uni_toolbox' filesep]));
    fdr_alpha = 0.005;
    disp('Applying FDR-correction: Note this may take a few minutes')
    tic;
    [~,~,adj_p]=fdr_bh(p(:,plot_times),fdr_alpha,'pdep');
    toc
    disp('FDR complete');
    
    %%
    figure();
    thresh=fdr_alpha;%correction for plots
    condavg_power_grp = squeeze(mean(mean(condavg_power(:,electrode,:,plot_times),1),2));
    condavg_power_head = squeeze(mean(mean(condavg_power(:,electrode,:,plot_times),1),2));
    condavg_power_grp(adj_p>thresh) = 0;
    % plotting
    contourf(times(plot_times),frequency,condavg_power_grp,100,'linecolor','none');caxis([-3 3]);
    hold on;
    % sig_lines = zeros(size(adj_p));sig_lines(adj_p>thresh)=1;
    sig_lines = zeros(size(condavg_power_grp));sig_lines(adj_p<thresh)=1;
    contour(times(plot_times),frequency,sig_lines,1,'k','LineWidth',3);axis square;caxis([-3 3]);
    ylabel('Frequency (Hz)','FontSize',16);xlabel('Time (ms)','FontSize',16);set(gca,'FontSize',16);
    title(cluster_name{cluster_i});
end
%% reload all data to compare at times of interest
for cluster_i = 2%:length(cluster_name)
    disp(cluster_names{cluster_i});
    %     condavg_power = zeros(length(wpms.names),length(labels),length(frequency),length(times),'single');
    condall_power = zeros(length(wpms.names),length(electrode_clusters{cluster_i}),length(wpms.conditions),length(wpms.responsetype),length(frequency),length(times),'single');
    electrode_count =0;
    for electrode = find(ismember(labels,electrode_clusters{cluster_i})); %= 1:length(labels)
        electrode_count=electrode_count+1;
        for name_i = 1:length(wpms.names)
            fprintf('\n%s %s\t','Working on:',wpms.names{name_i});
            tic;
            for cond_i = 1:length(wpms.conditions)
                fprintf('.');
                for response_i = 1:length(wpms.responsetype)
                    filename = [wpms.dirs.WAVELET_OUTPUT_DIR wpms.names{name_i} filesep ...
                        wpms.conditions{cond_i} filesep wpms.conditions{cond_i} ...
                        wpms.responsetype{response_i} '_' num2str(electrode) ...
                        '_imagcoh_mwtf.mat'];
                    load(filename,'mw_tf');
                    condall_power(name_i,electrode_count,cond_i,response_i,:,:) = mw_tf;
                    clear mw_tf;
                end%response_i loop
            end%cond_i loop
            t=toc;fprintf('\t%3.2f %s',t,'s');
            fprintf('\t%s %3.2f %s','estimated:',((length(wpms.names)-name_i)*t),'s time remaining');
        end%name_i loop
    end
end
%%
%
freq_bands = {find(floor(frequency)==2, 1 ):find(floor(frequency)==4, 1,'last' ),...
    find(floor(frequency)==4, 1 ):find(floor(frequency)==8, 1,'last' ),...
    find(floor(frequency)==8, 1 ):find(floor(frequency)==13, 1,'last' ),...
    find(floor(frequency)==13, 1 ):find(floor(frequency)==29, 1,'last' )};
freq_labels = {'delta','theta','alpha','beta'};

condall_power_av = squeeze(mean(mean(condall_power,1),2));
condall_power = squeeze(mean(mean(condall_power,1),2));

condall_power_av = condall_power;
% for freq_i=1:length(freq_labels)
%     freq_of_interest = cell2mat(freq_bands(strcmpi(freq_labels{freq_i},freq_labels)));
%     figure();bar(squeeze(mean(mean(condall_power_av(:,:,freq_of_interest,:),3),4)))
%     set(gca,'XTickLabel',wpms.conditionshort)
% %     bar(mean(mean(condall_power_av(:,:,sig_lines(freq_of_interest,:)~=0,1),3),4))
% %     squeeze(mean(mean(condall_power(:,:,:,sig_lines(freq_of_interest,:)~=0),1)));
% %     disp(['Frequency: ' freq_labels{freq_i} ' ' num2str(squeeze(mean(condavg_power_head(sig_lines(freq_of_interest,:)~=0))))]);
% %     figure();contourf(times(plot_times),frequency(freq_of_interest),squeeze(sig_lines(freq_of_interest,:)~=0),100,'linecolor','none');caxis([-2 2]);
% end
plot_times = find(times==400):find(times==600);%times to show for t-f
sig_power_values = zeros(length(freq_labels),length(wpms.conditions),2);
for freq_i=1:length(freq_labels)
    freq_of_interest = cell2mat(freq_bands(strcmpi(freq_labels{freq_i},freq_labels)));
    for cond_i = 1:length(wpms.conditions)
%         temp_cond = squeeze(mean(condall_power_av(cond_i,:,:,:),2));
        temp_cond = squeeze(mean(mean(condall_power_av(cond_i,:,freq_of_interest,plot_times),3),4));
        %disp([wpms.conditions{cond_i} ' ' num2str(mean(temp_cond(sig_lines(freq_of_interest,:)==1)))]);
%         sig_power_values(freq_i,cond_i)= mean(temp_cond(sig_lines(freq_of_interest,:)==1));
        sig_power_values(freq_i,cond_i,:)= temp_cond;
    end
end
figure();
for freq_i = 1:length(freq_labels)
subplot(2,2,freq_i);bar(squeeze(sig_power_values(freq_i,:,:)))
end

