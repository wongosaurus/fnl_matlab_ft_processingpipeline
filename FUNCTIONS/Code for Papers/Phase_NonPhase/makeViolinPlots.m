clearvars;close all;clc;

% set up initial parameters and determine final participants

cwd = 'E:\FNL_EEG_TOOLBOX\FUNCTIONS\Code for Papers\Phase_NonPhase\violinPlots\';

addpath(genpath('E:\FNL_EEG_TOOLBOX\FUNCTIONS\Code for Papers\Phase_NonPhase\violinPlots\violinPlots\gramm-master\'))

names ={'AGE002',	'AGE003',	'AGE004',	'AGE005',	'AGE007',	'AGE008',	'AGE012',	'AGE013',	'AGE014',	'AGE015',	'AGE017',	'AGE018',	'AGE019',	'AGE020',	'AGE021',	'AGE022',	'AGE023',	'AGE024',	'AGE026',	'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE046',	'AGE047',	'AGE050',	'AGE051',	'AGE053',	'AGE058',	'AGE059',	'AGE061',	'AGE062',	'AGE063',	'AGE066',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE072',	'AGE073',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE088',	'AGE089',	'AGE090',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE096',	'AGE097',	'AGE098',	'AGE100',	'AGE102',	'AGE103',	'AGE104',	'AGE107',	'AGE108',	'AGE109',	'AGE111',	'AGE114',	'AGE115',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE122',	'AGE123',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE133',	'AGE134',	'AGE135',	'AGE136',	'AGE138',	'AGE141',	'AGE145',	'AGE146',	'AGE147',	'AGE148',	'AGE149',	'AGE150',	'AGE151',	'AGE152',	'AGE153',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE163',	'AGE164',	'AGE165',	'AGE166',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE172',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE189',	'AGE190',	'AGE195',	'AGE197',	'AGE198',	'AGE199',	'AGE201',	'AGE202',	'AGE203',	'AGE205',	'AGE206',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE232',	'AGE233',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE241',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE273',	'AGE275',	'AGE276',	'AGE278',	'AGE279'};

% store data as easy to read table to verify

t=table();

t.Participant = names';

% load behavioural data

% data=csvread([cwd 'ERPCOST_MVPA_RT.csv'],1,27);

data = csvread([cwd 'errorRate.csv'],1,2);

%data = [data(:,1:3) data(:,3)-data(:,2) data(:,2)-data(:,1)];

header = {'AllRepeat_err','Repeat_err','Switch_err'};


for header_i = 1:length(header)

    t.(header{header_i}) = squeeze(data(:,header_i));

end

disp(t);
%%
fontSize = 24;

rtData = csvread([cwd 'RT.csv'],1,1);

conditionLabels = {'All Repeat','Repeat','Switch'};

conditionOrder = [find(ismember(conditionLabels,'Switch')),...

    find(ismember(conditionLabels,'Repeat')),...

    find(ismember(conditionLabels,'All Repeat'))];

RTData = rtData(:,conditionOrder);

accData = data(:,conditionOrder);

conditionColourMap = [225/256 76/256 169/256;%3-S
    
0 0 0;%2-R

255/256 128/256 0/256];%1-AR

plotTypes = {'\bf RT (ms)'};

plotNames = {'All Repeat','Repeat','Switch'};

xTickLabels = {'RA', 'R','S'};

% begin plotting


%%
data_t = reshape(RTData,1,numel(RTData))';

conditionNamesMatrix = repmat(plotNames,size(RTData,1),1);

conditionVector      = reshape(conditionNamesMatrix,1,numel(conditionNamesMatrix))';

xVector              = repmat([1:size(RTData,2)]',1,size(RTData,1))';

xVector              = reshape(xVector,1,numel(xVector))';

g(1,1)=gramm('x',xVector,'y',data_t,'color',conditionVector);

g(1,1).stat_violin('half','true','fill','transparent','normalization','width','width',1,'LineWidth',4,'force','right');

g(1,1).stat_boxplot('LineColor',[119 43 195]./255,'notch',true,'width',0.8,'draw_outliers',false,'LineWidth',4,'mean','true');

g(1,1).set_color_options('map',conditionColourMap);4

g(1,1).set_layout_options('legend',false);

g(1,1).set_names('x','','y','RT (ms)');

g(1,1).set_title('RT','FontSize',fontSize);

g(1,1).coord_flip();

g(1,1).axe_property('XLim',[0.5 4],'LineWidth',4,'FontSize',fontSize,...
'XTickLabels',{'Switch','Repeat','All Repeat', ' '});


data_t=reshape(accData,1,numel(accData))';

g(1,2)=gramm('x',xVector,'y',data_t,'color',conditionVector);

g(1,2).stat_violin('half','true','fill','transparent','normalization','width','width',1,'LineWidth',4,'force','right');

g(1,2).stat_boxplot('LineColor',[119 43 195]./255,'notch',true,'dodge',0.7,'width',0.8,'draw_outliers',false,'LineWidth',4,'mean','true');

g(1,2).set_color_options('map',conditionColourMap);

g(1,2).set_layout_options('legend',false);

g(1,2).set_names('x','','y','Error Rate (%)');

g(1,2).coord_flip();

g(1,2).set_title('Error Rate','FontSize',fontSize);

g(1,2).axe_property('XLim',[0.5 4],'LineWidth',4,'FontSize',fontSize,...
'XTickLabels',{'Switch','Repeat','All Repeat', ' '});

scrSize=get(0,'screensize');

figure('Position',[0 0 scrSize(3)*1 scrSize(4)*.52]);

%figure('Position',scrSize);

hndl=g.draw();

print(gcf,[cwd 'behaviouralData2.tiff'],'-painters','-dtiff','-r300');