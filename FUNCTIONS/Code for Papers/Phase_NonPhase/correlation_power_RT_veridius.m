% correlating behaviour with phase/nonphase power

clear all;close all;clc

% setup parameters
listingdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
datain = 'E:\FNL_EEG_TOOLBOX\ReferenceOutput\SurfaceLapacian';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30;
fbins = 80;
times = (-1:0.001953125000000:3.5)*1000;
conditions = {'allrepeat','mixrepeat','switchto'};
wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);
wpms.allnames=wpms.names;
wpms.names ={'AGE002',	'AGE003',	'AGE004',	'AGE005',	'AGE007',	'AGE008',	'AGE012',	'AGE013',	'AGE014',	'AGE015',	'AGE017',	'AGE018',	'AGE019',	'AGE020',	'AGE021',	'AGE022',	'AGE023',	'AGE024',	'AGE026',	'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE046',	'AGE047',	'AGE050',	'AGE051',	'AGE053',	'AGE058',	'AGE059',	'AGE061',	'AGE062',	'AGE063',	'AGE066',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE072',	'AGE073',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE088',	'AGE089',	'AGE090',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE096',	'AGE097',	'AGE098',	'AGE100',	'AGE102',	'AGE103',	'AGE104',	'AGE107',	'AGE108',	'AGE109',	'AGE111',	'AGE114',	'AGE115',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE122',	'AGE123',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE133',	'AGE134',	'AGE135',	'AGE136',	'AGE138',	'AGE141',	'AGE145',	'AGE146',	'AGE147',	'AGE148',	'AGE149',	'AGE150',	'AGE151',	'AGE152',	'AGE153',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE163',	'AGE164',	'AGE165',	'AGE166',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE172',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE189',	'AGE190',	'AGE195',	'AGE197',	'AGE198',	'AGE199',	'AGE201',	'AGE202',	'AGE203',	'AGE205',	'AGE206',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE232',	'AGE233',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE241',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE273',	'AGE275',	'AGE276',	'AGE278',	'AGE279'};
clearvars -except wpms

%%
% SWITCH COST


load('E:\FNL_EEG_TOOLBOX\FUNCTIONS\Code for Papers\Phase_NonPhase\LOGS\rts_mean.mat');

powertypes = {'Total' 'Phase-Locked' 'Non-Phase-Locked'};
clusters = {{'F1','Fz','F2','FC1','FCz','FC2'} {'C1','Cz', 'C2', 'CP1','CPz','CP2'} {'POz','PO3', 'PO4', 'P1', 'Pz', 'P2'}};

nameinds = find(ismember(wpms.allnames,wpms.names));
% to store correlation data
correlationdata = zeros(length(powertypes),length(clusters),length(wpms.frequencies),length(wpms.times),'single');
correlationpvalues = correlationdata;
correlationpvalues_old = correlationdata;
correlationdata_old = correlationdata;
RT = rts_mean(1:3,nameinds);
 
switchCost_old = RT(3,:)-RT(2,:);
switchCost = (RT(3,:)-RT(2,:))./RT(2,:);

for powertype_i = 1:length(powertypes)
    fprintf('\n%s\t',powertypes{powertype_i});
    tic
    for cluster_i = 1:length(clusters)
        fprintf('%s','.')
        electrodesofinterest = find(ismember(wpms.labels,clusters{cluster_i}));
        for freq_i = 1:length(wpms.frequencies)
            for time_i = 1:length(wpms.times)
                switchData = squeeze(mean(SWITCH_COST_DATA(powertype_i,:,electrodesofinterest,freq_i,time_i),3)); 
                [correlationdata(powertype_i,cluster_i,freq_i,time_i),correlationpvalues(powertype_i,cluster_i,freq_i,time_i)] = corr(switchCost',switchData');
                [correlationdata_old(powertype_i,cluster_i,freq_i,time_i),correlationpvalues_old(powertype_i,cluster_i,freq_i,time_i)] = corr(switchCost_old',switchData');
            end
        end
    end
    toc
end
% changed save name
savename = 'E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\switchCostCorrelationDataRatios3006.mat';
save(savename,'correlationdata','correlationpvalues','correlationdata_old','correlationpvalues_old');

%%
%Plots

load('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\switchCostCorrelationDataRatios3006.mat');
%load('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\TTEST_SwitchCost_Output.mat');

addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'));
clusternames = {'Frontal','Central','Parietal'};
alpha = 0.005;

powertypes = {'Total' 'Phase-Locked' 'Non-Phase-Locked'};
clusters = {{'F1','Fz','F2','FC1','FCz','FC2'} {'C1','Cz', 'C2', 'CP1','CPz','CP2'} {'POz','PO3', 'PO4', 'P1', 'Pz', 'P2'}};

powerPlotOrder = [1 3 2];

baseline_start = -300;
baseline_end = 2000;
plottimes = setbaseline(wpms, baseline_start, baseline_end);
count = 1;
figure();

map = 'viridis';
N = 256;
cmap=bids_color(map,N);

fontSize = 18;
lineWidth = 2;

for powertype_i = 1:length(powertypes)
    for cluster_i = 1:length(clusters)
        subplot(3,3,count);
        count = count+1;
        data = squeeze(correlationdata(powerPlotOrder(powertype_i),cluster_i,:,plottimes));
        h_r = fdr_bky(squeeze(correlationpvalues(powerPlotOrder(powertype_i),cluster_i,:,plottimes)),alpha);
        %h_t = fdr_bky(squeeze(pvalues(powerPlotOrder(powertype_i),cluster_i,:,plottimes)),alpha);
        contourf(wpms.times(plottimes),wpms.frequencies,data,50,'linecolor','none')
        if count==2||count==5||count==8
            ylabel({['\bf' 'Frequency (Hz)']}); 
        end
        if count==8
            xlabel(['\bf' 'Time (ms)']);
        end
        caxis([-.5 .5]);
        hold on
        colormap(cmap);
        if count==10
            ax = get(gca);
            origSize = ax.Position;
            ax.Position = [0 0 1 1];
            c = colorbar;
            c.Label.String = (['\bf' 'Correlation (' '\bf\it' 'r' '\rm' '\bf' ')']);
            c.FontSize = fontSize;
            set(gca,'Position',origSize);
        end
        contour(wpms.times(plottimes),wpms.frequencies,h_r,1,'w','linewidth',lineWidth);
        plot([0 0],[2 30],'--k','linewidth',lineWidth)
        plot([1000 1000],[2 30],'--k','linewidth',lineWidth)
        set(gca,'FontSize',fontSize,'linewidth',lineWidth)
    end
end


%% MIX COST

filename = 'E:\FNL_EEG_TOOLBOX\FUNCTIONS\Code for Papers\Phase_NonPhase\LOGS\rts_mean.mat';
load(filename);

powertypes = {'Total' 'Phase-Locked' 'Non-Phase-Locked'};
clusters = {{'F1','Fz','F2','FC1','FCz','FC2'} {'C1','Cz', 'C2', 'CP1','CPz','CP2'} {'POz','PO3', 'PO4', 'P1', 'Pz', 'P2'}};

nameinds = find(ismember(wpms.allnames,wpms.names));

% to store correlation data
correlationdata = zeros(length(powertypes),length(clusters),length(wpms.frequencies),length(wpms.times),'single');
correlationpvalues = correlationdata;
correlationpvalues_old = correlationdata;
correlationdata_old = correlationdata;

RT = rts_mean(1:3,nameinds);
 
mixCost = (RT(2,:)-RT(1,:))./RT(1,:);
mixCost_old = (RT(2,:)-RT(1,:));

for powertype_i = 1:length(powertypes)
    fprintf('\n%s\t',powertypes{powertype_i});
    tic
    for cluster_i = 1:length(clusters)
        fprintf('%s','.')
        electrodesofinterest = find(ismember(wpms.labels,clusters{cluster_i}));
        for freq_i = 1:length(wpms.frequencies)
            for time_i = 1:length(wpms.times)
                mixData = squeeze(mean(MIXED_COST_DATA(powertype_i,:,electrodesofinterest,freq_i,time_i),3));
                [correlationdata(powertype_i,cluster_i,freq_i,time_i),correlationpvalues(po