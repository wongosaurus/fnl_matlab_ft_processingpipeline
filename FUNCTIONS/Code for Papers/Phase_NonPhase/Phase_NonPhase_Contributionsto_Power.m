
clear all;close all;clc

% setup parameters
listingdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
datain = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30;
fbins = 80;
times = (-1:0.001953125000000:3.5)*1000;
conditions = {'allrepeat','mixrepeat','switchto'};
wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);
wpms.allnames=wpms.names;
wpms.names ={'AGE002',	'AGE003',	'AGE004',	'AGE005',	'AGE007',	'AGE008',	'AGE012',	'AGE013',	'AGE014',	'AGE015',	'AGE017',	'AGE018',	'AGE019',	'AGE020',	'AGE021',	'AGE022',	'AGE023',	'AGE024',	'AGE026',	'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE046',	'AGE047',	'AGE050',	'AGE051',	'AGE053',	'AGE058',	'AGE059',	'AGE061',	'AGE062',	'AGE063',	'AGE066',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE072',	'AGE073',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE088',	'AGE089',	'AGE090',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE096',	'AGE097',	'AGE098',	'AGE100',	'AGE102',	'AGE103',	'AGE104',	'AGE107',	'AGE108',	'AGE109',	'AGE111',	'AGE114',	'AGE115',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE122',	'AGE123',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE133',	'AGE134',	'AGE135',	'AGE136',	'AGE138',	'AGE141',	'AGE145',	'AGE146',	'AGE147',	'AGE148',	'AGE149',	'AGE150',	'AGE151',	'AGE152',	'AGE153',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE163',	'AGE164',	'AGE165',	'AGE166',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE172',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE189',	'AGE190',	'AGE195',	'AGE197',	'AGE198',	'AGE199',	'AGE201',	'AGE202',	'AGE203',	'AGE205',	'AGE206',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE232',	'AGE233',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE241',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE273',	'AGE275',	'AGE276',	'AGE278',	'AGE279'};
clearvars -except wpms
%%
% load in all EEG data

clusters = {{'F1','Fz','F2','FC1','FCz','FC2'} {'C1','Cz', 'C2', 'CP1','CPz','CP2'} {'POz','PO1', 'PO2', 'P1', 'Pz', 'P2'}};

analysistype = 'allpower';
conditionbaseline = 0;
epoch = 'cue';

baseline_start_cue = -300;
baseline_end_cue = -100;
baseline = setbaseline(wpms, baseline_start_cue, baseline_end_cue); %time points for baseline
wpms.baselineinds.cue = baseline;

percentnonphase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times);
percentphase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times);

for name_i = 1:length(wpms.names)
    for cond_i = 1:length(wpms.conditions)
        filename=[wpms.dirs.datain wpms.names{name_i} '_' wpms.conditions{cond_i} '_ALL_POWER3.mat'];
        alldat = load(filename,'eegpower_all','inducedpower_all');
        data = alldat.eegpower_all;
        data2 = alldat.inducedpower_all;
        for cluster_i = 1:length(clusters)
            electrodesofinterest = find(ismember(wpms.labels,clusters{cluster_i}));
            [totalbl, nonphaselockedbl, phaselockedbl] = baselinecorrect_ch_freq_time(wpms, data, data2, electrodesofinterest, analysistype, conditionbaseline,epoch);
            for time_i = 1:length(wpms.times)
                for freq_i = 1:length(wpms.frequencies)
                    percentnonphase = ((totabl(wpms.labels{cluster_i},freq_i,time_i) - phaselockedbl(wpms.labels{cluster_i},freq_i,time_i))/totabl(wpms.labels{cluster_i},freq_i,time_i)*100);
                    percentphase = totalbl - percentnonphase;
                end
            end
        end
    end
end

% need to save off total somewhere too!!

save('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\phasenonphasecontributions.mat','percentnonphase','percentphase','-v7.3');
clear percentnonphase;
clear percentphase;

%%

load('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\phasenonphasecontributions.mat');
addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'));
wpms.power_types = {'Total','Phase','Non-Phase'};
wpms.cluster_names = {'Frontal','Central','Parietal'};
clusters = {{'F1','Fz','F2','FC1','FCz','FC2'} {'C1','Cz', 'C2', 'CP1','CPz','CP2'} {'POz','PO1', 'PO2', 'P1', 'Pz', 'P2'}};
wpms.clusters = clusters;
CONDITIONS_NOAR = wpms.conditions;%CONDITIONS;
labels = wpms.labels;
count=0;
figure();

baseline_start = -300;
baseline_end = 2000;
plot_times = setbaseline(wpms, baseline_start, baseline_end);


for power_type_i = 1:length(wpms.power_types)
    for cluster_i = 1:length(wpms.clusters);
        chan_i = find(ismember(labels,wpms.clusters{cluster_i}));
        count = count +1;
        subplot(length(wpms.power_types),length(wpms.clusters),count);
        data = squeeze(mean(condition_average_eegpower(power_type_i,chan_i,:,plot_times),2));
% not sure how to refer to percentphase and percentnonphase.. :/
        contourf(times(plot_times),wpms.frequencies,data,50,'linecolor','none');
        if power_type_i == length(wpms.power_types)
            clims = [-5 5];
        else
            clims = [-10 10];
        end
        caxis(clims);
        colormap 'jet';
        if power_type_i == 1
            title({wpms.cluster_names{cluster_i};wpms.power_types{power_type_i}});
        else
            title(wpms.power_types{power_type_i});
        end
    end
end

% for each time*frequency pixel, what is decibel of total, PL, NPL
% use Cohen's formula - (total-phase)/total*100
% plot - would I plot percentage? i.e. have 3 plots - total, percent phase,
%percent nonphase. Total is just normal dB, percent phase plots percentage
%of total that is phase locked across each time*freq point, percent
%nonphase is nonphase version???

