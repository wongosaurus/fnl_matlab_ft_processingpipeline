clear all;close all;clc

% setup parameters
listingdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
datain = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\NoBL\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30;
fbins = 80;
times = (-1:0.001953125000000:3.5)*1000;
conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninf','noninfrepeat','noninfswitch'};
wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);
wpms.allnames=wpms.names;
wpms.names ={'AGE002',	'AGE003',	'AGE004',	'AGE005',	'AGE007',	'AGE008',	'AGE012',	'AGE013',	'AGE014',	'AGE015',	'AGE017',	'AGE018',	'AGE019',	'AGE020',	'AGE021',	'AGE022',	'AGE023',	'AGE024',	'AGE026',	'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE046',	'AGE047',	'AGE050',	'AGE051',	'AGE053',	'AGE058',	'AGE059',	'AGE061',	'AGE062',	'AGE063',	'AGE066',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE072',	'AGE073',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE088',	'AGE089',	'AGE090',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE096',	'AGE097',	'AGE098',	'AGE100',	'AGE102',	'AGE103',	'AGE104',	'AGE107',	'AGE108',	'AGE109',	'AGE111',	'AGE114',	'AGE115',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE122',	'AGE123',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE133',	'AGE134',	'AGE135',	'AGE136',	'AGE138',	'AGE141',	'AGE145',	'AGE146',	'AGE147',	'AGE148',	'AGE149',	'AGE150',	'AGE151',	'AGE152',	'AGE153',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE163',	'AGE164',	'AGE165',	'AGE166',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE172',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE189',	'AGE190',	'AGE195',	'AGE197',	'AGE198',	'AGE199',	'AGE201',	'AGE202',	'AGE203',	'AGE205',	'AGE206',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE232',	'AGE233',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE241',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE273',	'AGE275',	'AGE276',	'AGE278',	'AGE279'};
clearvars -except wpms
%% DO THIS FOR CUE AND THEN TARGET
% setup baseline

%cue
baseline_start = -300;
baseline_end = -100;
wpms.baselineinds.cue = setbaseline(wpms, baseline_start, baseline_end);

%target
baseline_start = 700;
baseline_end = 900;
wpms.baselineinds.target = setbaseline(wpms, baseline_start, baseline_end);

clusters = {{'FCz', 'FC1', 'FC2', 'Cz', 'C1', 'C2'} {'CPz', 'CP1', 'CP2', 'Pz', 'P1', 'P2'}};
analysistype = 'allpower';
epoch = {'cue' 'target'};
epochconditions = {[1 2 3 4 5];[1 2 3 4 6 7]};
epochconditionsttest = {[2 3 4 5];[2 3 4 6 7]};
clusternames = {'frontal', 'parietal'};

%tvalue structures
tpvalue.cue.total = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
tpvalue.cue.nonphase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
tpvalue.cue.phase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));

tvalue.cue.total = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
tvalue.cue.nonphase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
tvalue.cue.phase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));

tpvalue.target.total = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
tpvalue.target.nonphase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
tpvalue.target.phase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));

tvalue.target.total = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
tvalue.target.nonphase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
tvalue.target.phase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));

%fstat structures
fpvalue.cue.total = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
fpvalue.cue.nonphase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
fpvalue.cue.phase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));

fstat.cue.total = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
fstat.cue.nonphase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
fstat.cue.phase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));

fpvalue.target.total = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
fpvalue.target.nonphase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
fpvalue.target.phase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));

fstat.target.total = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
fstat.target.nonphase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
fstat.target.phase = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));


for freq_i = 1:length(wpms.frequencies)
    fprintf('\n%s\t%s','Frequency:',num2str(freq_i));
    tic;
    %average across clusters storage variable
    ERSPfilename = [wpms.dirs.datain 'ERSP_' num2str(freq_i) '.mat'];
    load(ERSPfilename);
    INDUCEDPOWERfilename = [wpms.dirs.datain 'INDUCEDPOWER_' num2str(freq_i) '.mat'];
    load(INDUCEDPOWERfilename);
    data = ERSP_F_t;
    data2 = INDUCEDPOWER_F_t;
    for cluster_i = 1%:length(clusters)
        electrodesofinterest = find(ismember(wpms.labels,clusters{cluster_i}));
        for epoch_i = 1%:length(epoch);
            [totalbl, nonphaselockedbl, phaselockedbl] = baselinecorrect(wpms, data, data2, electrodesofinterest, analysistype,1,epoch{epoch_i});
            
            % run t-tests for total, phaselocked, nonphaselocked
%             totaldata = permute(totalbl,[2,1,3]);
%             nonphasedata = permute(nonphaselockedbl,[2,1,3]);
%             phasedata = permute(phaselockedbl,[2,1,3]);
%             
%             [tpvalue.(epoch{epoch_i}).total(cluster_i,freq_i,:), tvalue.(epoch{epoch_i}).total(cluster_i,freq_i,:)] = runpowerttest(squeeze(mean(totaldata(:,:,epochconditionsttest{epoch_i}),3)));
%             [tpvalue.(epoch{epoch_i}).nonphase(cluster_i,freq_i,:), tvalue.(epoch{epoch_i}).nonphase(cluster_i,freq_i,:)] = runpowerttest(squeeze(mean(nonphasedata(:,:,epochconditionsttest{epoch_i}),3)));
%             [tpvalue.(epoch{epoch_i}).phase(cluster_i,freq_i,:), tvalue.(epoch{epoch_i}).phase(cluster_i,freq_i,:)] = runpowerttest(squeeze(mean(phasedata(:,:,epochconditionsttest{epoch_i}),3)));
            
            % run ANOVA for total, phaselocked, nonphaselocked
            %remove all repeat baseline
            [totalbl, nonphaselockedbl, phaselockedbl] = baselinecorrect(wpms, data, data2, electrodesofinterest, analysistype,0,epoch{epoch_i});
            
            totaldata = permute(totalbl,[2,1,3]);
            nonphasedata = permute(nonphaselockedbl,[2,1,3]);
            phasedata = permute(phaselockedbl,[2,1,3]);
            for time_i = 1:length(wpms.times);
                data_anova = squeeze(totaldata(:,time_i,epochconditions{epoch_i}));
                [fpvalue.(epoch{epoch_i}).total(cluster_i,freq_i,time_i),fstat.(epoch{epoch_i}).total(cluster_i,freq_i,time_i)] = runpoweranova(data_anova,epoch{epoch_i});
                data_anova = squeeze(nonphasedata(:,time_i,epochconditions{epoch_i}));
                [fpvalue.(epoch{epoch_i}).nonphase(cluster_i,freq_i,time_i),fstat.(epoch{epoch_i}).nonphase(cluster_i,freq_i,time_i)] = runpoweranova(data_anova,epoch{epoch_i});
                data_anova = squeeze(phasedata(:,time_i,epochconditions{epoch_i}));
                [fpvalue.(epoch{epoch_i}).phase(cluster_i,freq_i,time_i),fstat.(epoch{epoch_i}).phase(cluster_i,freq_i,time_i)] = runpoweranova(data_anova,epoch{epoch_i});
            end
        end
    end
    toc
end

savename = 'E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\frontalpowerstats_cueonly_291117.mat';
%save(savename,'tpvalue','tvalue');
%save(savename,'fpvalue','fstat','tpvalue','tvalue');
save(savename,'fpvalue','fstat');
%%
%fmap/tmap plots & correction
load('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\frontalpowerstats_cueonly_291117.mat','fpvalue','fstat');

alpha = 0.001; %Original = 0.001
%for cluster analysis only
clusteralpha_fvalues = 1e-25;
clusteralpha_tvalues = 1e-10; %Original: 00000001
width = 60;  %Time Bins
height = 6; %Frequency Bins:
freqindmin = 22; %4HZ: Max from 4Hz to 30Hz;
freqindmax = 68;%20Hz

plottime_start_cue = -300;
plottime_end_cue = 1200;
cuetimes = setbaseline(wpms, plottime_start_cue, plottime_end_cue);

plottime_start_target = 700;
plottime_end_target = 2000;
targettimes = setbaseline(wpms, plottime_start_target, plottime_end_target);
plottimes = {cuetimes;targettimes};

powernames = {'total' 'nonphase' 'phase'};
epochnames = {'cue' 'target'};
addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'));
%looping through powernames, epoch & clusters

for power_i = 1:length(powernames);
    for epoch_i = 1%:length(epochnames);
        for cluster_i = 1%:length(clusters);
%             %ttest
%             [h_t] = fdr_bky(squeeze(tpvalue.(epochnames{epoch_i}).(powernames{power_i})(cluster_i,:,:)),alpha);
            %anova
            [h_f] = fdr_bky(squeeze(fpvalue.(epochnames{epoch_i}).(powernames{power_i})(cluster_i,:,:)),alpha);
            
%             %ttest
%             [h_ct] = fdr_bky(squeeze(tpvalue.(epochnames{epoch_i}).(powernames{power_i})(cluster_i,:,:)),clusteralpha_tvalues);
            %anova
            [h_cf] = fdr_bky(squeeze(fpvalue.(epochnames{epoch_i}).(powernames{power_i})(cluster_i,:,:)),clusteralpha_fvalues);
            
%             %plotting t values
%             datat = squeeze(tvalue.(epochnames{epoch_i}).(powernames{power_i})(cluster_i,:,plottimes{epoch_i}));
%             figure
%             contourf(wpms.times(plottimes{epoch_i}),wpms.frequencies,datat,50,'linecolor','none')
%             caxis([-20 20]);
%             colormap jet
%             hold on
%             contour(wpms.times(plottimes{epoch_i}),wpms.frequencies,h_t(:,plottimes{epoch_i}),1,'k');
%             contour(wpms.times(plottimes{epoch_i}),wpms.frequencies,h_ct(:,plottimes{epoch_i}),1,'r');
%             fprintf('\n%s\t%s\t%s\t%s\n',powernames{power_i},epochnames{epoch_i}, clusternames{cluster_i},'ttest');
%             CC = getpeaks(width, height, freqindmin, freqindmax, h_ct, datat, plottimes{epoch_i}, wpms);
%             savename=['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\tcluster_' powernames{power_i} '_' epochnames{epoch_i} '_' clusternames{cluster_i} '.mat'];
%             save(savename,'CC');
%             title(['tvalue ' powernames{power_i} ' ' epochnames{epoch_i} ' ' clusternames{cluster_i}]);
%             
%             savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\tvalue ' powernames{power_i} '_' epochnames{epoch_i} '_' clusternames{cluster_i} '.tif'];
%             saveas(gcf,savename,'tiff');
            
            %plotting f stats
            figure
            dataf = squeeze(fstat.(epochnames{epoch_i}).(powernames{power_i})(cluster_i,:,plottimes{epoch_i}));
            contourf(wpms.times(plottimes{epoch_i}),wpms.frequencies,dataf,50,'linecolor','none')
            caxis([-100 100]);
            colormap jet
            hold on
            contour(wpms.times(plottimes{epoch_i}),wpms.frequencies,h_f(:,plottimes{epoch_i}),1,'k');
            contour(wpms.times(plottimes{epoch_i}),wpms.frequencies,h_cf(:,plottimes{epoch_i}),1,'r');
            fprintf('\n%s\t%s\t%s\t%s\n',powernames{power_i},epochnames{epoch_i}, clusternames{cluster_i},'ftest');
            CC = getpeaks(width, height, freqindmin, freqindmax, h_cf, dataf, plottimes{epoch_i}, wpms);
            savename=['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' powernames{power_i} '_' epochnames{epoch_i} '_' clusternames{cluster_i} '291117.mat'];
            save(savename,'CC');
            title(['fstat ' powernames{power_i} ' ' epochnames{epoch_i} ' ' clusternames{cluster_i}]);
            
            savenameanova = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fstat ' powernames{power_i} '_' epochnames{epoch_i} '_' clusternames{cluster_i} '291117.tif'];
            saveas(gcf,savenameanova,'tiff');
        end
    end
end
%%
% pulling out peaks for plotting
load('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\frontalpowerstats_cueonly_291117.mat','fpvalue','fstat');

%cue
baseline_start_cue = -300;
baseline_end_cue = -100;
wpms.baselineinds.epoch.cue = setbaseline(wpms, baseline_start_cue, baseline_end_cue);

%target
baseline_start_target = 700;
baseline_end_target = 900;
wpms.baselineinds.epoch.target = setbaseline(wpms, baseline_start_target, baseline_end_target);

clusters = {{'FCz', 'FC1', 'FC2', 'Cz', 'C1', 'C2'} {'CPz', 'CP1', 'CP2', 'Pz', 'P1', 'P2'}};
analysistype = 'allpower';

epochnames = {'cue' 'target'};
wpms.epochconditions.cue = {[1 2 3 4 5]};
wpms.epochconditions.target = {[1 2 3 4 6 7]};
epochconditionsttest = {[2 3 4 5];[2 3 4 6 7]};
clusternames = {'frontal', 'parietal'};

alpha = 0.001; %Original = 0.001
%for cluster analysis only
clusteralpha_fvalues = 1e-25;
clusteralpha_tvalues = 1e-10; %Original: 00000001
width = 60;  %Time Bins
height = 6; %Frequency Bins:
freqindmin = 22; %4HZ: Max from 4Hz to 30Hz;
freqindmax = 68;%20Hz

plottime_start_cue = -300;
plottime_end_cue = 1200;
cuetimes = setbaseline(wpms, plottime_start_cue, plottime_end_cue);

plottime_start_target = 700;
plottime_end_target = 2000;
targettimes = setbaseline(wpms, plottime_start_target, plottime_end_target);

plottimes = {cuetimes;targettimes};
electrodesofinterest = 1:64;
conditionbaseline = 0;

powernames = {'total' 'nonphase' 'phase'};

partinds = 1:197;

addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'));
%looping through powernames, epoch & clusters

for power_i = 1:length(powernames);
    for epoch_i = 1%:length(epochnames);
        for cluster_i = 1%:length(clusternames);
            filename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' powernames{power_i} '_' epochnames{epoch_i} '_' clusternames{cluster_i} '291117.mat'];
            load (filename);
            loadname = {[wpms.dirs.datain 'ERSP_'];[wpms.dirs.datain 'INDUCEDPOWER_']};
            for pixel_i = 1:length(CC.PixelIdxList)
                fprintf('\tWorking on Pixel Index: %i\n',pixel_i );
                Location = CC.PixelIdxLocation{pixel_i};
                if isempty(Location)
                    continue;
                end
                switch power_i
                    case 1
                        [totalpeaks, ~, ~] = peakwindow(Location, loadname, width, height, wpms, electrodesofinterest, analysistype, epochnames{epoch_i}, conditionbaseline, plottimes{epoch_i},partinds);
                        savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' powernames{power_i} '_' epochnames{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) '_windowedforplotting291117.mat'];
                        save(savename, 'totalpeaks');
                    case 2
                        [~, nonphasepeaks, ~] = peakwindow(Location, loadname, width, height, wpms, electrodesofinterest, analysistype, epochnames{epoch_i}, conditionbaseline, plottimes{epoch_i},partinds);
                        savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' powernames{power_i} '_' epochnames{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) '_windowedforplotting291117.mat'];
                        save(savename, 'nonphasepeaks');
                    case 3
                        [~, ~, phasepeaks] = peakwindow(Location, loadname, width, height, wpms, electrodesofinterest, analysistype, epochnames{epoch_i}, conditionbaseline, plottimes{epoch_i},partinds);
                        savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' powernames{power_i} '_' epochnames{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) '_windowedforplotting291117.mat'];
                        save(savename, 'phasepeaks');
                end
            end
        end
    end
end

%%
% Bar plots & head plots
addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip-20150902\'));
titlenames = {'Total' 'Non-Phase-Locked' 'Phase-Locked'};
conditionlabels = {{'RA' 'RM' 'ST' 'SA' 'NI'};{'RA' 'RM' 'ST' 'SA' 'RN' 'SN'}};

% phase extra clusters
phasecluster = {{'Cz' 'C1' 'C2' 'CP1' 'CPz'};{'C6' 'T8' 'CP6'}};

%nonphase extra clusters
nonphasecluster = {{'F3' 'F5' 'AF3' 'F1' 'FC1' 'FC3'};{'AF1' 'AFz' 'F3' 'F1' 'Fz'}};
%nonphasecluster = {{'F3' 'F5' 'AF3' 'F1' 'FC1' 'FC3'};{'O1' 'O2' 'PO3' 'PO4' 'PO8'}};

xtitle = 'Condition'; 
ylimits = [-5 5];
threshold = 0.001;
flimits = [-50 50];


for power_i = 1:length(powernames);
for epoch_i = 1%:length(epochnames);
    for cluster_i = 1%:length(clusternames);
        electrodesofinterest = find(ismember(wpms.labels,clusters{cluster_i}));
        for pixel_i = 1:5
            filename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' powernames{power_i} '_' epochnames{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) '_windowedforplotting291117.mat'];
            try
                A = load (filename);
                fname = fieldnames(A);
                peakdata = A.(fname{1});clear A fname;
                ytitle = [titlenames{power_i} ' (dB) '];
                xticks = conditionlabels{epoch_i};
                titlestr = [powernames{power_i} ' ' epochnames{epoch_i} ' ' clusternames{cluster_i} ' ' num2str(pixel_i)];
                plotdata(peakdata, wpms, ytitle, xtitle, ylimits, flimits,xticks, epochnames{epoch_i}, threshold, electrodesofinterest, titlestr);
                savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\plotsforphasenonphase\fcluster_' powernames{power_i} '_' epochnames{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) '291117_newscale.tif'];
                saveas(gcf, savename, 'tiff');
                close
            catch
            end
        end
    end
end
end

% %%
% %pulling out data
% 
% for power_i = 1:length(powernames);
%     for epoch_i = 1:length(epochnames);
%         for cluster_i = 1:length(clusternames);
%             electrodesofinterest = find(ismember(wpms.labels,clusters{cluster_i}));
%             for pixel_i = 1:5
%                 filename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' powernames{power_i} '_' epochnames{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) '_windowedforplotting.mat'];
%                 try
%                     A = load (filename);
%                     fname = fieldnames(A);
%                     peakdata = A.(fname{1});clear A fname;
%                     statsdata = squeeze(mean(peakdata(electrodesofinterest,:,:),1));
%                     savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' powernames{power_i} '_' epochnames{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) '_stats.txt'];
%                     dlmwrite(savename,statsdata,'delimiter','\t');
% %                     save (savename, 'statsdata','-tabs','-ascii');
%                 catch
%                 end
%             end
%         end
%     end
% end
% 
% 
% 
%                     