% setup parameters
listingdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
datain = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\NoBL\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30;
fbins = 80;
frequencies = logspace(log10(fmin),log10(fmax),fbins);
times = (-1:0.001953125000000:3.5)*1000;
conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninf','noninfrepeat','noninfswitch'};
wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);
wpms.allnames=wpms.names;
wpms.names ={'AGE002',	'AGE003',	'AGE004',	'AGE005',	'AGE007',	'AGE008',	'AGE012',	'AGE013',	'AGE014',	'AGE015',	'AGE017',	'AGE018',	'AGE019',	'AGE020',	'AGE021',	'AGE022',	'AGE023',	'AGE024',	'AGE026',	'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE046',	'AGE047',	'AGE050',	'AGE051',	'AGE053',	'AGE058',	'AGE059',	'AGE061',	'AGE062',	'AGE063',	'AGE066',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE072',	'AGE073',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE088',	'AGE089',	'AGE090',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE096',	'AGE097',	'AGE098',	'AGE100',	'AGE102',	'AGE103',	'AGE104',	'AGE107',	'AGE108',	'AGE109',	'AGE111',	'AGE114',	'AGE115',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE122',	'AGE123',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE133',	'AGE134',	'AGE135',	'AGE136',	'AGE138',	'AGE141',	'AGE145',	'AGE146',	'AGE147',	'AGE148',	'AGE149',	'AGE150',	'AGE151',	'AGE152',	'AGE153',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE163',	'AGE164',	'AGE165',	'AGE166',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE172',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE189',	'AGE190',	'AGE195',	'AGE197',	'AGE198',	'AGE199',	'AGE201',	'AGE202',	'AGE203',	'AGE205',	'AGE206',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE232',	'AGE233',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE241',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE273',	'AGE275',	'AGE276',	'AGE278',	'AGE279'};
clearvars -except wpms

SAVEDIR = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\';
LOADDIR = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\';
%%
loadname = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\ERPs.mat'];
load(loadname)

wpms.clusters = {{'F1','Fz','F2','FC1','FCz','FC2'} {'C1','Cz', 'C2', 'CP1','CPz','CP2'} {'POz','PO3', 'PO4', 'P1', 'Pz', 'P2'}};

erp_baseline_start = -50;
erp_baseline_end = 50;

erp_baselineinds = setbaseline(wpms, erp_baseline_start, erp_baseline_end);

blerpdata = bsxfun(@minus,erpdata,squeeze(mean(erpdata(:,:,:,erp_baselineinds),4)));
times = wpms.times;
plot_start = -300;
plot_end = 2000;

wpms.conditionNames = {'All Repeat', 'Mixed Repeat', 'Switch'};
fontSize = 18;
lineWidth = 2;
plot_times = setbaseline(wpms, plot_start,plot_end);
alpha = 0.005;

addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'));
close all
figure();set(gcf,'Position',get(0,'screensize'),'color',[1 1 1]);

f = zeros(1,length(wpms.conditions));
condIWant = find(ismember(wpms.conditions,{'mixrepeat','switchto'}));
lineColors = {[0 .8 0] [0 0 0] [255,128,0]/256};

for cluster_i = 1:length(wpms.clusters)
    clusterinds = find(ismember(wpms.labels,wpms.clusters{cluster_i}));
    subplot(4,length(wpms.clusters),cluster_i+9);
    plotdata1 = squeeze(mean(blerpdata(cluster_i,condIWant(1),:,plot_times),3))-squeeze(mean(blerpdata(cluster_i,condIWant(2),:,plot_times),3));
    for cond_i = condIWant
        plotdata2 = squeeze(mean(blerpdata(cluster_i,cond_i,:,plot_times),3));
        testdata = squeeze(blerpdata(cluster_i,condIWant,:,plot_times));
        f(cond_i) = plot(wpms.times(plot_times),plotdata2,'Color',lineColors{cond_i},'linewidth',lineWidth);
        if cluster_i == 1
            ylabel(['\bf' 'Amplitude (\muV/cm^2)']);
            xlabel(['\bf' 'Time (ms)']);
        end
        hold on
        plot(wpms.times(plot_times),plotdata1,'Color',lineColors{1},'linewidth',lineWidth);
        if cluster_i == 1
            ylabel(['\bf' 'Amplitude (\muV/cm^2)']);
            xlabel(['\bf' 'Time (ms)']);
        end
            [~,pvals] = ttest(squeeze(testdata(1,:,:)),squeeze(testdata(2,:,:)),'Alpha',alpha);
            h = fdr_bky(pvals,alpha);
            h = double(h);
            h(h==0)=NaN;
            h = h.*-19;
            plot(wpms.times(plot_times),h,'Color',lineColors{3},'LineWidth',2);
            xlim([-300 2000])
            ylim([-20 30])
            plot([-300 2000],[0 0],'k','linewidth',lineWidth)
            plot([0 0],[-20 30],'--k','linewidth',lineWidth)
            plot([1000 1000],[-20 35],'--k','linewidth',lineWidth)
            box off
            set(gca,'YDir','reverse','FontSize',fontSize,'linewidth',lineWidth)
        end
    end
    %%
    condIWant = find(ismember(wpms.conditions,{'allrepeat','mixrepeat'}));
    f = zeros(1,length(wpms.conditions));
    lineColors = {[255,128,0]/256 [0 0 0] [0 1 0]};
    for cluster_i = 1:length(wpms.clusters)
        clusterinds = find(ismember(wpms.labels,clusters{cluster_i}));
        subplot(4,length(wpms.clusters),cluster_i+9);
        plotdata1 = squeeze(mean(blerpdata(cluster_i,condIWant(2),:,plot_times),3))-squeeze(mean(blerpdata(cluster_i,condIWant(1),:,plot_times),3));
        for cond_i = condIWant
            plotdata2 = squeeze(mean(blerpdata(cluster_i,cond_i,:,plot_times),3));
            testdata = squeeze(blerpdata(cluster_i,condIWant,:,plot_times));
            f(cond_i) = plot(wpms.times(plot_times),plotdata2,'Color',lineColors{cond_i},'linewidth',lineWidth);
            hold on
            plot(wpms.times(plot_times),plotdata1,'Color',lineColors{3},'linewidth',lineWidth);
            [~,pvals] = ttest(squeeze(testdata(1,:,:)),squeeze(testdata(2,:,:)),'Alpha',alpha);
            h = fdr_bky(pvals,alpha);
            h = double(h);
            h(h==0)=NaN;
            h = h.*-19;
            plot(wpms.times(plot_times),h,'Color',[255,128,0]/256,'LineWidth',2);
            xlim([-300 2000])
            ylim([-20 30])
            plot([-300 2000],[0 0],'k','linewidth',lineWidth)
            if cluster_i == 1
                ylabel(['\bf' 'Amplitude (\muV/cm^2)']);
                xlabel(['\bf' 'Time (ms)']);
                plot([0 0],[-20 30],'--k','linewidth',lineWidth)
                plot([1000 1000],[-20 30],'--k','linewidth',lineWidth)
            end
            box off
            set(gca,'YDir','reverse','FontSize',fontSize,'linewidth',lineWidth)
        end
    end