% setup parameters
listingdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
datain = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\NoBL\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30;
fbins = 80;
frequencies = logspace(log10(fmin),log10(fmax),fbins);
times = (-1:0.001953125000000:3.5)*1000;
conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninf','noninfrepeat','noninfswitch'};
wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);
wpms.allnames=wpms.names;
wpms.names ={'AGE002',	'AGE003',	'AGE004',	'AGE005',	'AGE007',	'AGE008',	'AGE012',	'AGE013',	'AGE014',	'AGE015',	'AGE017',	'AGE018',	'AGE019',	'AGE020',	'AGE021',	'AGE022',	'AGE023',	'AGE024',	'AGE026',	'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE046',	'AGE047',	'AGE050',	'AGE051',	'AGE053',	'AGE058',	'AGE059',	'AGE061',	'AGE062',	'AGE063',	'AGE066',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE072',	'AGE073',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE088',	'AGE089',	'AGE090',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE096',	'AGE097',	'AGE098',	'AGE100',	'AGE102',	'AGE103',	'AGE104',	'AGE107',	'AGE108',	'AGE109',	'AGE111',	'AGE114',	'AGE115',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE122',	'AGE123',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE133',	'AGE134',	'AGE135',	'AGE136',	'AGE138',	'AGE141',	'AGE145',	'AGE146',	'AGE147',	'AGE148',	'AGE149',	'AGE150',	'AGE151',	'AGE152',	'AGE153',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE163',	'AGE164',	'AGE165',	'AGE166',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE172',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE189',	'AGE190',	'AGE195',	'AGE197',	'AGE198',	'AGE199',	'AGE201',	'AGE202',	'AGE203',	'AGE205',	'AGE206',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE232',	'AGE233',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE241',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE273',	'AGE275',	'AGE276',	'AGE278',	'AGE279'};
clearvars -except wpms

SAVEDIR = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\';
LOADDIR = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\';

%% %Cost t-tests

clusters = {{'F1','Fz','F2','FC1','FCz','FC2'} {'C1','Cz', 'C2', 'CP1','CPz','CP2'} {'POz','PO3', 'PO4', 'P1', 'Pz', 'P2'}};
POWER_TYPES = {'TOTAL','PHASE','NON_PHASE'};

COSTS           = {'MixCost',   'SwitchCost'};
COSTS_COMPARE   = {[1,2],       [2,3]}; 

baseline_start_cue = -300;
baseline_end_cue = -100;
baseline = setbaseline(wpms, baseline_start_cue, baseline_end_cue); %time points for baseline
wpms.baselineinds.cue = baseline;
analysistype = 'allpower';
conditionbaseline = 0;
epoch = 'cue';
% 
% TEST for 1 Participant:
wpms.names ={'AGE002'};
MIXED_COST_DATA = zeros(length(POWER_TYPES),length(wpms.names), length(wpms.labels), length(wpms.frequencies), length(wpms.times),'single');
% 
for name_i = 1:length(wpms.names)
    tic;
    for cost_i = 1%:length(COSTS)      
        filename=[LOADDIR wpms.names{name_i} '_' wpms.conditions{COSTS_COMPARE{cost_i}(1)} '_ALL_POWER3.mat']; 
        D1 = load(filename, 'eegpower_all', 'inducedpower_all');
        filename=[LOADDIR wpms.names{name_i} '_' wpms.conditions{COSTS_COMPARE{cost_i}(2)} '_ALL_POWER3.mat']; 
        D2 = load(filename, 'eegpower_all', 'inducedpower_all');
        for chan_i = 64;    
            [totalbl1, nonphaselockedbl1, phaselockedbl1] = baselinecorrect_ch_freq_time(wpms, D1.eegpower_all, D1.inducedpower_all, chan_i, analysistype, conditionbaseline,epoch);
            [totalbl2, nonphaselockedbl2, phaselockedbl2] = baselinecorrect_ch_freq_time(wpms, D2.eegpower_all, D2.inducedpower_all, chan_i, analysistype, conditionbaseline,epoch);
            MIXED_COST_DATA(1,name_i,chan_i,:,:) = totalbl2-totalbl1;
            MIXED_COST_DATA(2,name_i,chan_i,:,:) = phaselockedbl2-phaselockedbl1;
            MIXED_COST_DATA(3,name_i,chan_i,:,:) = nonphaselockedbl2-nonphaselockedbl1;
        end
    end
    t = toc;
    fprintf('\tTime Taken: %3.3f\n',t);
end

figure(3);
hold on;

A = sum(ifft(totalbl1).*conj(ifft(totalbl1)));
B = sum(ifft(phaselockedbl1).*conj(ifft(phaselockedbl1)));
C = sum(ifft(nonphaselockedbl1).*conj(ifft(nonphaselockedbl1)));
plot(A); plot(B); plot(C);

figure(10);
contourf(1:size(phaselockedbl1,2),frequencies,phaselockedbl1,50,'linecolor','none');
clims = [-20 20];
caxis(clims);
title('PHASE LOCKED DATA');
saveas(gcf,'PHASE_LOCKED_DATA.jpg');


% save('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\MIXED_COST_DATA.mat','MIXED_COST_DATA','-v7.3');
% clear MIXED_COST_DATA;
% 
% 
% % Switch Cost Code:
% SWITCH_COST_DATA = zeros(length(POWER_TYPES),length(wpms.names), length(wpms.labels), length(wpms.frequencies), length(wpms.times),'single');
% 
% for name_i = 1:length(wpms.names)
%     tic;
%     for cost_i = 2%:length(COSTS)      
%         filename=[LOADDIR wpms.names{name_i} '_' wpms.conditions{COSTS_COMPARE{cost_i}(1)} '_ALL_POWER3.mat']; 
%         D1 = load(filename, 'eegpower_all', 'inducedpower_all');
%         filename=[LOADDIR wpms.names{name_i} '_' wpms.conditions{COSTS_COMPARE{cost_i}(2)} '_ALL_POWER3.mat']; 
%         D2 = load(filename, 'eegpower_all', 'inducedpower_all');
%         for chan_i = 1:64;    
%             [totalbl1, nonphaselockedbl1, phaselockedbl1] = baselinecorrect_ch_freq_time(wpms, D1.eegpower_all, D1.inducedpower_all, chan_i, analysistype, conditionbaseline,epoch);
%             [totalbl2, nonphaselockedbl2, phaselockedbl2] = baselinecorrect_ch_freq_time(wpms, D2.eegpower_all, D2.inducedpower_all, chan_i, analysistype, conditionbaseline,epoch);
%             SWITCH_COST_DATA(1,name_i,chan_i,:,:) = totalbl2-totalbl1;
%             SWITCH_COST_DATA(2,name_i,chan_i,:,:) = phaselockedbl2-phaselockedbl1;
%             SWITCH_COST_DATA(3,name_i,chan_i,:,:) = nonphaselockedbl2-nonphaselockedbl1;
%         end
%     end
%     t = toc;
%     fprintf('\tTime Taken: %3.3f\n',t);
% end
% save('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\SWITCH_COST_DATA.mat','SWITCH_COST_DATA','-v7.3');
% clear SWITCH_COST_DATA;
%% TTEST: Mixed_COST
% FilenameMixed = 'E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\MIXED_COST_DATA.mat';
% load(FilenameMixed);
% The data loaded should look like this:
% MIXED_COST_DATA = zeros(length(POWER_TYPES),length(wpms.names), length(wpms.labels), length(wpms.frequencies), length(wpms.times),'single');



%Setup Output:
tvalues = zeros(length(POWER_TYPES),length(clusters),length(wpms.frequencies),length(wpms.times),'single');
pvalues = zeros(length(POWER_TYPES),length(clusters),length(wpms.frequencies),length(wpms.times),'single');
    
for cluster_i = 1:length(clusters)
    electrodes_of_interest = find(ismember(wpms.labels, clusters{cluster_i}));
    data_for_testing = squeeze(mean(MIXED_COST_DATA(:,:,electrodes_of_interest,:,:),3));
    % data_for_testing = [PowerType,Names,Freq,Time]
    
    for powertype_i = 1:length(POWER_TYPES)
        tic;
        fprintf('MixCost %i %10s ',cluster_i,POWER_TYPES{powertype_i});
        for freq_i = 1:length(wpms.frequencies)
            fprintf('.');
            testdata = squeeze(data_for_testing(powertype_i,:,freq_i,:));
            %h=0|1 for significant or not
            %pvalues = pvalues
            %~is for confidence interval, not used
            %stats is a structure containing degrees of freedom and t-statistic
            [h,pvalues(powertype_i,cluster_i,freq_i,:),~,stat]=ttest(testdata);%this runs the ttest
            tvalues(powertype_i,cluster_i,freq_i,:)=stat.tstat;%store the tvalue

        end
        t = toc;
        fprintf(' Time: %3.3f sec \n',t);
        
    end
end
save('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\TTEST_MixedCost_Output.mat','tvalues','pvalues','-v7.3');
clear 'MIXED_COST_DATA';

%Now for Switch Cost:
FilenameSwitch = 'E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\SWITCH_COST_DATA.mat';
load(FilenameSwitch);

%Setup Output:
tvalues = zeros(length(POWER_TYPES),length(clusters),length(wpms.frequencies),length(wpms.times),'single');
pvalues = zeros(length(POWER_TYPES),length(clusters),length(wpms.frequencies),length(wpms.times),'single');
    
for cluster_i = 1:length(clusters)
    electrodes_of_interest = find(ismember(wpms.labels, clusters{cluster_i}));
    data_for_testing = squeeze(mean(SWITCH_COST_DATA(:,:,electrodes_of_interest,:,:),3));
    % data_for_testing = [PowerType,Names,Freq,Time]
    for powertype_i = 1:length(POWER_TYPES)
        tic;
        fprintf('SwitchCost %i %10s ',cluster_i,POWER_TYPES{powertype_i});
        for freq_i = 1:length(wpms.frequencies)
            fprintf('.');
            testdata = squeeze(data_for_testing(powertype_i,:,freq_i,:));
            %h=0|1 for significant or not
            %pvalues = pvalues
            %~is for confidence interval, not used
            %stats is a structure containing degrees of freedom and t-statistic
            [h,pvalues(powertype_i,cluster_i,freq_i,:),~,stat]=ttest(testdata);%this runs the ttest
            tvalues(powertype_i,cluster_i,freq_i,:)=stat.tstat;%store the tvalue
        end
        t = toc;
        fprintf(' Time: %3.3f sec \n',t);
    end
end
save('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\TTEST_SwitchCost_Output.mat','tvalues','pvalues','-v7.3');
clear 'SWITCH_COST_DATA';

%% View the plots:
% EEG POWER
% CONDITIONS_NOAR  = {'switchto','switchaway','noninf','mixrepeat'};%{'mixrepeat','switchto','switchaway','noninf'};
% montana - 
%CONDITIONS = {'allrepeat','mixrepeat','switchto','switchaway','noninfrepeat','noninfswitch'};

% Load Switch Power Data:
load('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\SWITCH_COST_DATA.mat');
load ('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\TTEST_SwitchCost_Output.mat');
load('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\ERPs_160418.mat');
addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'));
POWER_TYPES = {'Total','Phase','Non Phase'};
wpms.power_types = POWER_TYPES;
CLUSTER_NAMES = {'Frontal','Central','Parietal'};
wpms.cluster_names = CLUSTER_NAMES;
clusters = {{'F1','Fz','F2','FC1','FCz','FC2'} {'C1','Cz', 'C2', 'CP1','CPz','CP2'} {'POz','PO3', 'PO4', 'P1', 'Pz', 'P2'}};
wpms.clusters = clusters;
CONDITIONS_NOAR = wpms.conditions;%CONDITIONS;
labels = wpms.labels;
count=0;
figure();
alpha = 0.005;
%alpha = 0.05;

erp_baseline_start = -50;
erp_baseline_end = 50;

erp_baselineinds = setbaseline(wpms, erp_baseline_start, erp_baseline_end);

blerpdata = bsxfun(@minus,erpdata,squeeze(mean(erpdata(:,:,:,erp_baselineinds),4)));
times = wpms.times;
plot_start = -300;
plot_end = 2000;

plot_times = setbaseline(wpms, plot_start,plot_end);
 
condIWant = find(ismember(wpms.conditions,{'mixrepeat','switchto'}));

condition_average_eegpower = squeeze(mean(SWITCH_COST_DATA,2));
wpms.conditionNames = {'All Repeat', 'Mixed Repeat', 'Switch'};
fontSize = 18;
lineWidth = 2;
lineColors = {[] [0 0 0] [225 76 169]/256}; 

addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'));
close all
figure();set(gcf,'Position',get(0,'screensize'),'color',[1 1 1]);
f = zeros(1,length(wpms.conditions));
for cond_i = condIWant
    for cluster_i = 1:length(wpms.clusters)
        clusterinds = find(ismember(wpms.labels,clusters{cluster_i}));
        subplot(4,length(wpms.clusters),cluster_i+9);
        plotdata = squeeze(mean(blerpdata(cluster_i,cond_i,:,plot_times),3));
        testdata = squeeze(blerpdata(cluster_i,condIWant,:,plot_times));
        f(cond_i) = plot(wpms.times(plot_times),plotdata,'Color',lineColors{cond_i},'linewidth',lineWidth);
        hold on
        [~,pvals] = ttest(squeeze(testdata(1,:,:)),squeeze(testdata(2,:,:)),'Alpha',alpha);
        h = fdr_bky(pvals,alpha);
        h = double(h);
        h(h==0)=NaN;
        h = h.*-19;
        plot(wpms.times(plot_times),h,'Color',lineColors{3},'LineWidth',2);
        xlim([-300 2000])
        ylim([-20 30])
        plot([-300 2000],[0 0],'k','linewidth',lineWidth)
        if cluster_i == 1
            ylabel(['\bf' 'Amplitude (\muV^2)']);
            xlabel(['\bf' 'Time (ms)']);
        end
        plot([0 0],[-20 30],'--k','linewidth',lineWidth)
        plot([1000 1000],[-20 35],'--k','linewidth',lineWidth)
            box off
    set(gca,'YDir','reverse','FontSize',fontSize,'linewidth',lineWidth)
    end
end

legend([f(2) f(3)],wpms.conditionNames{2},wpms.conditionNames{3});
count = 0;
map = 'plasma';
N = 256;
cmap=bids_color(map,N);
powerPlotOrder = [1 3 2];
for power_type_i = 1:length(wpms.power_types)
    for cluster_i = 1:length(wpms.clusters);
        chan_i = find(ismember(labels,wpms.clusters{cluster_i}));
        count = count +1;
        subplot(4,length(wpms.clusters),count);
        data = squeeze(mean(condition_average_eegpower(powerPlotOrder(power_type_i),chan_i,:,plot_times),2));
        contourf(times(plot_times),wpms.frequencies,data,50,'linecolor','none');
        if count==1||count==4||count==7
        ylabel({['\bf' 'Frequency (Hz)']});
        end
        if powerPlotOrder(power_type_i) == 3
            clims = [-1 1];
        else
            clims = [-4 4];
        end
        caxis(clims);
        colormap(cmap);
        if count==3||count==6||count==9
            ax = get(gca);
            origSize = ax.Position;
            ax.Position = [0 0 1 1];
            c = colorbar;
            c.Label.String = (['\bf' 'Power (dB)']);
            c.FontSize = fontSize;
            set(gca,'Position',origSize);
        end
        hold on;
        %[h_t] = fdr_bky(squeeze(pvalues(powerPlotOrder(power_type_i),cluster_i,:,plot_times)),alpha);
        h_t = squeeze(pvalues(powerPlotOrder(power_type_i),cluster_i,:,plot_times))<alpha;
        contour(times(plot_times),wpms.frequencies,h_t,1,'w','linewidth',1.5);
        %contour(times(plot_times),wpms.frequencies,alpha,1,'w','linewidth',1.5);
        set(gca,'FontSize',fontSize,'linewidth',lineWidth);
        plot([0 0],[2 30],'--k','linewidth',lineWidth)
        plot([1000 1000],[2 30],'--k','linewidth',lineWidth)
    end
end
%% View the plots:
% EEG POWER
% CONDITIONS_NOAR  = {'switchto','switchaway','noninf','mixrepeat'};%{'mixrepeat','switchto','switchaway','noninf'};
% montana - 
%CONDITIONS = {'allrepeat','mixrepeat','switchto','switchaway','noninfrepeat','noninfswitch'};

% Load Mix Power Data:
%load('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\MIXED_COST_DATA.mat');
load('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\TTEST_MixedCost_Output.mat');
load('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\ERPs_160418.mat');
addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'));
POWER_TYPES = {'Total','Phase','Non Phase'};
wpms.power_types = POWER_TYPES;
CLUSTER_NAMES = {'Frontal','Central','Parietal'};
wpms.cluster_names = CLUSTER_NAMES;
clusters = {{'F1','Fz','F2','FC1','FCz','FC2'} {'C1','Cz', 'C2', 'CP1','CPz','CP2'} {'POz','PO3', 'PO4', 'P1', 'Pz', 'P2'}};
wpms.clusters = clusters;
CONDITIONS_NOAR = wpms.conditions;%CONDITIONS;
labels = wpms.labels;
count=0;
figure();
alpha = 0.005;
%alpha = 0.05;
erp_baseline_start = -50;
erp_baseline_end = 50;

erp_baselineinds = setbaseline(wpms, erp_baseline_start, erp_baseline_end);

blerpdata = bsxfun(@minus,erpdata,squeeze(mean(erpdata(:,:,:,erp_baselineinds),4)));
times = wpms.times;
plot_start = -300;
plot_end = 2000;

plot_times = setbaseline(wpms, plot_start,plot_end);
 
condIWant = find(ismember(wpms.conditions,{'allrepeat','mixrepeat'}));

condition_average_eegpower = squeeze(mean(MIXED_COST_DATA,2));
wpms.conditionNames = {'All Repeat', 'Mixed Repeat', 'Switch'};
fontSize = 18;
lineWidth = 2;
lineColors = {[255,128,0]/256 [0 0 0]}; 

addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'));
close all
figure();set(gcf,'Position',get(0,'screensize'),'color',[1 1 1]);
f = zeros(1,length(wpms.conditions));
for cond_i = condIWant
    for cluster_i = 1:length(wpms.clusters)
    clusterinds = find(ismember(wpms.labels,clusters{cluster_i}));
    subplot(4,length(wpms.clusters),cluster_i+9);
        plotdata = squeeze(mean(blerpdata(cluster_i,cond_i,:,plot_times),3));
        testdata = squeeze(blerpdata(cluster_i,condIWant,:,plot_times));
    f(cond_i) = plot(wpms.times(plot_times),plotdata,'Color',lineColors{cond_i},'linewidth',lineWidth);
    hold on
    [~,pvals] = ttest(squeeze(testdata(1,:,:)),squeeze(testdata(2,:,:)),'Alpha',alpha);
    h = fdr_bky(pvals,alpha);
    h = double(h);
    h(h==0)=NaN;
    h = h.*-19;
    plot(wpms.times(plot_times),h,'Color',[255,128,0]/256,'LineWidth',2);
    xlim([-300 2000])
    ylim([-20 30])
    plot([-300 2000],[0 0],'k','linewidth',lineWidth)
    if cluster_i == 1
    ylabel(['\bf' 'Amplitude (\muV^2)']);
    xlabel(['\bf' 'Time (ms)']);
    plot([0 0],[-20 30],'--k','linewidth',lineWidth)
    plot([1000 1000],[-20 30],'--k','linewidth',lineWidth)
    end
    box off
    set(gca,'YDir','reverse','FontSize',fontSize,'linewidth',lineWidth)
    end
end
legend([f(1) f(2)],wpms.conditionNames{1},wpms.conditionNames{2});

count = 0;
map = 'plasma';
N = 256;
cmap=bids_color(map,N);
powerPlotOrder = [1 3 2];
for power_type_i = 1:length(wpms.power_types)
    for cluster_i = 1:length(wpms.clusters);
        chan_i = find(ismember(labels,wpms.clusters{cluster_i}));
        count = count +1;
        subplot(4,length(wpms.clusters),count);
        data = squeeze(mean(condition_average_eegpower(powerPlotOrder(power_type_i),chan_i,:,plot_times),2));
        contourf(times(plot_times),wpms.frequencies,data,50,'linecolor','none');
        if count==1||count==4||count==7
        ylabel({['\bf' wpms.power_types{powerPlotOrder(power_type_i)}];'Frequency (Hz)'});
        end
        if powerPlotOrder(power_type_i) == 3
            clims = [-1 1];
        else
            clims = [-4 4];
        end
        caxis(clims);
        colormap(cmap);
        if count==3||count==6||count==9
            ax = get(gca);
            origSize = ax.Position;
            ax.Position = [0 0 1 1];
            c = colorbar;
            c.Label.String = (['\bf' 'Power (dB)']);
            c.FontSize = fontSize;
            set(gca,'Position',origSize);
        end
        hold on;
        %[h_t] = fdr_bky(squeeze(pvalues(powerPlotOrder(power_type_i),cluster_i,:,plot_times)),alpha);
        %contour(times(plot_times),wpms.frequencies,h_t,1,'w','linewidth',1.5);
        contour(times(plot_times),wpms.frequencies,alpha,1,'w','linewidth',1.5);
        plot([0 0],[2 30],'--k','linewidth',lineWidth)
        plot([1000 1000],[2 30],'--k','linewidth',lineWidth)
        set(gca,'FontSize',fontSize,'linewidth',lineWidth)
    end
end
%%
% % Standard Deviation
% % switch cost
% POWER_TYPES = {'TOTAL','PHASE','NON PHASE'};
% wpms.power_types = POWER_TYPES;
% CLUSTER_NAMES = {'Frontal','Central','Parietal'};
% wpms.cluster_names = CLUSTER_NAMES;
% clusters = {{'F1','Fz','F2','FC1','FCz','FC2'} {'C1','Cz', 'C2', 'CP1','CPz','CP2'} {'POz','PO3', 'PO4', 'P1', 'Pz', 'P2'}};
% wpms.clusters = clusters;
% labels = wpms.labels;
% 
% baseline_start = -300;
% baseline_end = 2000;
% plot_times = setbaseline(wpms, baseline_start, baseline_end);
% 
% stddata = zeros(length(wpms.power_types),length(wpms.cluster_names),length(wpms.frequencies),length(plot_times));
% meandata = stddata;
% 
% for power_type_i = 1:length(wpms.power_types)
%     for cluster_i = 1:length(wpms.cluster_names)
%          chan_i = find(ismember(labels,wpms.clusters{cluster_i}));
%         for freq_i = 1:length(wpms.frequencies)
%             for time_i = 1:length(plot_times)
%                 data = squeeze(mean(SWITCH_COST_DATA(power_type_i,:,chan_i,freq_i,plot_times(time_i)),3));
%                 stddata(power_type_i,cluster_i,freq_i,time_i) = std(data);
%                 meandata(power_type_i,cluster_i,freq_i,time_i) = mean(data);
%             end
%         end
%     end
% end
% % plotting
% 
% count = 0;
% 
% for power_type_i = 1:length(wpms.power_types)
% clims = [0 1];
%     for cluster_i = 1:length(wpms.clusters);
%         count = count +1;
%         subplot(length(wpms.power_types),length(wpms.clusters),count)
%         data = squeeze(stddata(power_type_i,cluster_i,:,:));
%         normalized = (data-min(min(data)))/(max(max(data))-min(min(data)));
%         contourf(wpms.times(plot_times),wpms.frequencies,normalized,50,'linecolor','none');
%         if power_type_i == length(wpms.power_types)
%             clims = [0 1];
%         else
%             clims = [0 1];
%         end
%         caxis(clims);
%         colormap 'jet';
%         if power_type_i == 1
%             title({wpms.cluster_names{cluster_i};wpms.power_types{power_type_i}});
%         else
%             title(wpms.power_types{power_type_i});
%         end
%     end
% end
% %% mix cost
% % Standard Deviation
% POWER_TYPES = {'TOTAL','PHASE','NON PHASE'};
% wpms.power_types = POWER_TYPES;
% CLUSTER_NAMES = {'Frontal','Central','Parietal'};
% wpms.cluster_names = CLUSTER_NAMES;
% clusters = {{'F1','Fz','F2','FC1','FCz','FC2'} {'C1','Cz', 'C2', 'CP1','CPz','CP2'} {'POz','PO3', 'PO4', 'P1', 'Pz', 'P2'}};
% wpms.clusters = clusters;
% labels = wpms.labels;
% 
% baseline_start = -300;
% baseline_end = 2000;
% plot_times = setbaseline(wpms, baseline_start, baseline_end);
% 
% stddata = zeros(length(wpms.power_types),length(wpms.cluster_names),length(wpms.frequencies),length(plot_times));
% meandata = stddata;
% 
% for power_type_i = 1:length(wpms.power_types)
%     for cluster_i = 1:length(wpms.cluster_names)
%          chan_i = find(ismember(labels,wpms.clusters{cluster_i}));
%         for freq_i = 1:length(wpms.frequencies)
%             for time_i = 1:length(plot_times)
%                 data = squeeze(mean(MIXED_COST_DATA(power_type_i,:,chan_i,freq_i,plot_times(time_i)),3));
%                 stddata(power_type_i,cluster_i,freq_i,time_i) = std(data);
%                 meandata(power_type_i,cluster_i,freq_i,time_i) = mean(data);
%             end
%         end
%     end
% end
% % plotting
% 
% count = 0;
% 
% for power_type_i = 1:length(wpms.power_types)
% clims = [0 1];
%     for cluster_i = 1:length(wpms.clusters);
%         count = count +1;
%         subplot(length(wpms.power_types),length(wpms.clusters),count)
%         data = squeeze(stddata(power_type_i,cluster_i,:,:));
%         normalized = (data-min(min(data)))/(max(max(data))-min(min(data)));
%         contourf(wpms.times(plot_times),wpms.frequencies,normalized,50,'linecolor','none');
%         if power_type_i == length(wpms.power_types)
%             clims = [0 1];
%         else
%             clims = [0 1];
%         end
%         caxis(clims);
%         colormap 'jet';
%         if power_type_i == 1
%             title({wpms.cluster_names{cluster_i};wpms.power_types{power_type_i}});
%         else
%             title(wpms.power_types{power_type_i});
%         end
%     end
% end
% 
%%