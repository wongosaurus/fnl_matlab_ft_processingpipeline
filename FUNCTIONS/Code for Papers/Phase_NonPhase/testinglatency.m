% testing latency with binning

clear all;close all;clc

% setup parameters
listingdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
datain = 'E:\FNL_EEG_TOOLBOX\ReferenceOutput\SurfaceLapacian';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30;
fbins = 80;
times = (-1:0.001953125000000:3.5)*1000;
conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninf','noninfrepeat','noninfswitch'};
wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);
wpms.allnames=wpms.names;
wpms.names ={'AGE002',	'AGE003',	'AGE004',	'AGE005',	'AGE007',	'AGE008',	'AGE012',	'AGE013',	'AGE014',	'AGE015',	'AGE017',	'AGE018',	'AGE019',	'AGE020',	'AGE021',	'AGE022',	'AGE023',	'AGE024',	'AGE026',	'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE046',	'AGE047',	'AGE050',	'AGE051',	'AGE053',	'AGE058',	'AGE059',	'AGE061',	'AGE062',	'AGE063',	'AGE066',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE072',	'AGE073',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE088',	'AGE089',	'AGE090',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE096',	'AGE097',	'AGE098',	'AGE100',	'AGE102',	'AGE103',	'AGE104',	'AGE107',	'AGE108',	'AGE109',	'AGE111',	'AGE114',	'AGE115',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE122',	'AGE123',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE133',	'AGE134',	'AGE135',	'AGE136',	'AGE138',	'AGE141',	'AGE145',	'AGE146',	'AGE147',	'AGE148',	'AGE149',	'AGE150',	'AGE151',	'AGE152',	'AGE153',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE163',	'AGE164',	'AGE165',	'AGE166',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE172',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE189',	'AGE190',	'AGE195',	'AGE197',	'AGE198',	'AGE199',	'AGE201',	'AGE202',	'AGE203',	'AGE205',	'AGE206',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE232',	'AGE233',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE241',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE273',	'AGE275',	'AGE276',	'AGE278',	'AGE279'};
clearvars -except wpms

%%
% load in data

nameinds = find(ismember(wpms.allnames,wpms.names));

erpdata = zeros(length(nameinds),length(wpms.conditions),length(wpms.labels)+8,length(wpms.times),3,'single');

for part_i = 1:length(nameinds)
    tic
    fprintf('\n%s %i \t','Part:',part_i);
    for cond_i = 1:length(wpms.conditions)
        fprintf('.')
        ERPfilename = [wpms.dirs.datain '\' wpms.allnames{nameinds(part_i)} wpms.conditions{cond_i} '.mat'];
        load(ERPfilename);
        data = refdat.trial;
        totalntrials = length(data);
        tenpercent = floor(totalntrials*0.1);
        trialinds1 = 1:tenpercent;
        trialinds2 = tenpercent*5:tenpercent*6;
        trialinds3 = totalntrials-tenpercent:totalntrials;
        temp1 = zeros(length(trialinds1),size(data{1,1},1),size(data{1,1},2));
        temp2 = zeros(length(trialinds2),size(data{1,1},1),size(data{1,1},2));
        temp3 = zeros(length(trialinds3),size(data{1,1},1),size(data{1,1},2));
        for ti = 1:length(trialinds1)
            temp1(ti,:,:) = data{1,trialinds1(ti)};
            temp2(ti,:,:) = data{1,trialinds2(ti)};
            temp3(ti,:,:) = data{1,trialinds3(ti)};
        end
        erpdata(part_i,cond_i,:,:,1) = squeeze(mean(temp1,1));
        erpdata(part_i,cond_i,:,:,2) = squeeze(mean(temp2,1));
        erpdata(part_i,cond_i,:,:,3) = squeeze(mean(temp3,1));
    end
    toc
end


%%
%Plotting

clusters = {{'F1','Fz','F2','FC1','FCz','FC2'} {'C1','Cz', 'C2', 'CP1','CPz','CP2'} {'POz','PO3', 'PO4', 'P1', 'Pz', 'P2'}};
clusternames = {'Frontal','Central','Parietal'};

% baseline

baseline_start = -50;
baseline_end = 50;

baselineinds = setbaseline(wpms, baseline_start, baseline_end);
blerpdata = erpdata;


for cond_i = 1:3 %only want first 3
    for ti = 1:3
        blerpdata(:,cond_i,:,:,ti) = bsxfun(@minus,squeeze(erpdata(:,cond_i,:,:,ti)),squeeze(mean(erpdata(:,cond_i,:,baselineinds,ti),4)));
    end
end

plot_start = -200;
plot_end = 2000;

plottimeinds = setbaseline(wpms, plot_start,plot_end);

plotcolor = {'k', 'r', 'b'};
count = 0;
figure(); set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);
lineWidth = 2;
%plot

for ti = 1:3%length of time intervals;
    for cluster_i = 1:length(clusternames)
        clusterinds = find(ismember(wpms.labels,clusters{cluster_i}));
        count = count+1;
        subplot(3,length(clusternames),count); %3 is time interval
        plotdata = squeeze(mean(mean(blerpdata(:,:,clusterinds,plottimeinds,ti),1),3));
        %testdata = squeeze(mean(blerpdata(:,:,clusterinds,plottimeinds,ti),3));%is this right? What do we want to mean?
        plot(wpms.times(plottimeinds),squeeze(plotdata(1,:)),plotcolor{1},'LineWidth',2); %AllRepeat
        hold on;
        plot(wpms.times(plottimeinds),squeeze(plotdata(2,:)),plotcolor{2},'LineWidth',2); %MixRepeat
        plot(wpms.times(plottimeinds),squeeze(plotdata(3,:)),plotcolor{3},'LineWidth',2); %Switch
        %h = ttest(squeeze(testdata(:,1,:)),squeeze(testdata(:,3,:)),'Alpha',0.005);
        %h(h==0)=NaN;
        %h = h.*-17;
        %plot(wpms.times(plottimeinds),h,plotcolor{1},'LineWidth',2);
        xlim([plot_start plot_end]);
        ylim([-20 20]);
        set(gca,'YDir','reverse','LineWidth',2,'FontSize',14);box off;%axis square;
%         plot([0 0],[-20 30],'--k','linewidth',lineWidth)
%         plot([200 200],[-20 30],'--k','linewidth',lineWidth)
%         plot([400 400],[-20 30],'--k','linewidth',lineWidth)
%         plot([600 600],[-20 30],'--k','linewidth',lineWidth)
%         plot([800 800],[-20 30],'--k','linewidth',lineWidth)
%         plot([1000 1000],[-20 30],'--k','linewidth',lineWidth)
%         plot([1200 1200],[-20 30],'--k','linewidth',lineWidth)
%         plot([1400 1400],[-20 30],'--k','linewidth',lineWidth)
        title([clusternames{cluster_i} 'Time Interval ' {ti}],'FontSize',14);
    end
end
