% ERP
clear all
clc

% setup parameters
listingdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
datain = 'E:\FNL_EEG_TOOLBOX\ReferenceOutput\SurfaceLapacian\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30;
fbins = 80;
times = (-1:0.001953125000000:3.5)*1000;
%conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninf'};
conditions = {'allrepeat','mixrepeat','switchto'};
wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);

% addpath(genpath([wpms.dirs.CWD,wpms.dirs.FUNCTIONS]));
% addpath(genpath([wpms.dirs.CWD,wpms.dirs.packages '\fieldtrip-20150902']));

wpms.allnames=wpms.names;
wpms.names ={'AGE002',	'AGE003',	'AGE004',	'AGE005',	'AGE007',	'AGE008',	'AGE012',	'AGE013',	'AGE014',	'AGE015',	'AGE017',	'AGE018',	'AGE019',	'AGE020',	'AGE021',	'AGE022',	'AGE023',	'AGE024',	'AGE026',	'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE046',	'AGE047',	'AGE050',	'AGE051',	'AGE053',	'AGE058',	'AGE059',	'AGE061',	'AGE062',	'AGE063',	'AGE066',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE072',	'AGE073',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE088',	'AGE089',	'AGE090',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE096',	'AGE097',	'AGE098',	'AGE100',	'AGE102',	'AGE103',	'AGE104',	'AGE107',	'AGE108',	'AGE109',	'AGE111',	'AGE114',	'AGE115',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE122',	'AGE123',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE133',	'AGE134',	'AGE135',	'AGE136',	'AGE138',	'AGE141',	'AGE145',	'AGE146',	'AGE147',	'AGE148',	'AGE149',	'AGE150',	'AGE151',	'AGE152',	'AGE153',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE163',	'AGE164',	'AGE165',	'AGE166',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE172',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE189',	'AGE190',	'AGE195',	'AGE197',	'AGE198',	'AGE199',	'AGE201',	'AGE202',	'AGE203',	'AGE205',	'AGE206',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE232',	'AGE233',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE241',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE273',	'AGE275',	'AGE276',	'AGE278',	'AGE279'};
clearvars -except wpms
%%

clusternames = {'Frontal','Central','Parietal'};
electrodesofinterest = {{'O2'}};%{{'FCz', 'FC1', 'FC2', 'Fz', 'F1', 'F2'} {'CPz', 'CP1', 'CP2', 'Cz', 'C1', 'C2'} {'POz', 'PO3', 'PO4', 'Pz', 'P1', 'P2'}};
wpms.names = {'AGE002'};
erpdata = zeros(1,1,1,length(wpms.times));

%erpdata = zeros(length(clusternames),length(wpms.conditions),length(wpms.names),length(wpms.times));

% erpdata2.Frontal = zeros(length(electrodesofinterest{1}),length(wpms.conditions),length(wpms.names),length(wpms.times));
% erpdata2.Central = zeros(length(electrodesofinterest{2}),length(wpms.conditions),length(wpms.names),length(wpms.times));
% erpdata2.Parietal = zeros(length(electrodesofinterest{3}),length(wpms.conditions),length(wpms.names),length(wpms.times));

for condition_i = 1%:length(wpms.conditions)
    %fprintf('\n%s', wpms.conditions{condition_i});
    for name_i = 1%:length(wpms.names)
        %fprintf('\n%s\t', wpms.names{name_i});
        tic
        %if mod(name_i,2)==0
            fprintf('.');
        %end
        filename = [wpms.dirs.datain wpms.names{name_i} wpms.conditions{condition_i} '.mat'];
        load(filename);
        data = refdat.trial;
        totalntrials = length(data);
        temp = zeros(totalntrials,size(data{1,1},1),size(data{1,1},2));
        for ti = 1:totalntrials
            temp(ti,:,:) = data{1,ti};
        end
        for cluster_i = 1%:length(clusternames);
            wewant = find(ismember(wpms.labels,electrodesofinterest{cluster_i}));
            erpdata(cluster_i, condition_i, name_i, :) = squeeze(mean(mean(temp(:,wewant,:),1),2));
            %for electrode_i = 1:length(wewant)
            %erpdata2.(clusternames{cluster_i})(electrode_i, condition_i, name_i, :) = squeeze(mean(temp(:,wewant(electrode_i),:),1));
            %end
        end
        toc
    end
end

%close all;
figure(3);
plot(squeeze(erpdata(1, 1, 1, :)));

EEG = [];
DATA = squeeze(erpdata(1, 1, 1, :));
%DATA = mean(EEG.data,3);
frequencies = logspace(log10(2),log10(30),80);
s = logspace(log10(3),log10(10),length(frequencies))./(2*pi*frequencies); 
EEG.srate = 512; 
EEG.pnts = 2305;
EEG.trials = 1;
time    = -EEG.pnts/EEG.srate/2:1/EEG.srate:EEG.pnts/EEG.srate/2-1/EEG.srate;
n_wavelet     = length(time);%EEG.pnts;
n_data        = EEG.pnts*EEG.trials;
n_convolution = n_wavelet+n_data-1;
n_conv_pow2   = pow2(nextpow2(n_convolution));
eegfft = fft(reshape(DATA,1,[]),n_conv_pow2);

npnts = EEG.pnts;
ntrials = EEG.trials;

for fi=1:length(frequencies)
    wavelet = exp(2*1i*pi*frequencies(fi).*time) .* exp(-time.^2./(2*(s(fi)^2)))/frequencies(fi);
            
    % convolution
    eegconv = ifft(fft(wavelet,n_conv_pow2).*eegfft);
    eegconv = eegconv(1:n_convolution);
    eegconv = reshape(eegconv(floor((npnts-1)/2):end-1-ceil((npnts-1)/2)),npnts,ntrials);

    % extract EEG
    eegpower(fi,:) = abs(mean(eegconv,2)).^2; 
end

eegpower_db = 10*log10(eegpower);
%baseline:
eegpower_bl = bsxfun(@minus,eegpower_db,mean(eegpower_db(:,359:461),2));

figure(4);
contourf(1:size(eegpower,2),frequencies,eegpower_bl,50,'linecolor','none');
clims = [-20 20];
caxis(clims);
title('ERP Time Frequency');
%hold on;
saveas(gcf,'ERP_TF.jpg');


load('Z:\ITPC\ReferenceOutput\SurfaceLapacian\AGE002\allrepeat\64.mat');

npnts = EEG.pnts;
ntrials = EEG.trials;

n_data        = EEG.pnts*EEG.trials;
n_convolution = n_wavelet+n_data-1;
n_conv_pow2   = pow2(nextpow2(n_convolution));

eegfft = fft(reshape(EEG.data,1,[]),n_conv_pow2);

for fi=1:length(frequencies)
    wavelet = exp(2*1i*pi*frequencies(fi).*time) .* exp(-time.^2./(2*(s(fi)^2)))/frequencies(fi);
            
    % convolution
    eegconv = ifft(fft(wavelet,n_conv_pow2).*eegfft);
    eegconv = eegconv(1:n_convolution);
    eegconv = reshape(eegconv(floor((npnts-1)/2):end-1-ceil((npnts-1)/2)),npnts,ntrials);

    % extract ITPC
    eegpower_b(fi,:) = abs(mean(eegconv,2)).^2; 
end

eegpower_b_db = 10*log10(eegpower_b);
%baseline:
eegpower_b_bl = bsxfun(@minus,eegpower_b_db,mean(eegpower_b_db(:,359:461),2));

figure(5)
contourf(1:size(eegpower_b,2),frequencies,eegpower_b_bl,50,'linecolor','none');
clims = [-20 20];
caxis(clims);
title('EEG mean Time Frequency');
%hold on;
saveas(gcf,'EEG_Mean_TF.jpg');


A = sum(ifft(eegpower_b_bl).*conj(ifft(eegpower_b_bl)));

figure(3)
plot(A)
% savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\ERPs_160418.mat'];
% save(savename,'erpdata','erpdata2');

%%

% baseline

baseline_start = -50;
baseline_end = 50;

baselineinds = setbaseline(wpms, baseline_start, baseline_end);

blerpdata = bsxfun(@minus,erpdata,squeeze(mean(erpdata(:,:,:,baselineinds),4)));
blerpdata2.Frontal = bsxfun(@minus,erpdata2.Frontal(1:6,:,:,:),squeeze(mean(erpdata2.Frontal(1:6,:,:,baselineinds),4)));
blerpdata2.Central = bsxfun(@minus,erpdata2.Central(1:6,:,:,:),squeeze(mean(erpdata2.Central(1:6,:,:,baselineinds),4)));
blerpdata2.Parietal = bsxfun(@minus,erpdata2.Parietal(1:6,:,:,:),squeeze(mean(erpdata2.Parietal(1:6,:,:,baselineinds),4)));

plot_start = -300;
plot_end = 2000;
 
plottimeinds = setbaseline(wpms, plot_start,plot_end);

%plot

figure(); set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);

for cluster_i = 1:length(clusternames);
    for condition_i = 1:length(wpms.conditions)
        subplot(1,3,cluster_i);
        plotcolor = {'k', 'r', 'b'};
        plot(wpms.times(plottimeinds),squeeze(mean(blerpdata(cluster_i,condition_i,:,plottimeinds),3)),plotcolor{condition_i},'LineWidth',2);
        hold on;
        ylim([-20 30]);
        xlim([-300 2000]);
        plot(wpms.times(plottimeinds),zeros(1,length(plottimeinds)),'k','LineWidth',2);
        set(gca,'YDir','reverse','LineWidth',2,'FontSize',18);axis square;box off;
%         for electrode_i = 1:6
%             plot(wpms.times(plottimeinds),squeeze(mean(blerpdata2.(clusternames{cluster_i})(electrode_i,condition_i,:,plottimeinds),3)),'LineWidth',2);
%         end
    end
    title(clusternames{cluster_i},'FontSize',18);
    %     ax = gca;
    %     ax.YAxisLocation = 'origin';
    %     ax.XAxisLocation = 'origin';
end

%for cluster_i = 1:length(clusternames)
%     statsdata = squeeze(mean(blerpdata(cluster_i,:,:,plottimeinds),3));
%     savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\ERP_' clusternames{cluster_i} 'grandaverage_cue_newbl.txt'];
%     dlmwrite(savename,statsdata,'delimiter','\t');
% end
