%% 1. Determine study specific working parameters.
clc; clear all; close all;
disp('1. Determine study specific working parameters.');
% set up data directories
listingdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
workingvolume = 'E:\'; %this would vary per operating system.
wpms          = [];       %working parameters
wpms.dirs     = struct('CWD',[workingvolume 'FNL_EEG_TOOLBOX' filesep]);
wpms.dirs     = struct('CWD',[workingvolume 'FNL_EEG_TOOLBOX' filesep], ...
    'PACKAGES',[wpms.dirs.CWD 'PACKAGES' filesep], ...
    'FUNCTIONS',[wpms.dirs.CWD 'FUNCTIONS' filesep], ...
    'REPAIRED_DATA',[wpms.dirs.CWD 'REPAIRED_DATA' filesep],...
    'LOGS',[wpms.dirs.CWD 'FUNCTIONS\Code for Papers\Phase_NonPhase\LOGS' filesep]);
mkdir(wpms.dirs.LOGS);    
addpath(genpath(wpms.dirs.FUNCTIONS));%functions
% set up study conditions

wpms.conditions = {'allrepeat','mixrepeat','noninfrepeat','switchto','switchaway','noninfswitch','noninf'};

listings = dir(listingdir);
wpms.names = {length(listings)};
for file_i =1:length(listings)
    wpms.names{file_i} = listings(file_i).name(1:6);
end

clear workingvolume listing temp_names list_i participant_index space_index file_i good_inds NAMES listings %tidy as we travel

%%
disp('2. Bad channel counting')
badchannel_count = zeros(1,length(wpms.names));
for name_i=1:length(wpms.names)
    fprintf('\n%s\t',wpms.names{name_i});
    filename = [wpms.dirs.REPAIRED_DATA wpms.names{name_i} '_RepairedData_allrepeat.mat'];
    load(filename);
    badchannel_count(1,name_i)=length(ardata.cfg.badchannel);
    fprintf('%i\t%s',length(ardata.cfg.badchannel),'bad channels');
end%name_i loop
savename = [wpms.dirs.LOGS 'badchannel_count'];
save(savename,'badchannel_count');
save(savename,'badchannel_count','-ascii','-tabs');
fprintf('\n');
%%
disp('3. Counting number of ICA components removed AND determining valid trial counts')
component_count = zeros(1,length(wpms.names));
valid_trials = zeros(1,length(wpms.names));
for name_i=1:length(wpms.names)
    tic;
    try
        fprintf('\n%s\t',wpms.names{name_i});
        filename = [wpms.dirs.REPAIRED_DATA wpms.names{name_i} '_RepairedData_allrepeat.mat'];
        load(filename);
        component_count(1,name_i)=length(ardata.cfg.previous.previous.previous.previous.component);
        fprintf('%i\t%s\t\t',length(ardata.cfg.previous.previous.previous.previous.component),'components channels');
        valid_trials(1,name_i)=length(ardata.cfg.previous.previous.previous.previous.previous.previous.trl);
        fprintf('%i\t%s',length(ardata.cfg.previous.previous.previous.previous.previous.previous.trl),'valid trials');
    catch exception
        disp(['failed for ' wpms.names{name_i}]);
    end%try/catch loop
    t=toc;
    estimated_time = time_remaining(name_i,length(wpms.names),t);
    fprintf('\t%3.2f %s',estimated_time,'s remaining')
end%name_i loop
%ica components
savename = [wpms.dirs.LOGS 'icacomponent_count'];
save(savename,'component_count');
save(savename,'component_count','-ascii','-tabs');
%valid trials
savename = [wpms.dirs.LOGS 'validtrial_count'];
save(savename,'valid_trials');
save(savename,'valid_trials','-ascii','-tabs');
%% 
disp('4. Determining each trial''s count and RTs');
trial_count = zeros(length(wpms.conditions),length(wpms.names));
rts_mean = zeros(length(wpms.conditions),length(wpms.names));
rolling_average = zeros(1,length(wpms.names));
for name_i=1:length(wpms.names)
    fprintf('\n%s\t',wpms.names{name_i});
    ts = zeros(1,length(wpms.conditions));
    for cond_i = 1:length(wpms.conditions)
        tic;
        try
            fprintf('.');
            filename = [wpms.dirs.REPAIRED_DATA wpms.names{name_i} '_RepairedData_' wpms.conditions{cond_i} '.mat'];
            if exist(filename,'file')==2;
                A=load(filename);
                datname = fieldnames(A);
                trial_count(cond_i,name_i) = length(A.(datname{1}).trial);
                %meanRTs:
                originalTRL = A.(datname{1}).cfg.previous.previous.previous.previous.previous.previous.trl;
                indices = find(ismember(originalTRL(:,1),A.(datname{1}).sampleinfo(:,1)));
                rts_mean(cond_i,name_i) = mean(originalTRL(indices,5));
%             elseif exist(filename2,'file')==2;
%                 A=load(filename2);
%                 datname = fieldnames(A);
%                 trial_count(cond_i,name_i) = length(A.(datname{1}).trial);
%             elseif exist(filename3,'file')==2;
%                 A=load(filename3);
%                 datname = fieldnames(A);
%                 trial_count(cond_i,name_i) = length(A.(datname{1}).trial);
            else
                trial_count(cond_i,name_i) = NaN;
            end
        catch exception
            disp(['failed for ' wpms.names{name_i}]);
        end
        ts(1,cond_i) = toc;
    end%try/catch loop
    t=mean(ts);
    estimated_time = time_remaining(name_i,length(wpms.names),t);
    rolling_average(1,name_i) = estimated_time;
    fprintf('\t%3.2f %s',mean(rolling_average(1,1:name_i)),'s remaining')
end%name_i loop
savename = [wpms.dirs.LOGS 'trial_count'];
save(savename,'trial_count');
save(savename,'trial_count','-ascii','-tabs');
savename = [wpms.dirs.LOGS 'rts_mean'];
save(savename,'rts_mean');
save(savename,'rts_mean','-ascii','-tabs');

%WRite to a CSV:
fid = fopen([savename ,'.csv'],'w');
%Header:
fprintf(fid,'SUBJECT,');
for cond_i = 1:length(wpms.conditions)
    fprintf(fid,'%s,',wpms.conditions{cond_i});
end
fprintf(fid,'\n');

%main data:
for name_i = 1:length(wpms.names)
    fprintf(fid,'%s,',wpms.names{name_i});
    for cond_i = 1:length(wpms.conditions)
        fprintf(fid,'%4.6f,',rts_mean(cond_i,name_i));
    end
    fprintf(fid,'\n');
end
fclose(fid);

bar(mean(rts_mean,2));
set(gca,'XTickLabel',wpms.conditions);

%% 
disp('5. finding bad cases');
bad_cases = find(isnan(trial_count(1,:)));
for bad_i = 1:length(bad_cases)
    fprintf('\n%s %s',wpms.names{bad_cases(bad_i)},'did not have a trial file found');
end
fprintf('\n\n');
% these cases are probably located on another directory
% thus, we will try and find those there.
wpms.dirs.altdir = ['E:' filesep 'FNL_EEG_TOOLBOX' filesep 'PREPROC_OUTPUT' filesep];
rolling_average = zeros(1,length(bad_cases));
for name_i=1:length(bad_cases)
    fprintf('\n%s\t',wpms.names{bad_cases(name_i)});
    ts = zeros(1,length(wpms.conditions));
    for cond_i = 1:length(wpms.conditions)
        tic;
        try
            fprintf('.');
            filename = [wpms.dirs.altdir wpms.names{bad_cases(name_i)} '_ERP_' wpms.conditions{cond_i} '.mat'];
            filename2 = [wpms.dirs.altdir wpms.names{bad_cases(name_i)} '_CSD_' wpms.conditions{cond_i} '.mat'];
            filename3 = [wpms.dirs.altdir wpms.names{bad_cases(name_i)} '_' wpms.conditions{cond_i} '.mat'];
            if exist(filename,'file')==2;
                A=load(filename);
                datname = fieldnames(A);
                trial_count(cond_i,bad_cases(name_i)) = length(A.(datname{1}).trial);
            elseif exist(filename2,'file')==2;
                A=load(filename2);
                datname = fieldnames(A);
                trial_count(cond_i,bad_cases(name_i)) = length(A.(datname{1}).trial);
            elseif exist(filename3,'file')==2;
                A=load(filename3);
                datname = fieldnames(A);
                trial_count(cond_i,bad_cases(name_i)) = length(A.(datname{1}).trial);
            else
                trial_count(cond_i,bad_cases(name_i)) = NaN;
            end
        catch exception
            disp(['failed for ' wpms.names{bad_cases(name_i)}]);
        end
        ts(1,cond_i) = toc;
    end%try/catch loop
    t=mean(ts);
    estimated_time = time_remaining(name_i,length(bad_cases),t);
    rolling_average(1,name_i) = estimated_time;
    fprintf('\t%3.2f %s',mean(rolling_average(1,1:name_i)),'s remaining')
end%name_i loop

disp('5b. determining if any remaining bad cases');
bad_cases = find(isnan(trial_count(1,:)));
if isempty(bad_cases)==1
    fprintf('No bad cases remaining')
else
    for bad_i = 1:length(bad_cases)
        fprintf('\n%s %s',wpms.names{bad_cases(bad_i)},'did not have a trial file found');
    end
end
fprintf('\n\n');
savename = [wpms.dirs.LOGS 'trial_count'];
save(savename,'trial_count');
save(savename,'trial_count','-ascii','-tabs');

%% Save Names used in this calculation:
%Convert to  Character format:
name_temp = {};
for name_i = 1:length(wpms.names)
    name_temp{name_i} = wpms.names{name_i}(1:6);
end
names_used_count = name_temp;
savename = [wpms.dirs.LOGS 'names_used'];
save(savename,'names_used_count');