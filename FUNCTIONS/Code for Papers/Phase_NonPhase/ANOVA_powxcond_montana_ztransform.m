clear all;close all;clc

% setup parameters
listingdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
datain = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\NoBL\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30;
fbins = 80;
times = (-1:0.001953125000000:3.5)*1000;
conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninf','noninfrepeat','noninfswitch'};
wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);
wpms.allnames=wpms.names;
wpms.names ={'AGE002',	'AGE003',	'AGE004',	'AGE005',	'AGE007',	'AGE008',	'AGE012',	'AGE013',	'AGE014',	'AGE015',	'AGE017',	'AGE018',	'AGE019',	'AGE020',	'AGE021',	'AGE022',	'AGE023',	'AGE024',	'AGE026',	'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE046',	'AGE047',	'AGE050',	'AGE051',	'AGE053',	'AGE058',	'AGE059',	'AGE061',	'AGE062',	'AGE063',	'AGE066',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE072',	'AGE073',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE088',	'AGE089',	'AGE090',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE096',	'AGE097',	'AGE098',	'AGE100',	'AGE102',	'AGE103',	'AGE104',	'AGE107',	'AGE108',	'AGE109',	'AGE111',	'AGE114',	'AGE115',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE122',	'AGE123',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE133',	'AGE134',	'AGE135',	'AGE136',	'AGE138',	'AGE141',	'AGE145',	'AGE146',	'AGE147',	'AGE148',	'AGE149',	'AGE150',	'AGE151',	'AGE152',	'AGE153',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE163',	'AGE164',	'AGE165',	'AGE166',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE172',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE189',	'AGE190',	'AGE195',	'AGE197',	'AGE198',	'AGE199',	'AGE201',	'AGE202',	'AGE203',	'AGE205',	'AGE206',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE232',	'AGE233',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE241',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE273',	'AGE275',	'AGE276',	'AGE278',	'AGE279'};
clearvars -except wpms
%% DO THIS FOR CUE AND THEN TARGET
% setup baseline

%cue
baseline_start = -300;
baseline_end = -100;
wpms.baselineinds.cue = setbaseline(wpms, baseline_start, baseline_end);

baseline_start = 700;
baseline_end = 900;
wpms.baselineinds.target = setbaseline(wpms, baseline_start, baseline_end);

clusters = {{'FCz', 'FC1', 'FC2', 'Cz', 'C1', 'C2'} {'CPz', 'CP1', 'CP2', 'Pz', 'P1', 'P2'}};
analysistype = 'allpower';
epoch = {'cue' 'target'};
epochconditions = {[1 2 3 4 5];[1 2 3 4 6 7]};
epochconditionsttest = {[2 3 4 5];[2 3 4 6 7]};
clusternames = {'frontal', 'parietal'};

%fstat structures
fpvalue.cue.power_me = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
fpvalue.cue.cond_me = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
fpvalue.cue.powxcon = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));

fstat.cue.power_me = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
fstat.cue.cond_me = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
fstat.cue.powxcon = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));

fpvalue.target.power_me = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
fpvalue.target.cond_me = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
fpvalue.target.powxcon = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));

fstat.target.power_me = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
fstat.target.cond_me = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));
fstat.target.powxcon = zeros(length(clusters),length(wpms.frequencies),length(wpms.times));

clear baseline*;

%%
goodinds = find(ismember(wpms.allnames,wpms.names));
for freq_i = 1:length(wpms.frequencies)
    fprintf('\n%s\t%s','Frequency:',num2str(freq_i));
    tic;
    %average across clusters storage variable
    ERSPfilename = [wpms.dirs.datain 'ERSP_' num2str(freq_i) '.mat'];
    load(ERSPfilename);
    INDUCEDPOWERfilename = [wpms.dirs.datain 'INDUCEDPOWER_' num2str(freq_i) '.mat'];
    load(INDUCEDPOWERfilename);
    data = ERSP_F_t(1,:,:,goodinds,:);
    data2 = INDUCEDPOWER_F_t(1,:,:,goodinds,:);
    clear ERSP_F_t INDUCEDPOWER_F_t
    for cluster_i = 1:length(clusters)
        electrodesofinterest = find(ismember(wpms.labels,clusters{cluster_i}));
        for epoch_i = 1:length(epoch);
            [totalbl, nonphaselockedbl, phaselockedbl] = baselinecorrect(wpms, data, data2, electrodesofinterest, analysistype,0,epoch{epoch_i});
            
            % run t-tests for total, phaselocked, nonphaselocked
            totaldata = permute(totalbl,[2,1,3]);
            nonphasedata = permute(nonphaselockedbl,[2,1,3]);
            phasedata = permute(phaselockedbl,[2,1,3]);
            
            %z score it

            z_phase = phasedata;
            z_nonphase = nonphasedata;
            
            
            for time_i = 1:size(z_phase,2)
                for cond_i = 1:size(z_phase,3)
                    %phase
                    avTF_phase = mean(squeeze(phasedata(:,time_i,cond_i)));
                    sdTF_phase  = std(squeeze(phasedata(:,time_i,cond_i)));
                    z_phase(:,time_i,cond_i) = squeeze((phasedata(:,time_i,cond_i)-avTF_phase)./sdTF_phase);
                    clear avTF_phase sdTF_phase
                    %nonphase
                    avTF_nonphase = mean(squeeze(nonphasedata(:,time_i,cond_i)));
                    sdTF_nonphase  = std(squeeze(nonphasedata(:,time_i,cond_i)));
                    z_nonphase(:,time_i,cond_i) = squeeze((nonphasedata(:,time_i,cond_i)-avTF_nonphase)./sdTF_nonphase);
                    clear avTF_nonphase sdTF_nonphase
                end
            end
             phasedata=z_phase;
             nonphasedata=z_nonphase;

            
            % run anova
            nsubs = length(wpms.names);
            N=nsubs;
            % Make Goods
            %f-stats
            POWER_ME_f  = zeros(1,length(wpms.times));
            COND_ME_f  = POWER_ME_f;
            POWxCON_f = POWER_ME_f;
            %p-stats
            POWER_ME_p  = zeros(1,length(wpms.times));
            COND_ME_p  = POWER_ME_p;
            POWxCON_p = POWER_ME_p;
            %A = POWER (PHASE,NONPHASE)
            %B = CONDITION(ALLREPEAT,MIXREPEAT,SWITCHTO,SWITCHAWAY,NONINF)
            if epoch_i==1
                conditionsWeWant=find(~ismember(wpms.conditions,{'noninfrepeat','noninfswitch'}));
            elseif epoch_i ==2
                conditionsWeWant=find(~ismember(wpms.conditions,{'noninf'}));
            end
            tic;
            fprintf('\n%s\t%i','Frequency:',freq_i);
            for time_i = 1:length(wpms.times)
                if mod(time_i,50)==0
                    fprintf('.');
                end
                %extract each condition appropriately
                %first phasedata
                if epoch_i ==1
                    A1=squeeze(phasedata(:,time_i,conditionsWeWant(1)));
                    A2=squeeze(phasedata(:,time_i,conditionsWeWant(2)));
                    A3=squeeze(phasedata(:,time_i,conditionsWeWant(3)));
                    A4=squeeze(phasedata(:,time_i,conditionsWeWant(4)));
                    A5=squeeze(phasedata(:,time_i,conditionsWeWant(5)));
                    %second nonphase
                    B1=squeeze(nonphasedata(:,time_i,conditionsWeWant(1)));
                    B2=squeeze(nonphasedata(:,time_i,conditionsWeWant(2)));
                    B3=squeeze(nonphasedata(:,time_i,conditionsWeWant(3)));
                    B4=squeeze(nonphasedata(:,time_i,conditionsWeWant(4)));
                    B5=squeeze(nonphasedata(:,time_i,conditionsWeWant(5)));
                    % Set up goods
                    Y=[A1; B1; A2; B2; A3; B3; A4; B4; A5; B5]; % dependent variable (numeric) in a column vector
                    S=repmat([1:N]', 10,1); % grouping variable for SUBJECT
                    FACTNAMES={'POW','CON'};  % FACTNAMES  a cell array w/ two char arrays: {'factor1', 'factor2'}
                    
                    % Weights
                    %            P,C (power,conditon) levels -start at 0
                    A1w= repmat([0,0],N,1);
                    B1w= repmat([1,0],N,1);
                    A2w= repmat([0,1],N,1);
                    B2w= repmat([1,1],N,1);
                    A3w= repmat([0,2],N,1);
                    B3w= repmat([1,2],N,1);
                    A4w= repmat([0,3],N,1);
                    B4w= repmat([1,3],N,1);
                    A5w= repmat([0,4],N,1);
                    B5w= repmat([1,4],N,1);
                    codingvars=[A1w;B1w;A2w;B2w;A3w;B3w;A4w;B4w;A5w;B5w];
                elseif epoch_i ==2
                    A1=squeeze(phasedata(:,time_i,conditionsWeWant(1)));
                    A2=squeeze(phasedata(:,time_i,conditionsWeWant(2)));
                    A3=squeeze(phasedata(:,time_i,conditionsWeWant(3)));
                    A4=squeeze(phasedata(:,time_i,conditionsWeWant(4)));
                    A5=squeeze(phasedata(:,time_i,conditionsWeWant(5)));
                    A6=squeeze(phasedata(:,time_i,conditionsWeWant(6)));
                    %second nonphase
                    B1=squeeze(nonphasedata(:,time_i,conditionsWeWant(1)));
                    B2=squeeze(nonphasedata(:,time_i,conditionsWeWant(2)));
                    B3=squeeze(nonphasedata(:,time_i,conditionsWeWant(3)));
                    B4=squeeze(nonphasedata(:,time_i,conditionsWeWant(4)));
                    B5=squeeze(nonphasedata(:,time_i,conditionsWeWant(5)));
                    B6=squeeze(nonphasedata(:,time_i,conditionsWeWant(6)));
                    % Set up goods
                    Y=[A1; B1; A2; B2; A3; B3; A4; B4; A5; B5; A6; B6]; % dependent variable (numeric) in a column vector
                    S=repmat([1:N]', 12,1); % grouping variable for SUBJECT
                    FACTNAMES={'POW','CON'};  % FACTNAMES  a cell array w/ two char arrays: {'factor1', 'factor2'}
                    
                    % Weights
                    %            P,C (power,conditon) levels -start at 0
                    A1w= repmat([0,0],N,1);
                    B1w= repmat([1,0],N,1);
                    A2w= repmat([0,1],N,1);
                    B2w= repmat([1,1],N,1);
                    A3w= repmat([0,2],N,1);
                    B3w= repmat([1,2],N,1);
                    A4w= repmat([0,3],N,1);
                    B4w= repmat([1,3],N,1);
                    A5w= repmat([0,4],N,1);
                    B5w= repmat([1,4],N,1);
                    A6w= repmat([0,5],N,1);
                    B6w= repmat([1,5],N,1);
                    codingvars=[A1w;B1w;A2w;B2w;A3w;B3w;A4w;B4w;A5w;B5w;A6w;B6w];
                    %    F1         grouping variable for factor #1
                    %    F2         grouping variable for factor #2
                end
                % Run ANOVA
                stats=rm_anova2(Y,S,codingvars(:,1),codingvars(:,2),FACTNAMES);
                
                % Get f vals
                POWER_ME_f(1,time_i)=cell2mat(stats(2,5));
                POWER_ME_p(1,time_i)=cell2mat(stats(2,6));
                COND_ME_f(1,time_i)=cell2mat(stats(3,5));
                %get p vals
                COND_ME_p(1,time_i)=cell2mat(stats(3,6));
                POWxCON_f(1,time_i)=cell2mat(stats(4,5));
                POWxCON_p(1,time_i)=cell2mat(stats(4,6));
            end%time_i loop
            t=toc;fprintf('\t%2.2f %',t,'secs');
            if epoch_i==1
                fpvalue.cue.power_me(cluster_i,freq_i,:)=squeeze(POWER_ME_p);
                fpvalue.cue.cond_me(cluster_i,freq_i,:)=squeeze(COND_ME_p);
                fpvalue.cue.powxcon(cluster_i,freq_i,:)=squeeze(POWxCON_p);
                
                fstat.cue.power_me(cluster_i,freq_i,:)=squeeze(POWER_ME_f);
                fstat.cue.cond_me(cluster_i,freq_i,:)=squeeze(COND_ME_f);
                fstat.cue.powxcon(cluster_i,freq_i,:)=squeeze(POWxCON_f);
            elseif epoch_i==2
                fpvalue.target.power_me(cluster_i,freq_i,:)=squeeze(POWER_ME_p);
                fpvalue.target.cond_me(cluster_i,freq_i,:)=squeeze(COND_ME_p);
                fpvalue.target.powxcon(cluster_i,freq_i,:)=squeeze(POWxCON_p);
                
                fstat.target.power_me(cluster_i,freq_i,:)=squeeze(POWER_ME_f);
                fstat.target.cond_me(cluster_i,freq_i,:)=squeeze(COND_ME_f);
                fstat.target.powxcon(cluster_i,freq_i,:)=squeeze(POWxCON_f);
            end
        end
    end
end
savename = 'E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\anovaresults_zdata.mat';
save(savename,'fpvalue','fstat');
%%
load ('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\anovaresults_zdata.mat');
n = 50;
alpha = 0.05;
clims = [-10 10];
addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'));

fstatname = {'power_me' 'cond_me' 'powxcon'};

%Peak Finding inside TF Space: Rectangle(WidthxHieght) between the
%frequency range of interest: freqindmin to freqindmax.
width = 60;  %Time Bins
height = 6; %Frequency Bins:
freqindmin = 22; %4HZ: Max from 4Hz to 30Hz;
freqindmax = 68;%20Hz

plottime_start_cue = -300;
plottime_end_cue = 1200;
plottime_inds.cue = setbaseline(wpms, plottime_start_cue, plottime_end_cue);

plottime_start_target = 700;
plottime_end_target = 2000;
plottime_inds.target = setbaseline(wpms, plottime_start_target, plottime_end_target);

for epoch_i = 1:length(epoch)
    figure();
    for fstat_i = 1:length(fstatname)
        
        for cluster_i = 1:length(clusters)
            fprintf('%s: ',clusternames{cluster_i});
            if cluster_i == 1
                subplot(2,3,fstat_i);
            elseif cluster_i == 2
                subplot(2,3,fstat_i+length(fstatname));
            end
            contourf(wpms.times(plottime_inds.(epoch{epoch_i})),wpms.frequencies,squeeze(fstat.(epoch{epoch_i}).(fstatname{fstat_i})(cluster_i,:,plottime_inds.(epoch{epoch_i}))),n,'linecolor','none');
            hold on;
            %anova
            mask = fdr_bky(squeeze(fpvalue.(epoch{epoch_i}).(fstatname{fstat_i})(cluster_i,:,plottime_inds.(epoch{epoch_i}))),alpha);
            h_cf = fdr_bky(squeeze(fpvalue.(epoch{epoch_i}).(fstatname{fstat_i})(cluster_i,:,:)),alpha);
            dataf = squeeze(fstat.(epoch{epoch_i}).(fstatname{fstat_i})(cluster_i,:,plottime_inds.(epoch{epoch_i})));
            contour(wpms.times(plottime_inds.(epoch{epoch_i})),wpms.frequencies,mask,1,'k');
            caxis(clims)
            colormap jet;
            title(fstatname{fstat_i});
            axis square
%             fprintf('%s: Getting Peaks\n',fstatname{fstat_i});
%             CC = getpeaks(width, height, freqindmin, freqindmax, h_cf, dataf, plottime_inds.(epoch{epoch_i}), wpms);
%             fprintf('\n');
%             savename=['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\' fstatname{fstat_i} '_' epoch{epoch_i} '_' clusternames{cluster_i} '_peaks_zdata.mat'];
%             save(savename,'CC');
        end
    end
end

%% Extracting windows for Plotting from Clusters:
% load('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\frontalparietalpowerstats_tonly.mat');
% load ('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\frontalparietalpowerstats.mat','fpvalue','fstat');

%cue
baseline_start_cue = -300;
baseline_end_cue = -100;
wpms.baselineinds.epoch.cue = setbaseline(wpms, baseline_start_cue, baseline_end_cue);

%target
baseline_start_target = 700;
baseline_end_target = 900;
wpms.baselineinds.epoch.target = setbaseline(wpms, baseline_start_target, baseline_end_target);

clusters = {{'FCz', 'FC1', 'FC2', 'Cz', 'C1', 'C2'} {'CPz', 'CP1', 'CP2', 'Pz', 'P1', 'P2'}};
analysistype = 'allpower';

epochnames = {'cue' 'target'};
wpms.epochconditions.cue = {[1 2 3 4 5]};
wpms.epochconditions.target = {[1 2 3 4 6 7]};
% epochconditionsttest = {[2 3 4 5];[2 3 4 6 7]};
clusternames = {'frontal', 'parietal'};

alpha = 0.001; %Original = 0.001
%for cluster analysis only
clusteralpha_fvalues = 1e-25;
clusteralpha_tvalues = 1e-10; %Original: 00000001
width = 60;  %Time Bins
height = 6; %Frequency Bins:
freqindmin = 22; %4HZ: Max from 4Hz to 30Hz;
freqindmax = 68;%20Hz

plottime_start_cue = -300;
plottime_end_cue = 1200;
cuetimes = setbaseline(wpms, plottime_start_cue, plottime_end_cue);

plottime_start_target = 700;
plottime_end_target = 2000;
targettimes = setbaseline(wpms, plottime_start_target, plottime_end_target);

plottimes = {cuetimes;targettimes};
electrodesofinterest = 1:64;
conditionbaseline = 0;

powernames = {'total' 'nonphase' 'phase'};
addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'));
partinds = goodinds;%find(ismember(wpms.allnames,wpms.names));
%looping through powernames, epoch & clusters
for power_i = 1:length(powernames)
    for fstat_i = 1:length(fstatname);
        for epoch_i = 1:length(epoch);
            for cluster_i = 1:length(clusternames);
                filename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\' fstatname{fstat_i} '_' epoch{epoch_i} '_' clusternames{cluster_i} '_peaks.mat'];
                load (filename);
                loadname = {[wpms.dirs.datain 'ERSP_'];[wpms.dirs.datain 'INDUCEDPOWER_']};
                for pixel_i = 1:length(CC.PixelIdxList)
                    fprintf('\tWorking on Pixel Index: %i\n',pixel_i );
                    Location = CC.PixelIdxLocation{pixel_i};
                    if isempty(Location)
                        continue;
                    end
                    switch power_i
                        case 1
                            %                             [totalpeaks, ~, ~] = peakwindow(Location, loadname, width, height, wpms, electrodesofinterest, analysistype, epoch{epoch_i}, conditionbaseline, plottimes{epoch_i});
                            %                             savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' powernames{power_i} '_' epoch{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) '_windowedforplotting.mat'];
                            %                             save(savename, 'totalpeaks');
                        case 2
                            [~, nonphasepeaks, ~] = peakwindow_zdata(Location, loadname, width, height, wpms, electrodesofinterest, analysistype, epoch{epoch_i}, conditionbaseline, plottimes{epoch_i},partinds);
                            savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' fstatname{fstat_i} '_' powernames{power_i} '_' epoch{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) '_windowedforplotting_zdata.mat'];
                            save(savename, 'nonphasepeaks');
                            saveas(gcf,savename,'tiff');
                        case 3
                            [~, ~, phasepeaks] = peakwindow_zdata(Location, loadname, width, height, wpms, electrodesofinterest, analysistype, epoch{epoch_i}, conditionbaseline, plottimes{epoch_i},partinds);
                            savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' fstatname{fstat_i} '_' powernames{power_i} '_' epoch{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) '_windowedforplotting_zdata.mat'];
                            save(savename, 'phasepeaks');
                            saveas(gcf,savename,'tiff');
                    end
                end
            end
        end
    end
end
%%
% pulling out peaks for plotting
% load('E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\anovaresults.mat');
%
% analysistype = 'allpower';
%
%
% alpha = 0.001; %Original = 0.001
% %for cluster analysis only
% clusteralpha_fvalues = 1e-25;
% width = 60;  %Time Bins
% height = 6; %Frequency Bins:
% freqindmin = 22; %4HZ: Max from 4Hz to 30Hz;
% freqindmax = 68;%20Hz
%
% electrodesofinterest = 1:64;
% conditionbaseline = 0;
%
% fstatname = {'power_me' 'cond_me' 'powxcon'};
% fstat_i=1;
% cluster_i=1;
% epoch_i=1;
%
% addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'));
% %looping through powernames, epoch & clusters
% contourf(wpms.times(plottime_inds.(epoch{epoch_i})),wpms.frequencies,squeeze(fstat.(epoch{epoch_i}).(fstatname{fstat_i})(cluster_i,:,plottime_inds.(epoch{epoch_i}))),n,'linecolor','none');
% hold on;
% %anova
% mask = fdr_bky(squeeze(fpvalue.(epoch{epoch_i}).(fstatname{fstat_i})(cluster_i,:,plottime_inds.(epoch{epoch_i}))),alpha);
% %             mask = zeros(size(squeeze(fpvalue.(epoch{epoch_i}).(fstatname{fstat_i})(cluster_i,:,plottime_inds.(epoch{epoch_i})))));
% %             mask(squeeze(fpvalue.(epoch{epoch_i}).(fstatname{fstat_i})(cluster_i,:,plottime_inds.(epoch{epoch_i})))<alpha)=1;
% contour(wpms.times(plottime_inds.(epoch{epoch_i})),wpms.frequencies,mask,1,'k');
% caxis(clims)
% colormap jet;
% title(fstatname{fstat_i});
% axis square
% CC = getpeaks(width, height, freqindmin, freqindmax, h_cf, dataf, plottime_inds.(epoch{epoch_i}), wpms);
% savename=['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\' fstatname{fstat_i} '_' epoch{epoch_i} '_' clusternames{cluster_i} '_peaks.mat'];
% save(savename,'CC');
%

%%
% anova for all electrodes for plotting
powernames = {'total' 'nonphase' 'phase'};
electrodesofinterest = 1:64;

for fstat_i = 1:length(fstatname);
    fprintf('\n');
    for epoch_i = 1:length(epoch);
        for cluster_i = 1:length(clusters);
            for pixel_i = 1:5
                fpvalue.cue.power_me = zeros(1,length(electrodesofinterest));
                fpvalue.cue.cond_me = zeros(1,length(electrodesofinterest));
                fpvalue.cue.powxcon = zeros(1,length(electrodesofinterest));
                
                fstat.cue.power_me = zeros(1,length(electrodesofinterest));
                fstat.cue.cond_me = zeros(1,length(electrodesofinterest));
                fstat.cue.powxcon = zeros(1,length(electrodesofinterest));
                
                fpvalue.target.power_me = zeros(1,length(electrodesofinterest));
                fpvalue.target.cond_me = zeros(1,length(electrodesofinterest));
                fpvalue.target.powxcon = zeros(1,length(electrodesofinterest));
                
                fstat.target.power_me = zeros(1,length(electrodesofinterest));
                fstat.target.cond_me = zeros(1,length(electrodesofinterest));
                fstat.target.powxcon = zeros(1,length(electrodesofinterest));
                try
                    tic;
                    phasefilename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' fstatname{fstat_i} '_' powernames{3} '_' epoch{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) '_windowedforplotting_zdata.mat'];
                    load(phasefilename);
                    nonphasefilename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' fstatname{fstat_i} '_' powernames{2} '_' epoch{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) '_windowedforplotting_zdata.mat'];
                    load(nonphasefilename);
                    % run anova
                    nsubs = length(wpms.names);
                    N=nsubs;
                    % Make Goods
                    %f-stats
                    POWER_ME_f  = zeros(1,length(electrodesofinterest));
                    COND_ME_f  = POWER_ME_f;
                    POWxCON_f = POWER_ME_f;
                    %p-stats
                    POWER_ME_p  = zeros(1,length(electrodesofinterest));
                    COND_ME_p  = POWER_ME_p;
                    POWxCON_p = POWER_ME_p;
                    %A = POWER (PHASE,NONPHASE)
                    %B = CONDITION(ALLREPEAT,MIXREPEAT,SWITCHTO,SWITCHAWAY,NONINF)
                    if epoch_i == 1
                        conditionsWeWant=find(~ismember(wpms.conditions,{'noninfrepeat','noninfswitch'}));
                    elseif epoch_i == 2
                        conditionsWeWant=find(~ismember(wpms.conditions,{'noninf'}));
                    end
                    tic;
                    for electrode_i = 1:length(electrodesofinterest)
                        %extract each condition appropriately
                        %first phasedata
                        if epoch_i == 1
                            A1=squeeze(phasepeaks(electrode_i,:,1));
                            A2=squeeze(phasepeaks(electrode_i,:,2));
                            A3=squeeze(phasepeaks(electrode_i,:,3));
                            A4=squeeze(phasepeaks(electrode_i,:,4));
                            A5=squeeze(phasepeaks(electrode_i,:,5));
                            %second nonphase
                            B1=squeeze(nonphasepeaks(electrode_i,:,1));
                            B2=squeeze(nonphasepeaks(electrode_i,:,2));
                            B3=squeeze(nonphasepeaks(electrode_i,:,3));
                            B4=squeeze(nonphasepeaks(electrode_i,:,4));
                            B5=squeeze(nonphasepeaks(electrode_i,:,5));
                            % Set up goods
                            Y=[A1; B1; A2; B2; A3; B3; A4; B4; A5; B5]; % dependent variable (numeric) in a column vector
                            S=repmat([1:N]', 10,1); % grouping variable for SUBJECT
                            FACTNAMES={'POW','CON'};  % FACTNAMES  a cell array w/ two char arrays: {'factor1', 'factor2'}
                            
                            % Weights
                            %            P,C (power,conditon) levels -start at 0
                            A1w= repmat([0,0],N,1);
                            B1w= repmat([1,0],N,1);
                            A2w= repmat([0,1],N,1);
                            B2w= repmat([1,1],N,1);
                            A3w= repmat([0,2],N,1);
                            B3w= repmat([1,2],N,1);
                            A4w= repmat([0,3],N,1);
                            B4w= repmat([1,3],N,1);
                            A5w= repmat([0,4],N,1);
                            B5w= repmat([1,4],N,1);
                            codingvars=[A1w;B1w;A2w;B2w;A3w;B3w;A4w;B4w;A5w;B5w];
                            
                        elseif epoch_i == 2
                            A1=squeeze(phasepeaks(electrode_i,:,1));
                            A2=squeeze(phasepeaks(electrode_i,:,2));
                            A3=squeeze(phasepeaks(electrode_i,:,3));
                            A4=squeeze(phasepeaks(electrode_i,:,4));
                            A5=squeeze(phasepeaks(electrode_i,:,5));
                            A6=squeeze(phasepeaks(electrode_i,:,6));
                            %second nonphase
                            B1=squeeze(nonphasepeaks(electrode_i,:,1));
                            B2=squeeze(nonphasepeaks(electrode_i,:,2));
                            B3=squeeze(nonphasepeaks(electrode_i,:,3));
                            B4=squeeze(nonphasepeaks(electrode_i,:,4));
                            B5=squeeze(nonphasepeaks(electrode_i,:,5));
                            B6=squeeze(nonphasepeaks(electrode_i,:,6));
                            % Set up goods
                            Y=[A1; B1; A2; B2; A3; B3; A4; B4; A5; B5; A6; B6]; % dependent variable (numeric) in a column vector
                            S=repmat([1:N]', 12,1); % grouping variable for SUBJECT
                            FACTNAMES={'POW','CON'};  % FACTNAMES  a cell array w/ two char arrays: {'factor1', 'factor2'}
                            
                            % Weights
                            %            P,C (power,conditon) levels -start at 0
                            A1w= repmat([0,0],N,1);
                            B1w= repmat([1,0],N,1);
                            A2w= repmat([0,1],N,1);
                            B2w= repmat([1,1],N,1);
                            A3w= repmat([0,2],N,1);
                            B3w= repmat([1,2],N,1);
                            A4w= repmat([0,3],N,1);
                            B4w= repmat([1,3],N,1);
                            A5w= repmat([0,4],N,1);
                            B5w= repmat([1,4],N,1);
                            A6w= repmat([0,5],N,1);
                            B6w= repmat([1,5],N,1);
                            codingvars=[A1w;B1w;A2w;B2w;A3w;B3w;A4w;B4w;A5w;B5w;A6w;B6w];
                            %    F1         grouping variable for factor #1
                            %    F2         grouping variable for factor #2
                        end
                        % Run ANOVA
                        stats=rm_anova2(Y,S,codingvars(:,1),codingvars(:,2),FACTNAMES);
                        
                        
                        %this is what it was - i've changed below
                        
                        %                         % Get f vals
                        %
                        %                         POWER_ME_f(1,electrode_i)=cell2mat(stats(2,5));
                        %                         POWER_ME_p(1,electrode_i)=cell2mat(stats(2,6));
                        %                         COND_ME_f(1,electrode_i)=cell2mat(stats(3,5));
                        %                         %get p vals
                        %                         COND_ME_p(1,electrode_i)=cell2mat(stats(3,6));
                        %                         POWxCON_f(1,electrode_i)=cell2mat(stats(4,5));
                        %                         POWxCON_p(1,electrode_i)=cell2mat(stats(4,6));
                        %                         %time_i loop
                        %                         t=toc;fprintf('\t%2.2f %',t,'secs');
                        
                        % Get f vals
                        POWER_ME_f(1,electrode_i)=cell2mat(stats(2,5));
                        COND_ME_f(1,electrode_i)=cell2mat(stats(3,5));
                        POWxCON_f(1,electrode_i)=cell2mat(stats(4,5));
                        %get p vals
                        POWER_ME_p(1,electrode_i)=cell2mat(stats(2,6));
                        COND_ME_p(1,electrode_i)=cell2mat(stats(3,6));
                        POWxCON_p(1,electrode_i)=cell2mat(stats(4,6));
                        %time_i loop
                        t=toc;fprintf('\t%2.2f %',t,'secs');
                    end
                    if epoch_i == 1
                        fpvalue.cue.power_me(1,:)=squeeze(POWER_ME_p);
                        fpvalue.cue.cond_me(1,:)=squeeze(COND_ME_p);
                        fpvalue.cue.powxcon(1,:)=squeeze(POWxCON_p);
                        
                        fstat.cue.power_me(1,:)=squeeze(POWER_ME_f);
                        fstat.cue.cond_me(1,:)=squeeze(COND_ME_f);
                        fstat.cue.powxcon(1,:)=squeeze(POWxCON_f);
                        
                    elseif epoch_i == 2
                        fpvalue.target.power_me(1,:)=squeeze(POWER_ME_p);
                        fpvalue.target.cond_me(1,:)=squeeze(COND_ME_p);
                        fpvalue.target.powxcon(1,:)=squeeze(POWxCON_p);
                        
                        fstat.target.power_me(1,:)=squeeze(POWER_ME_f);
                        fstat.target.cond_me(1,:)=squeeze(COND_ME_f);
                        fstat.target.powxcon(1,:)=squeeze(POWxCON_f);
                    end
                catch
                    fprintf(2,'\tFailed on: fstat_i: %i, epoch_i: %i, cluster_i: %i pixel_i: %i\n',fstat_i,epoch_i,cluster_i,pixel_i);
                end
                savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' fstatname{fstat_i} '_' epoch{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) 'allelectrodeanova_zdata.mat'];
                save(savename,'fpvalue','fstat');
            end
        end
    end
end
%%
%plotting data

titlenames = {'Phase-Locked' 'Non-Phase-Locked'};
conditionlabels = {{'RA' 'RM' 'ST' 'SA' 'NI'};{'RA' 'RM' 'ST' 'SA' 'RN' 'SN'}};
plottime_start_cue = -300;
plottime_end_cue = 1200;
cuetimes = setbaseline(wpms, plottime_start_cue, plottime_end_cue);

plottime_start_target = 700;
plottime_end_target = 2000;
targettimes = setbaseline(wpms, plottime_start_target, plottime_end_target);

plottimes = {cuetimes;targettimes};
width = 60;  %Time Bins
height = 6; %Frequency Bins:
freqindmin = 22; %4HZ: Max from 4Hz to 30Hz;
freqindmax = 68;%20Hz
xtitle = 'Condition';
ylimits = [-10 10];
threshold = 0.001;
flimits = [-10 10];

fstatname = {'power_me' 'cond_me' 'powxcon'};

addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip\'));
for fstat_i = 1:length(fstatname);
    %     for power_i = 2:length(powernames);
    for epoch_i = 1:length(epoch);
        for cluster_i = 1:length(clusternames);
            electrodesofinterest = find(ismember(wpms.labels,clusters{cluster_i}));
            fprintf('\tAveraging over Frequencing and Time: fstat_i: %i, epoch_i: %i, cluster_i: %i\n',...
                    fstat_i,epoch_i,cluster_i);
                
            filename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\' fstatname{fstat_i} '_' ...
                        epoch{epoch_i} '_' clusternames{cluster_i} '_peaks_zdata.mat'];
                    
            load(filename);
            for pixel_i = 1:5
                try
                    figure
                    fprintf('\tWorking on Pixel Index: %i\n',pixel_i );
                    Location = CC.PixelIdxLocation{pixel_i};
                    if isempty(Location)
                        fprintf('Location is Empty.. Moving on..\n');
                        close all;
                        continue;
                    end
                    freqmax = Location(1);
                    timemax = Location(2);
                    timewindows = plottimes{epoch_i}(timemax-width/2):1:plottimes{epoch_i}(timemax+width/2);
                    centretime =wpms.times(plottimes{epoch_i}((timemax-width/2)+30));
                    centrefreq = wpms.frequencies((freqmax-height/2)+3);
                    
                    filenamebar_nonphase = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' ...
                                    fstatname{fstat_i} '_' powernames{2} '_' epoch{epoch_i} ...
                                    '_' clusternames{cluster_i} '_' num2str(pixel_i) '_windowedforplotting_zdata.mat'];
                    
                    filenamebar_phase = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' ...
                                    fstatname{fstat_i} '_' powernames{3} '_' epoch{epoch_i} ...
                                    '_' clusternames{cluster_i} '_' num2str(pixel_i) '_windowedforplotting_zdata.mat'];
                                
                    filenametopo = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' ...
                                    fstatname{fstat_i} '_' epoch{epoch_i} '_' clusternames{cluster_i} ...
                                    '_' num2str(pixel_i) 'allelectrodeanova_zdata.mat'];
                    
                    %For the Bar Plot:
                    A1 = load (filenamebar_phase);
                    fname = fieldnames(A1);
                    peakdata_phase = A1.(fname{1});clear A fname;
                    
                    A1 = load (filenamebar_nonphase);
                    fname = fieldnames(A1);
                    peakdata_nonphase = A1.(fname{1});clear A fname;
                                       
                    % 
                    B = load (filenametopo);
                    fname = fieldnames(B);
                    datatoplot = B.(fname{2});
                    pvalue = B.(fname{1});clear A fname;
                    
                    ytitle = [' (dB) '];
                    xticks = conditionlabels{epoch_i};
                    titlestr = [fstatname{fstat_i} ' ' epoch{epoch_i} ' ' clusternames{cluster_i} ' ' num2str(pixel_i) ' Freq: ' num2str(centrefreq) ' Time: ' num2str(centretime)];
                    plot_p_data_corr = fdr_bky(pvalue.(epoch{epoch_i}).(fstatname{fstat_i})',threshold);%apply fdr correction
                    plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
                    cfg = [];
                    data =[];
                    data.avg = zeros(72,1);
                    data.avg = [datatoplot.(epoch{epoch_i}).(fstatname{fstat_i})';0;0;0;0;0;0;0;0];
                    data.var = zeros(72,1);
                    data.time = 1;
                    data.label = wpms.labels;
                    data.dimord = 'chan_time';
                    data.cov = zeros(72,72);
                    cfg.highlightchannel = wpms.labels(plot_p_data_corr==1);
                    cfg.layout = 'biosemi64.lay';
                    cfg.zlim = flimits;
                    cfg.colormap = 'jet';
                    cfg.contournum = 2;
                    cfg.comment = 'no';
                    %ft_topoplotER(cfg, data);
                    %title(titlestr);axis square;box off;
                    
                    plotdata_newanova(peakdata_phase, peakdata_nonphase, datatoplot.(epoch{epoch_i}).(fstatname{fstat_i})',...
                        pvalue.(epoch{epoch_i}).(fstatname{fstat_i})',...
                        wpms, ytitle, xtitle, ylimits, flimits, xticks, ...
                        epoch, threshold, electrodesofinterest, titlestr);
                    if any((datatoplot.(epoch{epoch_i}).(fstatname{fstat_i})') < 0)
                        fprintf(2,'Sanity Less 0 fstat\n');
                    end
                    savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\newplotsforphasenonphase\fcluster_' fstatname{fstat_i} '_' epoch{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) 'v2_zdata.tif'];
                    saveas(gcf, savename, 'tiff');
                    %pause();
                    close all; 
                    fprintf('\n');
                catch
                    fprintf(2,'\tFailed on: fstat_i: %i, epoch_i: %i, cluster_i: %i pixel_i: %i\n',fstat_i,epoch_i,cluster_i,pixel_i);
                    close all;
                end
                
            end
        end
    end
end

%%
% Plotting bar plots & head plots with plotdata function

addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip-20150902\'));
titlenames = {'Total' 'Non-Phase-Locked' 'Phase-Locked'};
conditionlabels = {{'RA' 'RM' 'ST' 'SA' 'NI'};{'RA' 'RM' 'ST' 'SA' 'RN' 'SN'}};

ytitle = [' (dB) '];
xtitle = 'Condition';
ylimits = [-3 3];
flimits = [-10 10];
threshold = 0.001;

titlenames = {'Phase-Locked' 'Non-Phase-Locked'};
conditionlabels = {{'RA' 'RM' 'ST' 'SA' 'NI'};{'RA' 'RM' 'ST' 'SA' 'RN' 'SN'}};
plottime_start_cue = -300;
plottime_end_cue = 1200;
cuetimes = setbaseline(wpms, plottime_start_cue, plottime_end_cue);

plottime_start_target = 700;
plottime_end_target = 2000;
targettimes = setbaseline(wpms, plottime_start_target, plottime_end_target);

plottimes = {cuetimes;targettimes};
width = 60;  %Time Bins
height = 6; %Frequency Bins:
freqindmin = 22; %4HZ: Max from 4Hz to 30Hz;
freqindmax = 68;%20Hz
xtitle = 'Condition';
ylimits = [-3 3];
threshold = 0.001;
flimits = [-10 10];

fstatname = {'power_me' 'cond_me' 'powxcon'};

    
% A = load(filename);
% fname = fieldnames(A);
% peakdata = A.(fname{1});clear A fname;
% xticks = conditionlabels{epoch_i};
% titlestr = [fstatname{fstat_i} ' ' epoch{epoch_i} ' ' clusternames{cluster_i} ' ' num2str(pixel_i) ' Freq: ' num2str(centrefreq) ' Time: ' num2str(centretime)];
% plotdata(peakdata, wpms, ytitle, xtitle, ylimits, flimits,xticks, epochnames{epoch_i}, threshold, electrodesofinterest, titlestr);
% savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\newplotsforphasenonphase\fcluster_' fstatname{fstat_i} '_' epoch{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) 'v2.tif'];
% saveas(gcf, savename, 'tiff');
% close;

fprintf('\n');



for fstat_i = 1:length(fstatname)
    for epoch_i = 1:length(epoch)
        for cluster_i = 1:length(clusternames)
            electrodesofinterest = find(ismember(wpms.labels,clusters{cluster_i}));
            for pixel = 1:5
                try
                   filename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' ...
                                fstatname{fstat_i} '_' powernames{power_i} '_' epoch{epoch_i} '_' ...
                                clusternames{cluster_i} '_' num2str(pixel_i) '_windowedforplotting_zdata.mat'];

                    A = load(filename);
                    fname = fieldnames(A);
                    peakdata = A.(fname{1});clear A fname;
                    xticks = conditionlabels{epoch_i};

                    titlestr = [fstatname{fstat_i} ' ' epoch{epoch_i} ' ' clusternames{cluster_i} ...
                                ' ' num2str(pixel_i) ' Freq: ' num2str(centrefreq) ' Time: ' num2str(centretime)];

                    plotdata(peakdata, wpms, ytitle, xtitle, ylimits, flimits,xticks, epochnames{epoch_i}, ...
                        threshold, electrodesofinterest, titlestr);

                    savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\newplotsforphasenonphase\fcluster_' ...
                        fstatname{fstat_i} '_' epoch{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) 'v2_zdata.tif'];
                    saveas(gcf, savename, 'tiff');
                    close; 
                catch e
                    fprintf(2,'\tFailed on: fstat_i: %i, epoch_i: %i, cluster_i: %i pixel_i: %i\n',...
                        fstat_i,epoch_i,cluster_i,pixel_i);
                end
            end
        end
    end
end
                



%%
% pulling out stats

for fstat_i = 1:length(fstatname);
    for power_i = 1:length(powernames);
        for epoch_i = 1:length(epoch);
            for cluster_i = 1:length(clusternames);
                electrodesofinterest = find(ismember(wpms.labels,clusters{cluster_i}));
                for pixel_i = 1:5
                    filename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' fstatname{fstat_i} '_' powernames{power_i} '_' epoch{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) '_windowedforplotting_zdata.mat'];
                    try
                        A = load (filename);
                        fname = fieldnames(A);
                        peakdata = A.(fname{1});clear A fname;
                        statsdata = squeeze(mean(peakdata(electrodesofinterest,:,:),1));
                        savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' fstatname{fstat_i} '_' powernames{power_i} '_' epoch{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) '_stats_zdata.txt'];
                        dlmwrite(savename,statsdata,'delimiter','\t');
                        %                     save (savename, 'statsdata','-tabs','-ascii');
                    catch
                    end
                end
            end
        end
    end
end


%%
for fstat_i = 1:length(fstatnames);
    for epoch_i = 1:length(epoch);
        for cluster_i = 1:length(clusters);
            for pixel_i = 1:length(CC.PixelIdxList)
                fprintf('\tWorking on Pixel Index: %i\n',pixel_i );
                Location = CC.PixelIdxLocation{pixel_i};
                if isempty(Location)
                    continue;
                end
                switch power_i
                    case 1
                        [totalpeaks, ~, ~] = peakwindow(Location, loadname, width, height, wpms, electrodesofinterest, analysistype, epochnames{epoch_i}, conditionbaseline, plottimes{epoch_i});
                        savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' powernames{power_i} '_' epochnames{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) '_windowedforplotting_zdata.mat'];
                        save(savename, 'totalpeaks');
                    case 2
                        [~, nonphasepeaks, ~] = peakwindow(Location, loadname, width, height, wpms, electrodesofinterest, analysistype, epochnames{epoch_i}, conditionbaseline, plottimes{epoch_i});
                        savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' powernames{power_i} '_' epochnames{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) '_windowedforplotting_zdata.mat'];
                        save(savename, 'nonphasepeaks');
                    case 3
                        [~, ~, phasepeaks] = peakwindow(Location, loadname, width, height, wpms, electrodesofinterest, analysistype, epochnames{epoch_i}, conditionbaseline, plottimes{epoch_i});
                        savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\fcluster_' powernames{power_i} '_' epochnames{epoch_i} '_' clusternames{cluster_i} '_' num2str(pixel_i) '_windowedforplotting_zdata.mat'];
                        save(savename, 'phasepeaks');
                end
            end
        end
    end
end

%%
%Behavioural data

%data = 

%[RT, errorRate] = extractBehaviouralData(data);




