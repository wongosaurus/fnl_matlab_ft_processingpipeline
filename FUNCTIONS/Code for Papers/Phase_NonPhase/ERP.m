% ERP
clear all
clc

% setup parameters
listingdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
datain = 'Z:\ITPC\ReferenceOutput\SurfaceLapacian\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30;
fbins = 80;
times = (-1:0.001953125000000:3.5)*1000;
%conditions = {'allrepeat','mixrepeat','noninfrepeat','switchto','switchaway','noninfswitch'};
conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninf'};
wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);

addpath(genpath([wpms.dirs.CWD,wpms.dirs.FUNCTIONS]));
addpath(genpath([wpms.dirs.CWD,wpms.dirs.packages '\fieldtrip-20150902']));

clearvars -except wpms

%%

clusternames = {'Frontal','Central','Parietal'};
%electrodesofinterest = {{'FCz', 'FC1', 'FC2', 'Cz', 'C1', 'C2'} {'CPz', 'CP1', 'CP2', 'Pz', 'P1', 'P2'}};
electrodesofinterest = {{'Fz'} {'Cz'} {'Pz'}};

erpdata = zeros(length(clusternames),length(wpms.conditions),length(wpms.names),length(wpms.times));

for cluster_i = 1:length(clusternames);
    wewant = find(ismember(wpms.labels,electrodesofinterest{cluster_i}));
    for condition_i = 1:length(wpms.conditions)
        tic
        fprintf('\n%s', wpms.conditions{condition_i});
        for name_i = 1:length(wpms.names)
            if mod(name_i,2)==0
                fprintf('.');
            end
            temp = zeros(length(wewant),length(wpms.times));
            for electrode_i = 1:length(wewant)
                filename = [wpms.dirs.datain wpms.names{name_i} '\' wpms.conditions{condition_i} '\' num2str(wewant(electrode_i)) '.mat'];
                m=matfile(filename);
                EEG=m.EEG;
                %                 load(filename);
                temp(electrode_i,:) = squeeze(mean(EEG.data,3));
                clear m EEG
            end
            erpdata(cluster_i, condition_i, name_i, :) = squeeze(mean(temp,1));
        end
        toc
    end
end
%%

% baseline

baseline_start = -50;
baseline_end = 50;

baselineinds = setbaseline(wpms, baseline_start, baseline_end);

blerpdata = bsxfun(@minus,erpdata,squeeze(mean(erpdata(:,:,:,baselineinds),4)));

plot_start = -200;
plot_end = 2000;

plottimeinds = setbaseline(wpms, plot_start,plot_end);

plotcolor = {'k', 'r', 'b'};

%plot

for cluster_i = 1:length(clusternames);
    figure(); set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);
    for condition_i = 1:length(wpms.conditions)
%         subplot(3,1,cluster_i);

        plot(wpms.times(plottimeinds),squeeze(mean(blerpdata(cluster_i,condition_i,:,plottimeinds),3)),plotcolor{condition_i},'LineWidth',2);
        hold on;ylim([-20 20]);
        set(gca,'YDir','reverse','LineWidth',2,'FontSize',18);axis square;box off;
    end
    title(clusternames{cluster_i},'FontSize',18);
%     ax = gca;
%     ax.YAxisLocation = 'origin';
%     ax.XAxisLocation = 'origin';
end
% for cluster_i = 1:length(clusternames)
%     statsdata = squeeze(mean(blerpdata(cluster_i,:,:,plottimeinds),3));
%     savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\ERP_' clusternames{cluster_i} 'grandaverage_cue_newbl.txt'];
%     dlmwrite(savename,statsdata,'delimiter','\t');
% end
