clearvars;

clear all;close all;clc

% setup parameters
listingdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
datain = 'E:\FNL_EEG_TOOLBOX\REPAIRED_DATA\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30; 
fbins = 80;
times = (-1:0.001953125000000:3.5)*1000;
conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninfrepeat','noninfswitch'};

wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);
wpms.names = {'AGE019',	'AGE023',	'AGE024',	'AGE026',	'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE050',	'AGE051',	'AGE052',	'AGE053',	'AGE058',	'AGE059',	'AGE061',	'AGE064',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE072',	'AGE073',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE088',	'AGE089',	'AGE090',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE100',	'AGE102',	'AGE104',	'AGE106',	'AGE108',	'AGE109',	'AGE111',	'AGE114',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE123',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE133',	'AGE135',	'AGE138',	'AGE145',	'AGE147',	'AGE149',	'AGE150',	'AGE151',	'AGE153',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE164',	'AGE165',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE172',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE190',	'AGE197',	'AGE198',	'AGE199',	'AGE200',	'AGE201',	'AGE202',	'AGE203',	'AGE204',	'AGE205',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE216',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE224',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE233',	'AGE234',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE240',	'AGE241',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE262',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE273',	'AGE275',	'AGE276',	'AGE277',	'AGE279'};

%%

disp('1. Bad channel counting')
badChannelCount = zeros(1,length(wpms.names));

for name_i=1:length(wpms.names)
    filename = [wpms.dirs.datain wpms.names{name_i} '_RepairedData_allrepeat.mat'];
    load(filename);
    fprintf('\n%s\t',wpms.names{name_i});
    badChannelCount(1,name_i) = length(ardata.cfg.badchannel);
    fprintf('%i\t%s',length(ardata.cfg.badchannel),'bad channels');
end

savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\Phase1\badChannelCountPhase1.mat'];
save(savename,'badChannelCount');

fprintf('\n');
%%
disp('2. Counting number of ICA components removed AND determining valid trial counts')

ICAComponents = zeros(1,length(wpms.names));
validTrials = zeros(1,length(wpms.names));

for name_i=1:length(wpms.names)
    tic;
    try
        fprintf('\n%s\t',wpms.names{name_i});
        filename = [wpms.dirs.datain wpms.names{name_i} '_RepairedData_allrepeat.mat'];
        load(filename);
        ICAComponents(1,name_i) = length(ardata.cfg.previous.previous.previous.previous.component);
        fprintf('%i\t%s\t\t',length(ardata.cfg.previous.previous.previous.previous.component),'components channels');
        validTrials(1,name_i) = length(ardata.cfg.previous.previous.previous.previous.previous.previous.trl);
        fprintf('%i\t%s',length(ardata.cfg.previous.previous.previous.previous.previous.previous.trl),'valid trials');
    catch exception
        disp(['failed for ' wpms.names{name_i}]);
    end%try/catch loop
    t=toc;
    fprintf('\t%3.2f %s',t);
end

savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\Phase1\ICAComponentPhase1.mat'];
save(savename,'ICAComponents');

savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\Phase1\validTrialsPhase1.mat'];
save(savename,'validTrials');

fprintf('\n');

%% 
disp('3. Determining each trial count');

trialCount = zeros(length(wpms.conditions),length(wpms.names));


for name_i=1:length(wpms.names)
    fprintf('\n%s\t',wpms.names{name_i});
    ts = zeros(1,length(wpms.conditions));
    for cond_i = 1:length(wpms.conditions)
        tic;
        try
            fprintf('.');
            filename = [wpms.dirs.datain wpms.names{name_i} '_RepairedData_' wpms.conditions{cond_i} '.mat'];
            if exist(filename,'file')==2;
                A=load(filename);
                datname = fieldnames(A);
                trialCount(cond_i,name_i) = length(A.(datname{1}).trial);
            else
                trialCount(cond_i,name_i) = NaN;
            end
        catch exception
            disp(['failed for ' wpms.names{name_i}]);
        end
        ts(1,cond_i) = toc;
    end
    t=mean(ts);
end

savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\Phase1\trialCountPhase1.mat'];
save(savename,'trialCount');

%%
RT = zeros(length(wpms.names),length(wpms.conditions));
errorRate = zeros(length(wpms.names),length(wpms.conditions));
for name_i = 1:length(wpms.names)
    for cond_i = 1:length(wpms.conditions)
        filename = [wpms.dirs.datain wpms.names{name_i} '_RepairedData_' wpms.conditions{cond_i} '.mat'];
        A = load(filename);
        fname=fieldnames(A);
        refdat = A.(fname{1});
        clearvars A;
        data = refdat;
        [RT(name_i,cond_i),errorRate(name_i,cond_i)] = extractBehaviouralData(data);
    end
end
%%
% bad channels
meanBCPhase1 = mean(badChannelCount);
SDBCPhase1= std(badChannelCount);
minBCPhase1 = min(badChannelCount);
maxBCPhase1 = max(badChannelCount);

% ICA
meanICAPhase1 = mean(ICAComponents);
SDICAPhase1 = std(ICAComponents);
minICAPhase1 = min(ICAComponents);
maxICAPhase1 = max(ICAComponents);

% Valid trials
meanVTPhase1 = mean(validTrials);
SDVTPhase1 = std(validTrials);
minVTPhase1 = min(validTrials);
maxVTPhase1 = max(validTrials);

% trial count

for cond_i = 1:length(wpms.conditions)
    meanTC(cond_i) = mean(trialCount(cond_i,:));
    SDTC(cond_i) = std(trialCount(cond_i,:));
    minTC(cond_i) = min(trialCount(cond_i,:));
    maxTC(cond_i) = max(trialCount(cond_i,:));
end

validTrialProp = zeros(1,length(wpms.names));
for name_i = 1:length(wpms.names)
    validTrialProp(:,name_i) = 100-(validTrials(name_i)./929*100);
end

meanVT = mean(validTrialProp);
SDVT = std(validTrialProp);
    