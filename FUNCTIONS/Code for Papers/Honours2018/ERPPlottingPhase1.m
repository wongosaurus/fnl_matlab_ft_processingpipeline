% ERP plotting code - Phase 1
clearvars;

clear all;close all;clc

% setup parameters
listingdir = 'G:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
datain = 'G:\FNL_EEG_TOOLBOX\REPAIRED_DATA\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30;
fbins = 80;
times = (-1:0.001953125000000:3.5)*1000;
conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninfrepeat','noninfswitch'};

wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);

%%
loadname = 'E:\FNL_EEG_TOOLBOX\data_for_papers\ERPsPhase1AllElectodes.mat';
load(loadname);



badInds = {'AGE009',	'AGE052',	'AGE106',	'AGE178',	'AGE192',	'AGE200',	'AGE204',	'AGE216',	'AGE224',	'AGE234',	'AGE240',	'AGE259',	'AGE262',	'AGE277'};

erp_baseline_start = -50;
erp_baseline_end = 50;
erp_baselineinds = setbaseline(wpms, erp_baseline_start, erp_baseline_end);

blerpdata = bsxfun(@minus,erpdata,squeeze(mean(erpdata(:,:,:,erp_baselineinds),4)));

times = wpms.times;
plot_start = -300;
plot_end = 2000;
plot_times = setbaseline(wpms, plot_start,plot_end);

for name_i = 1:length(badInds)
    for cond_i = 1:length(wpms.conditions)
        %subplot(2,3,cond_i)
        data = squeeze(blerpdata(name_i,:,cond_i,plot_times));
        plot(wpms.times(plot_times),data);
        xlim([-300 2000])
        title([badInds{name_i} ' ' wpms.conditions(cond_i)]);
        %if cond_i == 1
        ylabel(['\bf' 'Amplitude (\muV/cm^2)']);
        xlabel(['\bf' 'Time (ms)']);
        lgd = legend(wpms.labels);
        lgd.Orientation = 'vertical';
        lgd.FontSize = 4;
        %end
        set(gca,'YDir','reverse')
        filename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\Phase1\ERPs' filesep badInds{name_i} wpms.conditions{cond_i} '_ERPsPhase1.fig'];
        saveas(gcf,filename,'fig');
    end
end


%%
%addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES'));
%addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
%addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip\utilities'));
%addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip\utilities\compat'));
%addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip\utilities\private'));

% for name_i = 1:length(badInds)
%     for cond_i = 1:length(wpms.conditions)
%         
%         cfg.dataset = 'E:\FNL_EEG_TOOLBOX\data_for_papers\ERPsPhase1AllElectodes.mat';
%         cfg.layout = 'biosemi64.lay';
%         cfg.continuous = 'yes';
%         cfg.channel = 1:64;
%         cfg.ylim = [-100 100];
%         cfg.zlim = 'maxmin';
%         cfg.blocksize = 10;
%         cfg.viewmode = 'vertical';
%         cfg.layout = 'biosemi64.lay';
%         cfg.continuous = 'yes';
%         cfg.channel = wpms.labels;
%         cfg.ylim = [-10 10];
%         cfg.blocksize = 2.0;
%         cfg.viewmode = 'vertical';
%         data = [];
%         data.avg = zeros (64,1);
%           data = squeeze(blerpdata(name_i,:,cond_i,plot_times));
%         data_temp = squeeze(blerpdata(name_i,:,cond_i,plot_times));
%         data.avg = [data_temp];
%         data.avg = (data.avg);
%         data.var = zeros(64,1);
%         data.time = times(plot_times)/1000;
%         data.label = wpms.labels;
%         data.dimord = 'chan_time';
%         data.cov = zeros(64,64);
%         
%         subplot(2,3,cond_i)
%         plot(wpms.times(plot_times),data_temp);
%         plot(wpms.times(plot_times),data);
%         xlim([-300 2000])
%         title([badInds{name_i} ' ' wpms.conditions(cond_i)]);
%         if cond_i == 1
%             ylabel(['\bf' 'Amplitude (\muV/cm^2)']);
%             xlabel(['\bf' 'Time (ms)']);
%             set(gca,'YDir','reverse')
%         end 
%          
%         cfg = ft_databrowser(cfg,data);
%         filename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\Phase1\ERPs' filesep badInds{name_i} '_ERPsPhase1Old.fig'];
%         saveas(gcf,filename,'fig');
%     end
% end

