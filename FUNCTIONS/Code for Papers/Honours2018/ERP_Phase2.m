% ERP
clear all
clc

% setup parameters
listingdir = 'E:\FNL_EEG_TOOLBOX\REPAIRED_DATA2\*_RepairedData_mixrepeat.mat';
%listingdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
datain = 'E:\FNL_EEG_TOOLBOX\REPAIRED_DATA2\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30;
fbins = 80;
times = (-1:0.001953125000000:3.5)*1000;
conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninfrepeat','noninfswitch'};
%conditions = {'allrepeat','mixrepeat','switchto'};
wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);

% addpath(genpath([wpms.dirs.CWD,wpms.dirs.FUNCTIONS]));
% addpath(genpath([wpms.dirs.CWD,wpms.dirs.packages '\fieldtrip-20150902']));


clearvars -except wpms
%%

%clusternames = {'Frontal','Central','Parietal'};
%electrodesofinterest = {{'FCz', 'FC1', 'FC2', 'Fz', 'F1', 'F2'} {'CPz', 'CP1', 'CP2', 'Cz', 'C1', 'C2'} {'POz', 'PO3', 'PO4', 'Pz', 'P1', 'P2'}};

erpdata = zeros(length(wpms.names),length(wpms.labels),length(wpms.conditions),length(wpms.times));

% erpdata2.Frontal = zeros(length(electrodesofinterest{1}),length(wpms.conditions),length(wpms.names),length(wpms.times));
% erpdata2.Central = zeros(length(electrodesofinterest{2}),length(wpms.conditions),length(wpms.names),length(wpms.times));
% erpdata2.Parietal = zeros(length(electrodesofinterest{3}),length(wpms.conditions),length(wpms.names),length(wpms.times));

for condition_i = 1:length(wpms.conditions)
    fprintf('\n%s', wpms.conditions{condition_i});
    for name_i = 1:length(wpms.names)
        try
        fprintf('\n%s\t', wpms.names{name_i});
        tic
        if mod(name_i,2)==0
            fprintf('.');
        end
        filename = [wpms.dirs.datain wpms.names{name_i} '_RepairedData_' wpms.conditions{condition_i} '.mat'];
        
        data_temp = load(filename);
        fnames = fieldnames(data_temp);
        data = data_temp.(fnames{1}); 
        data = data.trial;
        totalntrials = length(data);
        temp = zeros(totalntrials,size(data{1,1},1),size(data{1,1},2));
        for ti = 1:totalntrials
            temp(ti,:,:) = data{1,ti};
        end
        for cluster_i = 1:length(wpms.labels);
%             wewant = find(ismember(wpms.labels,electrodesofinterest{cluster_i}));
            erpdata(name_i, cluster_i, condition_i, :) = squeeze(mean(mean(temp(:,cluster_i,:),1),2));
%             for electrode_i = 1:length(wewant)
%                 erpdata2.(clusternames{cluster_i})(electrode_i, condition_i, name_i, :) = squeeze(mean(temp(:,wewant(electrode_i),:),1));
%             end
        end
        toc
        catch noTrials
            disp(noTrials)
        end
    end
end

savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\ERPsPhase2AllElectodes.mat'];
save(savename,'erpdata');

%%

% baseline

baseline_start = -50;
baseline_end = 50;

baselineinds = setbaseline(wpms, baseline_start, baseline_end);

blerpdata = bsxfun(@minus,erpdata,squeeze(mean(erpdata(:,:,:,baselineinds),4)));
blerpdata2.Frontal = bsxfun(@minus,erpdata2.Frontal(1:6,:,:,:),squeeze(mean(erpdata2.Frontal(1:6,:,:,baselineinds),4)));
blerpdata2.Central = bsxfun(@minus,erpdata2.Central(1:6,:,:,:),squeeze(mean(erpdata2.Central(1:6,:,:,baselineinds),4)));
blerpdata2.Parietal = bsxfun(@minus,erpdata2.Parietal(1:6,:,:,:),squeeze(mean(erpdata2.Parietal(1:6,:,:,baselineinds),4)));

plot_start = -300;
plot_end = 2000;
 
plottimeinds = setbaseline(wpms, plot_start,plot_end);

%plot

figure(); set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);

for cluster_i = 1:length(clusternames);
    for condition_i = 1:length(wpms.conditions)
        subplot(1,3,cluster_i);
        plotcolor = {'k', 'r', 'b'};
        plot(wpms.times(plottimeinds),squeeze(mean(blerpdata(cluster_i,condition_i,:,plottimeinds),3)),plotcolor{condition_i},'LineWidth',2);
        hold on;
        ylim([-20 30]);
        xlim([-300 2000]);
        plot(wpms.times(plottimeinds),zeros(1,length(plottimeinds)),'k','LineWidth',2);
        set(gca,'YDir','reverse','LineWidth',2,'FontSize',18);axis square;box off;
%         for electrode_i = 1:6
%             plot(wpms.times(plottimeinds),squeeze(mean(blerpdata2.(clusternames{cluster_i})(electrode_i,condition_i,:,plottimeinds),3)),'LineWidth',2);
%         end
    end
    title(clusternames{cluster_i},'FontSize',18);
    %     ax = gca;
    %     ax.YAxisLocation = 'origin';
    %     ax.XAxisLocation = 'origin';
end

%for cluster_i = 1:length(clusternames)
%     statsdata = squeeze(mean(blerpdata(cluster_i,:,:,plottimeinds),3));
%     savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\ERP_' clusternames{cluster_i} 'grandaverage_cue_newbl.txt'];
%     dlmwrite(savename,statsdata,'delimiter','\t');
% end
