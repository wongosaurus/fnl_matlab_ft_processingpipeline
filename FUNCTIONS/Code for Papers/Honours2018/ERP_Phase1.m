% ERP
clear all
clc

% setup parameters
%listingdir = 'E:\FNL_EEG_TOOLBOX\REPAIRED_DATA2\*_RepairedData_mixrepeat.mat';
listingdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
datain = 'E:\FNL_EEG_TOOLBOX\ReferenceOutput\SurfaceLapacian\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30;
fbins = 80;
times = (-1:0.001953125000000:3.5)*1000;
conditions = {'allrepeat','mixrepeat','switchto','switchaway'};
%conditions = {'allrepeat','mixrepeat','switchto'};
wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);


wpms.names = {'AGE019',	'AGE023',	'AGE024',	'AGE026',	'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE050',	'AGE051',	'AGE053',	'AGE058',	'AGE059',	'AGE061',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE072',	'AGE073',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE088',	'AGE089',	'AGE090',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE100',	'AGE102',	'AGE104',	'AGE108',	'AGE109',	'AGE111',	'AGE114',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE123',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE133',	'AGE135',	'AGE138',	'AGE145',	'AGE147',	'AGE149',	'AGE150',	'AGE151',	'AGE153',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE164',	'AGE165',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE172',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE190',	'AGE197',	'AGE198',	'AGE199',	'AGE200',	'AGE201',	'AGE202',	'AGE203',	'AGE204',	'AGE205',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE224',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE233',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE241',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE273',	'AGE275',	'AGE276',	'AGE277',	'AGE279'};

% addpath(genpath([wpms.dirs.CWD,wpms.dirs.FUNCTIONS]));
% addpath(genpath([wpms.dirs.CWD,wpms.dirs.packages '\fieldtrip-20150902']));

clearvars -except wpms
%%

%clusternames = {'Frontal','Central','Parietal'};
electrodesofinterest = {'POz' 'Pz' 'FCz'};

erpdata = zeros(length(wpms.names),length(wpms.labels),length(wpms.conditions),length(wpms.times));

% erpdata2.Frontal = zeros(length(electrodesofinterest{1}),length(wpms.conditions),length(wpms.names),length(wpms.times));
% erpdata2.Central = zeros(length(electrodesofinterest{2}),length(wpms.conditions),length(wpms.names),length(wpms.times));
% erpdata2.Parietal = zeros(length(electrodesofinterest{3}),length(wpms.conditions),length(wpms.names),length(wpms.times));

for condition_i = 1:length(wpms.conditions)
    fprintf('\n%s', wpms.conditions{condition_i});
    for name_i = 1:length(wpms.names)
        try
            fprintf('\n%s\t', wpms.names{name_i});
            tic
            if mod(name_i,2)==0
                fprintf('.');
            end
            filename = [wpms.dirs.datain wpms.names{name_i} wpms.conditions{condition_i} '.mat'];
            load(filename);
            data = refdat.trial;
            totalntrials = length(data);
            temp = zeros(totalntrials,size(data{1,1},1),size(data{1,1},2));
            for ti = 1:totalntrials
                temp(ti,:,:) = data{1,ti};
            end
            for cluster_i = 1:length(wpms.labels);
                %             wewant = find(ismember(wpms.labels,electrodesofinterest{cluster_i}));
                erpdata(name_i, cluster_i, condition_i, :) = squeeze(mean(mean(temp(:,cluster_i,:),1),2));
                %             for electrode_i = 1:length(wewant)
                %                 erpdata2.(clusternames{cluster_i})(electrode_i, condition_i, name_i, :) = squeeze(mean(temp(:,wewant(electrode_i),:),1));
                %             end
            end
            toc
        catch noTrials
            disp(noTrials)
        end
    end
end

savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\ERPsPhase1Nathan.mat'];
save(savename,'erpdata');

%%

% baseline

baseline_start = 950;
baseline_end = 1050;

baselineinds = setbaseline(wpms, baseline_start, baseline_end);

blerpdata = bsxfun(@minus,erpdata,squeeze(mean(erpdata(:,:,:,baselineinds),4)));


blerpdata2.Frontal = bsxfun(@minus,erpdata2.Frontal(1:6,:,:,:),squeeze(mean(erpdata2.Frontal(1:6,:,:,baselineinds),4)));
blerpdata2.Central = bsxfun(@minus,erpdata2.Central(1:6,:,:,:),squeeze(mean(erpdata2.Central(1:6,:,:,baselineinds),4)));
blerpdata2.Parietal = bsxfun(@minus,erpdata2.Parietal(1:6,:,:,:),squeeze(mean(erpdata2.Parietal(1:6,:,:,baselineinds),4)));

plot_start = 1000;
plot_end = 2000;

plottimeinds = setbaseline(wpms, plot_start,plot_end);

%plot

figure(); set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);

electrodesofinterest = {'POz'};
%electrodesofinterest = {'Pz'};
%electrodesofinterest = {'FCz'};

elec_i = find(ismember(wpms.labels,electrodesofinterest));

for cond_i = 1:length(wpms.conditions)
    plotcolor = {'k', 'r', 'b', 'g'};
    plotdata = mean(squeeze(blerpdata(:,elec_i,cond_i,plottimeinds)),1);
    plot(wpms.times(plottimeinds),plotdata,plotcolor{cond_i},'LineWidth',5);
    hold on;
    ylim([-21 30]);
    xlim([1000 2000]);
    plot(wpms.times(plottimeinds),zeros(1,length(plottimeinds)),'k','LineWidth',2);
    set(gca,'YDir','reverse','LineWidth',2,'FontSize',35);box off;
end
%title(clusternames{cluster_i},'FontSize',18);
%     ax = gca;
%     ax.YAxisLocation = 'origin';
    %     ax.XAxisLocation = 'origin';

%for cluster_i = 1:length(clusternames)
%     statsdata = squeeze(mean(blerpdata(cluster_i,:,:,plottimeinds),3));
%     savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\phase_nonphase\ERP_' clusternames{cluster_i} 'grandaverage_cue_newbl.txt'];
%     dlmwrite(savename,statsdata,'delimiter','\t');
% end
