
% setup parameters
listingdir = 'E:\FNL_EEG_TOOLBOX\REPAIRED_DATA2\*_RepairedData_mixrepeat.mat';
datain = 'E:\FNL_EEG_TOOLBOX\REPAIRED_DATA2\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30;
fbins = 80;
times = (-1:0.001953125000000:3.5)*1000;
conditions = {'allrepeat','mixrepeat','switchto'};
wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);

clearvars -except wpms

wpms.names = {'BGE026',	'BGE030',	'BGE032',	'BGE035',	'BGE038',	'BGE050',	'BGE058',	'BGE069',	'BGE077',	'BGE089',	'BGE100',	'BGE108',	'BGE109',	'BGE117',	'BGE118',	'BGE119',	'BGE129',	'BGE130',	'BGE138',	'BGE149',	'BGE165',	'BGE168',	'BGE169',	'BGE184',	'BGE187',	'BGE199',	'BGE200',	'BGE205',	'BGE224',	'BGE225',	'BGE226',	'BGE230',	'BGE237',	'BGE238',	'BGE245',	'BGE252',	'BGE258',	'BGE260',	'BGE267',	'BGE270',	'BGE277'};
%%
disp('1. Bad channel counting')
badChannelCount = zeros(1,length(wpms.names));

for name_i=1:length(wpms.names)
    filename = [wpms.dirs.datain wpms.names{name_i} '_RepairedData_allrepeat.mat'];
    load(filename);
    fprintf('\n%s\t',wpms.names{name_i});
    badChannelCount(1,name_i) = length(ardata.cfg.badchannel);
    fprintf('%i\t%s',length(ardata.cfg.badchannel),'bad channels');
end

% will this save each section???
savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\Phase2\badChannelCountPhase2.mat'];
save(savename,'badChannelCount');

fprintf('\n');
%%
disp('2. Counting number of ICA components removed AND determining valid trial counts')

ICAComponents = zeros(1,length(wpms.names));
validTrials = zeros(1,length(wpms.names));

for name_i=1:length(wpms.names)
    tic;
    try
        fprintf('\n%s\t',wpms.names{name_i});
        filename = [wpms.dirs.datain wpms.names{name_i} '_RepairedData_allrepeat.mat'];
        load(filename);
        ICAComponents(1,name_i) = length(ardata.cfg.previous.previous.previous.previous.component);
        fprintf('%i\t%s\t\t',length(ardata.cfg.previous.previous.previous.previous.component),'components channels');
        validTrials(1,name_i) = length(ardata.cfg.previous.previous.previous.previous.previous.previous.trl);
        fprintf('%i\t%s',length(ardata.cfg.previous.previous.previous.previous.previous.previous.trl),'valid trials');
    catch exception
        disp(['failed for ' wpms.names{name_i}]);
    end%try/catch loop
    t=toc;
    fprintf('\t%3.2f %s',t);
end

savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\Phase2\ICAComponentPhase2.mat'];
save(savename,'ICAComponents');

savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\Phase2\validTrialsPhase2.mat'];
save(savename,'validTrials');

fprintf('\n');

%% 
disp('3. Determining each trial count');

trialCount = zeros(length(wpms.conditions),length(wpms.names));


for name_i=1:length(wpms.names)
    fprintf('\n%s\t',wpms.names{name_i});
    ts = zeros(1,length(wpms.conditions));
    for cond_i = 1:length(wpms.conditions)
        tic;
        try
            fprintf('.');
            filename = [wpms.dirs.datain wpms.names{name_i} '_RepairedData_' wpms.conditions{cond_i} '.mat'];
            if exist(filename,'file')==2;
                A=load(filename);
                datname = fieldnames(A);
                trialCount(cond_i,name_i) = length(A.(datname{1}).trial);
            else
                trialCount(cond_i,name_i) = NaN;
            end
        catch exception
            disp(['failed for ' wpms.names{name_i}]);
        end
        ts(1,cond_i) = toc;
    end
    t=mean(ts);
end

savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\Phase2\trialCountPhase2.mat'];
save(savename,'trialCount');

%%
% bad channels
meanBCPhase2 = mean(badChannelCount);
SDBCPhase2= std(badChannelCount);
minBCPhase2 = min(badChannelCount);
maxBCPhase2 = max(badChannelCount);

% ICA
meanICAPhase2 = mean(ICAComponents);
SDICAPhase2 = std(ICAComponents);
minICAPhase2 = min(ICAComponents);
maxICAPhase2 = max(ICAComponents);

% Valid trials
meanVTPhase2 = mean(validTrials);
SDVTPhase2 = std(validTrials);
minVTPhase2 = min(validTrials);
maxVTPhase2 = max(validTrials);

% trial count

for cond_i = 1:length(wpms.conditions)
    meanTC(cond_i) = mean(trialCount(cond_i,:));
    SDTC(cond_i) = std(trialCount(cond_i,:));
    minTC(cond_i) = min(trialCount(cond_i,:));
    maxTC(cond_i) = max(trialCount(cond_i,:));
end