clearvars;

clear all;close all;clc

% setup parameters
listingdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
datain = 'E:\FNL_EEG_TOOLBOX\REPAIRED_DATA';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30; 
fbins = 80;
times = (-1:0.001953125000000:3.5)*1000;
conditions = {'allrepeat','mixrepeat','switchto'};
wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);

wpms.names = FinalSubjectNames161;

FinalSubjectNames161 = wpms.names; 

wpms.participants = wpms.names;

clearvars -except wpms

wpms.dirs.datain = 'E:\FNL_EEG_TOOLBOX\REPAIRED_DATA';




%%
disp('1. Bad channel counting')
badChannelCount = zeros(1,length(wpms.participants));

for name_i=1:length(wpms.participants)
    fprintf('\n%s\t',wpms.participants{name_i});
    filename = [wpms.dirs.datain '\' wpms.participants{name_i} '_RepairedData_allrepeat.mat'];
    load(filename);
    badChannelCount(1,name_i)=length(ardata.cfg.badchannel);
    fprintf('%i\t%s',length(ardata.cfg.badchannel),'bad channels');
end

% will this save each section???
savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\CABN\badChannelCount.mat'];
save(savename,'badChannelCount');

fprintf('\n');
%%
disp('2. Counting number of ICA components removed AND determining valid trial counts')

ICAComponents = zeros(1,length(wpms.participants));
validTrials = zeros(1,length(wpms.participants));


for name_i=1:length(wpms.participants)
    tic;
    try
        fprintf('\n%s\t',wpms.participants{name_i});
        filename = [wpms.dirs.datain '\' wpms.participants{name_i} '_RepairedData_allrepeat.mat'];
        load(filename);
        ICAComponents(1,name_i) = length(ardata.cfg.previous.previous.previous.previous.component);
        fprintf('%i\t%s\t\t',length(ardata.cfg.previous.previous.previous.previous.component),'components channels');
        validTrials(1,name_i) = length(ardata.cfg.previous.previous.previous.previous.previous.previous.trl);
        fprintf('%i\t%s',length(ardata.cfg.previous.previous.previous.previous.previous.previous.trl),'valid trials');
    catch exception
        disp(['failed for ' wpms.participants(name_i)]);
    end%try/catch loop
    t=toc;
    fprintf('\t%3.2f %s',t);
end
savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\CABN\ICAComponent.mat'];
save(savename,'ICAComponents');

savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\CABN\validTrials.mat'];
save(savename,'validTrials');

fprintf('\n');

%% 
disp('3. Determining each trial count');

trialCount = zeros(length(wpms.conditions),length(wpms.participants));

for name_i=1:length(wpms.participants)
    fprintf('\n%s\t',wpms.participants{name_i});
    ts = zeros(1,length(wpms.conditions));
    for cond_i = 1:length(wpms.conditions)
        tic;
        try
            fprintf('.');
            filename = [wpms.dirs.datain '\' wpms.participants{name_i} '_RepairedData_' wpms.conditions{cond_i} '.mat'];
            if exist(filename,'file')==2;
                A=load(filename);
                datname = fieldnames(A);
                trialCount(cond_i,name_i) = length(A.(datname{1}).trial);
            else
                trialCount(cond_i,name_i) = NaN;
            end
        catch exception
            disp(['failed for ' wpms.participants(name_i)]);
        end
        ts(1,cond_i) = toc;
    end
    t=mean(ts);
end

savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\CABN\trialCount.mat'];
save(savename,'trialCount');

%%
% bad channels
meanBC = mean(badChannelCount);
SDBC = std(badChannelCount);
minBC = min(badChannelCount);
maxBC = max(badChannelCount);

% ICA
meanICA = mean(ICAComponents);
SDICA = std(ICAComponents);
minICA = min(ICAComponents);
maxICA = max(ICAComponents);

% Valid trials
meanVT = mean(validTrials);
SDVT = std(validTrials);
minVT = min(validTrials);
maxVT = max(validTrials);

% trial count
for cond_i = 1:length(wpms.conditions)
    meanTC(cond_i) = mean(trialCount(cond_i,:));
    SDTC(cond_i) = std(trialCount(cond_i,:));
    minTC(cond_i) = min(trialCount(cond_i,:));
    maxTC(cond_i) = max(trialCount(cond_i,:));
end

savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\CABN\trialInfo.mat'];
