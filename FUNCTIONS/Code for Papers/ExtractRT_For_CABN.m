%Script to extract all RT from DATA -> Montage PAper: 170721

clear all;
tic;
%addpath(genpath('d:\AGEILITY\ComparisonMontage\ACNS_2015_CODE\FUNCTIONS'));

DATA_DIR = 'E:\FNL_EEG_TOOLBOX\REPAIRED_DATA2\';

% listings = dir(DATA_DIR);
% 
% subject_names = {listings(3:end).name};
% sub_codes = cell(1,length(subject_names));
% 
% for name_i = 1:length(subject_names)
%     sub_codes{name_i} = subject_names{name_i}(1:6);
% end
% 
% sub_codes = unique(sub_codes);

%% Initialize variables.
filename = 'E:\FNL_EEG_TOOLBOX\ReferenceOutput\SurfaceLapacian\Participants.txt';
delimiter = '';

%% Format string for each line of text:
%   column1: text (%s)
% For more information, see the TEXTSCAN documentation.
formatSpec = '%s%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r');

%% Read columns of data according to format string.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter,  'ReturnOnError', false);

%% Close the text file.
fclose(fileID);

%% Post processing for unimportable data.
% No unimportable data rules were applied during the import, so no post
% processing code is included. To generate code which works for
% unimportable data, select unimportable cells in a file and regenerate the
% script.

%% Create output variable
sub_codes = [dataArray{1:end-1}];
%% Clear temporary variables
clearvars filename delimiter formatSpec fileID dataArray ans;

conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninf','noninfrepeat','noninfswitch'};

RT_mean_DATA = zeros(length(sub_codes),length(conditions));
RT_median_DATA = RT_mean_DATA;

for name_i = 1:length(sub_codes)
    fprintf('\n%s: ',sub_codes{name_i});
    for cond_i = 1:length(conditions)
        fprintf('. ');
        loaded_data = load([DATA_DIR, sub_codes{name_i},'_RepairedData_',conditions{cond_i},'.mat']);
        varnames = fieldnames(loaded_data);
        RT_DATA = extract_RTvar(loaded_data.(varnames{1}));
        good_inds = RT_DATA<4800;
        RT_mean_DATA(name_i,cond_i) = mean(RT_DATA(good_inds));
        RT_median_DATA(name_i,cond_i) = median(RT_DATA(good_inds));
    end
end
toc;


% Write: File:
fid = fopen('RT_DATA_MVPA_PHASE2_180919.csv','w');
fprintf(fid,'Subject,');
for cond_i = 1:length(conditions)
    fprintf(fid,'RT_MEAN_%s,',conditions{cond_i});
end
for cond_i = 1:length(conditions)
    fprintf(fid,'RT_MEDIAN_%s,',conditions{cond_i});
end
fprintf(fid,'\n');

for name_i = 1:length(sub_codes)
    fprintf(fid,'%s,',sub_codes{name_i});
    %%4.4f,%4.4f,%4.4f,%4.4f,%4.4f,%4.4f\n',,...
    %        RT_mean_DATA(name_i,1),     RT_mean_DATA(name_i,2),     RT_mean_DATA(name_i,3),...
    %        RT_median_DATA(name_i,1),   RT_median_DATA(name_i,2),   RT_median_DATA(name_i,3));
    for cond_i = 1:length(conditions)
        fprintf(fid,'%4.4f,',RT_mean_DATA(name_i,cond_i));
    end
    for cond_i = 1:length(conditions)
        fprintf(fid,'%4.4f,',RT_median_DATA(name_i,cond_i));
    end
    fprintf(fid,'\n');
end
fclose(fid);

%% Accuracy DATA:
