%% create figures for publication
%% ERSP figure
%two time-frequency plots
% 5 topo plots for frontal
% 2 topo plots for parietal
% set up
clear all
clc

ISPCdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\';
SAVEDIR = ISPCdir;
datain = SAVEDIR;
%SAVEDIR = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\';
%comparison_name = cluster_names(~ismember(cluster_names,cluster_names{cluster_i}));
    %savename=[ISPCdir cluster_names{cluster_i} '_to_' comparison_name{1} '_ISPC_cue_alltfspace.mat'];
LOADDIR = SAVEDIR;
%datain = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\';
frequencies = logspace(log10(2),log10(30),80);
times = (-1:0.001953125000000:3.5)*1000;
wpms.dirs.PACKAGES = 'E:\FNL_EEG_TOOLBOX\PACKAGES\';

addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox'))
close all;
Frontal_Labels = {'FCz','FC1','FC2','Cz','C1','C2'};%frontal
Parietal_Labels = {'CPz','CP1','CP2','Pz','P1','P2'};%parietal

CHANNEL_CLUSTER = {Frontal_Labels,Parietal_Labels};

cluster_names = {'frontal','parietal'};
addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));

labels = {'FP1';'AF7';'AF3';'F1';'F3';'F5';'F7';'FT7';'FC5';'FC3'; ...
    'FC1';'C1';'C3';'C5';'T7';'TP7';'CP5';'CP3';'CP1';'P1';'P3'; ...
    'P5';'P7';'P9';'PO7';'PO3';'O1';'Iz';'Oz';'POz';'Pz';'CPz'; ...
    'Fpz';'Fp2';'AF8';'AF4';'AFz';'Fz';'F2';'F4';'F6';'F8';'FT8'; ...
    'FC6';'FC4';'FC2';'FCz';'Cz';'C2';'C4';'C6';'T8';'TP8';'CP6'; ...
    'CP4';'CP2';'P2';'P4';'P6';'P8';'P10';'PO8';'PO4';'O2';'M1'; ...
    'M2';'LO1';'LO2';'SO1';'SO2';'IO1';'IO2';};

cfg = [];
cfg.zlim = [-50.0 50.0];
cfg.colormap = 'jet';
cfg.parameter   = 'avg';
cfg.layout      = 'biosemi64.lay';
cfg.comment   = 'no';
cfg.marker = 'off';
cfg.markersize = 4;
cfg.contournum = 1;
cfg.gridscale = 600;
cfg.highlight = 'on';
cfg.highlightfontsize = 0.5; %no label writing
cfg.highlightsize = 4; %no label writing
cfg.highlightsymbol = '*';
cfg.highlightcolor = [0 0 1]; %blue
height = 1080*0.8;
width = 1920/2;
ylims = [-25 25]; %[AW 28/3/17: ERSP Plot Limits changed to [-2, 3.5], was [-2,10] ]
xrotation = 90;%rotation for bar plot x tick labels
%wpms.conditions         ={'allrepeat',  'mixrepeat',    'noninfrepeat', 'switchto',    'switchaway',   'noninfswitch'};
condname = {'RA','RM','RN','ST','SA','SN'};%reordered and shortened names
%RA vs. RM
%RM vs. RN
%RM vs. ST
%RM vs. SA
%RM vs. SN
%RN vs. SN
%ST vs. SA
%SA vs. SN

x_test_inds = [1 2 2 2 2 3 4 5];
y_test_inds = [2 3 4 5 6 6 5 6];

threshold = 0.001;

% set up fileID for ttests
stat_filename = [ISPCdir 'ANOVA_cluster_stats_ISPC_TARGET.txt'];
fid = fopen(stat_filename,'w');
%% plot ISPC topoclusters (frontal)
cluster_i = 1;
comparison_name = cluster_names(~ismember(cluster_names,cluster_names{cluster_i}));
load ([ISPCdir cluster_names{cluster_i} '_to_' comparison_name{1} '_ISPC_target_alltfspace.mat']);
load ([ISPCdir cluster_names{cluster_i} '_to_' comparison_name{1} '_ISPC_target_alltfspace_ANOVA_results.mat']);
load([ISPCdir 'ISPC_target_cluster_indx.mat']);

figure();set(gcf,'Color',[1 1 1],'Position',[0 0 width height]);


headplot_inds = [8  ];
barplot_inds  = [8  11];%headplot_inds+1;
count = 0;
%fprintf(fid,'%s\n%s\n','ERSP RESULTS','FRONTAL');
for pixel_i = [1 2];
    %fprintf(fid,'%s\t%s\n','Cluster:',num2str(pixel_i));
    count = count+1;
%     subplot(5,6,headplot_inds(count));% topoplot ERSP cluster 1, pixel 1
%     data =[];
%     plot_data = double(squeeze(cluster_fStat_ISPC(cluster_i,pixel_i,:)));
%     plot_p_data = double(squeeze(cluster_pfStat_ISPC(cluster_i,pixel_i,:)));
%     data.avg = zeros(72,1);
%     data.avg = [plot_data;0;0;0;0;0;0;0;0];
%     clear pval_i
%     data.var = zeros(72,1);
%     data.time = 1;
%     data.label = labels;
%     data.dimord = 'chan_time';
%     data.cov = zeros(72,72);
%     
%     
%     plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
%     plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
%     %         data4plot_ind = find(plot_p_data<threshold);
%     %         cfg.highlightchannel = labels(data4plot_ind);
%     cfg.highlightchannel = labels(plot_p_data_corr==1);
%     
%     ft_topoplotER(cfg, data);
    
    %bar plot
    %chan_i = find(ismember(labels,cluster{cluster_i}));
    %mean
%     plot_data = bsxfun(@minus,...
%         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
%         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
    plot_data = ispc_means(:,:,ISPC_clusters.(cluster_names{cluster_i}).PixelIdxList{pixel_i});
    plot_data = squeeze(mean(plot_data,3));
    barplot_data  = plot_data;%[plot_data(:,5),plot_data(:,3),plot_data(:,1),plot_data(:,2),plot_data(:,4)];
    errorbar_data = std(barplot_data)./sqrt(length(barplot_data));
    subplot(5,6,barplot_inds(count));
    bar(mean(barplot_data,1));box off;
    if barplot_inds(count) == 11 %[AW: Left Parietal BarPlot Location]
        ylim(ylims);
    else
        ylim(ylims);
    end
    xlim([0 7]);
    hold on;
    errorbar(1:size(barplot_data,2),mean(barplot_data),errorbar_data,'k.','MarkerEdgeColor','k');axis square;
    set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14);
    rmpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
    for test_i = 1:length(x_test_inds)
        Statement = [];
        x=squeeze(barplot_data(:,x_test_inds(test_i)));
        y=squeeze(barplot_data(:,y_test_inds(test_i)));
        [h,p,~,stats]=ttest(x,y);
        if (mean(x)>mean(y))
            direction = 'larger';
        else
            direction = 'smaller';
        end
        %if h== 1
            if p < .001
                pval_str = ', p < .001';
            else
                pval_str = [', p = ', sprintf('%3.3f',p)];
            end;
        %end
        
        fprintf(fid,'\tERSP Frontal Pixel: %3i, Head_Plot: %3i',pixel_i,count);
        fprintf(fid,'\t%s\t%s\t%s',condname{x_test_inds(test_i)},'vs',condname{y_test_inds(test_i)});
        fprintf(fid,'\t\t%s%i%s%2.2f%s\n','t(',stats.df,') = ',stats.tstat,pval_str);
     
    end
    addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
        
end

%% plot parietal clusters
cluster_i = 2;
headplot_inds = [20];
barplot_inds  = [26];%headplot_inds+1;
count = 0;
for pixel_i = [1];
    count = count+1;
%     subplot(5,6,headplot_inds(count));% topoplot ERSP cluster 1, pixel 1
%     data =[];
%     plot_data = double(squeeze(cluster_fStat_ISPC(cluster_i,pixel_i,:)));
%     plot_p_data = double(squeeze(cluster_pfStat_ISPC(cluster_i,pixel_i,:)));
%     data.avg = zeros(72,1);
%     data.avg = [plot_data;0;0;0;0;0;0;0;0];
%     clear pval_i
%     data.var = zeros(72,1);
%     data.time = 1;
%     data.label = labels;
%     data.dimord = 'chan_time';
%     data.cov = zeros(72,72);
%     
%     %threshold = 0.05;
%     plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
%     plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
%     %         data4plot_ind = find(plot_p_data<threshold);
%     %         cfg.highlightchannel = labels(data4plot_ind);
%     cfg.highlightchannel = labels(plot_p_data_corr==1);
%     
%     ft_topoplotER(cfg, data);
%     
%     chan_i = find(ismember(labels,cluster{cluster_i}));
    %mean
%     plot_data = bsxfun(@minus,...
%         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
%         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
    plot_data = ispc_means(:,:,ISPC_clusters.(cluster_names{cluster_i}).PixelIdxList{pixel_i});
    plot_data = squeeze(mean(plot_data,3));
    barplot_data  = plot_data;%[plot_data(:,5),plot_data(:,3),plot_data(:,1),plot_data(:,2),plot_data(:,4)];
    errorbar_data = std(barplot_data)./sqrt(length(barplot_data));
    subplot(5,6,barplot_inds(count));
    bar(mean(barplot_data,1));box off;
    xlim([0 7]);
    if barplot_inds(count) == 19 %[AW: Left Parietal BarPlot Location]
        ylim(ylims);
    elseif barplot_inds(count) == 16
        ylim(ylims); 
    else
        ylim(ylims);
    end
    hold on;
    errorbar(1:size(barplot_data,2),mean(barplot_data),errorbar_data,'k.','MarkerEdgeColor','k');axis square;
    set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14);
    rmpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
    for test_i = 1:length(x_test_inds)
        Statement = [];
        x=squeeze(barplot_data(:,x_test_inds(test_i)));
        y=squeeze(barplot_data(:,y_test_inds(test_i)));
        [h,p,~,stats]=ttest(x,y);
        if (mean(x)>mean(y))
            direction = 'larger';
        else
            direction = 'smaller';
        end
        %if h== 1
            if p < .001
                pval_str = ', p < .001';
            else
                pval_str = [', p = ', sprintf('%3.3f',p)];
            end;
        %end
        fprintf(fid,'\tERSP Parietal Pixel: %3i, Head_Plot: %3i',pixel_i,count);
        fprintf(fid,'\t%s\t%s\t%s',condname{x_test_inds(test_i)},'vs',condname{y_test_inds(test_i)});
        fprintf(fid,'\t\t%s%i%s%2.2f%s\n','t(',stats.df,') = ',stats.tstat,pval_str);
     
    end
    addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
    if count == 1
        ylabel('ISPC (% change)','FontSize',14);
    end
    
end
%% now plot time-frequency plots
%load ERSP
cluster_name = {'frontal','parietal'};
plot_name    = {sprintf('%s %s','Frontal -> Parietal','ISPC TARGET'),sprintf('%s %s','Parietal -> Frontal', 'ISPC TARGET')};
plot_inds = {[3 4 9 10];[21 22 27 28]};
for cluster_i = 1:length(cluster_name)
    comparison_name = cluster_names(~ismember(cluster_names,cluster_names{cluster_i}));
    load ([ISPCdir cluster_names{cluster_i} '_to_' comparison_name{1} '_ISPC_target_alltfspace_ANOVA_results.mat']);
    % set up plot times - no need to show or correct for additional times
    plottime_start = 700;%start of plot
    plottime_end = 2000;%end of plot
    %find values
    temp_times = times-plottime_start;
    [~,plottimestart_index] = min(abs(temp_times));
    temp_times = times-plottime_end;
    [~,plottimeend_index] = min(abs(temp_times));
    %store in a single variable
    plot_times = plottimestart_index:plottimeend_index;
    % apply fdr correction
    addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'))
    alpha = threshold;
    %ERSP
    [pfStatCorr_ISPC,~]=fdr_bky(cluster_pfStat_ISPC(:,:),alpha);%anova p
    %[prStatCorr_ERSP,~]=fdr_bky(prStat_ERSP(:,plot_times,:),alpha);%corr p

    subplot(5,6,plot_inds{cluster_i});
    contourf(times(plot_times),frequencies,cluster_fStat_ISPC(:,:),50,'linecolor','none');colormap jet;caxis([-50 50]);
    hold on;
    contour(times(plot_times),frequencies,pfStatCorr_ISPC,1,'k');axis square;
    title(plot_name{cluster_i});set(gca,'FontSize',14,'YTick',[2,10,20,29.9],'YTickLabel',[{'2'},{'10'},{'20'},{'30'}]);
end

% %% Start the INDUCED:
% 
% figure();set(gcf,'Color',[1 1 1],'Position',[0 0 width height]);
% 
% cluster_i = 1;
% headplot_inds = [8  ];
% barplot_inds  = [7  ];%headplot_inds+1;
% count = 0;
% fprintf(fid,'%s\n%s\n','INDUCED POWER RESULTS','FRONTAL');
% 
% for pixel_i = [4];
%     %fprintf(fid,'%s\t%s\n','Cluster:',num2str(pixel_i));
%     count = count+1;
%     subplot(5,6,headplot_inds(count));% topoplot ERSP cluster 1, pixel 1
%     data =[];
%     plot_data = double(squeeze(cluster_fStat_INDUCEDPOWER(cluster_i,pixel_i,:)));
%     plot_p_data = double(squeeze(cluster_pfStat_INDUCEDPOWER(cluster_i,pixel_i,:)));
%     data.avg = zeros(72,1);
%     data.avg = [plot_data;0;0;0;0;0;0;0;0];
%     clear pval_i
%     data.var = zeros(72,1);
%     data.time = 1;
%     data.label = labels;
%     data.dimord = 'chan_time';
%     data.cov = zeros(72,72);
%     
%     
%     plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
%     plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
%     %         data4plot_ind = find(plot_p_data<threshold);
%     %         cfg.highlightchannel = labels(data4plot_ind);
%     cfg.highlightchannel = labels(plot_p_data_corr==1);
%     
%     ft_topoplotER(cfg, data);
%     
%     %bar plot
%     chan_i = find(ismember(labels,cluster{cluster_i}));
%     %mean
% %     plot_data = bsxfun(@minus,...
% %         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
% %         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
%     plot_data = INDUCEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:5);
%     plot_data = squeeze(mean(plot_data,3));
%     barplot_data  = [plot_data(:,5),plot_data(:,3),plot_data(:,1),plot_data(:,2),plot_data(:,4)];
%     errorbar_data = std(barplot_data)./sqrt(length(barplot_data));
%     subplot(5,6,barplot_inds(count));
%     %fprintf('Induced Means:\n');
%     %mean(barplot_data,1);
%     bar(mean(barplot_data,1));
%     box off;
%     if barplot_inds(count) == 7 %[AW: Left Parietal BarPlot Location]
%         ylim([-1 1]);
%     else
%         ylim(ylims);
%     end
%     xlim([0 6]);
%     hold on;
%     errorbar(1:size(barplot_data,2),mean(barplot_data),errorbar_data,'k.','MarkerEdgeColor','k');axis square;
%     set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14);
%     rmpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
%     for test_i = 1:length(x_test_inds)
%         Statement = [];
%         x=squeeze(barplot_data(:,x_test_inds(test_i)));
%         y=squeeze(barplot_data(:,y_test_inds(test_i)));
%         [h,p,~,stats]=ttest(x,y);
%         if (mean(x)>mean(y))
%             direction = 'larger';
%         else
%             direction = 'smaller';
%         end
%         %if h== 1
%             if p < .001
%                 pval_str = ', p < .001';
%             else
%                 pval_str = [', p = ', sprintf('%3.3f',p)];
%             end;
%         %end
%         
%         fprintf(fid,'\tInduced Power Frontal Pixel: %3i, Head_Plot: %3i',pixel_i,count);
%         fprintf(fid,'\t%s\t%s\t%s',condname{x_test_inds(test_i)},'vs',condname{y_test_inds(test_i)});
%         fprintf(fid,'\t\t%s%i%s%2.2f%s\n','t(',stats.df,') = ',stats.tstat,pval_str);
%      
%     end
%     addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
%         
% end
% 
% %% plot parietal clusters
% cluster_i = 2;
% headplot_inds = [20 26 29];
% barplot_inds  = [19 25 30];%headplot_inds+1;
% count = 0;
% for pixel_i = [4];
%     count = count+1;
%     subplot(5,6,headplot_inds(count));% topoplot ERSP cluster 1, pixel 1
%     data =[];
%     plot_data = double(squeeze(cluster_fStat_INDUCEDPOWER(cluster_i,pixel_i,:)));
%     plot_p_data = double(squeeze(cluster_pfStat_INDUCEDPOWER(cluster_i,pixel_i,:)));
%     data.avg = zeros(72,1);
%     data.avg = [plot_data;0;0;0;0;0;0;0;0];
%     clear pval_i
%     data.var = zeros(72,1);
%     data.time = 1;
%     data.label = labels;
%     data.dimord = 'chan_time';
%     data.cov = zeros(72,72);
%     
%     %threshold = 0.05;
%     plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
%     plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
%     %         data4plot_ind = find(plot_p_data<threshold);
%     %         cfg.highlightchannel = labels(data4plot_ind);
%     cfg.highlightchannel = labels(plot_p_data_corr==1);
%     
%     ft_topoplotER(cfg, data);
%     
%     chan_i = find(ismember(labels,cluster{cluster_i}));
%     %mean
% %     plot_data = bsxfun(@minus,...
% %         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
% %         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
%     plot_data =INDUCEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:5);
%     plot_data = squeeze(mean(plot_data,3));
%     barplot_data  = [plot_data(:,5),plot_data(:,3),plot_data(:,1),plot_data(:,2),plot_data(:,4)];
%     errorbar_data = std(barplot_data)./sqrt(length(barplot_data));
%     subplot(5,6,barplot_inds(count));
%     fprintf('Induced Means:\n');
%     mean(barplot_data,1)
%     bar(mean(barplot_data,1));
%     
%     box off;
%     xlim([0 6]);
%     if barplot_inds(count) == 19 %[AW: Left Parietal BarPlot Location]
%         ylim([-0.1 1]);
%     elseif barplot_inds(count) == 16
%         ylim([-4.5 1]); 
%     else
%         ylim(ylims);
%     end
%     hold on;
%     errorbar(1:size(barplot_data,2),mean(barplot_data),errorbar_data,'k.','MarkerEdgeColor','k');axis square;
%     set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14);
%     rmpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
%     for test_i = 1:length(x_test_inds)
%         Statement = [];
%         x=squeeze(barplot_data(:,x_test_inds(test_i)));
%         y=squeeze(barplot_data(:,y_test_inds(test_i)));
%         [h,p,~,stats]=ttest(x,y);
%         if (mean(x)>mean(y))
%             direction = 'larger';
%         else
%             direction = 'smaller';
%         end
%         %if h== 1
%             if p < .001
%                 pval_str = ', p < .001';
%             else
%                 pval_str = [', p = ', sprintf('%3.3f',p)];
%             end;
%         %end
%         fprintf(fid,'\tINDUCED Parietal Pixel: %3i, Head_Plot: %3i',pixel_i,count);
%         fprintf(fid,'\t%s\t%s\t%s',condname{x_test_inds(test_i)},'vs',condname{y_test_inds(test_i)});
%         fprintf(fid,'\t\t%s%i%s%2.2f%s\n','t(',stats.df,') = ',stats.tstat,pval_str);
%      
%     end
%     addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
%     if count == 1
%         ylabel('Power (dB)','FontSize',14);
%     end
%     
% end
% %% now plot time-frequency plots
% %load ERSP
% cluster_name = {'frontal','parietal'};
% plot_name    = {sprintf('%s %s','Frontal','Induced Power'),sprintf('%s %s','Parietal', 'Induced Power')};
% plot_inds = {[3 4 9 10];[21 22 27 28]};
% for cluster_i = 1:length(cluster_name)
%     savename_fstat  = [datain 'fstat_INDUCEDPOWER_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_fstat);
%     savename_pfstat = [datain 'pfstat_INDUCEDPOWER_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_pfstat);
%     % set up plot times - no need to show or correct for additional times
%     plottime_start = 700;%start of plot
%     plottime_end =2000;%end of plot
%     %find values
%     temp_times = times-plottime_start;
%     [~,plottimestart_index] = min(abs(temp_times));
%     temp_times = times-plottime_end;
%     [~,plottimeend_index] = min(abs(temp_times));
%     %store in a single variable
%     plot_times = plottimestart_index:plottimeend_index;
%     % apply fdr correction
%     addpath(genpath([wpms.dirs.PACKAGES 'mass_uni_toolbox']))
%     alpha = threshold;
%     %ERSP
%     [pfStatCorr_INDUCEDPOWER,~]=fdr_bky(pfStat_INDUCEDPOWER(:,plot_times),alpha);%anova p
%     %[prStatCorr_ERSP,~]=fdr_bky(prStat_ERSP(:,plot_times,:),alpha);%corr p
% 
%     subplot(5,6,plot_inds{cluster_i});
%     contourf(times(plot_times),frequencies,fStat_INDUCEDPOWER(:,plot_times),50,'linecolor','none');colormap jet;caxis([-50 50]);
%     hold on;
%     contour(times(plot_times),frequencies,pfStatCorr_INDUCEDPOWER,1,'k');axis square;
%     title(plot_name{cluster_i});set(gca,'FontSize',14,'YTick',[2,10,20,29.9],'YTickLabel',[{'2'},{'10'},{'20'},{'30'}]);
% end
% 
% %% EVOKED POWER:
% 
% 
% figure();set(gcf,'Color',[1 1 1],'Position',[0 0 width height]);
% 
% cluster_i = 1;
% headplot_inds = [8  ];
% barplot_inds  = [7  ];%headplot_inds+1;
% count = 0;
% fprintf(fid,'%s\n%s\n','EVOKED POWER RESULTS','FRONTAL');
% 
% for pixel_i = [3];
%     %fprintf(fid,'%s\t%s\n','Cluster:',num2str(pixel_i));
%     count = count+1;
%     subplot(5,6,headplot_inds(count));% topoplot ERSP cluster 1, pixel 1
%     data =[];
%     plot_data = double(squeeze(cluster_fStat_EVOKEDPOWER(cluster_i,pixel_i,:)));
%     plot_p_data = double(squeeze(cluster_pfStat_EVOKEDPOWER(cluster_i,pixel_i,:)));
%     data.avg = zeros(72,1);
%     data.avg = [plot_data;0;0;0;0;0;0;0;0];
%     clear pval_i
%     data.var = zeros(72,1);
%     data.time = 1;
%     data.label = labels;
%     data.dimord = 'chan_time';
%     data.cov = zeros(72,72);
%     
%     
%     plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
%     plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
%     %         data4plot_ind = find(plot_p_data<threshold);
%     %         cfg.highlightchannel = labels(data4plot_ind);
%     cfg.highlightchannel = labels(plot_p_data_corr==1);
%     
%     ft_topoplotER(cfg, data);
%     
%     %bar plot
%     chan_i = find(ismember(labels,cluster{cluster_i}));
%     %mean
% %     plot_data = bsxfun(@minus,...
% %         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
% %         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
%     plot_data = EVOKEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:5);
%     plot_data = squeeze(mean(plot_data,3));
%     barplot_data  = [plot_data(:,5),plot_data(:,3),plot_data(:,1),plot_data(:,2),plot_data(:,4)];
%     errorbar_data = std(barplot_data)./sqrt(length(barplot_data));
%     subplot(5,6,barplot_inds(count));
%     bar(mean(barplot_data,1));
%     box off;
%     if barplot_inds(count) == 7 %[AW: Left Parietal BarPlot Location]
%         ylim([6 11]);
%     else
%         ylim(ylims);
%     end
%     xlim([0 6]);
%     hold on;
%     errorbar(1:size(barplot_data,2),mean(barplot_data),errorbar_data,'k.','MarkerEdgeColor','k');axis square;
%     set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14);
%     rmpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
%     for test_i = 1:length(x_test_inds)
%         Statement = [];
%         x=squeeze(barplot_data(:,x_test_inds(test_i)));
%         y=squeeze(barplot_data(:,y_test_inds(test_i)));
%         [h,p,~,stats]=ttest(x,y);
%         if (mean(x)>mean(y))
%             direction = 'larger';
%         else
%             direction = 'smaller';
%         end
%         %if h== 1
%             if p < .001
%                 pval_str = ', p < .001';
%             else
%                 pval_str = [', p = ', sprintf('%3.3f',p)];
%             end;
%         %end
%         
%         fprintf(fid,'\tEVOKED Power Frontal Pixel: %3i, Head_Plot: %3i',pixel_i,count);
%         fprintf(fid,'\t%s\t%s\t%s',condname{x_test_inds(test_i)},'vs',condname{y_test_inds(test_i)});
%         fprintf(fid,'\t\t%s%i%s%2.2f%s\n','t(',stats.df,') = ',stats.tstat,pval_str);
%      
%     end
%     addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
%         
% end
% 
% %% plot parietal clusters
% cluster_i = 2;
% headplot_inds = [20 26 29];
% barplot_inds  = [19 25 30];%headplot_inds+1;
% count = 0;
% for pixel_i = [2];
%     count = count+1;
%     subplot(5,6,headplot_inds(count));% topoplot ERSP cluster 1, pixel 1
%     data =[];
%     plot_data = double(squeeze(cluster_fStat_EVOKEDPOWER(cluster_i,pixel_i,:)));
%     plot_p_data = double(squeeze(cluster_pfStat_EVOKEDPOWER(cluster_i,pixel_i,:)));
%     data.avg = zeros(72,1);
%     data.avg = [plot_data;0;0;0;0;0;0;0;0];
%     clear pval_i
%     data.var = zeros(72,1);
%     data.time = 1;
%     data.label = labels;
%     data.dimord = 'chan_time';
%     data.cov = zeros(72,72);
%     
%     %threshold = 0.05;
%     plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
%     plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
%     %         data4plot_ind = find(plot_p_data<threshold);
%     %         cfg.highlightchannel = labels(data4plot_ind);
%     cfg.highlightchannel = labels(plot_p_data_corr==1);
%     
%     ft_topoplotER(cfg, data);
%     
%     chan_i = find(ismember(labels,cluster{cluster_i}));
%     %mean
% %     plot_data = bsxfun(@minus,...
% %         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
% %         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
%     plot_data =EVOKEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:5);
%     plot_data = squeeze(mean(plot_data,3));
%     barplot_data  = [plot_data(:,5),plot_data(:,3),plot_data(:,1),plot_data(:,2),plot_data(:,4)];
%     errorbar_data = std(barplot_data)./sqrt(length(barplot_data));
%     subplot(5,6,barplot_inds(count));
%     bar(mean(barplot_data,1));box off;
%     xlim([0 6]);
%     if barplot_inds(count) == 19 %[AW: Left Parietal BarPlot Location]
%         ylim([10 13.5]);
%     elseif barplot_inds(count) == 16
%         ylim([-4.5 1]); 
%     else
%         ylim(ylims);
%     end
%     hold on;
%     errorbar(1:size(barplot_data,2),mean(barplot_data),errorbar_data,'k.','MarkerEdgeColor','k');axis square;
%     set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14);
%     rmpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
%     for test_i = 1:length(x_test_inds)
%         Statement = [];
%         x=squeeze(barplot_data(:,x_test_inds(test_i)));
%         y=squeeze(barplot_data(:,y_test_inds(test_i)));
%         [h,p,~,stats]=ttest(x,y);
%         if (mean(x)>mean(y))
%             direction = 'larger';
%         else
%             direction = 'smaller';
%         end
%         %if h== 1
%             if p < .001
%                 pval_str = ', p < .001';
%             else
%                 pval_str = [', p = ', sprintf('%3.3f',p)];
%             end;
%         %end
%         fprintf(fid,'\tEVOKED Parietal Pixel: %3i, Head_Plot: %3i',pixel_i,count);
%         fprintf(fid,'\t%s\t%s\t%s',condname{x_test_inds(test_i)},'vs',condname{y_test_inds(test_i)});
%         fprintf(fid,'\t\t%s%i%s%2.2f%s\n','t(',stats.df,') = ',stats.tstat,pval_str);
%      
%     end
%     addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
%     if count == 1
%         ylabel('Power (dB)','FontSize',14);
%     end
%     
% end
% %% now plot time-frequency plots
% %load ERSP
% cluster_name = {'frontal','parietal'};
% plot_name    = {sprintf('%s %s','Frontal','EVOKED Power'),sprintf('%s %s','Parietal', 'EVOKED Power')};
% plot_inds = {[3 4 9 10];[21 22 27 28]};
% for cluster_i = 1:length(cluster_name)
%     savename_fstat  = [datain 'fstat_EVOKEDPOWER_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_fstat);
%     savename_pfstat = [datain 'pfstat_EVOKEDPOWER_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_pfstat);
%     % set up plot times - no need to show or correct for additional times
%     plottime_start = 700;%start of plot
%     plottime_end =2000;%end of plot
%     %find values
%     temp_times = times-plottime_start;
%     [~,plottimestart_index] = min(abs(temp_times));
%     temp_times = times-plottime_end;
%     [~,plottimeend_index] = min(abs(temp_times));
%     %store in a single variable
%     plot_times = plottimestart_index:plottimeend_index;
%     % apply fdr correction
%     addpath(genpath([wpms.dirs.PACKAGES 'mass_uni_toolbox']))
%     alpha = threshold;
%     %ERSP
%     [pfStatCorr_EVOKEDPOWER,~]=fdr_bky(pfStat_EVOKEDPOWER(:,plot_times),alpha);%anova p
%     %[prStatCorr_ERSP,~]=fdr_bky(prStat_ERSP(:,plot_times,:),alpha);%corr p
% 
%     subplot(5,6,plot_inds{cluster_i});
%     contourf(times(plot_times),frequencies,fStat_EVOKEDPOWER(:,plot_times),50,'linecolor','none');colormap jet;caxis([-50 50]);
%     hold on;
%     contour(times(plot_times),frequencies,pfStatCorr_EVOKEDPOWER,1,'k');axis square;
%     title(plot_name{cluster_i});set(gca,'FontSize',14,'YTick',[2,10,20,29.9],'YTickLabel',[{'2'},{'10'},{'20'},{'30'}]);
% end
% 
% 
% 
% % %% %% ITPC figure
% % %two time-frequency plots
% % % 2 topo plots for frontal
% % % 2 topo plots for parietal
% % % set up
% % 
% % labels = {'FP1';'AF7';'AF3';'F1';'F3';'F5';'F7';'FT7';'FC5';'FC3'; ...
% %     'FC1';'C1';'C3';'C5';'T7';'TP7';'CP5';'CP3';'CP1';'P1';'P3'; ...
% %     'P5';'P7';'P9';'PO7';'PO3';'O1';'Iz';'Oz';'POz';'Pz';'CPz'; ...
% %     'Fpz';'Fp2';'AF8';'AF4';'AFz';'Fz';'F2';'F4';'F6';'F8';'FT8'; ...
% %     'FC6';'FC4';'FC2';'FCz';'Cz';'C2';'C4';'C6';'T8';'TP8';'CP6'; ...
% %     'CP4';'CP2';'P2';'P4';'P6';'P8';'P10';'PO8';'PO4';'O2';'M1'; ...
% %     'M2';'LO1';'LO2';'SO1';'SO2';'IO1';'IO2';};
% % 
% % cfg = [];
% % cfg.zlim = [-50.0 50.0];
% % cfg.colormap = 'jet';
% % cfg.parameter   = 'avg';
% % cfg.layout      = 'biosemi64.lay';
% % cfg.comment   = 'no';
% % cfg.marker = 'off';
% % cfg.markersize = 4;
% % cfg.contournum = 1;
% % cfg.gridscale = 600;
% % cfg.highlight = 'on';
% % cfg.highlightfontsize = 1; %no label writing
% % cfg.highlightsymbol = '*';
% % cfg.highlightsize = 4; %no label writing
% % cfg.highlightcolor = [0 0 1]; %blue
% % %% plot ITPC topoclusters (frontal)
% % figure();set(gcf,'Color',[1 1 1],'Position',[0 0 width height]);
% % ylims = [0 0.15]; %[AW 28/3/2017: ylimits to bar plots changed from -0.025 to 0.01]
% % cluster_i = 1;
% % headplot_inds = [1 4];
% % barplot_inds =  [5 8];%headplot_inds+1;
% % count = 0;
% % for pixel_i = [9 10];
% %     count = count+1;
% %     subplot(5,6,headplot_inds(count));% topoplot ITPC cluster 1, pixel 1
% %     data =[];
% %     plot_data = double(squeeze(cluster_fStat_ITPC(cluster_i,pixel_i,:)));
% %     plot_p_data = double(squeeze(cluster_pfStat_ITPC(cluster_i,pixel_i,:)));
% %     data.avg = zeros(72,1);
% %     data.avg = [plot_data;0;0;0;0;0;0;0;0];
% %     clear pval_i
% %     data.var = zeros(72,1);
% %     data.time = 1;
% %     data.label = labels;
% %     data.dimord = 'chan_time';
% %     data.cov = zeros(72,72);
% %     
% %     %threshold = 0.05;
% %     plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
% %     plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
% %     %         data4plot_ind = find(plot_p_data<threshold);
% %     %         cfg.highlightchannel = labels(data4plot_ind);
% %     cfg.highlightchannel = labels(plot_p_data_corr==1);
% %     
% %     ft_topoplotER(cfg, data);
% %     
% %     %bar plot
% %     chan_i = find(ismember(labels,cluster{cluster_i}));
% %     %mean
% % %     plot_data = bsxfun(@minus,...
% % %         ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
% % %         ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
% %     plot_data = ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:5);
% %     plot_data = squeeze(mean(plot_data,3));
% %     barplot_data  = [plot_data(:,5),plot_data(:,3),plot_data(:,1),plot_data(:,2),plot_data(:,4)];
% %     errorbar_data = std(barplot_data)./sqrt(length(barplot_data));
% %     subplot(5,6,barplot_inds(count));
% %     bar(mean(barplot_data,1));box off;ylim(ylims);hold on;
% %     xlim([0 6]);
% %     errorbar(1:size(barplot_data,2),mean(barplot_data),errorbar_data,'k.','MarkerEdgeColor','k');axis square;
% %     %set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14,'YTickLabel',{'0','.05','.10'});
% %     set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14);
% %     rmpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
% %     for test_i = 1:length(x_test_inds)
% %         
% %         x=squeeze(barplot_data(:,x_test_inds(test_i)));
% %         y=squeeze(barplot_data(:,y_test_inds(test_i)));
% %         [h,p,~,stats]=ttest(x,y);
% %         if (mean(x)>mean(y))
% %             direction = 'larger';
% %         else
% %             direction = 'smaller';
% %         end
% %         %if h== 1
% %             if p < .001
% %                 pval_str = ', p < .001';
% %             else
% %                 pval_str = [', p = ', sprintf('%3.3f',p)];
% %             end;
% %         %end
% %         
% %         fprintf(fid,'\tITPC Frontal Pixel: %3i, Head_Plot: %3i',pixel_i,count);
% %         fprintf(fid,'\t%s\t%s\t%s',condname{x_test_inds(test_i)},'vs',condname{y_test_inds(test_i)});
% %         fprintf(fid,'\t\t%s%i%s%2.2f%s\n','t(',stats.df,') = ',stats.tstat,pval_str);
% %      
% %     end
% %     addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
% % end
% % 
% % %% plot parietal clusters
% % cluster_i = 2;
% % headplot_inds = [9 12];
% % barplot_inds = [13 16];%headplot_inds+1;
% % count = 0;
% % 
% % ylims = [0 0.16];
% % 
% % for pixel_i = [12 23];
% %     count = count+1;
% %     subplot(5,6,headplot_inds(count));% topoplot ERSP cluster 1, pixel 1
% %     data =[];
% %     plot_data = double(squeeze(cluster_fStat_ITPC(cluster_i,pixel_i,:)));
% %     plot_p_data = double(squeeze(cluster_pfStat_ITPC(cluster_i,pixel_i,:)));
% %     data.avg = zeros(72,1);
% %     data.avg = [plot_data;0;0;0;0;0;0;0;0];
% %     clear pval_i
% %     data.var = zeros(72,1);
% %     data.time = 1;
% %     data.label = labels;
% %     data.dimord = 'chan_time';
% %     data.cov = zeros(72,72);
% %     
% %     %threshold = 0.05;
% %     plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
% %     plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
% %     %         data4plot_ind = find(plot_p_data<threshold);
% %     %         cfg.highlightchannel = labels(data4plot_ind);
% %     cfg.highlightchannel = labels(plot_p_data_corr==1);
% %     
% %     ft_topoplotER(cfg, data);
% %         %bar plot
% %     chan_i = find(ismember(labels,cluster{cluster_i}));
% %     %mean
% % %     plot_data = bsxfun(@minus,...
% % %         ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
% % %         ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
% %     plot_data = ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:5);
% %     plot_data = squeeze(mean(plot_data,3));
% %     barplot_data  = [plot_data(:,5),plot_data(:,3),plot_data(:,1),plot_data(:,2),plot_data(:,4)];
% %     errorbar_data = std(barplot_data)./sqrt(length(barplot_data));
% %     subplot(5,6,barplot_inds(count));
% %     bar(mean(barplot_data,1));box off; xlim([0 6])
% % %      if barplot_inds(count) == 25 %[AW 28/3/2017: Left Parietal BarImage]
% %          ylim(ylims);
% %          set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14); %,'YTickLabel',{'0','.05','.10','.15'}
% % %      else
% % %         ylim(ylims);
% % %         set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14,'YTickLabel',{'0','.05','.10'});
% % %      end
% %     hold on;
% %     errorbar(1:size(barplot_data,2),mean(barplot_data),errorbar_data,'k.','MarkerEdgeColor','k');axis square;
% %     
% %     
% %     rmpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
% %     for test_i = 1:length(x_test_inds)
% %         
% %         x=squeeze(barplot_data(:,x_test_inds(test_i)));
% %         y=squeeze(barplot_data(:,y_test_inds(test_i)));
% %         [h,p,~,stats]=ttest(x,y);
% %         if (mean(x)>mean(y))
% %             direction = 'larger';
% %         else
% %             direction = 'smaller';
% %         end
% %         %if h== 1
% %             if p < .001
% %                 pval_str = ', p < .001';
% %             else
% %                 pval_str = [', p = ', sprintf('%3.3f',p)];
% %             end;
% %         %end
% %         
% %         fprintf(fid,'\tITPC Parietal Pixel: %3i, Head_Plot: %3i',pixel_i,count);
% %         fprintf(fid,'\t%s\t%s\t%s',condname{x_test_inds(test_i)},'vs',condname{y_test_inds(test_i)});
% %         fprintf(fid,'\t\t%s%i%s%2.2f%s\n','t(',stats.df,') = ',stats.tstat,pval_str);
% %      
% %     end
% %     addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
% %      if count == 1
% %         ylabel('ITPC','FontSize',14);
% %     end
% % end
% % fclose(fid);
% % %% now plot time-frequency plots
% % %load ERSP
% % cluster_name = {'frontal','parietal'};
% % plot_name    = {sprintf('%s %s','Frontal','ITPC'),sprintf('%s %s','Parietal', 'ITPC')};
% % plot_inds = {[2 3 6 7];[10 11 14 15]};
% % for cluster_i = 1:length(cluster_name)
% %     savename_fstat  = [datain 'fstat_ITPC_noAR_' cluster_name{cluster_i} '_20170426.mat'];load(savename_fstat);
% %     savename_pfstat = [datain 'pfstat_ITPC_noAR_' cluster_name{cluster_i} '_20170426.mat'];load(savename_pfstat);
% %     % set up plot times - no need to show or correct for additional times
% %     plottime_start = -300;%start of plot
% %     plottime_end = 1100;%end of plot
% %     %find values
% %     temp_times = times-plottime_start;
% %     [~,plottimestart_index] = min(abs(temp_times));
% %     temp_times = times-plottime_end;
% %     [~,plottimeend_index] = min(abs(temp_times));
% %     %store in a single variable
% %     plot_times = plottimestart_index:plottimeend_index;
% %     % apply fdr correction
% %     addpath(genpath([wpms.dirs.PACKAGES 'mass_uni_toolbox']))
% %     alpha = threshold;
% %     %ERSP
% %     [pfStatCorr_ITPC,~]=fdr_bky(pfStat_ITPC(:,plot_times),alpha);%anova p
% %     %[prStatCorr_ITPC,~]=fdr_bky(prStat_ITPC(:,plot_times,:),alpha);%corr p
% % 
% %     subplot(5,6,plot_inds{cluster_i});
% %     contourf(times(plot_times),frequencies,fStat_ITPC(:,plot_times),50,'linecolor','none');colormap jet;caxis([-50 50]);
% %     hold on;
% %     contour(times(plot_times),frequencies,pfStatCorr_ITPC,1,'k');axis square;
% %     title(plot_name{cluster_i});set(gca,'FontSize',14,'YTick',[2,10,20,29.9],'YTickLabel',[{'2'},{'10'},{'20'},{'30'}]);
% % %     if cluster_i ==2;
% % %         ylabel('             Frequency (Hz)','FontSize',16);
% % %         xlabel('Time (ms)','FontSize',16);
% % %     end
% % end