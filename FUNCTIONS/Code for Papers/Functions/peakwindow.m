function [totalpeaks, nonphasepeaks, phasepeaks] = peakwindow(Location, loadname, width, height, wpms, electrodesofinterest, analysistype, epoch, conditionbaseline, plottimes,partinds)

totaldata = zeros(height+1,length(wpms.times),length(wpms.labels),length(wpms.names),length(wpms.epochconditions.(epoch){1}),'single');
nonphasedata = zeros(height+1,length(wpms.times),length(wpms.labels),length(wpms.names),length(wpms.epochconditions.(epoch){1}),'single');
phasedata = zeros(height+1,length(wpms.times),length(wpms.labels),length(wpms.names),length(wpms.epochconditions.(epoch){1}),'single');

freqmax = Location(1);
timemax = Location(2);
count = 1;
for freq_i = freqmax-height/2:1:freqmax+height/2
    tic;
    fprintf('\t\tWorking on Frequency Index: %i\n',freq_i );
    A=load([loadname{1} num2str(freq_i) '.mat']);
    fname = fieldnames(A);
    data = A.(fname{1})(:,:,:,partinds,:);clear A fname;
    B=load([loadname{2} num2str(freq_i) '.mat']);
    fname = fieldnames(B);
    data2 = B.(fname{1})(:,:,:,partinds,:);clear B fname;
    [totalbl, nonphasebl, phasebl] = baselinecorrect(wpms, data, data2, electrodesofinterest, analysistype, conditionbaseline, epoch);
    totaldata(count,:,:,:,:) = totalbl;
    nonphasedata(count,:,:,:,:) = nonphasebl;
    phasedata(count,:,:,:,:) = phasebl;
    count = count+1;
    toc
end
fprintf('\tAveraging over Frequencing and Time\n' );
timewindows = plottimes(timemax-width/2):1:plottimes(timemax+width/2);
totalpeaks = squeeze(mean(mean(totaldata(:,timewindows,:,:,:),1),2));%squeeze(mean(squeeze(mean(totaldata(:,timewindows,:,:,:),1)),1));
nonphasepeaks = squeeze(mean(mean(nonphasedata(:,timewindows,:,:,:),1),2));
phasepeaks = squeeze(mean(mean(phasedata(:,timewindows,:,:,:),1),2));
