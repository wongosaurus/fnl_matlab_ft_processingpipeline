function plotdata(peakdata, wpms, ytitle, xtitle, ylimits, flimits, xticks, epoch, threshold, electrodesofinterest, titlestr)

%bar plot
figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);subplot(1,2,1);bar(squeeze(mean(mean(peakdata(electrodesofinterest,:,:),1),2)));
axis square; box off;
ylabel(ytitle);
xlabel(xtitle);
ylim(ylimits);
set(gca,'XTickLabels',xticks);
title(titlestr);

%topoplot
pvalue = zeros(size(peakdata,1),1);
fstat = pvalue;

% testdata = squeeze(mean(peakdata(:,:,:),2));
for chan_i = 1:length(pvalue)
    [pvalue(chan_i), fstat(chan_i)] = runpoweranova(squeeze(peakdata(chan_i,:,:)),epoch);
end

plotdata = fstat;
plot_p_data_corr = fdr_bky(pvalue,threshold);%apply fdr correction
plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
cfg = [];
data =[];
data.avg = zeros(72,1);
data.avg = [plotdata;0;0;0;0;0;0;0;0];
data.var = zeros(72,1);
data.time = 1;
data.label = wpms.labels;
data.dimord = 'chan_time';
data.cov = zeros(72,72);
cfg.highlightchannel = wpms.labels(plot_p_data_corr==1);
cfg.layout = 'biosemi64.lay';
cfg.zlim = flimits;
cfg.colormap = 'jet';
cfg.contournum = 2;
cfg.comment = 'no';
subplot(1,2,2);ft_topoplotER(cfg, data);
title(titlestr);axis square;box off;

