function plotdata_newanova(peakdata_phase, peakdata_nonphase, plotdata,pvalue,wpms, ytitle, xtitle, ylimits, flimits, xticks, epoch, threshold, electrodesofinterest, titlestr)

%bar plot
figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);
subplot(2,2,2);bar(squeeze(mean(mean(peakdata_phase(electrodesofinterest,:,:),1),2)));
axis square; box off;
ylabel(ytitle);
xlabel(xtitle);
%ylim(ylimits);
set(gca,'XTickLabels',xticks);
title(['Phase ',titlestr]);

subplot(2,2,4);bar(squeeze(mean(mean(peakdata_nonphase(electrodesofinterest,:,:),1),2)));
axis square; box off;
ylabel(ytitle);
xlabel(xtitle);
%ylim(ylimits);
set(gca,'XTickLabels',xticks);
title(['Non-Phase ',titlestr]);

plot_p_data_corr = fdr_bky(pvalue,threshold);%apply fdr correction
plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
cfg = [];
data =[];
data.avg = zeros(72,1);
data.avg = [plotdata;0;0;0;0;0;0;0;0];
data.var = zeros(72,1);
data.time = 1;
data.label = wpms.labels;
data.dimord = 'chan_time';
data.cov = zeros(72,72);
cfg.highlightchannel = wpms.labels(plot_p_data_corr==1);
cfg.layout = 'biosemi64.lay';
cfg.zlim = flimits;
cfg.colormap = 'jet';
cfg.contournum = 2;
cfg.comment = 'no';

cfg.gridscale = 300;
subplot(2,2,[1,3]);ft_topoplotER(cfg, data);
title(titlestr);axis square;box off;

