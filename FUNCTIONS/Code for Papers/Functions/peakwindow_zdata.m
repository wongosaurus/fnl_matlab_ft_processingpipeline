function [totalpeaks, nonphasepeaks, phasepeaks] = peakwindow_zdata(Location, loadname, width, height, wpms, electrodesofinterest, analysistype, epoch, conditionbaseline, plottimes,partinds)

totaldata = zeros(height+1,length(wpms.times),length(wpms.labels),length(wpms.names),length(wpms.epochconditions.(epoch){1}),'single');
nonphasedata = zeros(height+1,length(wpms.times),length(wpms.labels),length(wpms.names),length(wpms.epochconditions.(epoch){1}),'single');
phasedata = zeros(height+1,length(wpms.times),length(wpms.labels),length(wpms.names),length(wpms.epochconditions.(epoch){1}),'single');

freqmax = Location(1);
timemax = Location(2);
count = 1;
for freq_i = freqmax-height/2:1:freqmax+height/2
    tic;
    fprintf('\t\tWorking on Frequency Index: %i\n',freq_i );
    A=load([loadname{1} num2str(freq_i) '.mat']);
    fname = fieldnames(A);
    data = A.(fname{1})(:,:,:,partinds,:);clear A fname;
    B=load([loadname{2} num2str(freq_i) '.mat']);
    fname = fieldnames(B);
    data2 = B.(fname{1})(:,:,:,partinds,:);clear B fname;
    [totalbl, nonphasebl, phasebl] = baselinecorrect(wpms, data, data2, electrodesofinterest, analysistype, conditionbaseline, epoch);
    totaldata(count,:,:,:,:) = totalbl;
    nonphasedata(count,:,:,:,:) = nonphasebl;
    phasedata(count,:,:,:,:) = phasebl;
    count = count+1;
    toc
end
fprintf('\tAveraging over Frequencing and Time\n' );
timewindows = plottimes(timemax-width/2):1:plottimes(timemax+width/2);

z_phase = phasedata;
z_nonphase = nonphasedata;

for count_i = 1:size(z_phase,1)
    for time_i = 1:size(z_phase,2)
        for chann_i = 1:size(z_phase,3)
            for cond_i = 1:size(z_phase,5)
                %phase
                avTF_phase = mean(squeeze(phasedata(count_i,time_i,chann_i,:,cond_i)));
                sdTF_phase  = std(squeeze(phasedata(count_i,time_i,chann_i,:,cond_i)));
                z_phase(count_i,time_i,chann_i,:,cond_i) = squeeze((phasedata(count_i,time_i,chann_i,:,cond_i)-avTF_phase)./sdTF_phase);
                clear avTF_phase sdTF_phase
                %nonphase
                avTF_nonphase = mean(squeeze(nonphasedata(count_i,time_i,chann_i,:,cond_i)));
                sdTF_nonphase  = std(squeeze(nonphasedata(count_i,time_i,chann_i,:,cond_i)));
                z_nonphase(count_i,time_i,chann_i,:,cond_i) = squeeze((nonphasedata(count_i,time_i,chann_i,:,cond_i)-avTF_nonphase)./sdTF_nonphase);
                clear avTF_nonphase sdTF_nonphase
            end
        end
    end
end
phasedata=z_phase;
nonphasedata=z_nonphase;

totalpeaks = squeeze(mean(mean(totaldata(:,timewindows,:,:,:),1),2));%squeeze(mean(squeeze(mean(totaldata(:,timewindows,:,:,:),1)),1));
nonphasepeaks = squeeze(mean(mean(nonphasedata(:,timewindows,:,:,:),1),2));
phasepeaks = squeeze(mean(mean(phasedata(:,timewindows,:,:,:),1),2));
