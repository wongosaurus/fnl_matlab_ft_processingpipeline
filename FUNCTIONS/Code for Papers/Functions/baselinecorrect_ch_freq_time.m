function [totalbl, nonphaselockedbl, phaselockedbl, ITPCbl] = baselinecorrect_ch_freq_time(wpms, data, data2, electrodesofinterest, analysistype, conditionbaseline,epoch)
%if length(electrodesofinterest)<length(wpms.labels)%we mustn't want them all
    if strcmpi (analysistype, 'totalpower')
        if conditionbaseline >0
            total = bsxfun(@minus,10*log10(squeeze(mean(data(electrodesofinterest,:,:),1))),10*log10(squeeze(mean(data(electrodesofinterest,:,conditionbaseline),1))));
            totalbl = bsxfun(@minus,total,mean(total(:,wpms.baselineinds.(epoch)),2));
        else
            total = 10*log10(squeeze(mean(data(electrodesofinterest,:,:),1)));
            totalbl = bsxfun(@minus,total,mean(total(:,wpms.baselineinds.(epoch)),2));
        end
    elseif strcmpi (analysistype, 'allpower')
        if conditionbaseline >0
            %fprintf('\t%s','.');
            total = bsxfun(@minus,10*log10(squeeze(mean(data(electrodesofinterest,:,:),1))),10*log10(squeeze(mean(data(electrodesofinterest,:,conditionbaseline),1))));
            totalbl = bsxfun(@minus,total,mean(total(:,wpms.baselineinds.(epoch)),2));
            %fprintf('%s','.');
            nonphaselocked = bsxfun(@minus,10*log10(squeeze(mean(data2(electrodesofinterest,:,:),1))),10*log10(squeeze(mean(data2(electrodesofinterest,:,:),1))));
            nonphaselockedbl = bsxfun(@minus,nonphaselocked,mean(nonphaselocked(:,wpms.baselineinds.(epoch)),2));
            fprintf('%s','.');
            phaselocked = total-nonphaselocked;
            phaselockedbl = bsxfun(@minus,phaselocked,mean(phaselocked(:,wpms.baselineinds.(epoch)),2));
        else
            %fprintf('\t%s','.');
            total = 10*log10(squeeze(mean(data(electrodesofinterest,:,:),1)));
            totalbl = bsxfun(@minus,total,mean(total(:,wpms.baselineinds.(epoch)),2));
            %fprintf('%s','.');
            nonphaselocked = 10*log10(squeeze(mean(data2(electrodesofinterest,:,:),1)));
            nonphaselockedbl = bsxfun(@minus,nonphaselocked,mean(nonphaselocked(:,wpms.baselineinds.(epoch)),2));
            fprintf('%s','.');
            phaselocked = total-nonphaselocked;
            phaselockedbl = bsxfun(@minus,phaselocked,mean(phaselocked(:,wpms.baselineinds.(epoch)),2));
        end
    elseif strcmpi (analysistype, 'ITPC')
        if conditionbaseline>0
            ITPC = bsxfun(@minus,squeeze(mean(data(electrodesofinterest,:,:),1)),squeeze(mean(data(electrodesofinterest,:,conditionbaseline),1)));
            ITPCbl = abs(bsxfun(@minus,ITPC,mean(ITPC(:,wpms.baselineinds.(epoch)),2)));
        else
            ITPC = squeeze(mean(data(electrodesofinterest,:,:),1));
            ITPCbl = abs(bsxfun(@minus,ITPC,mean(ITPC(:,wpms.baselineinds.(epoch)),2)));
        end
%    end
end