function wpms = setwpmsMontana(listingdir.phase1, listingdir.phase2, datain, labels, fmin, fmax, fbins, times, conditions)

phase = fieldnames(listingdir);
listings = dir(listingdir);
wpms.names = {length(listings)};

for phase_i = 1:length(phase)
    for file_i = 1:length(listingdir.(phase{phase_i}))
    wpms.names.(phase{phase_i}){file_i} = listingdir.({phase_i}){file_i}.name(1:6);
    end
end

wpms.dirs.datain = datain;
wpms.labels = labels;

wpms.frequencies = logspace(log10(fmin),log10(fmax),fbins);
wpms.times = times;

wpms.conditions = conditions;

