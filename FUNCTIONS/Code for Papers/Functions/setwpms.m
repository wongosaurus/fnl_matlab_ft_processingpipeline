function wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions)

listings = dir(listingdir);
wpms.names = {length(listings)};
for file_i =1:length(listings)
    wpms.names{file_i} = listings(file_i).name(1:6);
end

wpms.dirs.datain = datain;
wpms.labels = labels;

wpms.frequencies = logspace(log10(fmin),log10(fmax),fbins);
wpms.times = times;

wpms.conditions = conditions;

