function RT = extract_RTvar(refdat)
% extract_RTvar(refdat)
% checks for the trl file that contains the RT data
% this is in a .previous field somewhere
% assumes it is no more than 10 steps back
% could fix to be recursive...
try
    trldata=refdat.cfg.previous.trl;
    sampledata=refdat.sampleinfo(:,1);
    RT=getRT(trldata,sampledata);
catch
    try
        %disp('refdat.cfg.previous.previous.trl');
        trldata=refdat.cfg.previous.previous.trl;
        sampledata=refdat.sampleinfo(:,1);
        RT=getRT(trldata,sampledata);
    catch
        try
            %disp('refdat.cfg.previous.previous.previous.trl');
            trldata=refdat.cfg.previous.previous.previous.trl;
            sampledata=refdat.sampleinfo(:,1);
            RT=getRT(trldata,sampledata);
        catch
            try
                %disp('refdat.cfg.previous.previous.previous.previous.trl');
                trldata=refdat.cfg.previous.previous.previous.previous.trl;
                sampledata=refdat.sampleinfo(:,1);
                RT=getRT(trldata,sampledata);
            catch
                try
                    %disp('refdat.cfg.previous.previous.previous.previous.previous.trl');
                    trldata=refdat.cfg.previous.previous.previous.previous.previous.trl;
                    sampledata=refdat.sampleinfo(:,1);
                    RT=getRT(trldata,sampledata);
                catch
                    try
                        %disp('refdat.cfg.previous.previous.previous.previous.previous.previous.trl');
                        trldata=refdat.cfg.previous.previous.previous.previous.previous.previous.trl;
                        sampledata=refdat.sampleinfo(:,1);
                        RT=getRT(trldata,sampledata);
                    catch
                        try
                            %disp('refdat.cfg.previous.previous.previous.previous.previous.previous.previous.trl');
                            trldata=refdat.cfg.previous.previous.previous.previous.previous.previous.previous.trl;
                            sampledata=refdat.sampleinfo(:,1);
                            RT=getRT(trldata,sampledata);
                        catch
                            try
                                %disp('refdat.cfg.previous.previous.previous.previous.previous.previous.previous.previous.trl');
                                trldata=refdat.cfg.previous.previous.previous.previous.previous.previous.previous.previous.trl;
                                sampledata=refdat.sampleinfo(:,1);
                                RT=getRT(trldata,sampledata);
                            catch
                                try
                                    %disp('refdat.cfg.previous.previous.previous.previous.previous.previous.previous.previous.previous.trl');
                                    trldata=refdat.cfg.previous.previous.previous.previous.previous.previous.previous.previous.previous.trl;
                                    sampledata=refdat.sampleinfo(:,1);
                                    RT=getRT(trldata,sampledata);
                                catch
                                    try
                                        %disp('refdat.cfg.previous.previous.previous.previous.previous.previous.previous.previous.previous.previous.trl');
                                        trldata=refdat.cfg.previous.previous.previous.previous.previous.previous.previous.previous.previous.previous.trl;
                                        sampledata=refdat.sampleinfo(:,1);
                                        RT=getRT(trldata,sampledata);
                                    catch
                                        fprintf('Failed\n');
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end

end
% getRT
function RT=getRT(trldata,sampledata)
RTvar=trldata;
RT=zeros(size(sampledata,1),1);
count=0;
for RT_i=1:length(RTvar)
    %find(sampledata==RTvar(RT_i,1))
    if ~isempty(find(sampledata==RTvar(RT_i,1)))
        
        count=count+1;
        RT(count,1)=RTvar(RT_i,5);
    end
end
end