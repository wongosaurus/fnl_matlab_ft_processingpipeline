function [power_data_eegpower, power_data_induced] = perform_waveletMM(time,windowsize,bins,freqrange,channels,n_trial,refdat,power_type);

times2test = time;
timewindow = windowsize;
frex=logspace(log10(freqrange(1)),log10(freqrange(length(freqrange))),bins);

pnts=size(refdat.time{1,1},2);
srate=refdat.fsample;



%% Begin Analysis: Loop Condition, Channel
fprintf('\n%s\t','Rearraging data for analyses');
if strcmpi(power_type,'total')
    power_data_eegpower         = zeros(n_trial,length(channels),length(frex),pnts,'single');
elseif strcmpi(power_type,'nonphase')
    power_data_induced         = zeros(n_trial,length(channels),length(frex),pnts,'single');
    whos
    erp = zeros(length(channels),pnts,n_trial);
    for trial_i = 1:length(refdat.trial)
        erp(:,:,trial_i) = refdat.trial{1,trial_i}(1:length(channels),:);
    end
    erp = squeeze(mean(erp,3));
end

for trial_i =1:n_trial%size(refdat.trial,2)
    tic;
    fprintf('\n%s\t%i','Trial:',trial_i);
    data=refdat.trial{1,trial_i}(1:length(channels),:);
    %in the end we want channel, time, trial
    %Wrong Dimension Order: Must be channel by time by trial.
    for channi = 1:length(channels)
        fprintf('.');
        %% Initialise time/frequency parameters:
        % convert times to idx
        times2testidx=zeros(size(times2test));
        for i=1:length(times2test)%dropped parfor -Patrick
            [~,times2testidx(i)]=min(abs(refdat.time{1,1}.*1000-times2test(i)));
        end
        % define time and cycles for wavelets
        t             =-pnts/srate/2:1/srate:pnts/srate/2-1/srate;
        numcycles     =logspace(log10(3),log10(14),length(frex));
        mw_tf         = zeros(length(frex),length(times2test),'single');
        nw_tf         = zeros(length(frex),length(times2test),'single');
        %% loop over frequencies:
        for fi=1:length(frex)
            % extract frequency band-specific oscillation power
            % create wavelet
            centerfreq = frex(fi);
            wavelet=exp(2*1i*pi*centerfreq.*t).*exp(-t.^2./(2*(numcycles(fi)/(2*pi*centerfreq))^2));
            % loop over channels
            % frequency-domain convolution parameters
            Ldata  =  numel(data(channi,:));
            Lconv1 =  Ldata+pnts-1;
            Lconv  =  pow2(nextpow2(Lconv1));
            if strcmpi(power_type,'total')
                % FFT of data
                EEGfft = fft(reshape(data(channi,:),1,[]),Lconv);
                % convolution (in frequency domain, thanks to convolution theorem)
                m = ifft(EEGfft.*fft(wavelet,Lconv),Lconv);
                m = m(1:Lconv1);
                m = reshape(m(floor((pnts-1)/2):end-1-ceil((pnts-1)/2)),pnts,1);
                % store data
                freqdata = abs(m).^2;
                mw_tf(fi,:) = freqdata';
            elseif strcmpi(power_type,'nonphase')
                % make nonphase
                induced_EEG = squeeze(data(channi,:)) - repmat(erp(channi,:),1);
                induced_eegfft = fft(reshape(induced_EEG,1,[]),Lconv);
                n = ifft(induced_eegfft.*fft(wavelet,Lconv),Lconv);
                n = n(1:Lconv1);
                n = reshape(n(floor((pnts-1)/2):end-1-ceil((pnts-1)/2)),pnts,1);
                % store data
                freqdata = abs(n).^2;
                nw_tf(fi,:) = freqdata';
            end
        end % end frequency loop
        if strcmpi(power_type,'total')
            power_data_eegpower(trial_i,channi,:,:)=mw_tf;
            power_data_induced = 0;
        elseif strcmpi(power_type,'nonphase')
            power_data_induced(trial_i,channi,:,:)=nw_tf;
            power_data_eegpower = 0;
        end
        %power_data_complexmwtf(trial_i,channi,:,:)=complex_mw_tf;
    end%channi loop
    t = toc;
    fprintf('%s %s\n',num2str(t),'seconds to process');
end%trial_i loop




