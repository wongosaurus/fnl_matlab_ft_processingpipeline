function singleTrialPowerBL(name_i,cond_i,power_i)

wpms.labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30;
fbins = 80;
wpms.frequencies = logspace(log10(fmin),log10(fmax),fbins);

wpms.times = (-1:0.001953125000000:3.5)*1000;
wpms.conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninf','noninfrepeat','noninfswitch'};
wpms.names ={'AGE002',	'AGE003',	'AGE004',	'AGE005',	'AGE007',	'AGE008',	'AGE012',	'AGE013',	'AGE014',	'AGE015',	'AGE017',	'AGE018',	'AGE019',	'AGE020',	'AGE021',	'AGE022',	'AGE023',	'AGE024',	'AGE026',	'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE046',	'AGE047',	'AGE050',	'AGE051',	'AGE053',	'AGE058',	'AGE059',	'AGE061',	'AGE062',	'AGE063',	'AGE066',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE072',	'AGE073',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE088',	'AGE089',	'AGE090',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE096',	'AGE097',	'AGE098',	'AGE100',	'AGE102',	'AGE103',	'AGE104',	'AGE107',	'AGE108',	'AGE109',	'AGE111',	'AGE114',	'AGE115',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE122',	'AGE123',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE133',	'AGE134',	'AGE135',	'AGE136',	'AGE138',	'AGE141',	'AGE145',	'AGE146',	'AGE147',	'AGE148',	'AGE149',	'AGE150',	'AGE151',	'AGE152',	'AGE153',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE163',	'AGE164',	'AGE165',	'AGE166',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE172',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE189',	'AGE190',	'AGE195',	'AGE197',	'AGE198',	'AGE199',	'AGE201',	'AGE202',	'AGE203',	'AGE205',	'AGE206',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE232',	'AGE233',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE241',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE273',	'AGE275',	'AGE276',	'AGE278',	'AGE279'};

SAVEDIR = ['.' filesep 'singleTrialOutput' filesep];
LOADDIR = ['.' filesep 'singleTrial' filesep];

baseline_start_cue = -300;
baseline_end_cue = -100;
baseline = setbaseline(wpms, baseline_start_cue, baseline_end_cue); %time points for baseline

powerTypes={'totalBL','nonphaseBL','phaseBL'};

if power_i == 1;
    loadname = [LOADDIR wpms.names{name_i} '_' wpms.conditions{cond_i} 'singleTrialTotal.mat'];
    load(loadname);
    totalBL = zeros(size(total,1),length(wpms.labels), length(wpms.frequencies),length(wpms.times),'single');
    for electrodesofinterest = 1:length(wpms.labels)
        for trial_i = 1:size(total,1)
            totalLog = 10*log10(squeeze(total(trial_i,electrodesofinterest,:,:)));
            totalBL(trial_i,electrodesofinterest,:,:) = bsxfun(@minus,totalLog,squeeze(mean(totalLog(:,baseline),2)));
        end
    end
    filename = [SAVEDIR wpms.names{name_i} '_' wpms.conditions{cond_i} 'singleTrialTotalBL.mat'];
    save(filename,'totalBL','-v7.3');
else if power_i == 2;
        loadname = [LOADDIR wpms.names{name_i} '_' wpms.conditions{cond_i} 'singleTrialNonphase.mat'];
        load(loadname);
        nonphaseBL = zeros(size(nonphase,1),length(wpms.labels),length(wpms.frequencies),length(wpms.times),'single');
        for electrodesofinterest = 1:length(wpms.labels)
            for trial_i = 1:size(nonphase,1)
                nonphaseLog = 10*log10(squeeze(nonphase(trial_i,electrodesofinterest,:,:)));
                nonphaseBL(trial_i,electrodesofinterest,:,:) = bsxfun(@minus,nonphaseLog,squeeze(mean(nonphaseLog(:,baseline),2)));
            end
        end
        filename = [SAVEDIR wpms.names{name_i} '_' wpms.conditions{cond_i} '_' powerTypes{power_i} 'singleTrialBL.mat'];
        save(filename,'nonphaseBL','-v7.3');
    elseif power_i == 3;
        load([LOADDIR wpms.names{name_i} '_' wpms.conditions{cond_i} 'singleTrialTotal.mat']);
        load([LOADDIR wpms.names{name_i} '_' wpms.conditions{cond_i} 'singleTrialNonphase.mat']);
        phaseBL = zeros(size(total,1),length(wpms.labels), length(wpms.frequencies),length(wpms.times),'single');
        for electrodesofinterest = 1:length(wpms.labels)
            for trial_i = 1:size(total,1)
                totalLog = 10*log10(squeeze(total(trial_i,electrodesofinterest,:,:)));
                nonphaseLog = 10*log10(squeeze(nonphase(trial_i,electrodesofinterest,:,:)));
                phaseData = totalLog-nonphaseLog;
                phaseBL(trial_i,electrodesofinterest,:,:) = bsxfun(@minus,phaseData,mean(phaseData(:,baseline),2));
            end
        end
        filename = [SAVEDIR wpms.names{name_i} '_' wpms.conditions{cond_i} '_' powerTypes{power_i} 'singleTrialBL.mat'];
        save(filename,'phaseBL','-v7.3');
    end
end
end