function [totalbl, nonphaselockedbl, phaselockedbl, ITPCbl] = baselinecorrect(wpms, data, data2, electrodesofinterest, analysistype, conditionbaseline,epoch)
if length(electrodesofinterest)<length(wpms.labels)%we mustn't want them all
    if strcmpi (analysistype, 'totalpower')
        if conditionbaseline >0
            total = bsxfun(@minus,10*log10(squeeze(mean(data(:,:,electrodesofinterest,:,:),3))),10*log10(squeeze(mean(data(:,:,electrodesofinterest,:,conditionbaseline),3))));
            totalbl = bsxfun(@minus,total,mean(total(wpms.baselineinds.(epoch),:,:),1));
        else
            total = 10*log10(squeeze(mean(data(:,:,electrodesofinterest,:,:),3)));
            totalbl = bsxfun(@minus,total,mean(total(wpms.baselineinds.(epoch),:,:),1));
        end
    elseif strcmpi (analysistype, 'allpower')
        if conditionbaseline >0
            fprintf('\t%s','.');
            total = bsxfun(@minus,10*log10(squeeze(mean(data(:,:,electrodesofinterest,:,:),3))),10*log10(squeeze(mean(data(:,:,electrodesofinterest,:,conditionbaseline),3))));
            totalbl = bsxfun(@minus,total,mean(total(wpms.baselineinds.(epoch),:,:),1));
            fprintf('%s','.');
            nonphaselocked = bsxfun(@minus,10*log10(squeeze(mean(data2(:,:,electrodesofinterest,:,:),3))),10*log10(squeeze(mean(data2(:,:,electrodesofinterest,:,conditionbaseline),3))));
            nonphaselockedbl = bsxfun(@minus,nonphaselocked,mean(nonphaselocked(wpms.baselineinds.(epoch),:,:),1));
            fprintf('%s','.');
            phaselocked = total-nonphaselocked;
            phaselockedbl = bsxfun(@minus,phaselocked,mean(phaselocked(wpms.baselineinds.(epoch),:,:),1));
        else
            fprintf('\t%s','.');
            total = 10*log10(squeeze(mean(data(:,:,electrodesofinterest,:,:),3)));
            totalbl = bsxfun(@minus,total,mean(total(wpms.baselineinds.(epoch),:,:),1));
            fprintf('%s','.');
            nonphaselocked = 10*log10(squeeze(mean(data2(:,:,electrodesofinterest,:,:),3)));
            nonphaselockedbl = bsxfun(@minus,nonphaselocked,mean(nonphaselocked(wpms.baselineinds.(epoch),:,:),1));
            fprintf('%s','.');
            phaselocked = total-nonphaselocked;
            phaselockedbl = bsxfun(@minus,phaselocked,mean(phaselocked(wpms.baselineinds.(epoch),:,:),1));
        end
    elseif strcmpi (analysistype, 'ITPC')
        if conditionbaseline>0
            ITPC = bsxfun(@minus,squeeze(mean(data(:,:,electrodesofinterest,:,:),3)),squeeze(mean(data(:,:,electrodesofinterest,:,conditionbaseline),3)));
            ITPCbl = abs(bsxfun(@minus,ITPC,mean(ITPC(wpms.baselineinds.(epoch),:,:),1)));
        else
            ITPC = squeeze(mean(data(:,:,electrodesofinterest,:,:),3));
            ITPCbl = abs(bsxfun(@minus,ITPC,mean(ITPC(wpms.baselineinds.(epoch),:,:),1)));
        end
    end
else
    if strcmpi (analysistype, 'totalpower')
        if conditionbaseline >0
            total = bsxfun(@minus,10*log10(squeeze(data(:,:,electrodesofinterest,:,:))),10*log10(squeeze(data(:,:,electrodesofinterest,:,conditionbaseline))));
            totalbl = bsxfun(@minus,total,mean(total(wpms.baselineinds.(epoch),:,:),1));
        else
            total = 10*log10(squeeze(data(:,:,electrodesofinterest,:,:)));
            totalbl = bsxfun(@minus,total,mean(total(wpms.baselineinds.(epoch),:,:),1));
        end
    elseif strcmpi (analysistype, 'allpower')
        if conditionbaseline >0
            fprintf('\t%s','.');
            total = bsxfun(@minus,10*log10(squeeze(data(:,:,electrodesofinterest,:,:))),10*log10(squeeze(data(:,:,electrodesofinterest,:,conditionbaseline))));
            totalbl = bsxfun(@minus,total,mean(total(wpms.baselineinds.(epoch),:,:),1));
            fprintf('%s','.');
            nonphaselocked = bsxfun(@minus,10*log10(squeeze(data2(:,:,electrodesofinterest,:,:))),10*log10(squeeze(data2(:,:,electrodesofinterest,:,conditionbaseline))));
            nonphaselockedbl = bsxfun(@minus,nonphaselocked,mean(nonphaselocked(wpms.baselineinds.(epoch),:,:),1));
            fprintf('%s','.');
            phaselocked = total-nonphaselocked;
            phaselockedbl = bsxfun(@minus,phaselocked,mean(phaselocked(wpms.baselineinds.(epoch),:,:),1));
        else
            fprintf('\t%s','.');
            total = 10*log10(squeeze(data(:,:,electrodesofinterest,:,wpms.epochconditions.(epoch){1})));
            totalbl = bsxfun(@minus,total,mean(total(wpms.baselineinds.epoch.(epoch),:,:,:),1));
            fprintf('%s','.');
            nonphaselocked = 10*log10(squeeze(data2(:,:,electrodesofinterest,:,wpms.epochconditions.(epoch){1})));
            nonphaselockedbl = bsxfun(@minus,nonphaselocked,mean(nonphaselocked(wpms.baselineinds.epoch.(epoch),:,:,:),1));
            fprintf('%s','.');
            phaselocked = total-nonphaselocked;
            phaselockedbl = bsxfun(@minus,phaselocked,mean(phaselocked(wpms.baselineinds.epoch.(epoch),:,:,:),1));
        end
    elseif strcmpi (analysistype, 'ITPC')
        if conditionbaseline>0
            ITPC = bsxfun(@minus,squeeze(data(:,:,electrodesofinterest,:,:)),squeeze(data(:,:,electrodesofinterest,:,conditionbaseline)));
            ITPCbl = abs(bsxfun(@minus,ITPC,mean(ITPC(wpms.baselineinds.(epoch),:,:),1)));
        else
            ITPC = squeeze(data(:,:,electrodesofinterest,:,:));
            ITPCbl = abs(bsxfun(@minus,ITPC,mean(ITPC(wpms.baselineinds.(epoch),:,:),1)));
        end
    end
end