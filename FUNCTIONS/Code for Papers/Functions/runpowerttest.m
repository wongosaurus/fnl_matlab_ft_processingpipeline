
function [pvalue, tvalue] = runpowerttest(data)

[~,pvalue,~,stats] = ttest(data);
tvalue = stats.tstat;

