function [CC] = getpeaks(width, height, freqindmin, freqindmax, h_t, data, plottimes, wpms)

CC = bwconncomp(h_t(freqindmin:freqindmax,plottimes));

for pixel_i = 1:length(CC.PixelIdxList)
    reduceddata = data(freqindmin:freqindmax,:);
    [m,~]=max(reduceddata(CC.PixelIdxList{pixel_i}));
    [freqmax,timemax] =find (reduceddata==m);
    freqmax = freqindmin+freqmax-1;
    if  timemax - width/2 < 0 || timemax + width/2 > length(plottimes) || ... %out of Sample conditions (Time)
        freqmax - height/2 < 0 || freqmax + height/2 > length(wpms.frequencies)
        fprintf('Shifting Peaks Back into Range:\n');
        if freqmax - height/2 < 0
            fprintf('Shifting Peaks Back into Range: Frequency too small: Shifting up\n');
            freqmax = freqmax + height/2;
        end
        if freqmax + height/2 > length(wpms.frequencies)
            fprintf('Shifting Peaks Back into Range: Frequency too large: Shifting down\n');
            freqmax = freqmax - height/2;
        end
        if timemax - width/2 < 0
            fprintf('Shifting Peaks Back into Range: Time too small: Shifting up\n');
            timemax = timemax + width/2;
        end
        if timemax + width/2 > length(plottimes)
            fprintf('Shifting Peaks Back into Range: Time too large: Shifting down\n');
            timemax = timemax - width/2;
        end
        
    end
        
    if freqmax > freqindmax
        CC.PixelIdxLocation{pixel_i} = [];
        fprintf('Pixel %i: Freq: %3.3f Hz, at Time: %3.3f ms. Out of Frequency Range...\n',pixel_i,wpms.frequencies(freqmax),wpms.times(plottimes(timemax)));
        continue;
    end
    
    if  wpms.times(plottimes(timemax))> plottimes(end) || wpms.frequencies(freqmax)< 4 || ...
            timemax - width/2 < 0 || timemax + width/2 > length(plottimes) || ... %out of Sample conditions (Time)
            freqmax - height/2 < 0 || freqmax + height/2 > length(wpms.frequencies)
        CC.PixelIdxLocation{pixel_i} = [];
        fprintf('Pixel %i: Freq: %3.3f Hz, at Time: %3.3f ms. Did not meet testing requirements, Moving on...\n',pixel_i,wpms.frequencies(freqmax),wpms.times(plottimes(timemax)));
        continue;
    end
    if numel(reduceddata(CC.PixelIdxList{pixel_i})) < 100 %(width+1)*(height+1)
        CC.PixelIdxLocation{pixel_i} = [];
        fprintf('Pixel %i: Freq: %3.3f Hz, at Time: %3.3f ms. Cluster too Small, Moving on...\n',pixel_i,wpms.frequencies(freqmax),wpms.times(plottimes(timemax)));
        continue;
    end

    fprintf('Plotting: Pixel %i: Freq: %3.3f Hz, at Time: %3.3f ms.\n',pixel_i,wpms.frequencies(freqmax),wpms.times(plottimes(timemax)));
    scatter(wpms.times(plottimes(timemax)),wpms.frequencies(freqmax),10,'k','filled');
    x = wpms.times(plottimes(timemax - width/2));
    width_time = wpms.times(plottimes(timemax + width/2)) - x;
    y = wpms.frequencies(freqmax - height/2);
    height_freq = wpms.frequencies(freqmax + height/2) - y;
    hold on;
    rectangle('Position',[x,y,width_time,height_freq]);
    
    CC.PixelIdxLocation{pixel_i} = [freqmax,timemax];
end
end
