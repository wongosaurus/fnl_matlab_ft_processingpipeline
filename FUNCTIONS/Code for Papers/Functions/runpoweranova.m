function [pvalue, fstat] = runpoweranova(data,epoch)

if strcmpi(epoch,'cue')
    
    t = table(data(:,1),data(:,2),data(:,3),data(:,4),data(:,5),...
        'VariableNames',{'data1','data2','data3','data4','data5'});
    Meas = dataset([1 2 3 4 5]','VarNames',{'Measurements'});
    
    rm = fitrm(t,'data1-data5~1','WithinDesign',Meas);
    
    ranovatbl = ranova(rm);
    fstat=ranovatbl{1,4};
    pvalue=ranovatbl{1,5};
    
elseif strcmpi(epoch,'target')
    t = table(data(:,1),data(:,2),data(:,3),data(:,4),data(:,5),data(:,6),...
        'VariableNames',{'data1','data2','data3','data4','data5','data6'});
    Meas = dataset([1 2 3 4 5 6]','VarNames',{'Measurements'});
    
    rm = fitrm(t,'data1-data6~1','WithinDesign',Meas);
    
    ranovatbl = ranova(rm);
    fstat=ranovatbl{1,4};
   pvalue=ranovatbl{1,5};
end