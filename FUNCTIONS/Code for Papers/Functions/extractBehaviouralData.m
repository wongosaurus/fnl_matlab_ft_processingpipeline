function [RT,errorRate]=extractBehaviouralData(data)
% data = EEG file (usually refdat)
%grab trial codes
%depending on processing steps, trl file can be buried in a variable number
%of .previous steps - so first we need to find it
%the trl should have 5 columns, so that's what we'll look for
trialCodes   = unique(data.trialinfo);
trialIndices = [];
origFound = 0;
searchString = 'data.cfg.previous';
% fprintf('\n%s','Searching for original trl file');
while origFound==0
    fprintf('.');
    try
        eval(['trlChk=size(' searchString '.trl,2);'])
        if trlChk == 5
            eval(['origTrl=' searchString '.trl;']);
%             fprintf('\t%s\n','Found it!')
            origFound=1;
        else
            searchString=strcat(searchString,'.previous');
        end
    catch
        searchString=strcat(searchString,'.previous');
    end
end
for trial_i = 1:length(trialCodes)
    trialIndices = [trialIndices; find(origTrl(:,4)==trialCodes(trial_i))];
end
trialIndices=sort(trialIndices);
%first, pull out RT for trials entered into EEG analyses
sampleInfo = data.sampleinfo(:,1);
validInds=find(ismember(origTrl(:,1),sampleInfo));
RT = mean(origTrl(validInds,5));

%now we need to find the event structure, again buried in .previous
eventFound = 0;
searchString = 'data.cfg.previous';
% fprintf('\n%s','Searching for event stream');
while eventFound==0
    fprintf('.');
    try
        eval(['evntChk=size({' searchString '.event.value},2);'])
        if evntChk > 1
            eval(['eventStream=cell2mat({' searchString '.event.value});']);
%             fprintf('\t%s\n','Found it!')
            eventFound=1;
        else
            searchString=strcat(searchString,'.previous');
        end
    catch
        searchString=strcat(searchString,'.previous');
    end
end

%find trial indices again in the event stream
errorTrialIndices = [];
for trial_i = 1:length(trialCodes)
    errorTrialIndices = [errorTrialIndices find(eventStream==trialCodes(trial_i))];
end
errorTrialIndices = sort(errorTrialIndices);errorTrialIndices=errorTrialIndices';
responseIndices=errorTrialIndices+2;%responses follow targets
%check if each trigger code is accurate or not
%let zero = correct and one = incorrect (so we can just sum the error rate
%at the end)
errorVals = zeros(1,length(errorTrialIndices));
for error_i = 1:length(errorTrialIndices)
    val = eventStream(errorTrialIndices(error_i));
    %check if number is even - if so, needs an even response (i.e., 2)
    isodd=mod(val,2);
    if isodd==0 && eventStream(responseIndices(error_i))==65282||isodd==0 && eventStream(responseIndices(error_i))==2 %just in case a decimal slips through
        errorVals(1,error_i) = 0;
    elseif isodd==1 && eventStream(responseIndices(error_i))==65282||isodd==1 && eventStream(responseIndices(error_i))==2 %just in case a decimal slips through
        errorVals(1,error_i) = 1;
    elseif isodd==0 && eventStream(responseIndices(error_i))==65281||isodd==0 && eventStream(responseIndices(error_i))==1 %just in case a decimal slips through
        errorVals(1,error_i) = 1;
    elseif isodd==1 && eventStream(responseIndices(error_i))==65281||isodd==1 && eventStream(responseIndices(error_i))==1 %just in case a decimal slips through
        errorVals(1,error_i) = 0;
    end
end
errorRate = (sum(errorVals)/length(errorTrialIndices))*100;
