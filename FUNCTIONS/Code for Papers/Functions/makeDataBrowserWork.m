function makeDataBrowserWork(data)


cfg = [];
cfg.layout = 'biosemi64.lay';
cfg.continuous = 'yes';
cfg.channel = 1:64;
cfg.ylim = [-100 100];
cfg.blocksize = 10;
cfg.viewmode = 'vertical';
cfg = ft_databrowser(cfg,data);