function power_data_mwtf=perform_waveletMM(time,windowsize,bins,freqrange,channels,n_trial,refdat)

times2test = time;
timewindow = windowsize;
frex=logspace(log10(freqrange(1)),log10(freqrange(length(freqrange))),bins);

pnts=size(refdat.time{1,1},2);
srate=refdat.fsample;



%% Begin Analysis: Loop Condition, Channel
fprintf('\n%s\t','Rearraging data for analyses');
power_data_eegpower         = zeros(n_trial,length(channels),length(frex),pnts,'single');
power_data_induced         = zeros(n_trial,length(channels),length(frex),pnts,'single');
whos
catdat = refdat.trial{1};
for trial_i = 2:length(refdat.trial)
    catdat = cat(3,catdat,refdat.trial{trial_i});
end
erp = squeeze(mean(catdat,3));
clearvars catdat
for trial_i =1:n_trial%size(refdat.trial,2)
    tic;
    fprintf('\n%s\t%i','Trial:',trial_i);
    data=refdat.trial{1,trial_i};
    %in the end we want channel, time, trial
    %Wrong Dimension Order: Must be channel by time by trial.
    for channi = 1:length(channels)
        fprintf('.');
        %% Initialise time/frequency parameters:
        % convert times to idx
        times2testidx=zeros(size(times2test));
        for i=1:length(times2test)%dropped parfor -Patrick
            [~,times2testidx(i)]=min(abs(refdat.time{1,1}.*1000-times2test(i)));
        end
        % define time and cycles for wavelets
        t             =-pnts/srate/2:1/srate:pnts/srate/2-1/srate;
        numcycles     =logspace(log10(3),log10(14),length(frex));
        mw_tf         = zeros(length(frex),length(times2test),'single');
        nw_tf         = zeros(length(frex),length(times2test),'single');
        %% loop over frequencies:
        for fi=1:length(frex)
            % extract frequency band-specific oscillation power
            % create wavelet
            centerfreq = frex(fi);
            wavelet=exp(2*1i*pi*centerfreq.*t).*exp(-t.^2./(2*(numcycles(fi)/(2*pi*centerfreq))^2));
            % loop over channels
            % frequency-domain convolution parameters
            Ldata  =  numel(data(channi,:));
            Lconv1 =  Ldata+pnts-1;
            Lconv  =  pow2(nextpow2(Lconv1));
            % FFT of data
            EEGfft = fft(reshape(data(channi,:),1,[]),Lconv);
            % convolution (in frequency domain, thanks to convolution theorem)
            m = ifft(EEGfft.*fft(wavelet,Lconv),Lconv);
            m = m(1:Lconv1);
            m = reshape(m(floor((pnts-1)/2):end-1-ceil((pnts-1)/2)),pnts,1);
            % store data
            freqdata = abs(m).^2;
            mw_tf(fi,:) = freqdata';
            % make nonphase
            induced_EEG = squeeze(data(channi,:)) - repmat(erp(channi,:),1);
            
            induced_eegfft = fft(reshape(induced_EEG,1,[]),Lconv);
            n = ifft(EEGfft.*fft(wavelet,Lconv),Lconv);
            n = n(1:Lconv1);
            n = reshape(n(floor((pnts-1)/2):end-1-ceil((pnts-1)/2)),pnts,1);
            % store data
            freqdata = abs(n).^2;
            nw_tf(fi,:) = freqdata';
        end % end frequency loop
        power_data_eegpower(trial_i,channi,:,:)=mw_tf;
        power_data_induced(trial_i,channi,:,:)=nw_tf;
        %power_data_complexmwtf(trial_i,channi,:,:)=complex_mw_tf;
    end%channi loop
    t = toc;
    fprintf('%s %s\n',num2str(t),'seconds to process');
end%trial_i loop

baseline_start_cue = -300;
baseline_end_cue = -100;
baseline = setbaseline(wpms, baseline_start_cue, baseline_end_cue); %time points for baseline

total = zeros(length(data.trial),length(channels), length(frex),length(time),'single');
nonphase = zeros(length(data.trial),length(channels), length(frex),length(time),'single');
phase = zeros(length(data.trial),length(channels), length(frex),length(time),'single');

totalBL = zeros(length(data.trial),length(channels), length(frex),length(time),'single');
nonphaseBL = zeros(length(data.trial),length(channels), length(frex),length(time),'single');
phaseBL = zeros(length(data.trial),length(channels), length(frex),length(time),'single');

total = 10*log10(squeeze(power_data_eegpower(:,:,:,:)));
totalBL = bsxfun(@minus,total,mean(total(:,baseline),2));
nonphase = 10*log10(squeeze(power_data_induced(:,:,:,:)));
nonphaseBL = bsxfun(@minus,nonphase,mean(nonphase(:,baseline),2));
phase = total-nonphase;
phaseBL = bsxfun(@minus,phase,mean(phase(:,baseline),2));

filename = ['F:\singleTrial' wpms.names{name_i} '_' wpms.conditions{cond_i} 'singleTrial.mat'];
save(filename,'total','totalBL','nonphase','nonphaseBL','phase','phaseBL','-v7.3');


