
x = zeros(19,20);

load('Z:\graphTheory\graphMeasures\phaseBL_noninfswitch_alpha_neg.mat')
x(1,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_noninfswitch_theta_neg.mat')
x(2,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_noninfswitch_delta_neg.mat')
x(3,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_noninfswitch_beta_neg.mat')
x(4,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_noninfrepeat_alpha_neg.mat')
x(5,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_switchto_alpha_neg.mat')
x(6,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_noninfrepeat_delta_neg.mat')
x(7,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_noninfrepeat_theta_neg.mat')
x(8,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_noninfrepeat_beta_neg.mat')
x(9,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_noninf_alpha_neg.mat')
x(10,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_noninf_theta_neg.mat')
x(11,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_noninf_beta_neg.mat')
x(12,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_noninf_delta_neg.mat')
x(13,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_switchaway_alpha_neg.mat')
x(14,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_switchaway_beta_neg.mat')
x(15,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_switchaway_theta_neg.mat')
x(16,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_switchaway_delta_neg.mat')
x(17,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_switchto_beta_neg.mat')
x(18,:) = normClusEff;
load('Z:\graphTheory\graphMeasures\phaseBL_switchto_theta_neg.mat')
x(19,:) = normClusEff;


y = [1:-.05:.05];
z = x;
b = 1:19;

surf(y,b,z);


