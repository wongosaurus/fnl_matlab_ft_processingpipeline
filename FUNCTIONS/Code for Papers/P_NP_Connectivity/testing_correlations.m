% Correlating Phase/Nonphase Power
clear all
close all
% setup parameters
listingdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
datain = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\NoBL\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30;
fbins = 80;
frequencies = logspace(log10(fmin),log10(fmax),fbins);
times = (-1:0.001953125000000:3.5)*1000;
conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninf','noninfrepeat','noninfswitch'};
wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);
wpms.allnames=wpms.names;
wpms.names ={'AGE002',	'AGE003',	'AGE004',	'AGE005',	'AGE007',	'AGE008',	'AGE012',	'AGE013',	'AGE014',	'AGE015',	'AGE017',	'AGE018',	'AGE019',	'AGE020',	'AGE021',	'AGE022',	'AGE023',	'AGE024',	'AGE026',	'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE046',	'AGE047',	'AGE050',	'AGE051',	'AGE053',	'AGE058',	'AGE059',	'AGE061',	'AGE062',	'AGE063',	'AGE066',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE072',	'AGE073',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE088',	'AGE089',	'AGE090',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE096',	'AGE097',	'AGE098',	'AGE100',	'AGE102',	'AGE103',	'AGE104',	'AGE107',	'AGE108',	'AGE109',	'AGE111',	'AGE114',	'AGE115',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE122',	'AGE123',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE133',	'AGE134',	'AGE135',	'AGE136',	'AGE138',	'AGE141',	'AGE145',	'AGE146',	'AGE147',	'AGE148',	'AGE149',	'AGE150',	'AGE151',	'AGE152',	'AGE153',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE163',	'AGE164',	'AGE165',	'AGE166',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE172',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE189',	'AGE190',	'AGE195',	'AGE197',	'AGE198',	'AGE199',	'AGE201',	'AGE202',	'AGE203',	'AGE205',	'AGE206',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE232',	'AGE233',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE241',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE273',	'AGE275',	'AGE276',	'AGE278',	'AGE279'};
clearvars -except wpms

SAVEDIR = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\';
LOADDIR = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\';

%%
% load in data & baseline

baseline_start = -300;
baseline_end = -100;
wpms.baselineinds.cue = setbaseline(wpms, baseline_start, baseline_end);

totalbl = zeros(length(wpms.times),length(wpms.names),length(wpms.conditions),length(wpms.frequencies));
nonphaselockedbl = totalbl;
phaselockedbl = totalbl;

clusters = {{'F1','Fz','F2','FC1','FCz','FC2'} {'C1','Cz', 'C2', 'CP1','CPz','CP2'} {'POz','PO1', 'PO2', 'P1', 'Pz', 'P2'}};
CLUSTER_NAMES = {'Frontal','Central','Parietal'};
POWER_TYPES = {'TOTAL','PHASE','NON PHASE'};
analysistype = 'allpower';
epoch = {'cue' 'target'};

nameinds = find(ismember(wpms.allnames,wpms.names));

for freq_i = 1:length(wpms.frequencies)
    tic
    fprintf('\n%s %i \t','Frequency:',freq_i);
    ERSPfilename = [wpms.dirs.datain 'ERSP_' num2str(freq_i) '.mat'];
    load(ERSPfilename);
    INDUCEDPOWERfilename = [wpms.dirs.datain 'INDUCEDPOWER_' num2str(freq_i) '.mat'];
    load(INDUCEDPOWERfilename);
    data = ERSP_F_t(:,:,:,nameinds,:);
    data2 = INDUCEDPOWER_F_t(:,:,:,nameinds,:);
    for cluster_i = 1%:length(clusters)
        electrodesofinterest = find(ismember(wpms.labels,clusters{cluster_i}));
        for epoch_i = 1%:length(epoch);
            [totalbl(:,:,:,freq_i), nonphaselockedbl(:,:,:,freq_i), phaselockedbl(:,:,:,freq_i)] = baselinecorrect(wpms, data, data2, electrodesofinterest, analysistype,0,epoch{epoch_i});
        end
    end
    toc
end

%%
% run correlation

rvalues = zeros(length(wpms.frequencies),length(wpms.times),length(wpms.conditions),'single');
pvalues = rvalues;
for freq_i = 1:length(wpms.frequencies)
    for time_i = 1:length(wpms.times)
        for cond_i = 1:length(wpms.conditions)
            X = squeeze(phaselockedbl(time_i,:,cond_i,freq_i)); 
            Y = squeeze(nonphaselockedbl(time_i,:,cond_i,freq_i));
            [rvalues(freq_i,time_i,cond_i),pvalues(freq_i,time_i,cond_i)] = corr(X',Y');
        end
    end
end
%%
%Plotting data
alpha = 0.005;

baseline_start = -300;
baseline_end = 2000;
plottimes = setbaseline(wpms, baseline_start, baseline_end);

for cond_i = 1:length(wpms.conditions)
    subplot(2,4,cond_i);
    contourf(wpms.times(plottimes),wpms.frequencies,squeeze(rvalues(:,plottimes,cond_i)),50,'linecolor','none')
    caxis([-1 1]);
    colormap jet
    hold on
    contour(wpms.times(plottimes),wpms.frequencies,squeeze(pvalues(:,plottimes,cond_i))<alpha,1,'k');
    title(['rvalue: ' wpms.conditions{cond_i}]);
end