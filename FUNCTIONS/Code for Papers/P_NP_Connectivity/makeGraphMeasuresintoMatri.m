% plotting graph measures
clear all;
clc;
disp('Yaaasss bitch');
wpms.labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30;
fbins = 80;
wpms.frequencies = logspace(log10(fmin),log10(fmax),fbins);

wpms.times = (-1:0.001953125000000:3.5)*1000;
wpms.conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninf','noninfrepeat','noninfswitch'};
wpms.names ={'AGE002',	'AGE003',	'AGE004',	'AGE005',	'AGE007',	'AGE008',	'AGE012',	'AGE013',	'AGE014',	'AGE015',	'AGE017',	'AGE018',	'AGE019',	'AGE020',	'AGE021',	'AGE022',	'AGE023',	'AGE024',	'AGE026',	'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE046',	'AGE047',	'AGE050',	'AGE051',	'AGE053',	'AGE058',	'AGE059',	'AGE061',	'AGE062',	'AGE063',	'AGE066',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE072',	'AGE073',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE088',	'AGE089',	'AGE090',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE096',	'AGE097',	'AGE098',	'AGE100',	'AGE102',	'AGE103',	'AGE104',	'AGE107',	'AGE108',	'AGE109',	'AGE111',	'AGE114',	'AGE115',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE122',	'AGE123',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE133',	'AGE134',	'AGE135',	'AGE136',	'AGE138',	'AGE141',	'AGE145',	'AGE146',	'AGE147',	'AGE148',	'AGE149',	'AGE150',	'AGE151',	'AGE152',	'AGE153',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE163',	'AGE164',	'AGE165',	'AGE166',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE172',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE189',	'AGE190',	'AGE195',	'AGE197',	'AGE198',	'AGE199',	'AGE201',	'AGE202',	'AGE203',	'AGE205',	'AGE206',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE232',	'AGE233',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE241',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE273',	'AGE275',	'AGE276',	'AGE278',	'AGE279'};

SAVEDIR = ['.' filesep 'graphMeasures' filesep];
LOADDIR = ['.' filesep 'corrMatrices' filesep];
addpath(genpath(['.' filesep 'BCT' filesep]));

powerTypes={'totalBL','nonphaseBL','phaseBL'};
freqBands = {'delta','theta','alpha','beta'};

timestart = [0:100:1800];
timeend = timestart+200;

%%

allPeople.normClusEffMatrix = zeros(length(wpms.names),length(powerTypes),length(wpms.conditions),length(freqBands),length(timestart),20).*NaN;
allPeople.normPathLengthMatrix = zeros(length(wpms.names),length(powerTypes),length(wpms.conditions),length(freqBands),length(timestart),20).*NaN;
allPeople.smallWorldMatrix = zeros(length(wpms.names),length(powerTypes),length(wpms.conditions),length(freqBands),length(timestart),20).*NaN;
allPeople.normRichClubMatrix = zeros(length(wpms.names),length(powerTypes),length(wpms.conditions),length(freqBands),length(timestart),length(wpms.labels),20).*NaN;


for name_i = 120:length(wpms.names)%1:length(wpms.names)
    try
    fprintf('\n%s\t',wpms.names{name_i})
    tic
    for power_i = 1:length(powerTypes)
        for cond_i = 1:length(wpms.conditions)
            for freq_i = 1:length(freqBands)
                for time_i = 1:length(timestart)
                    
                    load(['Z:\graphTheory\graphMeasures\' wpms.names{name_i} '_' powerTypes{power_i} '_' wpms.conditions{cond_i} '_' freqBands{freq_i} '_' num2str(timestart(time_i)) '_to_' num2str(timeend(time_i)) '_pos.mat']);
                    allPeople.normClusEffMatrix(name_i,power_i,cond_i,freq_i,time_i,:) = normClusEff;
                    allPeople.normPathLengthMatrix(name_i,power_i,cond_i,freq_i,time_i,:) = normPathLength;
                    allPeople.smallWorldMatrix(name_i,power_i,cond_i,freq_i,time_i,:) = smallWorldRatio;
                    allPeople.normRichClubMatrix(name_i,power_i,cond_i,freq_i,time_i,:,:) = normRichClub;
                end
            end
        end
    end
    catch
        fprintf(2,'\n%s%s\t','---**FAILED**---- ',wpms.names{name_i})
    end
    toc
end
savename = 'allPeopleGraphMeasures';
save(savename,'allPeople','-v7.3');
%%
clusEffTValues = zeros(length(wpms.names),length(powerTypes),length(wpms.conditions),length(wpms.conditions),length(freqBands),length(timestart),length(direction),20);
clusEffPValues = zeros(length(wpms.names),length(powerTypes),length(wpms.conditions),length(wpms.conditions),length(freqBands),length(timestart),length(direction),20);


for name_i = 1:length(wpms.names)
    load(['F:\graphTheory\graphMeasures\' wpms.names{name_i} 'allPowerCondFreqTimePos.mat'])
    for power_i = 1:length(powerTypes)
        for freq_i = 1:length(freqBands)
            for time_i = 1:length(timestart)
                for dir_i = 1:length(direction)
                    for cond_i = 1:length(wpms.conditions)
                        for cond_j = 1:length(wpms.conditions)
                            [clusEffTValues(name_i,power_i,:,:,freq_i,time_i,dir_i,:),...
                                clusEffPValues(name_i,power_i,:,:,freq_i,time_i,dir_i,:)]...
                                = ttest((isfinite(normClusEffMatrix(power_i,cond_i,freq_i,time_i,dir_i,:))),isfinite(normClusEffMatrix(power_i,cond_j,freq_i,time_i,dir_i,:)));
                        end
                    end
                end
            end
        end
    end
end

%%


%%
clusEffTValues = zeros(length(powerTypes),length(wpms.conditions),length(wpms.conditions),length(freqBands),length(timestart),length(direction),20);
clusEffPValues = zeros(length(powerTypes),length(wpms.conditions),length(wpms.conditions),length(freqBands),length(timestart),length(direction),20);

smallWorldTValues = zeros(length(powerTypes),length(wpms.conditions),length(wpms.conditions),length(freqBands),length(timestart),length(direction),20);
smallWorldPValues = zeros(length(powerTypes),length(wpms.conditions),length(wpms.conditions),length(freqBands),length(timestart),length(direction),20);

pathLengthTValues = zeros(length(powerTypes),length(wpms.conditions),length(wpms.conditions),length(freqBands),length(timestart),length(direction),20);
pathLengthPValues = zeros(length(powerTypes),length(wpms.conditions),length(wpms.conditions),length(freqBands),length(timestart),length(direction),20);

for cond_i = 1:5
    for cond_j = 1:5
        for power_i = 1:length(powerTypes)
            for freq_i = 1:length(freqBands)
                for time_i = 1:length(timestart)
                    x = squeeze(allPeople.normClusEffMatrix(1:75,power_i,cond_i,freq_i,time_i,dir_i,:));
                    y = squeeze(allPeople.normClusEffMatrix(1:75,power_i,cond_j,freq_i,time_i,dir_i,:));
                    [~,clusEffPValues(power_i,cond_i,cond_j,freq_i,time_i,dir_i,:),~,stats] = ttest(x,y);
                    clusEffTValues(power_i,cond_i,cond_j,freq_i,time_i,dir_i,:) = stats.tstat;
                    clearvars x y
                    x = squeeze(allPeople.normPathLengthMatrix(1:75,power_i,cond_i,freq_i,time_i,dir_i,:));
                    y = squeeze(allPeople.normPathLengthMatrix(1:75,power_i,cond_j,freq_i,time_i,dir_i,:));
                    [~,pathLengthPValues(power_i,cond_i,cond_j,freq_i,time_i,dir_i,:),~,stats] = ttest(x,y);
                    pathLengthTValues(power_i,cond_i,cond_j,freq_i,time_i,dir_i,:) = stats.tstat;
                    clearvars x y
                    x = squeeze(allPeople.smallWorldMatrix(1:75,power_i,cond_i,freq_i,time_i,dir_i,:));
                    y = squeeze(allPeople.smallWorldMatrix(1:75,power_i,cond_j,freq_i,time_i,dir_i,:));
                    [~,smallWorldPValues(power_i,cond_i,cond_j,freq_i,time_i,dir_i,:),~,stats] = ttest(x,y);
                    smallWorldTValues(power_i,cond_i,cond_j,freq_i,time_i,dir_i,:) = stats.tstat;
                end
            end
        end
    end
end
%%
figure();
set(gcf,'Position',get(0,'screensize'));
for time_i = 1:length(timestart)
    for thres_i = 1:20
        subplot(4,5,thres_i)
        imagesc(squeeze(smallWorldPValues(2,:,:,4,time_i,1,thres_i)));
        caxis([0 .05]);
        xlim([.5 5.5]);
        ylim([.5 5.5]);
        if thres_i == 1
            title(['Time is ' num2str(timestart(time_i)) ' to ' num2str(timeend(time_i))]);
        end
    end
    pause(2)
end
%%
freq_i =2;
dir_i = 1;

time_i = 7;
figure();

nrand = 1000;
k = 64;
p = [1:-.05:.05];

power_i = 3;
count = 0;
for cond_i = 1:5;
    count = count +1;
    subplot(1,3,1)
    plot(p,squeeze(nanmean(allPeople.normPathLengthMatrix(:,power_i,cond_i,freq_i,time_i,dir_i,:),1)));
    hold on
    subplot(1,3,2)
    plot(p,squeeze(nanmean(allPeople.normClusEffMatrix(:,power_i,cond_i,freq_i,time_i,dir_i,:),1)),'-.');
    hold on
    subplot(1,3,3)
    plot(p,squeeze(nanmean(allPeople.smallWorldMatrix(:,power_i,cond_i,freq_i,time_i,dir_i,:),1)),'--');
    hold on
    %if count==1||count==10||count==19
    %if count==1||count==2||count==3
    %ylabel([powerTypes{power_i}]);
    %if power_i == 2
    %if count==1||count==2||count==3||count==4||count==5||count==6||count==7||count==8||count==9
    %title([timestart(time_i) ' to ' timeend(time_i)]);
    %end
    %end
    %axis('square')
end
%end
