clc
clearvars
disp('Yaaaass')

% setup parameters
listingdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
datain = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\NoBL\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30;
fbins = 80;
frequencies = logspace(log10(fmin),log10(fmax),fbins);
times = (-1:0.001953125000000:3.5)*1000;
conditions = {'allrepeat','mixrepeat','switchto','switchaway','noninf','noninfrepeat','noninfswitch'};
wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);
wpms.allnames=wpms.names;
wpms.names ={'AGE002',	'AGE003',	'AGE004',	'AGE005',	'AGE007',	'AGE008',	'AGE012',	'AGE013',	'AGE014',	'AGE015',	'AGE017',	'AGE018',	'AGE019',	'AGE020',	'AGE021',	'AGE022',	'AGE023',	'AGE024',	'AGE026',	'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE046',	'AGE047',	'AGE050',	'AGE051',	'AGE053',	'AGE058',	'AGE059',	'AGE061',	'AGE062',	'AGE063',	'AGE066',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE072',	'AGE073',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE088',	'AGE089',	'AGE090',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE096',	'AGE097',	'AGE098',	'AGE100',	'AGE102',	'AGE103',	'AGE104',	'AGE107',	'AGE108',	'AGE109',	'AGE111',	'AGE114',	'AGE115',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE122',	'AGE123',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE133',	'AGE134',	'AGE135',	'AGE136',	'AGE138',	'AGE141',	'AGE145',	'AGE146',	'AGE147',	'AGE148',	'AGE149',	'AGE150',	'AGE151',	'AGE152',	'AGE153',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE163',	'AGE164',	'AGE165',	'AGE166',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE172',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE189',	'AGE190',	'AGE195',	'AGE197',	'AGE198',	'AGE199',	'AGE201',	'AGE202',	'AGE203',	'AGE205',	'AGE206',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE232',	'AGE233',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE241',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE273',	'AGE275',	'AGE276',	'AGE278',	'AGE279'};
clearvars -except wpms

SAVEDIR = 'F:\graphTheory';
LOADDIR = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\';

%% total power

baseline_start_cue = -300;
baseline_end_cue = -100;
baseline = setbaseline(wpms, baseline_start_cue, baseline_end_cue); %time points for baseline

totalBL = zeros(length(wpms.names),length(wpms.labels), length(wpms.frequencies),length(wpms.times),'single');

for cond_i = 1:length(wpms.conditions)
    fprintf('\n%s\t%s','Working on condition:',wpms.conditions{cond_i});
    for name_i  = 1:length(wpms.names)
        fprintf('%s',['working on ' wpms.names{name_i}]);
        tic
        loadname = ['F:\singleTrial' wpms.names{name_i} '_' wpms.conditions{cond_i} 'singleTrialTotal.mat'];
        load(loadname);
        for electrodesofinterest = 1:length(wpms.labels)
            total = 10*log10(squeeze(data(:,electrodesofinterest,:,:)));
            totalBL(:,electrodesofinterest,:,:) = bsxfun(@minus,total,mean(total(:,baseline),2));
        end
        toc
    end
    filename = ['F:\graphTheory\totalBL_' wpms.names{name_i} wpms.conditions{cond_i} 'singeTrialTotalBL.mat'];
    save(filename,'totalBL','-v7.3');
end

%% nonphase
clearvars -except wpms;
baseline_start_cue = -300;
baseline_end_cue = -100;
baseline = setbaseline(wpms, baseline_start_cue, baseline_end_cue); %time points for baseline

nonphaseBL = zeros(length(wpms.names),length(wpms.labels), length(wpms.frequencies),length(wpms.times),'single');

for cond_i = 1:length(wpms.conditions)
    fprintf('\n%s\t%s','Working on condition:',wpms.conditions{cond_i});
    for name_i  = 1:length(wpms.names)
        fprintf('%s',['working on ' wpms.names{name_i}]);
        tic
        loadname = ['E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\' wpms.names{name_i} '_' wpms.conditions{cond_i} '_ALL_POWER3.mat'];
        load(loadname);
        data = eegpower_all;
        data2 = inducedpower_all;
        for electrodesofinterest = 1:length(wpms.labels)
            nonphase = 10*log10(squeeze(data2(electrodesofinterest,:,:)));
            nonphaseBL(name_i,electrodesofinterest,:,:) = bsxfun(@minus,nonphase,mean(nonphase(:,baseline),2));
        end
        toc
    end
    filename = ['F:\graphTheory\nonphaseBL_' wpms.conditions{cond_i} '.mat'];
    save(filename,'nonphaseBL','-v7.3');
end

%% phase
clearvars -except wpms;

baseline_start_cue = -300;
baseline_end_cue = -100;
baseline = setbaseline(wpms, baseline_start_cue, baseline_end_cue); %time points for baseline

phaseBL = zeros(length(wpms.names),length(wpms.labels), length(wpms.frequencies),length(wpms.times),'single');

for cond_i = 1:length(wpms.conditions)
    fprintf('\n%s\t%s','Working on condition:',wpms.conditions{cond_i});
    for name_i  = 1:length(wpms.names)
        fprintf('%s',['working on ' wpms.names{name_i}]);
        tic
        loadname = ['E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\' wpms.names{name_i} '_' wpms.conditions{cond_i} '_ALL_POWER3.mat'];
        load(loadname);
        data = eegpower_all;
        data2 = inducedpower_all;
        for electrodesofinterest = 1:length(wpms.labels)
            total = 10*log10(squeeze(data(electrodesofinterest,:,:)));
            nonphase = 10*log10(squeeze(data2(electrodesofinterest,:,:)));
            phaseData = total-nonphase;
            phaseBL(name_i,electrodesofinterest,:,:) = bsxfun(@minus,phaseData,mean(phaseData(:,baseline),2));
        end
        toc
    end
    filename = ['F:\graphTheory\phaseBL_' wpms.conditions{cond_i} '.mat'];
    save(filename,'phaseBL','-v7.3');
end

%% meaning over freqency & setting up time points

clearvars -except wpms;

powerTypes={'totalBL','nonphaseBL','phaseBL'};

freqStart = [2   4   8    13];
freqEnd =   [3.9 7.9 12.9 30] ;

timestart = [0:100:1800];
timeend = timestart+200;

for power_type = 3%1:length(powerTypes)
    fprintf('\n%s\t%s','Setting up frequency correlation matrices for power type:',powerTypes{power_type});
    for cond_i = 1:length(wpms.conditions)
        fprintf('\n%s\t%s','Condition:',wpms.conditions{cond_i});
        tic
        A=load(['F:\graphTheory\' powerTypes{power_type} '_' wpms.conditions{cond_i}  '.mat']);
        fname = fieldnames(A);
        data=A.(fname{1});
        corrMatrix = zeros(length(freqStart),length(timestart),length(wpms.labels),length(wpms.labels));
        for freq_i = 1:length(freqStart)
            fprintf('\n%s\t%i\t','working on freq ', freq_i);
            tempFreq = wpms.frequencies-freqStart(freq_i);
            [~,freqStartIndex] = min(abs(tempFreq));
            tempFreq = wpms.frequencies-freqEnd(freq_i);
            [~,freqEndIndex] = min(abs(tempFreq));
            freqInds = freqStartIndex:freqEndIndex;
            for time_i = 1:length(timestart)
                fprintf('.')
                timePoints= setbaseline(wpms, timestart(time_i), timeend(time_i));
                for electrode_i = 1:length(wpms.labels)
                    for electrode_j = 1:length(wpms.labels)
                        data1 = squeeze(mean(mean(data(:,electrode_i,freqInds,timePoints),3),4));
                        data2 = squeeze(mean(mean(data(:,electrode_j,freqInds,timePoints),3),4));
                        corrMatrix(freq_i,time_i,electrode_i,electrode_j) = corr(data1,data2);
                    end
                end
            end
            toc
        end
        save(['F:\graphTheory\correlationMatrices\' powerTypes{power_type} '_' wpms.conditions{cond_i}  '.mat'],'corrMatrix', '-v7.3');
    end
end

%%
clearvars -except wpms;
powerTypes={'totalBL','nonphaseBL','phaseBL'};

% make a network to compute, start with positive correlations (do neg
% later)
%run positive and negative loops

powerTypes={'totalBL','nonphaseBL','phaseBL'};

freqStart = [2   4   8    13];
freqEnd =   [3.9 7.9 12.9 30] ;

timestart = [0:100:1800];
timeend = timestart+200;

nrand = 1000;
k = 64;
p = [1:-.05:.05];
freqBands = {'delta','theta','alpha','beta'};

randNetwork = zeros(length(powerTypes),length(wpms.conditions),length(timestart),length(freqStart));
for randn = 1:1000;
    for power_type = 1:length(powerTypes)
        fprintf('\n%s\t%s','Setting up frequency correlation matrices for power type:',powerTypes{power_type});
        for cond_i = 1:length(wpms.conditions)
            fprintf('\n%s\t%s','Condition:',wpms.conditions{cond_i});
            load(['F:\graphTheory\correlationMatrices\' powerTypes{power_type} '_' wpms.conditions{cond_i}  '.mat'])
            origData = corrMatrix;
            for direction = 1:2
                switch direction
                    case 1
                        %postitive data
                        data = origData;
                        data(data <= 0) = 0;
                        netLabel = 'pos';
                    case 2
                        %negative data
                        data = origData;
                        data(data>=0)=0;
                        %flip signs to become positive
                        data=data./-1;
                        netLabel = 'neg';
                end
                normRichClub = zeros(k,length(timestart),length(p));
                normClusEff = zeros(1,length(timestart),length(p));
                normPathLength = zeros(1,length(timestart),length(p));
                normLocalEff = zeros(length(wpms.labels),length(timestart),length(p));
                for freq_i = 1:length(freqBands);
                    fprintf('\n%s\t%s','Working on frequency band:',freqBands{freq_i});
                    for thres_i = 1:length(p)
                        if mod(thres_i,100)==0
                            fprintf('\n%s\t%i','working on thres ', thres_i);
                        end
                        for time_i = 1:length(timestart)
                            W = threshold_proportional(squeeze(data(freq_i,time_i,:,:)), p(thres_i));
                            %binarise network
                            b = ~~W;
                            % run clustering coefficent (b for binarised, u for undirected
                            C=clustering_coef_bu(b);
                            % efficency - 1 = local, 0 = global
                            E = efficiency_bin(b,0);
                            F = efficiency_bin(b,1);
                            [R,Nk,Ek] = rich_club_bu(b,k);
                            % randomised networks
                            rC = zeros(1,nrand);
                            rE = zeros(1,nrand);
                            rR = zeros(length(wpms.labels),nrand);
                            rNk= zeros(length(wpms.labels),nrand);
                            rEk = zeros(length(wpms.labels),nrand);
                            rF = zeros(length(wpms.labels),nrand);
                            for ri = 1:nrand
                                fprintf('\n%s\t%i','working on network ', ri);
                                ITER = 5;
                                [rb,eff] = randmio_und(b, ITER);
                                % run clustering coefficent for randomised network(b for binarised, u for undirected
                                rC(1,ri)=mean(clustering_coef_bu(rb));
                                % efficency for randomised - 1 = local, 0 = global
                                rE(1,ri)=efficiency_bin(rb,0);
                                [rR(:,ri),rNk(:,ri),rEk(:,ri)] = rich_club_bu(rb,k);
                                rF(:,ri) = efficiency_bin(rb,1);
                            end
                            normRichClub(:,time_i,thres_i) = R./nanmean(rR,2)';
                            normClusEff(1,time_i,thres_i) = mean(C)/mean(rC);
                            normPathLength(1,time_i,thres_i) = mean(E)/mean(rE);
                            normLocalEff(:,time_i,thres_i) = F./mean(rF,2);
                        end
                    end
                    smallWorldRatio = normClusEff./normPathLength;
                    filename = ['F:\graphTheory\graphMeasures\' powerTypes{power_type} '_' wpms.conditions{cond_i} '_' freqBands{freq_i} '_' netLabel '.mat'];
                    save(filename,'smallWorldRatio','normRichClub','normClusEff','normPathLength', 'normLocalEff', '-v7.3');
                end
            end
        end
    end
end
%
% posData = origData;
% posData(posData <= 0) = 0;
%posData(posData==1) = 0;

% thresholding
% subplot(1,2,1)
% imagesc(squeeze(posData(2,7,:,:)));

% subplot(1,2,2)
% imagesc(W)
% imagesc(b)
% plot(C)
%% computing graph measures

normRichClub = zeros(k,length(timestart),length(p));
normClusEff = zeros(1,length(timestart),length(p));
normPathLength = zeros(1,length(timestart),length(p));
normLocalEff = zeros(length(wpms.labels),length(timestart),length(p));

for thres_i = 1:length(p)
    fprintf('\n%s\t%i','working on thres ', thres_i);
    for time_i = 1:length(timestart)
        W = threshold_proportional(squeeze(posData(1,time_i,:,:)), p(thres_i));
        %binarise network
        b = ~~W;
        % run clustering coefficent (b for binarised, u for undirected
        C=clustering_coef_bu(b);
        % efficency - 1 = local, 0 = global
        E = efficiency_bin(b,0);
        F = efficiency_bin(b,1);
        [R,Nk,Ek] = rich_club_bu(b,k);
        % randomised networks
        rC = zeros(1,nrand);
        rE = zeros(1,nrand);
        rR = zeros(length(wpms.labels),nrand);
        rNk= zeros(length(wpms.labels),nrand);
        rEk = zeros(length(wpms.labels),nrand);
        rF = zeros(length(wpms.labels),nrand);
        for ri = 1:nrand
            fprintf('\n%s\t%i','working on network ', ri);
            ITER = 5;
            [rb,eff] = randmio_und(b, ITER);
            % run clustering coefficent for randomised network(b for binarised, u for undirected
            rC(1,ri)=mean(clustering_coef_bu(rb));
            % efficency for randomised - 1 = local, 0 = global
            rE(1,ri)=efficiency_bin(rb,0);
            [rR(:,ri),rNk(:,ri),rEk(:,ri)] = rich_club_bu(rb,k);
            rF(:,ri) = efficiency_bin(rb,1);
        end
        % you want to save these later
        normRichClub(:,time_i,thres_i) = R./nanmean(rR,2)';
        normClusEff(1,time_i,thres_i) = mean(C)/mean(rC);
        normPathLength(1,time_i,thres_i) = mean(E)/mean(rE);
        normLocalEff(:,time_i,thres_i) = F./mean(rF,2);
    end
end
smallWorldRatio = normClusEff./normPathLength;

save('E:\FNL_EEG_TOOLBOX\data_for_papers\connectivity\testDataTotalSTdelta','smallWorldRatio','normRichClub','normClusEff','normPathLength', 'normLocalEff', '-v7.3');

% plot(p,smallWorldRatio)
% imagesc(normRichClub)
%
%%

plot(p,squeeze(normPathLength(1,:)));
hold on
plot(p,squeeze(normClusEff(1,:)))
plot(p,squeeze(smallWorldRatio(1,:)))

for time_i = 1:length(timestart)
    subplot(5,4,time_i);
    %imagesc(squeeze(normRichClub(:,time_i,:))');
    contourf(1:length(wpms.labels),p,squeeze(normRichClub(:,time_i,:))',50,'linecolor','none');
    title(['Time ' num2str(timestart(time_i)) ' to ' num2str(timeend(time_i))]);
    caxis([0 1.5]);
    set(gca,'YTick',[5:5:20],'YTickLabel',p(5:5:20));
end



%%

freqNames = {'delta', 'theta', 'alpha','beta'};

for freq_i = 1:length(freqStart)
    loadname = ['E:\FNL_EEG_TOOLBOX\data_for_papers\connectivity\testDataTotalST' freqNames{freq_i}];
    load(loadname)
    subplot(1,4,freq_i)
    plot(p,squeeze(normPathLength(freq_i,7,:)));
    hold on
    plot(p,squeeze(normClusEff(freq_i,7,:)))
    plot(p,squeeze(smallWorldRatio(freq_i,7,:)))
    title(freqNames{freq_i});
end

