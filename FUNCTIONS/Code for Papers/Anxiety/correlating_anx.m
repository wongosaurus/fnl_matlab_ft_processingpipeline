% load data

datain = 'E:\FNL_EEG_TOOLBOX\data_for_papers\anxiety\Single-trial Beta Values\';
wpms.conditions = {'mixrepeat','switchto','noninfrepeat','noninfswitch'};

wpms.names = {'AGE003','AGE004','AGE005','AGE012','AGE022','AGE026',...
    'AGE030','AGE032','AGE035','AGE038','AGE046','AGE050','AGE058',...
    'AGE062','AGE064','AGE069','AGE077','AGE081','AGE089','AGE094',...
    'AGE100','AGE107','AGE108','AGE109','AGE114','AGE117','AGE118',...
    'AGE119','AGE129','AGE134','AGE138','AGE155','AGE162','AGE165',...
    'AGE166','AGE168','AGE169','AGE179','AGE182','AGE183','AGE184',...
    'AGE187','AGE192','AGE200','AGE205','AGE224','AGE225','AGE226',...
    'AGE230','AGE237','AGE238','AGE245','AGE247','AGE252','AGE260',...
    'AGE267','AGE268','AGE269','AGE270','AGE277',...
    'AGE002', 'AGE007', 'AGE008', 'AGE009', 'AGE013', 'AGE014', ...
    'AGE015', 'AGE017', 'AGE018', 'AGE019', 'AGE020', 'AGE021', 'AGE023', ...
    'AGE024', 'AGE027', 'AGE028', 'AGE033', 'AGE034', 'AGE036', 'AGE043', ...
    'AGE047', 'AGE051', 'AGE052', 'AGE053', 'AGE059', 'AGE061', 'AGE063', ...
    'AGE066', 'AGE067', 'AGE068', 'AGE070', 'AGE072', 'AGE073', 'AGE075', ...
    'AGE079', 'AGE083', 'AGE084', 'AGE085', 'AGE086', 'AGE088', 'AGE090', ...
    'AGE092', 'AGE093', 'AGE095', 'AGE096', 'AGE097', 'AGE098', 'AGE102', ...
    'AGE103', 'AGE104', 'AGE106', 'AGE111', 'AGE115', 'AGE116', 'AGE120', ...
    'AGE121', 'AGE122', 'AGE123', 'AGE124', 'AGE127', 'AGE128', 'AGE131', ...
    'AGE133', 'AGE135', 'AGE136', 'AGE141', 'AGE145', 'AGE146', 'AGE147', ...
    'AGE148', 'AGE150', 'AGE151', 'AGE152', 'AGE153', 'AGE158', 'AGE159', ...
    'AGE160', 'AGE161', 'AGE163', 'AGE164', 'AGE167', 'AGE170', 'AGE172', ...
    'AGE176', 'AGE177', 'AGE178', 'AGE180', 'AGE181', 'AGE185', 'AGE186', ...
    'AGE189', 'AGE190', 'AGE195', 'AGE197', 'AGE198', 'AGE201', 'AGE202', ...
    'AGE203', 'AGE204', 'AGE206', 'AGE207', 'AGE208', 'AGE209', 'AGE210', ...
    'AGE211', 'AGE216', 'AGE217', 'AGE218', 'AGE219', 'AGE220', 'AGE221', ...
    'AGE222', 'AGE227', 'AGE228', 'AGE229', 'AGE231', 'AGE232', 'AGE233', ...
    'AGE234', 'AGE236', 'AGE239', 'AGE240', 'AGE241', 'AGE243', 'AGE244', ...
    'AGE246', 'AGE248', 'AGE249', 'AGE251', 'AGE253', 'AGE254', 'AGE255', ...
    'AGE256', 'AGE257', 'AGE259', 'AGE261', 'AGE262', 'AGE264', 'AGE266', ...
    'AGE273', 'AGE275', 'AGE276', 'AGE278', 'AGE279'};

PARTID = {'AGE001',	'AGE002',	'AGE003',	'AGE004',	'AGE005',	'AGE006',	'AGE007',	'AGE008',	'AGE009',	'AGE010',	'AGE011',	'AGE012',	'AGE013',	'AGE014',	'AGE015',	'AGE016',	'AGE017',	'AGE018',	'AGE019',	'AGE020',	'AGE021',	'AGE022',	'AGE023',	'AGE024',	'AGE025',	'AGE026',	'AGE027',	'AGE028',	'AGE029',	'AGE030',	'AGE031',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE037',	'AGE038',	'AGE039',	'AGE040',	'AGE041',	'AGE042',	'AGE043',	'AGE044',	'AGE045',	'AGE046',	'AGE047',	'AGE048',	'AGE049',	'AGE050',	'AGE051',	'AGE052',	'AGE053',	'AGE054',	'AGE055',	'AGE056',	'AGE057',	'AGE058',	'AGE059',	'AGE060',	'AGE061',	'AGE062',	'AGE063',	'AGE064',	'AGE065',	'AGE066',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE071',	'AGE072',	'AGE073',	'AGE074',	'AGE075',	'AGE076',	'AGE077',	'AGE078',	'AGE079',	'AGE080',	'AGE081',	'AGE082',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE087',	'AGE088',	'AGE089',	'AGE090',	'AGE091',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE096',	'AGE097',	'AGE098',	'AGE099',	'AGE100',	'AGE101',	'AGE102',	'AGE103',	'AGE104',	'AGE105',	'AGE106',	'AGE107',	'AGE108',	'AGE109',	'AGE110',	'AGE111',	'AGE112',	'AGE113',	'AGE114',	'AGE115',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE122',	'AGE123',	'AGE124',	'AGE125',	'AGE126',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE132',	'AGE133',	'AGE134',	'AGE135',	'AGE136',	'AGE137',	'AGE138',	'AGE139',	'AGE140',	'AGE141',	'AGE142',	'AGE143',	'AGE144',	'AGE145',	'AGE146',	'AGE147',	'AGE148',	'AGE149',	'AGE150',	'AGE151',	'AGE152',	'AGE153',	'AGE154',	'AGE155',	'AGE156',	'AGE157',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE163',	'AGE164',	'AGE165',	'AGE166',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE171',	'AGE172',	'AGE173',	'AGE174',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE188',	'AGE189',	'AGE190',	'AGE191',	'AGE192',	'AGE193',	'AGE194',	'AGE195',	'AGE196',	'AGE197',	'AGE198',	'AGE199',	'AGE200',	'AGE201',	'AGE202',	'AGE203',	'AGE204',	'AGE205',	'AGE206',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE212',	'AGE213',	'AGE214',	'AGE215',	'AGE216',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE223',	'AGE224',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE232',	'AGE233',	'AGE234',	'AGE235',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE240',	'AGE241',	'AGE242',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE250',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE262',	'AGE263',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE271',	'AGE272',	'AGE273',	'AGE274',	'AGE275',	'AGE276',	'AGE277',	'AGE278',	'AGE279'};
DASSanx = [NaN	NaN	NaN	4	3	NaN	12	9	6	NaN	NaN	0	0	1	NaN	NaN	0	1	3	NaN	2	0	3	4	NaN	2	8	0	NaN	0	NaN	1	8	8	10	1	NaN	7	NaN	NaN	NaN	6	1	NaN	NaN	NaN	1	NaN	NaN	1	2	5	9	NaN	NaN	NaN	NaN	0	1	NaN	13	5	NaN	9	NaN	NaN	3	7	3	0	NaN	9	0	NaN	1	NaN	0	NaN	NaN	NaN	1	NaN	0	1	2	0	NaN	12	0	4	NaN	1	5	2	2	NaN	NaN	NaN	NaN	2	NaN	1	NaN	0	NaN	1	NaN	13	2	NaN	1	NaN	NaN	11	NaN	1	0	2	0	1	8	NaN	3	2	NaN	NaN	6	8	2	6	14	NaN	16	1	1	NaN	NaN	1	NaN	NaN	NaN	NaN	21	NaN	3	8	0	NaN	2	8	4	NaN	4	NaN	2	5	5	7	1	1	2	0	NaN	7	8	1	2	0	0	3	1	0	NaN	NaN	1	5	1	0	0	8	14	17	2	1	2	1	2	NaN	6	5	NaN	6	NaN	NaN	0	NaN	9	2	1	3	0	1	4	1	6	NaN	1	1	3	2	0	1	NaN	NaN	NaN	1	1	0	0	1	0	12	NaN	3	3	1	0	0	3	0	3	NaN	2	12	3	1	8	5	6	0	0	NaN	0	0	4	2	5	6	8	2	1	0	2	2	0	1	2	2	5	16	6	4	0	5	0	0	2	0	6	9	NaN	2	0	NaN	1	0	11	4	1];
DASSstr	= [NaN	NaN	NaN	12	6	NaN	10	6	10	NaN	NaN	0	0	4	NaN	NaN	0	1	1	NaN	1	2	8	4	NaN	7	7	0	NaN	1	NaN	2	11	13	6	6	NaN	7	NaN	NaN	NaN	6	8	NaN	NaN	NaN	1	NaN	NaN	3	0	5	9	NaN	NaN	NaN	NaN	1	0	NaN	13	7	NaN	12	NaN	NaN	13	1	7	2	NaN	12	4	NaN	5	NaN	3	NaN	NaN	NaN	3	NaN	2	1	7	0	NaN	19.5	0	4	NaN	6	11	12	7	NaN	NaN	NaN	NaN	2	NaN	9	NaN	2	NaN	11	NaN	17	4	NaN	5	NaN	NaN	17	NaN	2	3	2	3	3	6	NaN	5	1	NaN	NaN	10	7	1	16	17	NaN	14	2	1	NaN	NaN	2	NaN	NaN	NaN	NaN	21	NaN	7	8	9	NaN	5	7	4	NaN	3	NaN	4	3	13	7	2	10	2	2	NaN	6	11	3	8	4	0	8	4	0	NaN	NaN	3	3	6	2	0	14	13	16	8	2	4	5	8	NaN	11	10	NaN	2	NaN	NaN	3	NaN	11	8	1	3	2	5	1	5	8	NaN	3	3	8	1	0	2	NaN	NaN	NaN	4	5	1	5	5	2	14	NaN	0	3	2	0	5	9	0	8	NaN	4	14	3	2	12	8	14	6	16	NaN	0	1	4	2	6	9	7	3	6	0	7	2	0	4	9	9	4	15	5	9	0	2	0	3	3	4	6	7	NaN	8	0	NaN	13	4	9	4	7];
DASSdep	= [NaN	NaN	NaN	0	2	NaN	10	2	3	NaN	NaN	0	0	2	NaN	NaN	1	0	0	NaN	1	9	3	5	NaN	2	11	1	NaN	0	NaN	11	12	5	1	0	NaN	17	NaN	NaN	NaN	2	4	NaN	NaN	NaN	2	NaN	NaN	0	5	3	8	NaN	NaN	NaN	NaN	0	1	NaN	9	1	NaN	2	NaN	NaN	17	1	1	1	NaN	5	1	NaN	1	NaN	0	NaN	NaN	NaN	2	NaN	0	1	3	0	NaN	12	0	1	NaN	6	9	3	2	NaN	NaN	NaN	NaN	1	NaN	6	NaN	1	NaN	3	NaN	16	1	NaN	2	NaN	NaN	8	NaN	0	1	2	1	2	1	NaN	4	2	NaN	NaN	15	5	1	4	8	NaN	7	0	1	NaN	NaN	1	NaN	NaN	NaN	NaN	21	NaN	8	6	5	NaN	2	12	5	NaN	2	NaN	12	1	14	12	3	1	2	3	NaN	4	12	1	9	0	0	3	3	0	NaN	NaN	2	5	3	0	0	6	11	16	6	3	2	1	1	NaN	5	8	NaN	2	NaN	NaN	2	NaN	12	1	2	6	2	4	0	8	10	NaN	2	1	1	1	0	0	NaN	NaN	NaN	0	1	1	2	2	0	10	NaN	1	3	1	0	2	5	0	7	NaN	0	14	0	2	9	2	9	1	4	NaN	0	2	3	2	4	6	14	1	0	0	6	1	0	3	10	0	2	14	1	3	0	2	0	0	1	4	5	4	NaN	4	0	NaN	6	0	7	2	2];
allDASS = nansum([DASSanx;DASSstr;DASSdep]);

%%
betas = zeros(length(wpms.names),length(wpms.conditions),80,2305);
for name_i = 1:length(wpms.names)
    for cond_i = 1:length(wpms.conditions)
        filename = [datain wpms.names{name_i} '_' wpms.conditions{cond_i} '_Frontal_singleTrialRegression_z.mat'];
        load (filename,'betas_bl')
        betas(name_i, cond_i,:,:) = squeeze(betas_bl(2,:,:));
    end
end

%%

inds = find(ismember(PARTID,wpms.names));
RHO = zeros(length(wpms.conditions)+1,80,2305);
PVAL = RHO;
X = allDASS(inds)';%DASSanx(inds)';

for cond_i = 1:length(wpms.conditions)+1
    if cond_i == length(wpms.conditions)+1
    fprintf('\n%s\t%s','Condition:','average');
    else
        fprintf('\n%s\t%s','Condition:',wpms.conditions{cond_i});
    end
    for freq_i = 1:80
        fprintf('.');
        for time_i = 1:2305
            if cond_i == length(wpms.conditions)+1
                %average across all conditions
                Y = squeeze(mean(betas(:,:,freq_i,time_i),2));
                [RHO(cond_i,freq_i,time_i),PVAL(cond_i,freq_i,time_i)] = corr(X,Y,'rows','pairwise');
            else
                Y = squeeze(betas(:,cond_i,freq_i,time_i));
                [RHO(cond_i,freq_i,time_i),PVAL(cond_i,freq_i,time_i)] = corr(X,Y,'rows','pairwise');
            end
        end
    end
end

%%
%plotting
%add fdr path
% addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'));
%first anxiety
%grab screen size to figure out width and height
screenRes = get(0,'ScreenSize');
width =  screenRes(3);
height = screenRes(4);
condNames = [wpms.conditions 'average'];
figure();set(gcf,'Color',[1 1 1],'Position',[0 0 width height]);
time = -1000:(1/512)*1000:3500;
plot_times = find(round(time)==-301,1):find(time==2000);
frex = logspace(log10(2),log10(30),80);
for cond_i = 1:length(condNames)
    subplot(4,4,cond_i);
    contourf(time(plot_times),frex,squeeze(RHO_anx(cond_i,:,plot_times)),50,'linecolor','none');
    hold on
    mask = squeeze(PVAL_anx(cond_i,:,:)<0.05);
    contour(time(plot_times),frex,mask(:,plot_times),1,'k','LineWidth',2);
    title(condNames{cond_i});
    caxis([-.3 .3]);axis square;colormap jet
    if cond_i ==1
        ylabel('Anxiety');
    end
end
%now depression
for cond_i = 1:length(wpms.conditions)
    subplot(4,4,cond_i+4);
    contourf(time(plot_times),frex,squeeze(RHO_dep(cond_i,:,plot_times)),50,'linecolor','none');
    hold on
    mask = squeeze(PVAL_dep(cond_i,:,:)<0.05);
    contour(time(plot_times),frex,mask(:,plot_times),1,'k','LineWidth',2);
%     title(wpms.conditions{cond_i});
    caxis([-.3 .3]);axis square;colormap jet
    if cond_i ==1
        ylabel('Depression');
    end
end
%now stress
for cond_i = 1:length(condNames)
    subplot(4,4,cond_i+8);
    contourf(time(plot_times),frex,squeeze(RHO_str(cond_i,:,plot_times)),50,'linecolor','none');
    hold on
    mask = squeeze(PVAL_str(cond_i,:,:)<0.05);
    contour(time(plot_times),frex,mask(:,plot_times),1,'k','LineWidth',2);
%     title(wpms.conditions{cond_i});
    caxis([-.3 .3]);axis square;colormap jet
    if cond_i ==1
        ylabel('Stress');
    end
end
%now total
for cond_i = 1:length(condNames)
    subplot(1,5,cond_i);
    contourf(time(plot_times),frex,squeeze(RHO_tot(cond_i,:,plot_times)),50,'linecolor','none');
    hold on
    mask = squeeze(PVAL_tot(cond_i,:,:)<0.05);
    contour(time(plot_times),frex,mask(:,plot_times),1,'k','LineWidth',2);
%     title(wpms.conditions{cond_i});
    caxis([-.3 .3]);axis square;colormap jet
    if cond_i ==1
        ylabel({'Total';'Frequency (Hz)'});
        xlabel('Time (ms)');
    end
end

%% 
figure();set(gcf,'Color',[1 1 1],'Position',[0 0 width height]);
subplot(1,2,2);
contourf(time(plot_times),frex,squeeze(RHO_tot(5,:,plot_times)),50,'linecolor','none');
hold on;
mask = squeeze(PVAL_tot(5,:,:)<0.05);
contour(time(plot_times),frex,mask(:,plot_times),1,'k','LineWidth',2);
caxis([-.3 .3]);axis square;colormap jet

xlim([0 1000]);ylim([2 10]);
title('w/ distress')
set(gca,'FontSize',24,'LineWidth',2);
h=ttest(squeeze(mean(betas,2)));
subplot(1,2,1);
contourf(time(plot_times),frex,squeeze(mean(mean(betas(:,:,:,plot_times),1),2)),50,'linecolor','none');
hold on;
mask = squeeze(h);
contour(time(plot_times),frex,mask(:,plot_times),1,'k','LineWidth',2);
caxis([-.05 .05]);axis square;colormap jet
xlim([0 1000]);ylim([2 10])
title('w/o distress')
ylabel('Frequency (Hz)');
xlabel('Time (ms)');
set(gca,'FontSize',24,'LineWidth',2);
%% scatter
figure();
for t=1:500;
    tinds=find(round(time(plot_times))==t,1):find(round(time(plot_times))==t+100,1,'last');
    finds=find(round(frex)==6,1):find(round(frex)==6,1,'last');
    x=squeeze(mean(mean(mean(betas(:,:,finds,tinds),2),3),4));
    y=allDASS(inds)';
    lowInds = y<100
    scatter(x(lowInds),y(lowInds));
    title([num2str(t) ' to ' num2str(t+100)]);
    pause(0.25);
end
%%
p = polyfit(x(lowInds),y(lowInds),1)
f = polyval(p,x(lowInds));
hold on;
plot( polyval(p,x(lowInds)));