names = { 'AGE009',...
    'AGE052',...
    'AGE064',...
    'AGE079',...
    'AGE106',...
    'AGE192',...
    'AGE216',...
    'AGE234',...
    'AGE240',...
    'AGE262'};

conditions = {'allrepeat','mixrepeat','switchto'};

labels = {'RT_allrepeat'	'RT_mixrepeat'	'RT_switchto'	'RTCOST_Switch'	'RTCOST_Mix'};
BEH_DATA = zeros(length(names),length(labels));
%names = {'AGE002'};
for name_i = 1:length(names)
    fprintf('%s ',names{name_i});
    for cond_i = 1:length(conditions)
        fprintf('.');
        A = load(['E:\FNL_EEG_TOOLBOX\REPAIRED_DATA\',names{name_i},'_RepairedData_',conditions{cond_i},'.mat']);
        fnames = fieldnames(A);
        [BEH_DATA(name_i,cond_i),~] = extractBehaviouralData(A.(fnames{1}));
    end
    fprintf('\n');
end
BEH_DATA(:,4) = BEH_DATA(:,2)-BEH_DATA(:,1); %All-Mix
BEH_DATA(:,5) = BEH_DATA(:,3)-BEH_DATA(:,2); %Sw-Rep

%Write to file:

fid = fopen('Additional_BEH_DATA.txt','w');

fprintf(fid,'Subject,RT_allrepeat,RT_mixrepeat,RT_switchto,RTCOST_Switch,RTCOST_Mix,\n');
for name_i = 1:length(names)
    fprintf(fid,'%s,',names{name_i});
    for col_i = 1:5
        fprintf(fid,'%4.4f,',BEH_DATA(name_i,col_i));
    end
    fprintf(fid,'\n');
end
fclose(fid);