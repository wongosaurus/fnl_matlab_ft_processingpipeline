clearvars;

clear all;close all;clc

% setup parameters
listingdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
datain = 'E:\FNL_EEG_TOOLBOX\REPAIRED_DATA';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'};
fmin = 2;
fmax = 30; 
fbins = 80;
times = (-1:0.001953125000000:3.5)*1000;
%conditions = {'mixrepeat','switchto','noninfrepeat','noninfswitch'};
conditions = {'mixrepeat','switchto','noninf'};

wpms = setwpms(listingdir, datain, labels, fmin, fmax, fbins, times, conditions);

wpms.dirs.datain.time1 = 'E:\FNL_EEG_TOOLBOX\REPAIRED_DATA';
wpms.dirs.datain.time2 = 'E:\FNL_EEG_TOOLBOX\REPAIRED_DATA2';

listingdir.phase1 = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat';
listingdir.phase2 = 'E:\FNL_EEG_TOOLBOX\REPAIRED_DATA2\*_RepedData_mixrepeat';

listings = dir(listingdir);
wpms.names.phase1 = {length(listings)};
for file_i =1:length(listings)
    wpms.names{file_i} = listings(file_i).name(1:6);
end



wpms.participants.time1    = {'AGE003','AGE004','AGE005','AGE012','AGE022',...
'AGE026','AGE030','AGE032','AGE035','AGE038','AGE046','AGE058','AGE062',...
'AGE069','AGE077','AGE081','AGE089','AGE094','AGE100','AGE107',...
'AGE108','AGE109','AGE114','AGE117','AGE118','AGE119','AGE129','AGE134',...
'AGE155','AGE162','AGE165','AGE166','AGE179','AGE182','AGE183',...
'AGE184','AGE187','AGE200','AGE205','AGE225','AGE226','AGE230',...
'AGE237','AGE238','AGE245','AGE247','AGE252','AGE260','AGE267','AGE268',...
'AGE269','AGE270','AGE277'};

wpms.participants.time2 = {'BGE003','BGE004','BGE005','BGE012','BGE022','BGE026','BGE030','BGE032',...
    'BGE035','BGE038','BGE046','BGE058','BGE062','BGE069','BGE077','BGE081','BGE089',...
    'BGE094','BGE100','BGE107','BGE108','BGE109','BGE114','BGE117','BGE118','BGE119','BGE129','BGE134',...
    'BGE155','BGE162','BGE165','BGE166','BGE179','BGE182','BGE183','BGE184','BGE187',...
    'BGE200','BGE205','BGE225','BGE226','BGE230','BGE237','BGE238','BGE245','BGE247','BGE252','BGE260',...
    'BGE267','BGE268','BGE269','BGE270','BGE277'};



%%
disp('1. Bad channel counting')
badChannelCount.time1 = zeros(1,length(wpms.participants.time1));
badChannelCount.time2 = zeros(1,length(wpms.participants.time2));
badChannelCount.outgroup = zeros(1,length(wpms.participants.outgroup));

fname=fieldnames(wpms.participants);
for f_i = 1:length(fname)
    for name_i=1:length(wpms.participants.(fname{f_i}))
        fprintf('\n%s\t',wpms.participants.(fname{f_i}){name_i});
        filename = [wpms.dirs.datain.(fname{f_i}) '\' wpms.participants.(fname{f_i}){name_i} '_RepairedData_allrepeat.mat'];
        load(filename);
        badChannelCount.(fname{f_i})(1,name_i)=length(ardata.cfg.badchannel);
        fprintf('%i\t%s',length(ardata.cfg.badchannel),'bad channels');
    end
end
% will this save each section???
savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\single_trial\badChannelCount.mat'];
save(savename,'badChannelCount');

fprintf('\n');
%%
disp('2. Counting number of ICA components removed AND determining valid trial counts')

ICAComponents.time1 = zeros(1,length(wpms.participants.time1));
ICAComponents.time2 = zeros(1,length(wpms.participants.time2));
ICAComponents.outgroup = zeros(1,length(wpms.participants.outgroup));

validTrials.time1 = zeros(1,length(wpms.participants.time1));
validTrials.time2 = zeros(1,length(wpms.participants.time2));
validTrials.outgroup = zeros(1,length(wpms.participants.outgroup));

fname=fieldnames(wpms.participants);
for f_i = 1:length(fname)
    for name_i=1:length(wpms.participants.(fname{f_i}))
        tic;
        try
            fprintf('\n%s\t',wpms.participants.(fname{f_i}){name_i});
            filename = [wpms.dirs.datain.(fname{f_i}) '\' wpms.participants.(fname{f_i}){name_i} '_RepairedData_allrepeat.mat'];
            load(filename);
            ICAComponents.(fname{f_i})(1,name_i) = length(ardata.cfg.previous.previous.previous.previous.component);
            fprintf('%i\t%s\t\t',length(ardata.cfg.previous.previous.previous.previous.component),'components channels');
            validTrials.(fname{f_i})(1,name_i) = length(ardata.cfg.previous.previous.previous.previous.previous.previous.trl);
            fprintf('%i\t%s',length(ardata.cfg.previous.previous.previous.previous.previous.previous.trl),'valid trials');
        catch exception
            disp(['failed for ' wpms.participants.(fname{f_i})(name_i)]);
        end%try/catch loop
        t=toc;
        fprintf('\t%3.2f %s',t);
    end
end

savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\single_trial\ICAComponent.mat'];
save(savename,'ICAComponents');

savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\single_trial\validTrials.mat'];
save(savename,'validTrials');

fprintf('\n');

%% 
disp('3. Determining each trial count');

trialCount.time1 = zeros(length(wpms.conditions),length(wpms.participants.time1));
trialCount.time2 = zeros(length(wpms.conditions),length(wpms.participants.time2));
trialCount.outgroup = zeros(length(wpms.conditions),length(wpms.participants.outgroup));

fname=fieldnames(wpms.participants);
for f_i = 1:length(fname)
    for name_i=1:length(wpms.participants.(fname{f_i}))
        fprintf('\n%s\t',wpms.participants.(fname{f_i}){name_i});
        ts = zeros(1,length(wpms.conditions));
        for cond_i = 1:length(wpms.conditions)
            tic;
            try
                fprintf('.');
                filename = [wpms.dirs.datain.(fname{f_i}) '\' wpms.participants.(fname{f_i}){name_i} '_RepairedData_' wpms.conditions{cond_i} '.mat'];
                if exist(filename,'file')==2;
                    A=load(filename);
                    datname = fieldnames(A);
                    trialCount.(fname{f_i})(cond_i,name_i) = length(A.(datname{1}).trial);
                else
                    trialCount.(fname{f_i})(cond_i,name_i) = NaN; 
                end
            catch exception
                disp(['failed for ' wpms.participants.(fname{f_i})(name_i)]);
            end
            ts(1,cond_i) = toc;
        end
        t=mean(ts);
    end
end

savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\single_trial\trialCount.mat'];
save(savename,'trialCount');

%%
% bad channels
fname=fieldnames(wpms.participants);
for f_i = 1:length(fname)
    meanBC(f_i) = mean(badChannelCount.(fname{f_i}));
    SDBC(f_i) = std(badChannelCount.(fname{f_i}));
    minBC(f_i) = min(badChannelCount.(fname{f_i}));
    maxBC(f_i) = max(badChannelCount.(fname{f_i}));
end 
% ICA
for f_i = 1:length(fname)
    meanICA(f_i) = mean(ICAComponents.(fname{f_i}));
    SDICA(f_i) = std(ICAComponents.(fname{f_i}));
    minICA(f_i) = min(ICAComponents.(fname{f_i}));
    maxICA(f_i) = max(ICAComponents.(fname{f_i}));
end 
% Valid trials
for f_i = 1:length(fname)
    meanVT(f_i) = mean(validTrials.(fname{f_i}));
    SDVT(f_i) = std(validTrials.(fname{f_i}));
    minVT(f_i) = min(validTrials.(fname{f_i}));
    maxVT(f_i) = max(validTrials.(fname{f_i}));
end 
% trial count
for f_i = 1:length(fname)
    for cond_i = 1:length(wpms.conditions)
        meanTC(f_i,cond_i) = mean(trialCount.(fname{f_i})(cond_i,:));
        SDTC(f_i,cond_i) = std(trialCount.(fname{f_i})(cond_i,:));
        minTC(f_i,cond_i) = min(trialCount.(fname{f_i})(cond_i,:));
        maxTC(f_i,cond_i) = max(trialCount.(fname{f_i})(cond_i,:));
    end
end



% savename = ['E:\FNL_EEG_TOOLBOX\data_for_papers\single_trial\trialInfo.mat'];
% 
% trialCount.time2<30
% 
% time1Inds   = find(ismember(wpms.participants.itpc,wpms.participants.time1));
% time1ITPC   = wpms.participants.itpc(time1Inds);
% notNeededT1 = {wpms.participants.time1{~ismember(wpms.participants.time1,time1ITPC)}};
% find(~ismember(wpms.participants.time1,time1Inds))
% 
% badT2Inds = trialCount.time2<50;
% 
% 
% outgroupInds   = find(ismember(wpms.participants.itpc,wpms.participants.outgroup));
% outgroupITPC   = wpms.participants.itpc(outgroupInds);
% notNeededOG = {wpms.participants.outgroup{~ismember(wpms.participants.outgroup,outgroupITPC)}};
% find(~ismember(wpms.participants.outgroup,notNeededOG))
% 
% badoutInds = trialCount.outgroup<50;
% badT1Inds = trialCount.time1<50;