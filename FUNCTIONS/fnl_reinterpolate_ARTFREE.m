%% FNL_REINTERPOLATE_ARTFREE
%   Reinterpolate bad channels back into data after applying scalp current
%   density montage
%   Requires a bad channel list made previously (this can be empty)
%  USEAGE:
%         fnl_reinterpolate(wpms,name_i)
%  INPUTS:
%         wpms: working parameters stucture (contains names,dirs)
%         name_i: current participant index
%
%  Patrick Cooper & Aaron Wong, 2015
%  Functional Neuroimaging Laboratory, University of Newcastle

function fnl_reinterpolate_ARTFREE(wpms,name_i)


    fprintf('Loading: %s%s \n',wpms.names{name_i},'''s data');
    load([wpms.dirs.CWD wpms.dirs.preproc wpms.names{name_i} '_ARTFREEDATA']);%csd data %ARTFREEDATA for ERPs
    load([wpms.dirs.CWD wpms.dirs.preproc wpms.names{name_i} '_badchannellist']);%badchannel list
    if any(ismember(fieldnames(data.cfg),'flag_to_interpolate_bad_chan'))
        fprintf('Adding to BadChann List: %s\n',[data.cfg.flag_to_interpolate_bad_chan{:}]);
        badchann = [badchann,data.cfg.flag_to_interpolate_bad_chan'];
    end
    %Generating Full Array:
    %load original montage - Labels
    load([wpms.dirs.CWD wpms.dirs.FUNCTIONS 'original_labels.mat']);
    label = labels(1:72,1);
    %get new structure:
    time = data.time;
    trial = cell(1,size(data.trial,2));
    for i = 1:size(data.trial,2)
        trial{1,i} = zeros((length(label)),size(data.trial{1,1},2));
        %copy if labels match
        for index_i = 1:length(data.label)
            for j = 1:length(label)
                if strcmp(data.label{index_i,1},label{j,1})
                    trial{1,i}(j,:) = data.trial{1,i}(index_i,:);
                    break;
                end
            end
        end
    end
    cfg = data.cfg;
    data_2 = struct('hdr',data.hdr,'fsample',data.fsample,'sampleinfo',data.sampleinfo,'trialinfo',data.trialinfo,'trial',{trial},'time',{time},'cfg',cfg,'label',{label});
    clear data;
    data = data_2;
    clear data_2 trial time j i
    fprintf('Preparing electrode neighbourhood. . . ');
    cfg = [];
    fprintf('. ');
    cfg.method = 'triangulation';
    fprintf('. ');
    cfg.layout = 'FNL_biosemi64.lay';
    fprintf('. \n');
    %Check for errors: FP2
    num_badchann = length(badchann);
    for i = 1:num_badchann
        if strcmpi(badchann{i},'fp2')
            badchann{i} = 'Fp2';
        end 
        if strcmpi(badchann{i},'oz')
            badchann{i} = 'Oz';
        end
        if strcmpi(badchann{i},'af8')
            badchann{i} = 'AF8';
        end
        if strcmpi(badchann{i},'fp2 af8')
            badchann{1} = 'Fp2';
            badchann{2} = 'AF8';
        end
        if strcmpi(badchann{i},'fp1')
            badchann{i} = 'FP1';
        end
        if strcmpi(badchann{i},'poz')
            badchann{i} = 'POz';
        end
        if strcmpi(badchann{i},'Cp5')
            badchann{i} = 'CP5';
        end
    end
    
    neighbours = ft_prepare_neighbours(cfg, data);
    fprintf('Selecting bad channels from prepared list. . . \n');
    channel = ft_channelselection(badchann,data.label);
    fprintf('Channel selection complete! \n');
    % repair bad channels
    fprintf('Repairing bad channels. . .');
    cfg.method = 'nearest';
    fprintf('. ');
    cfg.badchannel = badchann; % cell-array of channels from channel
    fprintf('. ');
    cfg.missingchannel = []; % can be the same
    fprintf('. ');
    cfg.neighbours = neighbours;
    fprintf('. ');
    cfg.trials = 'all';
    fprintf('. ');
    temp = data;
    fprintf('. \n');
    interp = ft_channelrepair(cfg, temp);
    %
    repeat = {};count = 1;
    for bad_i = 1:length(badchann)
        fprintf('After Interpolation 1: Check %s:\n',badchann{bad_i});
        index = find(ismember({neighbours.label},badchann{bad_i}));
        current_neighbours=neighbours(index).neighblabel;
        if sum(ismember(current_neighbours,badchann)) == length(current_neighbours)
            fprintf('\tFound Very Bad Channel: %s\n',badchann{bad_i});
            repeat{count} = badchann{bad_i};
            count=count +1;
        end
    end
    if ~isempty(repeat)
        cfg.badchannel = repeat;
        interp = ft_channelrepair(cfg, interp);
    end
    
    %
    interp.fsample = data.fsample;
    
    refdat = interp;
    save([wpms.dirs.CWD wpms.dirs.preproc wpms.names{name_i} '_RepairedData'],'refdat','-v7.3')
    %save(['F:\' wpms.names{name_i} '_RepairedData'],'refdat','-v7.3');
    clear data channel badchann data interp lay neighbours w temp% tidying
end