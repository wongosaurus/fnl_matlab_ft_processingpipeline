%% Export Standard Deviations from TSWT Data for XXX Paper
%  Patrick Cooper, February 2014,
%  Aaron Wong, September 2017.
%  University of Newcastle
%% Set up globals
clear all
close all
%CWD    = '/Users/c3075693/Documents/ITL';
CWD    = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\raw_trl\';
% prefix = 'AGE';
 ext    = '.trl';
% cd(CWD);
% listings = dir('..\*_allrepeat_ISPC.mat');
% names = {length(listings)};
% for file_i =1:length(listings)
%     names{file_i} = listings(file_i).name(1:6);
% end
% clear num_i nums name_i

names ={'AGE002',	'AGE003',	'AGE004',	'AGE005',	'AGE007',	'AGE008',	'AGE012',	'AGE013',	'AGE014',	'AGE015',	'AGE017',	'AGE018',	'AGE019',	'AGE020',	'AGE021',	'AGE022',	'AGE023',	'AGE024',	'AGE026',	'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE046',	'AGE047',	'AGE050',	'AGE051',	'AGE053',	'AGE058',	'AGE059',	'AGE061',	'AGE062',	'AGE063',	'AGE066',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE072',	'AGE073',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE088',	'AGE089',	'AGE090',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE096',	'AGE097',	'AGE098',	'AGE100',	'AGE102',	'AGE103',	'AGE104',	'AGE107',	'AGE108',	'AGE109',	'AGE111',	'AGE114',	'AGE115',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE122',	'AGE123',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE133',	'AGE134',	'AGE135',	'AGE136',	'AGE138',	'AGE141',	'AGE145',	'AGE146',	'AGE147',	'AGE148',	'AGE149',	'AGE150',	'AGE151',	'AGE152',	'AGE153',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE163',	'AGE164',	'AGE165',	'AGE166',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE172',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE189',	'AGE190',	'AGE195',	'AGE197',	'AGE198',	'AGE199',	'AGE201',	'AGE202',	'AGE203',	'AGE205',	'AGE206',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE232',	'AGE233',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE241',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE273',	'AGE275',	'AGE276',	'AGE278',	'AGE279'};

cd(CWD)
%% Load raw data in and save to react,code and acc matrices
%preallocate matrices
code = zeros(length(names),929,1); %929=total number of trials (magic number :( )
acc = zeros(length(names),929,1)*NaN;
react = zeros(length(names),929,1)*NaN;
for name_i = 1:length(names);
    filename = strcat(names(1,name_i),ext);
    fprintf('%s %s\n','Loading',filename{1,1}(1:6));
    delimiter = '\t';
    startRow = 2;
    formatSpec = '%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%[^\n\r]';
    
    fileID = fopen(filename{1,1},'r');
    if fileID == -1
        fprintf(2,'%s: %s\n',names{name_i}, '\t Missing TRL');
        continue;
    end
    
    dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
    fclose(fileID);
    
    rawtxt = [dataArray{:,1:end-1}];
    numericData = NaN(size(dataArray{1},1),size(dataArray,2));
    for col=[1,2,3,4,5,6,10,13,14,15,20,21,22,23,24,25,26,27,28] % convert strings to numbers if applicable
        fprintf('.');
        rawData = dataArray{col};
        for row=1:size(rawData, 1);
            % Create a regular expression to detect and remove non-numeric prefixes and
            % suffixes.
            regexstr = '(?<prefix>.*?)(?<numbers>([-]*(\d+[\,]*)+[\.]{0,1}\d*[eEdD]{0,1}[-+]*\d*[i]{0,1})|([-]*(\d+[\,]*)*[\.]{1,1}\d+[eEdD]{0,1}[-+]*\d*[i]{0,1}))(?<suffix>.*)';
            try
                result = regexp(rawData{row}, regexstr, 'names');
                numbers = result.numbers;
                invalidThousandsSeparator = false;
                if any(numbers==',');
                    thousandsRegExp = '^\d+?(\,\d{3})*\.{0,1}\d*$';
                    if isempty(regexp(thousandsRegExp, ',', 'once'));
                        numbers = NaN;
                        invalidThousandsSeparator = true;
                    end
                end
                % Convert numeric strings to numbers.
                if ~invalidThousandsSeparator;
                    numbers = textscan(strrep(numbers, ',', ''), '%f');
                    numericData(row, col) = numbers{1};
                    rawtxt{row, col} = numbers{1};
                end
            catch me
                %fprintf(2,'%s: %s\n',names{name_i}, me.identifier);
                %continue;
            end
        end
    end
    
    % Split data into numeric and cell columns.
    rawNumericColumns = rawtxt(:, [1,2,3,4,5,6,10,13,14,15,20,21,22,23,24,25,26,27,28]);
    rawCellColumns = rawtxt(:, [7,8,9,11,12,16,17,18,19]);
    % Replace non-numeric cells with NaN
    R = cellfun(@(x) ~isnumeric(x) && ~islogical(x),rawNumericColumns); % Find non-numeric cells
    rawNumericColumns(R) = {NaN}; % Replace non-numeric cells
    % set up data labels given current file layout
    codetmp = cell2mat(rawNumericColumns(:, 11));
    acctmp = cell2mat(rawNumericColumns(:, 13));
    reacttmp = cell2mat(rawNumericColumns(:, 15));
    % Clear temporary variables
    code(name_i,:,1) = codetmp;
    acc(name_i,:,1) = acctmp;
    react(name_i,:,1) = reacttmp;
    clearvars filename delimiter startRow formatSpec fileID dataArray ans;
    clearvars rawtxt numericData col rawData row regexstr result numbers;
    clearvars invalidThousandsSeparator thousandsRegExp me rawNumericColumns;
    clearvars codetmp acctmp reacttmp rawCellColumns R;
    fprintf('\n')
end%name_i loop
%% determine valid trials
% (i.e. not too fast, not too slow, correct)
toofast = 200; %200ms
tooslow = zeros(length(names),1); %subject specific value created here
for name_i = 1:size(react,1)
    tooslow(name_i,1) = (mean(react(name_i,1:length(react))) ...
        + (3*(std(react(name_i,1:length(react))))));
end%name_i loop

validtrials = [];
validcodes  = [];
for name_i = 1:size(acc,1)
    count = 0;
    for trial_i = 1:size(acc,2)
        if react(name_i,trial_i) > toofast && react(name_i,trial_i) < tooslow(name_i,1) ...
                && acc(name_i,trial_i) ~= 0;
            count = count+1;
            validtrials(name_i,count) = react(name_i,trial_i);
            validcodes(name_i,count)  = code(name_i,trial_i);
        end%logical test for valid trials
    end%trial_i loop
end%name_i loop
clear name_i trial_i count
%remove zero values from matrices (as these will influence the means)
for name_i = 1:size(validtrials,1)
    for trials_i = 1:size(validtrials,2)
        if validtrials(name_i,trials_i) == 0
            validtrials_tmp(name_i,trials_i) = NaN;
        else validtrials_tmp(name_i,trials_i) = validtrials(name_i,trials_i);
        end%validtrials logical test
        if validcodes(name_i,trials_i) == 0
            validcodes_tmp(name_i,trials_i) = NaN;
        else validcodes_tmp(name_i,trials_i) = validcodes(name_i,trials_i);
        end%validcodes logical test
    end%trials_i loop
end%name_i loop

validcodes  = validcodes_tmp;
validtrials = validtrials_tmp;
clear validcodes_tmp validtrials_tmp name_i trials_i
%% Calculate means and SDs for each condition based off codes
condition = {'Repeat','SwitchTo','SwitchAway','NonInformative','NonInformativeRepeat','NonInformativeSwitch'};
% Condition                   Code
% Repeat               61,62,71,72,81,82
% SwitchTo             63,64,73,74,83,84
% SwitchAway           65,66,75,76,85,86
% NonInformative       67,68,69,70,77,78,
%                      79,80,87,88,89,90
% NR = 67, 68, 77, 78, 87, 88
% NS = 69, 70, 79, 80, 89, 90

% preallocate arrays
means      = zeros(length(names),length(condition));
sdevs      = zeros(length(names),length(condition));
repeat     = [];
switchto   = [];
switchaway = [];
noninform  = [];
noninformrepeat  = [];
noninformswitch  = [];
for name_i = 1:length(names)
    try
    rptcnt = 0;
    swtcnt = 0;
    swacnt = 0;
    noncnt = 0;
    nircnt = 0;
    niscnt = 0;
    for trials_i = 1:length(validtrials)
        if validcodes(name_i,trials_i) == 61 || validcodes(name_i,trials_i) == 62 || ...
                validcodes(name_i,trials_i) == 71 || validcodes(name_i,trials_i) == 72 || ...
                validcodes(name_i,trials_i) == 81 || validcodes(name_i,trials_i) == 82
            rptcnt = rptcnt+1;
            repeat(name_i,rptcnt) = validtrials(name_i,trials_i);
        end%logical test for repeat
        if validcodes(name_i,trials_i) == 63 || validcodes(name_i,trials_i) == 64 || ...
                validcodes(name_i,trials_i) == 73 || validcodes(name_i,trials_i) == 74 || ...
                validcodes(name_i,trials_i) == 83 || validcodes(name_i,trials_i) == 84
            swtcnt = swtcnt+1;
            switchto(name_i,swtcnt) = validtrials(name_i,trials_i);
        end%logical test for switchto
        if validcodes(name_i,trials_i) == 65 || validcodes(name_i,trials_i) == 66 || ...
                validcodes(name_i,trials_i) == 75 || validcodes(name_i,trials_i) == 76 || ...
                validcodes(name_i,trials_i) == 85 || validcodes(name_i,trials_i) == 86
            swacnt = swacnt+1;
            switchaway(name_i,swacnt) = validtrials(name_i,trials_i);
        end%logical test for switchaway
        if validcodes(name_i,trials_i) == 67 || validcodes(name_i,trials_i) == 68 || ...
                validcodes(name_i,trials_i) == 69 || validcodes(name_i,trials_i) == 70 || ...
                validcodes(name_i,trials_i) == 77 || validcodes(name_i,trials_i) == 78 || ...
                validcodes(name_i,trials_i) == 79 || validcodes(name_i,trials_i) == 80 || ...
                validcodes(name_i,trials_i) == 87 || validcodes(name_i,trials_i) == 88 || ...
                validcodes(name_i,trials_i) == 89 || validcodes(name_i,trials_i) == 90
            noncnt = noncnt+1;
            noninform(name_i,noncnt) = validtrials(name_i,trials_i);
        end%logical test for noninformative
        if validcodes(name_i,trials_i) == 67 || validcodes(name_i,trials_i) == 68 || ...
                validcodes(name_i,trials_i) == 77 || validcodes(name_i,trials_i) == 78 || ...
                validcodes(name_i,trials_i) == 87 || validcodes(name_i,trials_i) == 88
            nircnt = nircnt+1;
            noninformrepeat(name_i,nircnt) = validtrials(name_i,trials_i);
        end%logical test for noninformativerepeat
        if validcodes(name_i,trials_i) == 69 || validcodes(name_i,trials_i) == 70 || ...
                validcodes(name_i,trials_i) == 79 || validcodes(name_i,trials_i) == 80 || ...
                validcodes(name_i,trials_i) == 89 || validcodes(name_i,trials_i) == 90
            niscnt = niscnt+1;
            noninformswitch(name_i,niscnt) = validtrials(name_i,trials_i);
        end%logical test for noninformative
    end%trials_i loop
    %repeat
    means(name_i,1) = nanmean(repeat(name_i,1:size(repeat,2)));
    sdevs(name_i,1) = nanstd(repeat(name_i,1:size(repeat,2)));
    %switchto
    means(name_i,2) = nanmean(switchto(name_i,1:size(switchto,2)));
    sdevs(name_i,2) = nanstd(switchto(name_i,1:size(switchto,2)));
    %switchaway
    means(name_i,3) = nanmean(switchaway(name_i,1:size(switchaway,2)));
    sdevs(name_i,3) = nanstd(switchaway(name_i,1:size(switchaway,2)));
    %noninformative
    means(name_i,4) = nanmean(noninform(name_i,1:size(noninform,2)));
    sdevs(name_i,4) = nanstd(noninform(name_i,1:size(noninform,2)));
    %noninformative
    means(name_i,5) = nanmean(noninformrepeat(name_i,1:size(noninformrepeat,2)));
    sdevs(name_i,5) = nanstd(noninformrepeat(name_i,1:size(noninformrepeat,2)));
    %noninformative
    means(name_i,6) = nanmean(noninformswitch(name_i,1:size(noninformswitch,2)));
    sdevs(name_i,6) = nanstd(noninformswitch(name_i,1:size(noninformswitch,2)));
    catch
    end
end%name_i loop
clear *cnt name_i trials_i too* valid*

%% Accuracy:
AllRepeat_CODES             = [31 32 41 42 51 52];
MixedRepeat_CODES           = [61 62 71 72 81 82];
Switchto_CODES              = [63 64 73 74 83 84];
Switchaway_CODES            = [65 66 75 76 85 86];
Noninformative_CODES        = [67 68 69 70 77 78 79 80 87 88 89 90];
Noninformativerepeat_CODES  = [67 68 77 78 87 88];      
Noninformativeswitch_CODES  = [69 70 79 80 89 90];

RA_Codes = nan(size(code));
RA_Codes(ismember(code,AllRepeat_CODES)) = acc(ismember(code,AllRepeat_CODES));
RA_Codes_Correct = sum(RA_Codes==1,2);
RA_Codes_Error   = sum(RA_Codes==0,2);
RA_Codes_Total = RA_Codes_Correct+RA_Codes_Error;

RM_Codes = nan(size(code));
RM_Codes(ismember(code,MixedRepeat_CODES)) = acc(ismember(code,MixedRepeat_CODES));
RM_Codes_Correct = sum(RM_Codes==1,2);
RM_Codes_Error   = sum(RM_Codes==0,2);
RM_Codes_Total = RM_Codes_Correct+RM_Codes_Error;

ST_Codes = nan(size(code));
ST_Codes(ismember(code,Switchto_CODES)) = acc(ismember(code,Switchto_CODES));
ST_Codes_Correct = sum(ST_Codes==1,2);
ST_Codes_Error   = sum(ST_Codes==0,2);
ST_Codes_Total = ST_Codes_Correct+ST_Codes_Error;

SA_Codes = nan(size(code));
SA_Codes(ismember(code,Switchaway_CODES)) = acc(ismember(code,Switchaway_CODES));
SA_Codes_Correct = sum(SA_Codes==1,2);
SA_Codes_Error   = sum(SA_Codes==0,2);
SA_Codes_Total = SA_Codes_Correct+SA_Codes_Error;

NI_Codes = nan(size(code));
NI_Codes(ismember(code,Noninformative_CODES)) = acc(ismember(code,Noninformative_CODES));
NI_Codes_Correct = sum(NI_Codes==1,2);
NI_Codes_Error   = sum(NI_Codes==0,2);
NI_Codes_Total = NI_Codes_Correct+NI_Codes_Error;

NR_Codes = nan(size(code));
NR_Codes(ismember(code,Noninformativerepeat_CODES)) = acc(ismember(code,Noninformativerepeat_CODES));
NR_Codes_Correct = sum(NR_Codes==1,2);
NR_Codes_Error   = sum(NR_Codes==0,2);
NR_Codes_Total = NR_Codes_Correct+NR_Codes_Error;

NS_Codes = nan(size(code));
NS_Codes(ismember(code,Noninformativeswitch_CODES)) = acc(ismember(code,Noninformativeswitch_CODES));
NS_Codes_Correct = sum(NS_Codes==1,2);
NS_Codes_Error   = sum(NS_Codes==0,2);
NS_Codes_Total = NS_Codes_Correct+NS_Codes_Error;

fid = fopen('AccuracyData_PercentError_Phase1_TSWT_180920.txt','w');
fprintf(fid,'SUBJECT\tSUBNUM\tRA_err\tRM_err\tST_err\tSA_err\tNI_err\tNR_err\tNS_err\n');
for i = 1:length(RA_Codes_Total)
    fprintf(fid,'%s\t',names{i});
    fprintf(fid,'%s\t',names{i}(4:6));
    fprintf(fid,'%3.5f\t',RA_Codes_Error(i)./RA_Codes_Total(i)*100);
    fprintf(fid,'%3.5f\t',RM_Codes_Error(i)./RM_Codes_Total(i)*100);
    fprintf(fid,'%3.5f\t',ST_Codes_Error(i)./ST_Codes_Total(i)*100);
    fprintf(fid,'%3.5f\t',SA_Codes_Error(i)./SA_Codes_Total(i)*100);
    fprintf(fid,'%3.5f\t',NI_Codes_Error(i)./NI_Codes_Total(i)*100);
    fprintf(fid,'%3.5f\t',NR_Codes_Error(i)./NR_Codes_Total(i)*100);
    fprintf(fid,'%3.5f\t',NS_Codes_Error(i)./NS_Codes_Total(i)*100);
    fprintf(fid,'\n');
end
fclose(fid);


%% save off 
meansFILENAME = 'meansITC.txt';
sdevsFILENAME = 'stdevITC.txt';
%as text
save(meansFILENAME,'means','-ascii','-tabs');
save(sdevsFILENAME,'sdevs','-ascii','-tabs');
%as .mat
save(meansFILENAME(1:8),'means');
save(sdevsFILENAME(1:8),'sdevs');

