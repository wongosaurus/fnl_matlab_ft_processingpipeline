%% Extract tSNR for EEG resting data
%% Set up workspace
clc;clear all;close all;
CWD     = 'G:\FNL_EEG_TOOLBOX\';
DATAIN  = [CWD 'PREPROC_OUTPUT\RestingEEG\RestingEEG_FINAL\'];
DATAOUT = [CWD 'PREPROC_OUTPUT\RestingEEG\RestingEEG_FINAL_setfiles\'];
disp(['Adding path to EEGLAB   --->  ' CWD 'PACKAGES\eeglab12_0_1_0b\'])
addpath(genpath([CWD 'PACKAGES\eeglab12_0_1_0b\']));
disp(['Adding path to custom functions    --->  ' CWD 'FUNCTIONS\RestingEEG\']);
addpath(genpath([CWD 'FUNCTIONS\RestingEEG\']));
cd(DATAIN);
listing = dir('Resting_Period1*');
names{1,length(listing)} = [];%preallocate
%find index where subject name can be pulled from
for string_i = 1:length(listing(1).name)
    if strcmp(listing(1).name(string_i),'A');
        ind = string_i;
    end
end
for list_i = 1:length(listing)
    names{1,list_i} = listing(list_i).name(ind:(ind+5));
end
%% first open .edf using EEGLAB plugin
for name_i = 1:length(names)
    for resting_period = 1:3
        if name_i ==1
            listing = dir(['Resting_period_' num2str(resting_period) '*']);
        end%only need to run this once per resting period
        filename = ['RestingPeriod_' num2str(resting_period) '_' names{name_i} '.edf'];
        %ensure that file exists
        file_found = 0;
        file_found = checkFileExists(listing,filename,file_found);
        if file_found ==1;
            loadname = [DATAING filename];
            EEG = pop_biosig(loadname);
            EEG = eeg_checkset( EEG );
        else
            fprintf('%s %s %s\n','File:',filename','not found, moving on');
        end
    end%resting_period loop
end%name_i loop
