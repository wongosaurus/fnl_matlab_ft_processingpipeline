function [estimated_time] = time_remaining(currentpos,total,currenttoc)
%time_remaining Estimates amount of time remaining in a loop of processes
%   Considers the current tic toc cycle representative of a process
%   and uses this to estimate remaining time left.
%   Informative for long processes but do not use for accurate time
%   keeping.
%
%   USAGE: estimate_time = time_remaining(currentpos,total,currenttoc)
%
%         INPUTS: currentpos = current positiion in a loop (integer)
%                 total      = length of loop (integer)
%                 currenttoc = toc from tic/toc cycle
%
%         OUTPUTS: estimated_time = estimated time remaining
%
%   Patrick Cooper, 2016.
%   University of Newcastle, Australia
narginchk(3, Inf);%check that correct number of variables entered
estimated_time = (total-currentpos)*currenttoc;


end

