% Only for OneWayAnova Repeated Measures:

function [pvalue, fstat,DF_Int, DF_Error] = runRepeatedMeasuresAnova(DATA)

    DATA_SIZE = size(DATA);
    
    %Generate Table:
       
    data_str = [];
    data_name_str = [];
    for i=1:DATA_SIZE(2)
        data_str = [data_str, 'DATA(:,',num2str(i),'),']; %#ok<AGROW>
        data_name_str= [data_name_str, '''data',num2str(i),''',']; %#ok<AGROW>
    end
    CMD_STR = ['t = table(',data_str,'''VariableNames'',{',data_name_str,'});'];
    eval(CMD_STR);
    %t = table(data(:,1),data(:,2),data(:,3),data(:,4),data(:,5),...
    %    'VariableNames',{'data1','data2','data3','data4','data5'});
    Meas = dataset([1:DATA_SIZE(2)]','VarNames',{'Measurements'});
    modelspec = ['data1-data',num2str(DATA_SIZE(2)),'~1'];
    rm = fitrm(t,modelspec,'WithinDesign',Meas);
    
    ranovatbl = ranova(rm);
    fstat=ranovatbl{1,4};
    pvalue=ranovatbl{1,5};
    DF_Int = ranovatbl{1,2};
    DF_Error = ranovatbl{2,2};
    
end
