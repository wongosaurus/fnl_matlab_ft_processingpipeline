clearvars; close all; clc;
CONDITIONS  = {'switchto','switchaway','noninfrepeat', 'noninfswitch', 'mixrepeat','allrepeat'};
% Find the names of participants:
listings = dir('F:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\*mixrepeat_ITPC.mat');
NAMES = {length(listings)};
for file_i =1:length(listings)
    NAMES{file_i} = listings(file_i).name(1:6);
end

%computes trial count

SAVEDIR = 'F:\FNL_EEG_TOOLBOX\ReferenceOutput\SurfaceLapacian\';

trialcount=zeros(length(NAMES),1);
for name_i=1:length(NAMES) 
    disp(NAMES{name_i})
    for cond_i=length(CONDITIONS)
        fprintf('.')
    filename=[SAVEDIR NAMES{name_i} CONDITIONS{cond_i} '.mat'];
    load(filename); 
    trialcount(name_i,cond_i)=length(refdat.trial); %length of trial numbers
    end
end

%computes bad channel count

SAVEDIR = 'F:\FNL_EEG_TOOLBOX\PREPROC_OUTPUT\';
badchannelcount=zeros(length(NAMES),1);
for name_i=1:length(NAMES) 
    disp(NAMES{name_i})   
    filename=[SAVEDIR NAMES{name_i} '_badchannellist.mat'];
    load(filename); 
   badchannelcount(name_i,1)=length(badchann); %length of bad channels
end

%computes valid trials

SAVEDIR = 'F:\FNL_EEG_TOOLBOX\PREPROC_OUTPUT\';
validtrials=zeros(length(NAMES),1);
for name_i=1:length(NAMES)
    disp(NAMES{name_i})
    filename=[SAVEDIR NAMES{name_i} '_REPAIRED_AND_REFERENCED.mat'];
    if exist(filename,'file')
        load(filename);
        validtrials(name_i,1)=size(refdat.cfg.previous.trl,1);
    else
            filename=[SAVEDIR NAMES{name_i} '_ARTFREEDATA.mat'];
            load(filename);
        validtrials(name_i,1)=size(data.cfg.trl,1); %used size instead of length because its got 2 or more dimensions
    end
end
        

disp(mean(trialcount,1))
disp(std(trialcount,1))

disp(mean(badchannelcount,1))
disp(std(badchannelcount,1))

total=(48*3)+(720);

proportionvalid=(validtrials./total)*100;

disp(mean(proportionvalid,1))
disp(std(proportionvalid,1))


