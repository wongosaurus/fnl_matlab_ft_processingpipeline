%% FNL_IMPORTEEG_and_downsample
%
%  Imports a participant's EEG datafile into pipeline
%
%
%  USEAGE:
%           [dat] = fnl_importeeg(wpms,filext,name_i,fsample)
%  INPUTS:
%         wpms   = working parameters structure
%                  (must include names and CWD fields)
%         filext = string denoting eeg file format (e.g. 'bdf')
%         name_i = current participant in a processing loop
%         fsample = required new sampling frequency e.g. 512
% OUTPUT:
%         dat    = data structure
%
% Patrick Cooper and Aaron Wong, May 2014
% Functional Neuroimaging Laboratory, University of Newcastle
function [dat_final] = fnl_importeeg_and_downsample(wpms,filext,name_i,fsample)
% check function inputs
    narginchk(4,4)
    fprintf('----- Importing EEG for : %s ------\n', ...
            wpms.names{name_i});
    %Check number of files in this dataset:
    listings = dir([wpms.dirs.CWD wpms.dirs.RAW]);
    filenames = {listings(3:end).name};
    count = 0;
    for name_j = 1:length(filenames)
        if strcmpi(wpms.names{name_i},filenames{name_j}(1:6));
            count = count+1;
            fprintf('Multiple Files Found for %s: %s\n',wpms.names{name_i},filenames{name_j});
        end
    end

    if count == 1
        
        header = ft_read_header([wpms.dirs.CWD wpms.dirs.RAW wpms.names{name_i} '_TSWT.' filext]);
        if header.nChans == 265
            channels = [1:64,257:265];
        else
            channels = [1:73];
        end
        
        dat_final.hdr = header;
        dat_final.label = cell(header.nChans,1);
        dat_final.trial{1,1} = zeros(header.nChans,ceil(header.nSamples/4));
        dat_final.time{1,1} = zeros(1,ceil(header.nSamples/4));
        dat_final.fsample = fsample;
        dat_final.sampleinfo = ones(1,2);
        
        for i = channels
            cfg= [];
            cfg.datafile = [wpms.dirs.CWD wpms.dirs.RAW wpms.names{name_i} '_TSWT.' filext];
            cfg.channel = i;
            cfg.continuous = 'yes';
            dat = ft_preprocessing(cfg);
            dat_final.trial{1,1}(i,:) = ft_preproc_resample(cell2mat(dat.trial(1,1)), dat.fsample, fsample, 'resample');%changed 512 to fsample -PC
            dat_final.time{1,1} = ft_preproc_resample(cell2mat(dat.time(1,1)), dat.fsample, fsample, 'resample');%changed 512 to fsample -PC
            dat_final.fsample = fsample;
            dat_final.label(i) = dat.label;
            dat_final.sampleinfo(2) = dat.sampleinfo(2)/(dat.hdr.Fs/fsample);
        end
        
        %Remove Blank Channels if larger 73;
        if header.nChans == 265
            dat_final.label = dat_final.label([1:64,257:265],1);
            dat_final.trial = {dat_final.trial{1,1}([1:64,257:265],:)};
        end
    else
        segment_names = {'_A','_B','_C'};
        for count_i = 1:count
            header(count_i) = ft_read_header([wpms.dirs.CWD wpms.dirs.RAW wpms.names{name_i} '_TSWT',segment_names{count_i},'.' filext]);    
        end
        %Generate New Structure:
        dat_final.hdr = header;
        dat_final.label = cell(header(1).nChans,1);
        switch count
            case 2
                nsamp = header(1).nSamples+header(2).nSamples;
            case 3
                nsamp = header(1).nSamples+header(2).nSamples+header(3).nSamples; 
            otherwise
                fprintf('Failed to calculate number of sameples\n');
                dat_final = -1;
                return 
        end
        dat_final.trial{1,1} = zeros(header(1).nChans,ceil(nsamp/4));
        dat_final.time{1,1} = zeros(1,ceil(nsamp/4));
        dat_final.fsample = fsample;
        dat_final.sampleinfo = ones(1,2);
        
        lastend = 1;
        lasttime = 0;
        %Read each segment:
        for count_i = 1:count 
            %Read Each Channel:
            for i = 1:header(count_i).nChans
                cfg= [];
                cfg.datafile = [wpms.dirs.CWD wpms.dirs.RAW wpms.names{name_i} '_TSWT',segment_names{count_i},'.' filext];
                cfg.channel = i;
                cfg.continuous = 'yes';
                dat = ft_preprocessing(cfg);
                temp_a = ft_preproc_resample(cell2mat(dat.trial(1,1)), dat.fsample, fsample, 'resample');%changed 512 to fsample -PC
                dat_final.trial{1,1}(i,lastend:lastend+ceil(header(count_i).nSamples/4)-1) = temp_a;
                
                dat_final.fsample = fsample;
                dat_final.label(i) = dat.label;
                
            end
            dat_final.time{1,1}(lastend:lastend+ceil(header(count_i).nSamples/4)-1) = ft_preproc_resample(cell2mat(dat.time(1,1)), dat.fsample, fsample, 'resample')+lasttime;%changed 512 to fsample -PC
            lastend = lastend+ceil(header(count_i).nSamples/4);
            lasttime = lasttime+dat_final.time{1,1}(lastend-fsample-1)+1; %hack, -fsample samples of data from the end and add 1second, resample introduces noise at the begining and end of the file
            
        end
        dat_final.sampleinfo = [1, length(dat_final.time{1,1})];
    end
end