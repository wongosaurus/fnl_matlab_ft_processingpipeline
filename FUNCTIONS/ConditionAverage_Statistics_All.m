%% For NeuroImage Reviewer comments
% [...] consider starting with condition-average time-frequency plots,
% drawing boxes around general task-related power modulations,
% run ANOVAs and show bar plots of the differences and interactions
%% 1. Determine study specific working parameters.
clc; clear all; close all;
disp('1. Determine study specific working parameters.');
% set up data directories
workingvolume = 'F:\'; %this would vary per operating system.
wpms          = [];       %working parameters
wpms.dirs     = struct('CWD',[workingvolume 'FNL_EEG_TOOLBOX' filesep]);
wpms.dirs     = struct('CWD',[workingvolume 'FNL_EEG_TOOLBOX' filesep], ...
    'PACKAGES',[wpms.dirs.CWD 'PACKAGES' filesep], ...
    'FUNCTIONS',[wpms.dirs.CWD 'FUNCTIONS' filesep], ...
    'WAVELET_OUTPUT_DIR',[wpms.dirs.CWD 'PowerOutput_Processed' filesep 'SurfaceLapacian_FULL' filesep]);
    
% set up study conditions

wpms.conditions = {'switchto','switchaway','noninfrepeat','noninfswitch','mixrepeat'};
wpms.conditionshort = {'ST','SA','NR','NS','RM'};


% set up participant list

listings = dir([wpms.dirs.WAVELET_OUTPUT_DIR,filesep,'*mixrepeat_ITPC.mat']);
NAMES = {length(listings)};
for file_i =1:length(listings)
    NAMES{file_i} = listings(file_i).name(1:6);
end
% Only Include data that is in the analysis. (GOOD_INDS from ITV5)
load('good_inds_ITCv5.mat');
wpms.names = {NAMES{good_inds}};


clear workingvolume listing temp_names list_i participant_index space_index%tidy as we travel
% add relevant paths
fprintf('\t%s\t%s %s\n','Adding',wpms.dirs.FUNCTIONS,'to MATLAB path');
addpath(genpath(wpms.dirs.FUNCTIONS))%functions
fprintf('\t%s\t%s %s\n','Adding',[wpms.dirs.PACKAGES 'eeglab12_0_1_0b'],'to MATLAB path');
addpath(genpath([wpms.dirs.PACKAGES 'eeglab12_0_1_0b']));%eeglab
% create condition average time-frequency plots
clearvars -except wpms
disp('Setting up working time-frequency-specific parameters')
%EEG       = pop_loadset([wpms.dirs.PREPROC wpms.conditions{1} filesep wpms.names{1} '_N-CUE-R-C-f.set']);
wpms.data.labels    =  {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
                        'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
                        'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','Afz','Fz','F2','F4','F6','F8','FT8','FC6',...
                        'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
                        'P8','P10','PO8','PO4','O2'}';
wpms.data.times     = (-1:0.001953125000000:3.5)*1000;
wpms.data.frequency = logspace(log10(2),log10(30),80);%frequency bins
%clear EEG;
disp('Beginning condition averaging')
wpms.data.cluster_name = {'Frontocentral'};
wpms.data.electrode_clusters = {{'FC1','FCz','FC2','Cz','C1','C2'}};


cluster_i=1;

basetime_start = -800;
basetime_end = -600;

temp_times = wpms.data.times-basetime_start;
[~,basetimestart_index] = min(abs(temp_times));
temp_times = wpms.data.times-basetime_end;
[~,basetimeend_index] = min(abs(temp_times));

%%
% 
% disp('Extracting significant freq*time indices for analyses')
% 
% condavg_power = zeros(length(wpms.names),length(wpms.data.electrode_clusters{cluster_i}),length(wpms.data.frequency),length(wpms.data.times),'single');
% condavg_itpc = zeros(length(wpms.names),length(wpms.data.electrode_clusters{cluster_i}),length(wpms.data.frequency),length(wpms.data.times),'single');
% 
% 
% for name_i = 1:length(wpms.names)
%     fprintf('\n%s %s\t','Working on:',wpms.names{name_i});
%     tic;
%     count=0;
%     for cond_i = 1:length(wpms.conditions)%[3 5 8 11];%[2 4 7 10]%[1 6 9]%1:length(wpms.conditions)
%         fprintf('.');
%         count=count+1;
%         filename = [wpms.dirs.WAVELET_OUTPUT_DIR wpms.names{name_i} '_' wpms.conditions{cond_i} '_ITPC.mat'];
%         load(filename,'eegpower_all','itpc_all');
%         electrodes = find(ismember(wpms.data.labels,wpms.data.electrode_clusters{cluster_i}));
%         for  electrode = 1:length(electrodes)
%             %fprintf('\n%s %s\t','Working on:',wpms.data.labels{electrodes(electrode)});
%             pos = find(strcmpi(wpms.data.labels{electrodes(electrode)},wpms.data.electrode_clusters{cluster_i}));
%             condavg_power(name_i,pos,:,:) = squeeze(condavg_power(name_i,pos,:,:))+squeeze(eegpower_all(electrodes(electrode),:,:));
%             condavg_itpc(name_i,pos,:,:) = squeeze(condavg_itpc(name_i,pos,:,:))+squeeze(itpc_all(electrodes(electrode),:,:));
%         end%electrode loop
%         clear *_all
%     end%cond_i loop
%     condavg_power(name_i,:,:,:) = condavg_power(name_i,:,:,:)./count;
%     condavg_itpc(name_i,:,:,:) = condavg_itpc(name_i,:,:,:)./count;
%     t=toc;fprintf('\t%3.2f %s',t,'s');
%     fprintf('\t%s %3.2f %s','estimated:',((length(wpms.names)-name_i)*t),'s time remaining');
% end%name_i loop
% 
% clear filename name_i electrode cond_i response_i count t%tidy as we go
% fprintf('\n');
% save('Cond_avg_Extracted.mat','condavg_power','condavg_itpc');
%% Baseline:
load('Cond_avg_Extracted.mat','condavg_power','condavg_itpc');


disp('Applying Baseline: Note this may take a few minutes')
for name_i = 1:length(wpms.names)
    fprintf('\nWorking on %s \t',wpms.names{name_i});
    tic;
    for chan_i = 1:size(condavg_power,2)
        fprintf('.')
        condavg_power(name_i,chan_i,:,:)=(bsxfun(@minus,10*log10(squeeze(condavg_power(name_i,chan_i,:,:))),10*log10(mean(squeeze(condavg_power(name_i,chan_i,:,basetimestart_index:basetimeend_index)),2))));
        condavg_itpc(name_i,chan_i,:,:)=abs(bsxfun(@minus,squeeze(condavg_itpc(name_i,chan_i,:,:)),mean(squeeze(condavg_itpc(name_i,chan_i,:,basetimestart_index:basetimeend_index)),2)));
    end
    t=toc;
    fprintf('\t %3.3f',t);
end
fprintf('\nBaseline complete');

%% perform t-test
electrode = find(ismember(wpms.data.labels,wpms.data.electrode_clusters{cluster_i}));
disp('Performing t-tests')
power_p=zeros(length(wpms.data.frequency),length(wpms.data.times));
itpc_p=zeros(length(wpms.data.frequency),length(wpms.data.times));
for freq_i=1:size(condavg_power,3)
    [~,power_p(freq_i,:)] = ttest(squeeze(mean(condavg_power(:,:,freq_i,:),2)));
    [~,itpc_p(freq_i,:)] = ttest(squeeze(mean((condavg_itpc(:,:,freq_i,:)),2)));
end
% apply multiple comparisons FDR correction
%wpms.data.plot_times = find(wpms.data.times==-200):find(wpms.data.times==1200);%times to show for t-f
temp_starttime = wpms.data.times -(-200);
[~,start_ind] = min(abs(temp_starttime));
temp_endtime = wpms.data.times -(1600);
[~,end_ind] = min(abs(temp_endtime));
wpms.data.plot_times = start_ind:end_ind;
addpath(genpath([wpms.dirs.PACKAGES 'mass_uni_toolbox' filesep]));
wpms.stats.fdr_alpha = 0.001;
disp('Applying FDR-correction: Note this may take a few minutes')
tic;
% [~,crit_p_bh,~]=fdr_bh(p(:,wpms.data.plot_times),wpms.stats.fdr_alpha,'pdep');
%FDR HERE:
% [~,power_crit_p_bky]=fdr_bky(power_p(:,wpms.data.plot_times),wpms.stats.fdr_alpha);
% [~,itpc_crit_p_bky]=fdr_bky(itpc_p(:,wpms.data.plot_times),wpms.stats.fdr_alpha);
% toc
% 
power_p_bh = zeros(length(wpms.data.frequency),length(wpms.data.plot_times));
power_p_bky = power_p_bh;
itpc_p_bh = zeros(length(wpms.data.frequency),length(wpms.data.plot_times));
itpc_p_bky = itpc_p_bh;

% % p_bh(p(:,wpms.data.plot_times)<crit_p_bh)=1;
% power_p_bky(power_p(:,wpms.data.plot_times)<power_crit_p_bky)=1;
% itpc_p_bky(itpc_p(:,wpms.data.plot_times)<itpc_crit_p_bky)=1;

%ttest with bonf connection:


% subplot(1,3,1);imagesc(p_bh);axis square;subplot(1,3,2);imagesc(p_bky);axis square;
% subplot(1,3,3);imagesc(p_bky-p_bh);axis square;

disp('FDR complete');
% create figure as quality check and for manuscript
figure();set(gcf,'Color',[1 1 1],'Position',[0 0 1920 1080]);
plot_frequencies = find(floor(wpms.data.frequency)==2, 1 ):find(floor(wpms.data.frequency)==10, 1,'last' );
condavg_power_grp = squeeze(mean(mean(condavg_power(:,:,:,wpms.data.plot_times),1),2));
condavg_itpc_grp = squeeze(mean(mean(condavg_itpc(:,:,:,wpms.data.plot_times),1),2));
bonf_value = 0.05/numel(condavg_power_grp);
power_p_bky(power_p(:,wpms.data.plot_times) <bonf_value)=1;
itpc_p_bky(itpc_p(:,wpms.data.plot_times)   <bonf_value)=1;
%condavg_power_grp(adj_p>wpms.stats.fdr_alpha) = 0;
% condavg_power_grp(power_p(:,wpms.data.plot_times)>power_crit_p_bky) = 0;
% condavg_itpc_grp(itpc_p(:,wpms.data.plot_times)>itpc_crit_p_bky) = 0;
condavg_power_grp(power_p(:,wpms.data.plot_times)>bonf_value) = 0;
condavg_itpc_grp(itpc_p(:,wpms.data.plot_times)>bonf_value) = 0;
condavg_itpc_grp(condavg_itpc_grp<0.1) = 0;
% POWER plotting
contourf(wpms.data.times(wpms.data.plot_times),wpms.data.frequency(plot_frequencies),condavg_power_grp(plot_frequencies,:),100,'linecolor','none');caxis([-5 5]); colormap jet;
hold on;
% sig_lines = zeros(size(adj_p));sig_lines(adj_p>thresh)=1;
power_sig_lines = zeros(size(condavg_power_grp));
% sig_lines(adj_p<wpms.stats.fdr_alpha)=1;
power_sig_lines(power_p(:,wpms.data.plot_times)<bonf_value)=1;
contour(wpms.data.times(wpms.data.plot_times),wpms.data.frequency(plot_frequencies),power_sig_lines(plot_frequencies,:),1,'k','LineWidth',3);caxis([-5 5]); colormap jet;%axis square;
ylabel('Frequency (Hz)','FontSize',16);xlabel('Time (ms)','FontSize',16);set(gca,'FontSize',16);
title(wpms.data.cluster_name{cluster_i});
saveas(gcf,[wpms.dirs.WAVELET_OUTPUT_DIR 'Condition_Average_ALLPOWER_' wpms.data.cluster_name{cluster_i} num2str(wpms.stats.fdr_alpha) '.fig'],'fig');

% ITPC plotting
figure();set(gcf,'Color',[1 1 1],'Position',[0 0 1920 1080]);
contourf(wpms.data.times(wpms.data.plot_times),wpms.data.frequency(plot_frequencies),abs(condavg_itpc_grp(plot_frequencies,:)),100,'linecolor','none');caxis([0 0.2]); colormap jet;
hold on;
% sig_lines = zeros(size(adj_p));sig_lines(adj_p>thresh)=1;
itpc_sig_lines = zeros(size(condavg_itpc_grp));
% sig_lines(adj_p<wpms.stats.fdr_alpha)=1;

%------------------- IMPORTANT: SIGNIFICANT and MUST ALSO have at least 10 percent COHERENCE -----------------------
itpc_sig_lines(itpc_p(:,wpms.data.plot_times)<bonf_value & condavg_itpc_grp>0.1)=1;

contour(wpms.data.times(wpms.data.plot_times),wpms.data.frequency(plot_frequencies),itpc_sig_lines(plot_frequencies,:),1,'k','LineWidth',3);caxis([0 0.2]); colormap jet;%axis square;
ylabel('Frequency (Hz)','FontSize',16);xlabel('Time (ms)','FontSize',16);set(gca,'FontSize',16);
title(wpms.data.cluster_name{cluster_i});
saveas(gcf,[wpms.dirs.WAVELET_OUTPUT_DIR 'Condition_Average_ALLITPC_' wpms.data.cluster_name{cluster_i} num2str(wpms.stats.fdr_alpha) '.fig'],'fig');

%%
close;
% set up significant indices per frequency band for statistics
% find frequency indices
wpms.stats.freq_bands = {find(floor(wpms.data.frequency)==2, 1 ):find(floor(wpms.data.frequency)==4, 1,'last' ),...
    find(floor(wpms.data.frequency)==4, 1 ):find(floor(wpms.data.frequency)==7, 1,'last' )};
wpms.stats.freq_labels = {'delta','theta'};
%wpms.stats.sigmask = [];%storage for significant pixel indices
for freq_i = 1:length(wpms.stats.freq_labels)
    reduced_data = squeeze(power_sig_lines(wpms.stats.freq_bands{freq_i},:));
    wpms.stats.power_sigmask.(wpms.stats.freq_labels{freq_i}) = find(reduced_data==1);
    reduced_data = squeeze(itpc_sig_lines(wpms.stats.freq_bands{freq_i},:));
    wpms.stats.itpc_sigmask.(wpms.stats.freq_labels{freq_i}) = find(reduced_data==1);
    fprintf('\n%s %i %s %s','Found',length(wpms.stats.power_sigmask.(wpms.stats.freq_labels{freq_i})),'significant voxels for',wpms.stats.freq_labels{freq_i});
    fprintf('\n%s %i %s %s','Found',length(wpms.stats.itpc_sigmask.(wpms.stats.freq_labels{freq_i})),'significant voxels for',wpms.stats.freq_labels{freq_i});
end%freq_i loop
clearvars -except wpms%tidy as we go
fprintf('\n');

%% extract individual power values for each person*freq*cluster
disp('Extracting individual power values for each person for statistics');
wpms.data.cluster_name_short = {'FC'};
outfile_power = [wpms.dirs.WAVELET_OUTPUT_DIR 'AllTARGETPowerValues_context_specific_plottimes.txt'];
outfile_itpc = [wpms.dirs.WAVELET_OUTPUT_DIR 'AllTARGETItpcValues_context_specific_plottimes.txt'];
fid_power = fopen(outfile_power,'w');
fid_itpc = fopen(outfile_itpc,'w');
%first print header
fprintf(fid_itpc,'%s\t','Participant');
fprintf(fid_power,'%s\t','Participant');

for cond_i=1:length(wpms.conditions)
    for cluster_i = 1:length(wpms.data.cluster_name_short)
        for freq_i = 1:length(wpms.stats.freq_labels)
            fprintf(fid_power,'%s\t',[wpms.conditionshort{cond_i} '.' wpms.data.cluster_name_short{cluster_i} '.' wpms.stats.freq_labels{freq_i}]);
            fprintf(fid_itpc,'%s\t',[wpms.conditionshort{cond_i} '.' wpms.data.cluster_name_short{cluster_i} '.' wpms.stats.freq_labels{freq_i}]);
        end%freq_i loop
    end%cluster_i loop
end%cond_i loop

clear *_i%tidy as we go

      
for name_i = 1:length(wpms.names)
    fprintf('\n%s\t',wpms.names{name_i});
    fprintf(fid_power,'\n%s\t',wpms.names{name_i});%to text file
    fprintf(fid_itpc,'\n%s\t',wpms.names{name_i});%to text file
    tic;
    for cond_i=1:length(wpms.conditions)
        fprintf('%s ','.');
        filename = [wpms.dirs.WAVELET_OUTPUT_DIR wpms.names{name_i} '_' wpms.conditions{cond_i} '_ITPC.mat'];
        load(filename,'eegpower_all','itpc_all');
        
        for cluster_i = 1:length(wpms.data.cluster_name_short)
            power_data = zeros(length(wpms.data.electrode_clusters{cluster_i}),length(wpms.data.frequency),length(wpms.data.times));%temp storage of power data
            itpc_data = zeros(length(wpms.data.electrode_clusters{cluster_i}),length(wpms.data.frequency),length(wpms.data.times));%temp storage of power data
            electrode_count = 0;%cluster member counter
            electrodes = find(ismember(wpms.data.labels,wpms.data.electrode_clusters{cluster_i}));
            for electrode = 1:length(electrodes)
                pos = find(strcmpi(wpms.data.labels{electrodes(electrode)},wpms.data.electrode_clusters{cluster_i}));
                electrode_count = electrode_count+1;
                power_data(pos,:,:) = bsxfun(@minus,10*log10(squeeze(eegpower_all(electrodes(electrode),:,:))),10*log10(mean(squeeze(eegpower_all(electrodes(electrode),:,basetimestart_index:basetimeend_index)),2)));
                itpc_data(pos,:,:) = bsxfun(@minus,abs(squeeze(itpc_all(electrodes(electrode),:,:))),mean(squeeze(itpc_all(electrodes(electrode),:,basetimestart_index:basetimeend_index)),2));
            end%electrode loop
            power_data = squeeze(mean(power_data,1));%average over cluster
            itpc_data = squeeze(mean(itpc_data,1));%average over cluster
            for freq_i = 1:length(wpms.stats.freq_labels)
                power_reduced_data = squeeze(power_data(wpms.stats.freq_bands{freq_i},wpms.data.plot_times));
                power_datavalue = squeeze(mean(power_reduced_data(wpms.stats.power_sigmask.(wpms.stats.freq_labels{freq_i}))));
                fprintf(fid_power,'%2.4f\t',power_datavalue);%to text file
                itpc_reduced_data = squeeze(itpc_data(wpms.stats.freq_bands{freq_i},wpms.data.plot_times));
                itpc_datavalue = squeeze(mean(itpc_reduced_data(wpms.stats.itpc_sigmask.(wpms.stats.freq_labels{freq_i}))));
                fprintf(fid_itpc,'%2.4f\t',itpc_datavalue);%to text file
            end%freq_i loop
        end%cluster_i loop
    end%cond_i loop

    t=toc; fprintf('%3.2f %s',t,'s to complete');
end%name_i loop
fclose(fid_power);
fclose(fid_itpc);

%% finally, create headplots for each condition based on significance index
clims={[-2 2];[-2 2];[-2 2];[-2 2]};
EEG       = pop_loadset([wpms.dirs.PREPROC wpms.conditions{1} filesep wpms.names{1} '_N-CUE-R-C-f.set']);
tic;
for context_i = 1:length(wpms.contexts.names)
    fprintf('\n%s',wpms.contexts.names{context_i});
    for cond_i=wpms.contexts.indices{context_i}
        if cond_i > 8;%1:length(wpms.conditions)
            data = zeros(length(wpms.data.labels)-2,length(wpms.responsetype),length(wpms.data.frequency),length(wpms.data.times));%temp storage of power data
            for response_i = 1:length(wpms.responsetype)
                
                for electrode = 1:length(wpms.data.labels)-2;%find cluster_specific electrodes
                    tic;
                    fprintf('\n%s\t',wpms.data.labels{electrode});
                    for name_i = 1:length(wpms.names)
                        fprintf('.');
                        filename = [wpms.dirs.WAVELET_OUTPUT_DIR wpms.names{name_i} filesep ...
                            wpms.conditions{cond_i} filesep wpms.conditions{cond_i} ...
                            wpms.responsetype{response_i} '_' num2str(electrode) ...
                            '_imagcoh_mwtf.mat'];
                        load(filename,'mw_tf');
                        data(electrode,response_i,:,:) = squeeze(data(electrode,response_i,:,:))+mw_tf; clear mw_tf
                    end%electrode loop
                    t=toc;
                    fprintf('%s\t%2.3f %s\t%s %2.3f %s','Electrode completed in:',t,'s','estimated time remaining for current stimulus:',((length(wpms.data.labels)-electrode)*t),'s');
                end
            end
            if cond_i < 9;%orientation doesn't matter
                data = squeeze(mean(data,2));
                data = data./length(wpms.names);%average over participants
            else
                data = data./length(wpms.names);%average over participants
            end
            for freq_i = 1:length(wpms.stats.freq_labels)
                datavector = zeros(length(wpms.data.labels)-2,1);
                if cond_i<9;
                    for electrode = 1:length(wpms.data.labels)-2;
                        %                 if cond_i < 9;
                        reduced_data = squeeze(data(electrode,wpms.stats.freq_bands{freq_i},wpms.data.plot_times));
                        datavector(electrode,1) = squeeze(mean(reduced_data(wpms.stats.sigmask.(wpms.contexts.names{context_i}).(wpms.stats.freq_labels{freq_i}))));
                    end
                    figure();topoplot(datavector,EEG.chanlocs,'numcontour',2,'whitebk','on','gridscale',60,'shading','interp','maplimits',clims{freq_i},'colormap','jet');
                    savename = [wpms.dirs.WAVELET_OUTPUT_DIR wpms.contexts.names{context_i} '_' wpms.conditionshort{cond_i} '_' wpms.stats.freq_labels{freq_i} '.fig'];
                    saveas(gcf,savename,'fig');
                    close;
                    %                     figure();topoplot(datavector,EEG.chanlocs,'numcontour',2,'whitebk','on','gridscale',60,'shading','interp','maplimits',clims{freq_i},'colormap','jet');
                    %                     savename = [wpms.dirs.WAVELET_OUTPUT_DIR wpms.contexts.names{context_i} '_' wpms.conditionshort{cond_i} '_' wpms.stats.freq_labels{freq_i} '.fig'];
                    %                     saveas(gcf,savename,'fig');
                    %                     close;
                else
                    for response_i = 1:length(wpms.responsetype)
                        datavector = zeros(length(wpms.data.labels)-2,1);
                        for electrode = 1:length(wpms.data.labels)-2;
                            reduced_data = squeeze(data(electrode,response_i,wpms.stats.freq_bands{freq_i},wpms.data.plot_times));
                            datavector(electrode,1) = squeeze(mean(reduced_data(wpms.stats.sigmask.(wpms.contexts.names{context_i}).(wpms.stats.freq_labels{freq_i}))));
                        end
                        figure();topoplot(datavector,EEG.chanlocs,'numcontour',2,'whitebk','on','gridscale',60,'shading','interp','maplimits',clims{freq_i},'colormap','jet');
                        savename = [wpms.dirs.WAVELET_OUTPUT_DIR wpms.contexts.names{context_i} '_' wpms.conditionshort{cond_i} '_' wpms.responsetype{response_i} '_' wpms.stats.freq_labels{freq_i} '.fig'];
                        saveas(gcf,savename,'fig');
                        close;
                    end
                end
            end
        end%freq_i loop
    end
end%cluster_i loop
% end%response_i loop
% end%cond_i loop
t=toc; fprintf('%3.2f %s',t,'s to complete');

%% create subplots of frequency topoplots
close all;clc;
h1= openfig('E:\UIB_DATA\WAVELET_OUTPUT\grey_N-CUE_delta.fig','reuse');
set(h1,'Position',[0 0 1920 1080]);
ax1=gca;
h2 = openfig('E:\UIB_DATA\WAVELET_OUTPUT\grey_N-CUE_theta.fig','reuse');
set(h2,'Position',[0 0 1920 1080]);
ax2=gca;
h3= openfig('E:\UIB_DATA\WAVELET_OUTPUT\grey_N-CUE_alpha.fig','reuse');
set(h3,'Position',[0 0 1920 1080]);
ax3=gca;
h4 = openfig('E:\UIB_DATA\WAVELET_OUTPUT\grey_N-CUE_beta.fig','reuse');
set(h4,'Position',[0 0 1920 1080]);
ax4=gca;
h5=figure();set(gcf,'Position',[0 0 1920 1080]);
s1 = subplot(4,1,1); axis off;axis square;caxis([-2 2]);%create and get handle to the subplot axes
s2 = subplot(4,1,2);axis off;axis square;caxis([-2 2]);
s3 = subplot(4,1,3); axis off;axis square;caxis([-2 2]);
s4 = subplot(4,1,4);axis off;axis square;caxis([-1 1]);
fig1 = get(ax1,'children'); %get handle to all the children in the figure
fig2 = get(ax2,'children');
fig3 = get(ax3,'children');
fig4 = get(ax4,'children');
copyobj(fig1,s1); %copy children to new parent axes i.e. the subplot axes
copyobj(fig2,s2);
copyobj(fig3,s3);
copyobj(fig4,s4);
%%
for context_i = 1:length(wpms.contexts.names)
    for cond_i = wpms.contexts.indices{context_i}%do con_i and context loops here
        if cond_i <9
%             close all;clc;
%             h1= openfig([wpms.dirs.WAVELET_OUTPUT_DIR wpms.contexts.names{context_i} '_' wpms.conditionshort{cond_i} '_' wpms.stats.freq_labels{1} '.fig'],'reuse');
%             set(h1,'Position',[0 0 1920 1080]);
%             ax1=gca;
%             h2= openfig([wpms.dirs.WAVELET_OUTPUT_DIR wpms.contexts.names{context_i} '_' wpms.conditionshort{cond_i} '_' wpms.stats.freq_labels{2} '.fig'],'reuse');
%             set(h2,'Position',[0 0 1920 1080]);
%             ax2=gca;
%             h3= openfig([wpms.dirs.WAVELET_OUTPUT_DIR wpms.contexts.names{context_i} '_' wpms.conditionshort{cond_i} '_' wpms.stats.freq_labels{3} '.fig'],'reuse');
%             set(h3,'Position',[0 0 1920 1080]);
%             ax3=gca;
%             h4= openfig([wpms.dirs.WAVELET_OUTPUT_DIR wpms.contexts.names{context_i} '_' wpms.conditionshort{cond_i} '_' wpms.stats.freq_labels{4} '.fig'],'reuse');
%             set(h4,'Position',[0 0 1920 1080]);
%             ax4=gca;
%             figure();set(gcf,'Position',[0 0 1920 1080]);h5=tight_subplot(4,1,[.001 .001],[.001 .001],[.001 .001]);
%             % s1 = axes(h5(1)); axis off;axis square;caxis([-2 2]);%create and get handle to the subplot axes
%             % s2 = axes(h5(2));axis off;axis square;caxis([-2 2]);
%             % s3 = axes(h5(3)); axis off;axis square;caxis([-2 2]);
%             % s4 = axes(h5(4));axis off;axis square;caxis([-1 1]);
%             fig1 = get(ax1,'children'); %get handle to all the children in the figure
%             fig2 = get(ax2,'children');
%             fig3 = get(ax3,'children');
%             fig4 = get(ax4,'children');
%             axes(h5(1));axis off;axis square;caxis([-2 2]);
%             axes(h5(2));axis off;axis square;caxis([-2 2]);
%             axes(h5(3));axis off;axis square;caxis([-2 2]);
%             axes(h5(4));axis off;axis square;caxis([-1 1]);
%             copyobj(fig1,h5(1)); %copy children to new parent axes i.e. the subplot axes
%             copyobj(fig2,h5(2));
%             copyobj(fig3,h5(3));
%             copyobj(fig4,h5(4));
%             close(h1);close(h2);close(h3);close(h4)
%             saveas(gcf,[wpms.dirs.CWD 'ANALYSIS' filesep 'NeuroImage_Revision' filesep wpms.conditionshort{cond_i} 'headplots.fig'],'fig')
%             close all;
%             uiopen([wpms.dirs.CWD 'ANALYSIS' filesep 'NeuroImage_Revision' filesep wpms.conditionshort{cond_i} 'headplots.fig'],1);
%             uiwait(gcf);
        else
            for response_i = 1:length(wpms.responsetype)
                close all;clc;
                h1= openfig([wpms.dirs.WAVELET_OUTPUT_DIR wpms.contexts.names{context_i} '_' wpms.conditionshort{cond_i} '_' wpms.responsetype{response_i} '_' wpms.stats.freq_labels{1} '.fig'],'reuse');
                set(h1,'Position',[0 0 1920 1080]);
                ax1=gca;
                h2= openfig([wpms.dirs.WAVELET_OUTPUT_DIR wpms.contexts.names{context_i} '_' wpms.conditionshort{cond_i} '_' wpms.responsetype{response_i} '_' wpms.stats.freq_labels{2} '.fig'],'reuse');
                set(h2,'Position',[0 0 1920 1080]);
                ax2=gca;
                h3= openfig([wpms.dirs.WAVELET_OUTPUT_DIR wpms.contexts.names{context_i} '_' wpms.conditionshort{cond_i} '_' wpms.responsetype{response_i} '_' wpms.stats.freq_labels{3} '.fig'],'reuse');
                set(h3,'Position',[0 0 1920 1080]);
                ax3=gca;
                h4= openfig([wpms.dirs.WAVELET_OUTPUT_DIR wpms.contexts.names{context_i} '_' wpms.conditionshort{cond_i} '_' wpms.responsetype{response_i} '_' wpms.stats.freq_labels{4} '.fig'],'reuse');
                set(h4,'Position',[0 0 1920 1080]);
                ax4=gca;
                figure();set(gcf,'Position',[0 0 1920 1080]);h5=tight_subplot(4,1,[.001 .001],[.001 .001],[.001 .001]);
                % s1 = axes(h5(1)); axis off;axis square;caxis([-2 2]);%create and get handle to the subplot axes
                % s2 = axes(h5(2));axis off;axis square;caxis([-2 2]);
                % s3 = axes(h5(3)); axis off;axis square;caxis([-2 2]);
                % s4 = axes(h5(4));axis off;axis square;caxis([-1 1]);
                fig1 = get(ax1,'children'); %get handle to all the children in the figure
                fig2 = get(ax2,'children');
                fig3 = get(ax3,'children');
                fig4 = get(ax4,'children');
                axes(h5(1));axis off;axis square;caxis([-2 2]);
                axes(h5(2));axis off;axis square;caxis([-2 2]);
                axes(h5(3));axis off;axis square;caxis([-2 2]);
                axes(h5(4));axis off;axis square;caxis([-1 1]);
                copyobj(fig1,h5(1)); %copy children to new parent axes i.e. the subplot axes
                copyobj(fig2,h5(2));
                copyobj(fig3,h5(3));
                copyobj(fig4,h5(4));
                close(h1);close(h2);close(h3);close(h4)
                saveas(gcf,[wpms.dirs.CWD 'ANALYSIS' filesep 'NeuroImage_Revision' filesep wpms.conditionshort{cond_i} '_' wpms.responsetype{response_i} 'headplots.fig'],'fig')
                close all;
                uiopen([wpms.dirs.CWD 'ANALYSIS' filesep 'NeuroImage_Revision' filesep wpms.conditionshort{cond_i} '_' wpms.responsetype{response_i} 'headplots.fig'],1);
                uiwait(gcf);
            end
        end
    end
end