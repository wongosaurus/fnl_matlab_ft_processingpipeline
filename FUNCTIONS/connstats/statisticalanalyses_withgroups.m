%% Statistical Analyses of Connectivity Data
% Reworked version of ImportandCondenseMatrices
% Removed reliance on eval functions to speed up processing pipeline
% Patrick Cooper, University of Newcastle
% March, 2014
%% Set up globals
clear all
close all
warning off;
feature('DefaultCharacterSet','UTF-8');%for checkmarks
names = {'AGE002' 'AGE003' 'AGE008' 'AGE012' 'AGE013' 'AGE014' ...
    'AGE015' 'AGE017' 'AGE018' 'AGE019' 'AGE021' 'AGE022' ...
    'AGE023' 'AGE024' 'AGE026' 'AGE027' 'AGE028' 'AGE030' ...
    'AGE032' 'AGE033' 'AGE035' 'AGE036' 'AGE038' 'AGE046' ...
    'AGE047' 'AGE050' 'AGE051' 'AGE052' 'AGE058' ...
    'AGE005' 'AGE007' 'AGE020'  ... %'AGE043' 'AGE048'...%AGE106
    'AGE053' 'AGE059' 'AGE061' 'AGE062' 'AGE063'...
    'AGE064' 'AGE066' 'AGE067' 'AGE068' 'AGE069' 'AGE070'...
    'AGE072' 'AGE075' 'AGE077' 'AGE079' 'AGE081' 'AGE083'...
    'AGE084' 'AGE085' 'AGE086' 'AGE088' 'AGE089' 'AGE090'...
    'AGE092' 'AGE093' 'AGE094' 'AGE095' 'AGE096' 'AGE097'...
    'AGE098' 'AGE100' 'AGE102' 'AGE103' 'AGE104'...
    'AGE107' 'AGE108' 'AGE109' 'AGE114' 'AGE115' 'AGE116'...
    'AGE117' 'AGE118' 'AGE119' 'AGE120' ...
    'AGE121' 'AGE122' 'AGE123'  ... %'AGE043' 'AGE048'...
    'AGE124' 'AGE128' 'AGE129' 'AGE130' 'AGE131'...
    'AGE133' 'AGE134' 'AGE136' 'AGE138' 'AGE146'...
    'AGE147' 'AGE148' 'AGE149' 'AGE150' 'AGE151' 'AGE152'...
    'AGE153' 'AGE155' 'AGE156' 'AGE158' 'AGE159' 'AGE161'...
    'AGE162' 'AGE164' 'AGE165' 'AGE166' 'AGE167' 'AGE168'...
    'AGE169' 'AGE170' 'AGE172' 'AGE175' 'AGE176' 'AGE177'...
    'AGE178' 'AGE180' 'AGE181' 'AGE182' 'AGE184' 'AGE185'...
    'AGE186' 'AGE187' 'AGE195'};
% names = {'AGE007' 'AGE020' 'AGE053' 'AGE063' 'AGE064' 'AGE067' 'AGE069' 'AGE070' ...
%     'AGE077' 'AGE081' 'AGE083' 'AGE084' 'AGE086' 'AGE088' 'AGE089' ...
%    'AGE090' 'AGE092' 'AGE093' 'AGE094' 'AGE100' 'AGE104' ...
%    'AGE107' 'AGE108' 'AGE109' 'AGE116' 'AGE120' 'AGE130' 'AGE133' 'AGE148'};
conditions = {'switchto','switchaway','mixrepeat','noninf'};
frequencies = {'delta','theta','loweralpha','upperalpha','beta'};
times = {};
starttime = -200:100:1400;
endtime   = starttime+200;
for time_i = 1:length(starttime)
    times{time_i} = strcat(num2str(starttime(time_i)),'to',num2str(endtime(time_i)));
end
CWD  ='F:\FNL_EEG_TOOLBOX';
IMAG ='\IMAGCOH_OUTPUT';
addpath(genpath([CWD,'\PACKAGES\mass_uni_toolbox'])); %for FDR correction
addpath([CWD,'\FUNCTIONS']);
addpath([CWD,'\FUNCTIONS\connstats']);
addpath([CWD,IMAG]);
%% Generate file list and load files into structure
% list of files to be read in
filelist{1,(length(names)*length(conditions)*length(times))} = [];
count = 0;
for name_i = 1:length(names)
    for cond_i = 1:length(conditions)
        for time_i = 1:length(times)
            count = count+1;
            if name_i < 30;%different named values for original set
                filelist(count) = strcat(CWD,IMAG,'\',names(name_i),'\',conditions(cond_i),'\',conditions(cond_i),times(time_i),'_CONECTIVITY_IMAG.mat');
            else
                filelist(count) = strcat(CWD,IMAG,'\',names(name_i),'\',conditions(cond_i),'\',conditions(cond_i),times(time_i),'_CONNECTIVITY_IMAG.mat');
            end
        end%time_i loop
    end%cond_i loop
end%name_i loop
clear count name_i cond_i time_i freqstruct condstruct
%% Preallocate structure that house ALL data
%  ALLDATA.conditions.{condition}.{frequency}(Subject,Times,64,64)
%  {condition} == switchto switchaway noninf mixrepeat
%  {frequency} == delta theta loweralpha upperalpha beta
freqstruct = struct('delta',zeros(length(names),length(times),64,64),'theta',zeros(length(names),length(times),64,64), ...
    'loweralpha',zeros(length(names),length(times),64,64),'upperalpha',zeros(length(names),length(times),64,64), ...
    'beta',zeros(length(names),length(times),64,64));
condstruct = struct('switchto',freqstruct,'switchaway',freqstruct,'mixrepeat',freqstruct,'noninf',freqstruct);
ALLDATA = struct('conditions',condstruct);
% load data into structure
count = 0;
tic;
fprintf('Loading data for:\n');
for name_i = 1:length(names)
    fprintf('\n%s %s\t','Participant',names{name_i})
    for cond_i = 1:length(conditions)
        if cond_i == 1
            fprintf('%s\t',conditions{cond_i});
        elseif cond_i ~= 1
            fprintf('\t\t\t\t\t%s\t',conditions{cond_i});
        end
        for time_i = 1:length(times)
            fprintf('.');
            count = count+1;
            filename = filelist(1,count);
            ALLDATA = readConnMats(filename,name_i,cond_i,time_i,ALLDATA);
        end%time_i loop
        %         sprintf(char(hex2dec('2713')))%checkmark
        fprintf('\n');
    end%cond_i loop
end%name_i loop
toc
clear count cond_i name_i time_i
% save off structure
mkdir([CWD,'\ANALYSES\CONNECTIVITY_MATRICES\']);
cd([CWD,'\ANALYSES\CONNECTIVITY_MATRICES\']);
save('ALLDATA','ALLDATA');
%% Preallocate average structures
% freqstruct = struct('delta',zeros(length(times),64,64),'theta',zeros(length(times),64,64), ...
%                     'loweralpha',zeros(length(times),64,64),'upperalpha',zeros(length(times),64,64), ...
%                     'beta',zeros(length(times),64,64));
% condstruct = struct('switchto',freqstruct,'switchaway',freqstruct,'mixrepeat',freqstruct,'noninf',freqstruct);
% AVDATA = struct('conditions',condstruct);
% clear freqstruct condstruct
% tic;
% fprintf('Averaging connectivity matrices for:\n');
% for name_i = 1:length(names)
%     fprintf('\n%s %s\t','Participant',names{name_i})
%     for cond_i = 1:length(conditions)
%         if cond_i == 1
%             fprintf('%s\t',conditions{cond_i});
%         elseif cond_i ~= 1
%             fprintf('\t\t\t\t\t%s\t',conditions{cond_i});
%         end
%         for time_i = 1:length(times)
%             fprintf('.');
%             [AVDATA] = averageConnMats(ALLDATA,cond_i,time_i,AVDATA);
%         end%time_i loop
% %         sprintf(char(hex2dec('2713')))%checkmark
%         fprintf('\n');
%     end%cond_i loop
% end%name_i loop
% toc
% clear count cond_i name_i time_i
% cd([CWD,'\ANALYSES\CONNECTIVITY_MATRICES\']);
% save('AVDATA','AVDATA');
%% Statistics
% First create difference matrices (i.e. condition - mixrepeat)
% Preallocate difference structure

freqstruct = struct('delta',zeros(length(names),length(times),64,64),'theta',zeros(length(names),length(times),64,64), ...
    'loweralpha',zeros(length(names),length(times),64,64),'upperalpha',zeros(length(names),length(times),64,64), ...
    'beta',zeros(length(names),length(times),64,64));
condstruct = struct('switchto',freqstruct,'switchaway',freqstruct,'mixrepeat',freqstruct,'noninf',freqstruct);
DIFFDATA = struct('conditions',condstruct);
clear freqstruct condstruct
fprintf('Creating difference matrices for\n');
for name_i = 1:length(names)
    tic;
    fprintf('\n%s %s\t','Participant',names{name_i})
    for time_i = 1:length(times)
        fprintf('.');
        [DIFFDATA] = diffConnMats(ALLDATA,name_i,time_i,DIFFDATA);
        %             [DIFFDATA] = diffConnMats_noninf(ALLDATA,cond_i,name_i,time_i,DIFFDATA);
    end%time_i loop
    %         sprintf(char(hex2dec('2713')));%checkmark
    toc
end%name_i loop
cd([CWD,'\ANALYSES\CONNECTIVITY_MATRICES\']);
save('DIFFDATA','DIFFDATA');
%% Compute t-tests
% First create t-test matrices that will store t-values
% Preallocate t-value & p-value structure
g1 = [34
44
98
105
107
108
109
110
111
114
115
116
];
    g2 = [10
12
15
21
23
26
32
33
35
40
41
45
49
50
52
58
59
60
61
63
64
73
74
76
78
79
80
81
83
85
86
89
94
95
97
99
103
106
117
118
121
];
    g3 = [13
14
20
24
27
43
46
47
48
54
55
66
69
70
72
77
87
88
92
93
101
102
104
113
120
122
];
    g4 = [1
2
9
11
16
19
28
36
42
51
67
84
90
91
96
100
112
119
];
    g5 = [3
30
38
39
56
57
62
65
];
    g6 = [6
8
18
22
75
82
];
    g7 = [4
5
7
17
25
29
31
37
53
68
71
];
for group = 1:7;
    eval(['ind = g' num2str(group) ';']);
    freqstruct = struct('delta',zeros(length(times),64,64),'theta',zeros(length(times),64,64), ...
        'loweralpha',zeros(length(times),64,64),'upperalpha',zeros(length(times),64,64), ...
        'beta',zeros(length(times),64,64));
    condstruct = struct('switchto',freqstruct,'switchaway',freqstruct,'mixrepeat',freqstruct,'noninf',freqstruct);
    TVALS = struct('conditions',condstruct);
    PVALS = struct('conditions',condstruct);
    clear freqstruct condstruct
    fprintf('Computing t-tests for:\n');
    for cond_i = 1:length(conditions)
        if cond_i == 1
            fprintf('%s\t',conditions{cond_i});
        elseif cond_i ~= 1
            fprintf('\t\t\t\t\t%s\t',conditions{cond_i});
        end
        tic;
        for time_i = 1:length(times)
            fprintf('.');
            [TVALS,PVALS] = ttestConnMatsGrouped(DIFFDATA,cond_i,time_i,TVALS,PVALS,ind);
            %         [TVALS,PVALS] = ttestConnMats(DIFFDATA,cond_i,time_i,TVALS,PVALS);
            %         [TVALS,PVALS] = ttestConnMats_noninf(DIFFDATA,cond_i,time_i,TVALS,PVALS);
        end%time_i loop
        toc
        %     sprintf(char(hex2dec('2713')));%checkmark
        fprintf('\n');
    end%cond_i loop
    cd([CWD,'\ANALYSES\CONNECTIVITY_MATRICES\']);
    save(['TVALS_g' num2str(group)],'TVALS');
    save(['PVALS_g' num2str(group)]','PVALS');
end

%% Find Significant t-values
% First create t-test matrices that will store significant t-values
% Preallocate t-value & p-value structure
%freqstruct = struct('delta',zeros(length(times),48,48),'theta',zeros(length(times),48,48), ...
%                    'loweralpha',zeros(length(times),48,48),'upperalpha',zeros(length(times),48,48), ...
%                    'beta',zeros(length(times),48,48));
for group = 1:7;
    load(['PVALS_g' num2str(group)])
    load(['TVALS_g' num2str(group)])
    freqstruct = struct('delta',zeros(length(times),64,64),'theta',zeros(length(times),64,64), ...
        'loweralpha',zeros(length(times),64,64),'upperalpha',zeros(length(times),64,64), ...
        'beta',zeros(length(times),64,64));
    condstruct = struct('switchto',freqstruct,'switchaway',freqstruct,'mixrepeat',freqstruct,'noninf',freqstruct);
    SIGTVALS = struct('conditions',condstruct);
    clear freqstruct condstruct
    fprintf('Searching t-maps for significant values for:\n');
    for cond_i = 1:length(conditions)
        fprintf('%s\t',conditions{cond_i});
        for time_i = 1:length(times)
            fprintf('.');
            SIGNIFICANCE = 0.005; %threshold of significance
            [SIGTVALS] = findSigTVals(TVALS,PVALS,cond_i,time_i,SIGTVALS,SIGNIFICANCE);
            %         [SIGTVALS] = findSigTVals_noninf(TVALS,PVALS,cond_i,time_i,SIGTVALS,SIGNIFICANCE);
        end%time_i loop
        %     sprintf(char(hex2dec('2713')));%checkmark
        fprintf('\n');
    end%cond_i loop
    cd([CWD,'\ANALYSES\CONNECTIVITY_MATRICES\']);
    save(['SIGTVALS_g' num2str(group)],'SIGTVALS');
end
%% Reorder t-Maps to better reflect topology and save off maps
for group =1:7;
    load(['SIGTVALS_g' num2str(group)])
    freqstruct = struct('delta',zeros(length(times),48,48),'theta',zeros(length(times),48,48), ...
        'loweralpha',zeros(length(times),48,48),'upperalpha',zeros(length(times),48,48), ...
        'beta',zeros(length(times),48,48));
    condstruct = struct('switchto',freqstruct,'switchaway',freqstruct,'mixrepeat',freqstruct,'noninf',freqstruct);
    REORDSIGTVALS = struct('conditions',condstruct);
    fprintf('Reordering t-maps to better reflect topology for:\n');
    for cond_i = 1:length(conditions)
        fprintf('%s\t',conditions{cond_i});
        for time_i = 1:length(times)
            fprintf('.');
            [REORDSIGTVALS] = reorderSigTVals(SIGTVALS,cond_i,time_i,REORDSIGTVALS);
            %         [REORDSIGTVALS] = reorderSigTVals_noninf(SIGTVALS,cond_i,time_i,REORDSIGTVALS);
        end%time_i loop
        %     sprintf(char(hex2dec('2713')));%checkmark
        fprintf('\n');
    end%cond_i loop
    cd([CWD,'\ANALYSES\CONNECTIVITY_MATRICES\']);
    save(['REORDSIGTVALS_g' num2str(group)],'REORDSIGTVALS');
end