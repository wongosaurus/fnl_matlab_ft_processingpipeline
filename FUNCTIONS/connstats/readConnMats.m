function [ALLDATA] = readConnMats(filename,name_i,cond_i,time_i,ALLDATA)
load(filename{1,1})
switch cond_i
    case 1
        for row = 1:size(ConnectivityMatrix_beta,1)
            for col = 1:size(ConnectivityMatrix_beta,2)
                ALLDATA.conditions.switchto.delta(name_i,time_i,row,col)      = ConnectivityMatrix_delta(row,col);
                ALLDATA.conditions.switchto.theta(name_i,time_i,row,col)      = ConnectivityMatrix_theta(row,col);
                ALLDATA.conditions.switchto.loweralpha(name_i,time_i,row,col) = ConnectivityMatrix_loweralpha(row,col);
                ALLDATA.conditions.switchto.upperalpha(name_i,time_i,row,col) = ConnectivityMatrix_upperalpha(row,col);
                ALLDATA.conditions.switchto.beta(name_i,time_i,row,col)       = ConnectivityMatrix_beta(row,col);
            end
        end
    case 2
        for row = 1:size(ConnectivityMatrix_beta,1)
            for col = 1:size(ConnectivityMatrix_beta,2)
                ALLDATA.conditions.switchaway.delta(name_i,time_i,row,col)      = ConnectivityMatrix_delta(row,col);
                ALLDATA.conditions.switchaway.theta(name_i,time_i,row,col)      = ConnectivityMatrix_theta(row,col);
                ALLDATA.conditions.switchaway.loweralpha(name_i,time_i,row,col) = ConnectivityMatrix_loweralpha(row,col);
                ALLDATA.conditions.switchaway.upperalpha(name_i,time_i,row,col) = ConnectivityMatrix_upperalpha(row,col);
                ALLDATA.conditions.switchaway.beta(name_i,time_i,row,col)       = ConnectivityMatrix_beta(row,col);
            end
        end
    case 3
        for row = 1:size(ConnectivityMatrix_beta,1)
            for col = 1:size(ConnectivityMatrix_beta,2)
                ALLDATA.conditions.mixrepeat.delta(name_i,time_i,row,col)      = ConnectivityMatrix_delta(row,col);
                ALLDATA.conditions.mixrepeat.theta(name_i,time_i,row,col)      = ConnectivityMatrix_theta(row,col);
                ALLDATA.conditions.mixrepeat.loweralpha(name_i,time_i,row,col) = ConnectivityMatrix_loweralpha(row,col);
                ALLDATA.conditions.mixrepeat.upperalpha(name_i,time_i,row,col) = ConnectivityMatrix_upperalpha(row,col);
                ALLDATA.conditions.mixrepeat.beta(name_i,time_i,row,col)       = ConnectivityMatrix_beta(row,col);
            end
        end
    case 4
        for row = 1:size(ConnectivityMatrix_beta,1)
            for col = 1:size(ConnectivityMatrix_beta,2)
                ALLDATA.conditions.noninf.delta(name_i,time_i,row,col)      = ConnectivityMatrix_delta(row,col);
                ALLDATA.conditions.noninf.theta(name_i,time_i,row,col)      = ConnectivityMatrix_theta(row,col);
                ALLDATA.conditions.noninf.loweralpha(name_i,time_i,row,col) = ConnectivityMatrix_loweralpha(row,col);
                ALLDATA.conditions.noninf.upperalpha(name_i,time_i,row,col) = ConnectivityMatrix_upperalpha(row,col);
                ALLDATA.conditions.noninf.beta(name_i,time_i,row,col)       = ConnectivityMatrix_beta(row,col);
            end
        end
end
