function [TVALS,PVALS] = ttestConnMatsGrouped(DIFFDATA,cond_i,time_i,TVALS,PVALS,ind)
switch cond_i
    case 1
        for row = 1:size(DIFFDATA.conditions.switchto.delta,3)
            for col = 1:size(DIFFDATA.conditions.switchto.delta,4)
                [~,PVALS.conditions.switchto.delta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchto.delta(ind,time_i,row,col),0);
                TVALS.conditions.switchto.delta(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.switchto.theta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchto.theta(ind,time_i,row,col),0);
                TVALS.conditions.switchto.theta(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.switchto.loweralpha(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchto.loweralpha(ind,time_i,row,col),0);
                TVALS.conditions.switchto.loweralpha(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.switchto.upperalpha(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchto.upperalpha(ind,time_i,row,col),0);
                TVALS.conditions.switchto.upperalpha(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.switchto.beta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchto.beta(ind,time_i,row,col),0);
                TVALS.conditions.switchto.beta(time_i,row,col) = t.tstat;
                clear t
            end
        end
    case 2
        for row = 1:size(DIFFDATA.conditions.switchto.delta,3)
            for col = 1:size(DIFFDATA.conditions.switchto.delta,4)
                [~,PVALS.conditions.switchaway.delta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchaway.delta(ind,time_i,row,col),0);
                TVALS.conditions.switchaway.delta(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.switchaway.theta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchaway.theta(ind,time_i,row,col),0);
                TVALS.conditions.switchaway.theta(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.switchaway.loweralpha(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchaway.loweralpha(ind,time_i,row,col),0);
                TVALS.conditions.switchaway.loweralpha(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.switchaway.upperalpha(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchaway.upperalpha(ind,time_i,row,col),0);
                TVALS.conditions.switchaway.upperalpha(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.switchaway.beta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchaway.beta(ind,time_i,row,col),0);
                TVALS.conditions.switchaway.beta(time_i,row,col) = t.tstat;
                clear t
            end
        end
    case 4
        for row = 1:size(DIFFDATA.conditions.switchto.delta,3)
            for col = 1:size(DIFFDATA.conditions.switchto.delta,4)
                [~,PVALS.conditions.noninf.delta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.noninf.delta(ind,time_i,row,col),0);
                TVALS.conditions.noninf.delta(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.noninf.theta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.noninf.theta(ind,time_i,row,col),0);
                TVALS.conditions.noninf.theta(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.noninf.loweralpha(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.noninf.loweralpha(ind,time_i,row,col),0);
                TVALS.conditions.noninf.loweralpha(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.noninf.upperalpha(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.noninf.upperalpha(ind,time_i,row,col),0);
                TVALS.conditions.noninf.upperalpha(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.noninf.beta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.noninf.beta(ind,time_i,row,col),0);
                TVALS.conditions.noninf.beta(time_i,row,col) = t.tstat;
                clear t
            end
        end
end