function [TVALS,PVALS] = ttestConnMats(DIFFDATA,cond_i,time_i,TVALS,PVALS)
switch cond_i
    case 1
        for row = 1:size(DIFFDATA.conditions.switchto.delta,3)
            for col = 1:size(DIFFDATA.conditions.switchto.delta,4)
                [~,PVALS.conditions.switchto.delta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchto.delta(1:size(DIFFDATA.conditions.switchto.delta,1),time_i,row,col),0);
                TVALS.conditions.switchto.delta(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.switchto.theta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchto.theta(1:size(DIFFDATA.conditions.switchto.theta,1),time_i,row,col),0);
                TVALS.conditions.switchto.theta(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.switchto.loweralpha(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchto.loweralpha(1:size(DIFFDATA.conditions.switchto.loweralpha,1),time_i,row,col),0);
                TVALS.conditions.switchto.loweralpha(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.switchto.upperalpha(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchto.upperalpha(1:size(DIFFDATA.conditions.switchto.upperalpha,1),time_i,row,col),0);
                TVALS.conditions.switchto.upperalpha(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.switchto.beta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchto.beta(1:size(DIFFDATA.conditions.switchto.beta,1),time_i,row,col),0);
                TVALS.conditions.switchto.beta(time_i,row,col) = t.tstat;
                clear t
            end
        end
    case 2
        for row = 1:size(DIFFDATA.conditions.switchto.delta,3)
            for col = 1:size(DIFFDATA.conditions.switchto.delta,4)
                [~,PVALS.conditions.switchaway.delta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchaway.delta(1:size(DIFFDATA.conditions.switchaway.delta,1),time_i,row,col),0);
                TVALS.conditions.switchaway.delta(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.switchaway.theta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchaway.theta(1:size(DIFFDATA.conditions.switchaway.theta,1),time_i,row,col),0);
                TVALS.conditions.switchaway.theta(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.switchaway.loweralpha(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchaway.loweralpha(1:size(DIFFDATA.conditions.switchaway.loweralpha,1),time_i,row,col),0);
                TVALS.conditions.switchaway.loweralpha(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.switchaway.upperalpha(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchaway.upperalpha(1:size(DIFFDATA.conditions.switchaway.upperalpha,1),time_i,row,col),0);
                TVALS.conditions.switchaway.upperalpha(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.switchaway.beta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.switchaway.beta(1:size(DIFFDATA.conditions.switchaway.beta,1),time_i,row,col),0);
                TVALS.conditions.switchaway.beta(time_i,row,col) = t.tstat;
                clear t
            end
        end
    case 4
        for row = 1:size(DIFFDATA.conditions.switchto.delta,3)
            for col = 1:size(DIFFDATA.conditions.switchto.delta,4)
                [~,PVALS.conditions.noninf.delta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.noninf.delta(1:size(DIFFDATA.conditions.noninf.delta,1),time_i,row,col),0);
                TVALS.conditions.noninf.delta(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.noninf.theta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.noninf.theta(1:size(DIFFDATA.conditions.noninf.theta,1),time_i,row,col),0);
                TVALS.conditions.noninf.theta(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.noninf.loweralpha(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.noninf.loweralpha(1:size(DIFFDATA.conditions.noninf.loweralpha,1),time_i,row,col),0);
                TVALS.conditions.noninf.loweralpha(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.noninf.upperalpha(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.noninf.upperalpha(1:size(DIFFDATA.conditions.noninf.upperalpha,1),time_i,row,col),0);
                TVALS.conditions.noninf.upperalpha(time_i,row,col) = t.tstat;
                clear t
                [~,PVALS.conditions.noninf.beta(time_i,row,col),~,t]= ttest(DIFFDATA.conditions.noninf.beta(1:size(DIFFDATA.conditions.noninf.beta,1),time_i,row,col),0);
                TVALS.conditions.noninf.beta(time_i,row,col) = t.tstat;
                clear t
            end
        end
end