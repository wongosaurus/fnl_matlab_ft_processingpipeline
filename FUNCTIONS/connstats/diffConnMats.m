function [DIFFDATA] = diffConnMats(ALLDATA,name_i,time_i,DIFFDATA)
condfield = fieldnames(ALLDATA.conditions);
freqfield = fieldnames(ALLDATA.conditions.(condfield{1}));
for condfield_i = 1:length(condfield)
    for freqfield_i = 1:length(freqfield)
        DIFFDATA.conditions.(condfield{condfield_i}).(freqfield{freqfield_i})(name_i,time_i,:,:) = ALLDATA.conditions.(condfield{condfield_i}).(freqfield{freqfield_i})(name_i,time_i,:,:)- ALLDATA.conditions.mixrepeat.(freqfield{freqfield_i})(name_i,time_i,:,:);
    end
end
% switch cond_i
%     case 1
% %         for row = 1:size(ALLDATA.conditions.switchto.delta,3)
% %             for col = 1:size(ALLDATA.conditions.switchto.delta,4)
%                 DIFFDATA.conditions.switchto.delta(name_i,time_i,row,col)      = ALLDATA.conditions.switchto.delta(name_i,time_i,row,col)- ALLDATA.conditions.mixrepeat.delta(name_i,time_i,row,col);
%                 DIFFDATA.conditions.switchto.theta(name_i,time_i,row,col)      = ALLDATA.conditions.switchto.theta(name_i,time_i,row,col)- ALLDATA.conditions.mixrepeat.theta(name_i,time_i,row,col);
%                 DIFFDATA.conditions.switchto.loweralpha(name_i,time_i,row,col) = ALLDATA.conditions.switchto.loweralpha(name_i,time_i,row,col)- ALLDATA.conditions.mixrepeat.loweralpha(name_i,time_i,row,col);
%                 DIFFDATA.conditions.switchto.upperalpha(name_i,time_i,row,col) = ALLDATA.conditions.switchto.upperalpha(name_i,time_i,row,col)- ALLDATA.conditions.mixrepeat.upperalpha(name_i,time_i,row,col);
%                 DIFFDATA.conditions.switchto.beta(name_i,time_i,row,col)       = ALLDATA.conditions.switchto.beta(name_i,time_i,row,col)- ALLDATA.conditions.mixrepeat.beta(name_i,time_i,row,col);
% %             end
% %         end
%     case 2
% %         for row = 1:size(ALLDATA.conditions.switchto.delta,3)
% %             for col = 1:size(ALLDATA.conditions.switchto.delta,4)
%                 DIFFDATA.conditions.switchaway.delta(name_i,time_i,row,col)      = ALLDATA.conditions.switchaway.delta(name_i,time_i,row,col)- ALLDATA.conditions.mixrepeat.delta(name_i,time_i,row,col);
%                 DIFFDATA.conditions.switchaway.theta(name_i,time_i,row,col)      = ALLDATA.conditions.switchaway.theta(name_i,time_i,row,col)- ALLDATA.conditions.mixrepeat.theta(name_i,time_i,row,col);
%                 DIFFDATA.conditions.switchaway.loweralpha(name_i,time_i,row,col) = ALLDATA.conditions.switchaway.loweralpha(name_i,time_i,row,col)- ALLDATA.conditions.mixrepeat.loweralpha(name_i,time_i,row,col);
%                 DIFFDATA.conditions.switchaway.upperalpha(name_i,time_i,row,col) = ALLDATA.conditions.switchaway.upperalpha(name_i,time_i,row,col)- ALLDATA.conditions.mixrepeat.upperalpha(name_i,time_i,row,col);
%                 DIFFDATA.conditions.switchaway.beta(name_i,time_i,row,col)       = ALLDATA.conditions.switchaway.beta(name_i,time_i,row,col)- ALLDATA.conditions.mixrepeat.beta(name_i,time_i,row,col);
% %             end
% %         end
%     case 4
% %         for row = 1:size(ALLDATA.conditions.switchto.delta,3)
% %             for col = 1:size(ALLDATA.conditions.switchto.delta,4)
%                 DIFFDATA.conditions.noninf.delta(name_i,time_i,row,col)      = ALLDATA.conditions.noninf.delta(name_i,time_i,row,col)- ALLDATA.conditions.mixrepeat.delta(name_i,time_i,row,col);
%                 DIFFDATA.conditions.noninf.theta(name_i,time_i,row,col)      = ALLDATA.conditions.noninf.theta(name_i,time_i,row,col)- ALLDATA.conditions.mixrepeat.theta(name_i,time_i,row,col);
%                 DIFFDATA.conditions.noninf.loweralpha(name_i,time_i,row,col) = ALLDATA.conditions.noninf.loweralpha(name_i,time_i,row,col)- ALLDATA.conditions.mixrepeat.loweralpha(name_i,time_i,row,col);
%                 DIFFDATA.conditions.noninf.upperalpha(name_i,time_i,row,col) = ALLDATA.conditions.noninf.upperalpha(name_i,time_i,row,col)- ALLDATA.conditions.mixrepeat.upperalpha(name_i,time_i,row,col);
%                 DIFFDATA.conditions.noninf.beta(name_i,time_i,row,col)       = ALLDATA.conditions.noninf.beta(name_i,time_i,row,col)- ALLDATA.conditions.mixrepeat.beta(name_i,time_i,row,col);
% %             end
% %         end
% end