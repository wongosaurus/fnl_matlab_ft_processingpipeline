function [REORDSIGTVALS] = reorderSigTVals(SIGTVALS,cond_i,time_i,REORDSIGTVALS)
switch cond_i
    case 1
        rowcount = 0;
        for row = [3 4 5 6 9 10 11 12 13 14 17 18 19 20 21 22 23 25 26 ...
                27 29 30 31 32 36 37 38 39 40 41 44 45 46 47 48 49 ...
                50 51 54 55 56 57 58 59 60 62 63 64];
            rowcount = rowcount +1;
            colcount = 0;
            for col = [3 4 5 6 9 10 11 12 13 14 17 18 19 20 21 22 23 25 26 ...
                    27 29 30 31 32 36 37 38 39 40 41 44 45 46 47 48 49 ...
                    50 51 54 55 56 57 58 59 60 62 63 64];
                if isinf(SIGTVALS.conditions.switchto.delta(time_i,row,col));
                    SIGTVALS.conditions.switchto.delta(time_i,row,col) = 0;
                end
                if isinf(SIGTVALS.conditions.switchto.theta(time_i,row,col));
                    SIGTVALS.conditions.switchto.theta(time_i,row,col) = 0;
                end
                if isinf(SIGTVALS.conditions.switchto.loweralpha(time_i,row,col));
                    SIGTVALS.conditions.switchto.loweralpha(time_i,row,col) = 0;
                end
                if isinf(SIGTVALS.conditions.switchto.upperalpha(time_i,row,col));
                    SIGTVALS.conditions.switchto.upperalpha(time_i,row,col) = 0;
                end
                if isinf(SIGTVALS.conditions.switchto.beta(time_i,row,col));
                    SIGTVALS.conditions.switchto.beta(time_i,row,col) = 0;
                end
                colcount = colcount+1;
                REORDSIGTVALS.conditions.switchto.delta(time_i,rowcount,colcount) = SIGTVALS.conditions.switchto.delta(time_i,row,col);
                REORDSIGTVALS.conditions.switchto.theta(time_i,rowcount,colcount) = SIGTVALS.conditions.switchto.theta(time_i,row,col);
                REORDSIGTVALS.conditions.switchto.loweralpha(time_i,rowcount,colcount) = SIGTVALS.conditions.switchto.loweralpha(time_i,row,col);
                REORDSIGTVALS.conditions.switchto.upperalpha(time_i,rowcount,colcount) = SIGTVALS.conditions.switchto.upperalpha(time_i,row,col);
                REORDSIGTVALS.conditions.switchto.beta(time_i,rowcount,colcount) = SIGTVALS.conditions.switchto.beta(time_i,row,col);
            end
        end
    case 2
        rowcount = 0;
        for row = [3 4 5 6 9 10 11 12 13 14 17 18 19 20 21 22 23 25 26 ...
                27 29 30 31 32 36 37 38 39 40 41 44 45 46 47 48 49 ...
                50 51 54 55 56 57 58 59 60 62 63 64];
            rowcount = rowcount +1;
            colcount = 0;
            for col = [3 4 5 6 9 10 11 12 13 14 17 18 19 20 21 22 23 25 26 ...
                    27 29 30 31 32 36 37 38 39 40 41 44 45 46 47 48 49 ...
                    50 51 54 55 56 57 58 59 60 62 63 64];
                if isinf(SIGTVALS.conditions.switchaway.delta(time_i,row,col));
                    SIGTVALS.conditions.switchaway.delta(time_i,row,col) = 0;
                end
                if isinf(SIGTVALS.conditions.switchaway.theta(time_i,row,col));
                    SIGTVALS.conditions.switchaway.theta(time_i,row,col) = 0;
                end
                if isinf(SIGTVALS.conditions.switchaway.loweralpha(time_i,row,col));
                    SIGTVALS.conditions.switchaway.loweralpha(time_i,row,col) = 0;
                end
                if isinf(SIGTVALS.conditions.switchaway.upperalpha(time_i,row,col));
                    SIGTVALS.conditions.switchaway.upperalpha(time_i,row,col) = 0;
                end
                if isinf(SIGTVALS.conditions.switchaway.beta(time_i,row,col));
                    SIGTVALS.conditions.switchaway.beta(time_i,row,col) = 0;
                end
                colcount = colcount+1;
                REORDSIGTVALS.conditions.switchaway.delta(time_i,rowcount,colcount) = SIGTVALS.conditions.switchaway.delta(time_i,row,col);
                REORDSIGTVALS.conditions.switchaway.theta(time_i,rowcount,colcount) = SIGTVALS.conditions.switchaway.theta(time_i,row,col);
                REORDSIGTVALS.conditions.switchaway.loweralpha(time_i,rowcount,colcount) = SIGTVALS.conditions.switchaway.loweralpha(time_i,row,col);
                REORDSIGTVALS.conditions.switchaway.upperalpha(time_i,rowcount,colcount) = SIGTVALS.conditions.switchaway.upperalpha(time_i,row,col);
                REORDSIGTVALS.conditions.switchaway.beta(time_i,rowcount,colcount) = SIGTVALS.conditions.switchaway.beta(time_i,row,col);
            end
        end
    case 3
        rowcount = 0;
        for row = [3 4 5 6 9 10 11 12 13 14 17 18 19 20 21 22 23 25 26 ...
                27 29 30 31 32 36 37 38 39 40 41 44 45 46 47 48 49 ...
                50 51 54 55 56 57 58 59 60 62 63 64];
            rowcount = rowcount +1;
            colcount = 0;
            for col = [3 4 5 6 9 10 11 12 13 14 17 18 19 20 21 22 23 25 26 ...
                    27 29 30 31 32 36 37 38 39 40 41 44 45 46 47 48 49 ...
                    50 51 54 55 56 57 58 59 60 62 63 64];
                if isinf(SIGTVALS.conditions.mixrepeat.delta(time_i,row,col));
                    SIGTVALS.conditions.mixrepeat.delta(time_i,row,col) = 0;
                end
                if isinf(SIGTVALS.conditions.mixrepeat.theta(time_i,row,col));
                    SIGTVALS.conditions.mixrepeat.theta(time_i,row,col) = 0;
                end
                if isinf(SIGTVALS.conditions.mixrepeat.loweralpha(time_i,row,col));
                    SIGTVALS.conditions.mixrepeat.loweralpha(time_i,row,col) = 0;
                end
                if isinf(SIGTVALS.conditions.mixrepeat.upperalpha(time_i,row,col));
                    SIGTVALS.conditions.mixrepeat.upperalpha(time_i,row,col) = 0;
                end
                if isinf(SIGTVALS.conditions.mixrepeat.beta(time_i,row,col));
                    SIGTVALS.conditions.mixrepeat.beta(time_i,row,col) = 0;
                end
                colcount = colcount+1;
                REORDSIGTVALS.conditions.mixrepeat.delta(time_i,rowcount,colcount) = SIGTVALS.conditions.mixrepeat.delta(time_i,row,col);
                REORDSIGTVALS.conditions.mixrepeat.theta(time_i,rowcount,colcount) = SIGTVALS.conditions.mixrepeat.theta(time_i,row,col);
                REORDSIGTVALS.conditions.mixrepeat.loweralpha(time_i,rowcount,colcount) = SIGTVALS.conditions.mixrepeat.loweralpha(time_i,row,col);
                REORDSIGTVALS.conditions.mixrepeat.upperalpha(time_i,rowcount,colcount) = SIGTVALS.conditions.mixrepeat.upperalpha(time_i,row,col);
                REORDSIGTVALS.conditions.mixrepeat.beta(time_i,rowcount,colcount) = SIGTVALS.conditions.mixrepeat.beta(time_i,row,col);
            end
        end
end