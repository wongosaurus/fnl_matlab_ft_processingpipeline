function [SIGTVALS] = findSigTVals(TVALS,PVALS,cond_i,time_i,SIGTVALS,SIGNIFICANCE)
switch cond_i
    case 1
        for row = [3 4 5 6 9 10 11 12 13 14 17 18 19 20 21 22 23 25 26 ...
                   27 29 30 31 32 36 37 38 39 40 41 44 45 46 47 48 49 ...
                   50 51 54 55 56 57 58 59 60 62 63 64];
            for col = [3 4 5 6 9 10 11 12 13 14 17 18 19 20 21 22 23 25 26 ...
                   27 29 30 31 32 36 37 38 39 40 41 44 45 46 47 48 49 ...
                   50 51 54 55 56 57 58 59 60 62 63 64];
               [h,~,~] = fdr_bh(PVALS.conditions.switchto.delta(time_i,row,col),SIGNIFICANCE,'pdep','no');
               if h == 1;
                   SIGTVALS.conditions.switchto.delta(time_i,row,col) = TVALS.conditions.switchto.delta(time_i,row,col);
               end
               clear h
               [h,~,~] = fdr_bh(PVALS.conditions.switchto.theta(time_i,row,col),SIGNIFICANCE,'pdep','no');
               if h == 1;
                   SIGTVALS.conditions.switchto.theta(time_i,row,col) = TVALS.conditions.switchto.theta(time_i,row,col);
               end
               clear h
               [h,~,~] = fdr_bh(PVALS.conditions.switchto.loweralpha(time_i,row,col),SIGNIFICANCE,'pdep','no');
               if h == 1;
                   SIGTVALS.conditions.switchto.loweralpha(time_i,row,col) = TVALS.conditions.switchto.loweralpha(time_i,row,col);
               end
               clear h
               [h,~,~] = fdr_bh(PVALS.conditions.switchto.upperalpha(time_i,row,col),SIGNIFICANCE,'pdep','no');
               if h == 1;
                   SIGTVALS.conditions.switchto.upperalpha(time_i,row,col) = TVALS.conditions.switchto.upperalpha(time_i,row,col);
               end
               clear h
               [h,~,~] = fdr_bh(PVALS.conditions.switchto.beta(time_i,row,col),SIGNIFICANCE,'pdep','no');
               if h == 1;
                   SIGTVALS.conditions.switchto.beta(time_i,row,col) = TVALS.conditions.switchto.beta(time_i,row,col);
               end
               clear h
            end
        end
    case 2
        for row = [3 4 5 6 9 10 11 12 13 14 17 18 19 20 21 22 23 25 26 ...
                   27 29 30 31 32 36 37 38 39 40 41 44 45 46 47 48 49 ...
                   50 51 54 55 56 57 58 59 60 62 63 64];
            for col = [3 4 5 6 9 10 11 12 13 14 17 18 19 20 21 22 23 25 26 ...
                   27 29 30 31 32 36 37 38 39 40 41 44 45 46 47 48 49 ...
                   50 51 54 55 56 57 58 59 60 62 63 64];
               [h,~,~] = fdr_bh(PVALS.conditions.switchaway.delta(time_i,row,col),SIGNIFICANCE,'pdep','no');
               if h == 1;
                   SIGTVALS.conditions.switchaway.delta(time_i,row,col) = TVALS.conditions.switchaway.delta(time_i,row,col);
               end
               clear h
               [h,~,~] = fdr_bh(PVALS.conditions.switchaway.theta(time_i,row,col),SIGNIFICANCE,'pdep','no');
               if h == 1;
                   SIGTVALS.conditions.switchaway.theta(time_i,row,col) = TVALS.conditions.switchaway.theta(time_i,row,col);
               end
               clear h
               [h,~,~] = fdr_bh(PVALS.conditions.switchaway.loweralpha(time_i,row,col),SIGNIFICANCE,'pdep','no');
               if h == 1;
                   SIGTVALS.conditions.switchaway.loweralpha(time_i,row,col) = TVALS.conditions.switchaway.loweralpha(time_i,row,col);
               end
               clear h
               [h,~,~] = fdr_bh(PVALS.conditions.switchaway.upperalpha(time_i,row,col),SIGNIFICANCE,'pdep','no');
               if h == 1;
                   SIGTVALS.conditions.switchaway.upperalpha(time_i,row,col) = TVALS.conditions.switchaway.upperalpha(time_i,row,col);
               end
               clear h
               [h,~,~] = fdr_bh(PVALS.conditions.switchaway.beta(time_i,row,col),SIGNIFICANCE,'pdep','no');
               if h == 1;
                   SIGTVALS.conditions.switchaway.beta(time_i,row,col) = TVALS.conditions.switchaway.beta(time_i,row,col);
               end
               clear h
            end
        end
    case 3
        for row = [3 4 5 6 9 10 11 12 13 14 17 18 19 20 21 22 23 25 26 ...
                   27 29 30 31 32 36 37 38 39 40 41 44 45 46 47 48 49 ...
                   50 51 54 55 56 57 58 59 60 62 63 64];
            for col = [3 4 5 6 9 10 11 12 13 14 17 18 19 20 21 22 23 25 26 ...
                   27 29 30 31 32 36 37 38 39 40 41 44 45 46 47 48 49 ...
                   50 51 54 55 56 57 58 59 60 62 63 64];
               [h,~,~] = fdr_bh(PVALS.conditions.mixrepeat.delta(time_i,row,col),SIGNIFICANCE,'pdep','no');
               if h == 1;
                   SIGTVALS.conditions.mixrepeat.delta(time_i,row,col) = TVALS.conditions.mixrepeat.delta(time_i,row,col);
               end
               clear h
               [h,~,~] = fdr_bh(PVALS.conditions.mixrepeat.theta(time_i,row,col),SIGNIFICANCE,'pdep','no');
               if h == 1;
                   SIGTVALS.conditions.mixrepeat.theta(time_i,row,col) = TVALS.conditions.mixrepeat.theta(time_i,row,col);
               end
               clear h
               [h,~,~] = fdr_bh(PVALS.conditions.mixrepeat.loweralpha(time_i,row,col),SIGNIFICANCE,'pdep','no');
               if h == 1;
                   SIGTVALS.conditions.mixrepeat.loweralpha(time_i,row,col) = TVALS.conditions.mixrepeat.loweralpha(time_i,row,col);
               end
               clear h
               [h,~,~] = fdr_bh(PVALS.conditions.mixrepeat.upperalpha(time_i,row,col),SIGNIFICANCE,'pdep','no');
               if h == 1;
                   SIGTVALS.conditions.mixrepeat.upperalpha(time_i,row,col) = TVALS.conditions.mixrepeat.upperalpha(time_i,row,col);
               end
               clear h
               [h,~,~] = fdr_bh(PVALS.conditions.mixrepeat.beta(time_i,row,col),SIGNIFICANCE,'pdep','no');
               if h == 1;
                   SIGTVALS.conditions.mixrepeat.beta(time_i,row,col) = TVALS.conditions.mixrepeat.beta(time_i,row,col);
               end
               clear h
            end
        end
end