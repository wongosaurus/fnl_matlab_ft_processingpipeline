function [AVDATA] = averageConnMats(ALLDATA,cond_i,time_i,AVDATA)
switch cond_i
    case 1
        for row = 1:size(ALLDATA.conditions.switchto.delta,3)
            for col = 1:size(ALLDATA.conditions.switchto.delta,4)
                AVDATA.conditions.switchto.delta(time_i,row,col)      = mean(ALLDATA.conditions.switchto.delta(:,time_i,row,col));
                AVDATA.conditions.switchto.theta(time_i,row,col)      = mean(ALLDATA.conditions.switchto.theta(:,time_i,row,col));
                AVDATA.conditions.switchto.loweralpha(time_i,row,col) = mean(ALLDATA.conditions.switchto.loweralpha(:,time_i,row,col));
                AVDATA.conditions.switchto.upperalpha(time_i,row,col) = mean(ALLDATA.conditions.switchto.upperalpha(:,time_i,row,col));
                AVDATA.conditions.switchto.beta(time_i,row,col)       = mean(ALLDATA.conditions.switchto.beta(:,time_i,row,col));
            end
        end
    case 2
        for row = 1:size(ALLDATA.conditions.switchto.delta,3)
            for col = 1:size(ALLDATA.conditions.switchto.delta,4)
                AVDATA.conditions.switchaway.delta(time_i,row,col)      = mean(ALLDATA.conditions.switchaway.delta(:,time_i,row,col));
                AVDATA.conditions.switchaway.theta(time_i,row,col)      = mean(ALLDATA.conditions.switchaway.theta(:,time_i,row,col));
                AVDATA.conditions.switchaway.loweralpha(time_i,row,col) = mean(ALLDATA.conditions.switchaway.loweralpha(:,time_i,row,col));
                AVDATA.conditions.switchaway.upperalpha(time_i,row,col) = mean(ALLDATA.conditions.switchaway.upperalpha(:,time_i,row,col));
                AVDATA.conditions.switchaway.beta(time_i,row,col)       = mean(ALLDATA.conditions.switchaway.beta(:,time_i,row,col));
            end
        end
    case 3
        for row = 1:size(ALLDATA.conditions.switchto.delta,3)
            for col = 1:size(ALLDATA.conditions.switchto.delta,4)
                AVDATA.conditions.mixrepeat.delta(time_i,row,col)      = mean(ALLDATA.conditions.mixrepeat.delta(:,time_i,row,col));
                AVDATA.conditions.mixrepeat.theta(time_i,row,col)      = mean(ALLDATA.conditions.mixrepeat.theta(:,time_i,row,col));
                AVDATA.conditions.mixrepeat.loweralpha(time_i,row,col) = mean(ALLDATA.conditions.mixrepeat.loweralpha(:,time_i,row,col));
                AVDATA.conditions.mixrepeat.upperalpha(time_i,row,col) = mean(ALLDATA.conditions.mixrepeat.upperalpha(:,time_i,row,col));
                AVDATA.conditions.mixrepeat.beta(time_i,row,col)       = mean(ALLDATA.conditions.mixrepeat.beta(:,time_i,row,col));
            end
        end
    case 4
        for row = 1:size(ALLDATA.conditions.switchto.delta,3)
            for col = 1:size(ALLDATA.conditions.switchto.delta,4)
                AVDATA.conditions.noninf.delta(time_i,row,col)      = mean(ALLDATA.conditions.noninf.delta(:,time_i,row,col));
                AVDATA.conditions.noninf.theta(time_i,row,col)      = mean(ALLDATA.conditions.noninf.theta(:,time_i,row,col));
                AVDATA.conditions.noninf.loweralpha(time_i,row,col) = mean(ALLDATA.conditions.noninf.loweralpha(:,time_i,row,col));
                AVDATA.conditions.noninf.upperalpha(time_i,row,col) = mean(ALLDATA.conditions.noninf.upperalpha(:,time_i,row,col));
                AVDATA.conditions.noninf.beta(time_i,row,col)       = mean(ALLDATA.conditions.noninf.beta(:,time_i,row,col));
            end
        end
end