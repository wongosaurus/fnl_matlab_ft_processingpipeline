clearvars; close all; clc;
%Data Structure ERP definition
times = (-1:0.001953125000000:3.5)*1000;
%Electrode Labels:
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
CHANNELS = 1:length(labels);
CONDITIONS  = {'switchto','switchaway','noninf','mixrepeat','allrepeat'};
% Find the names of participants:
% listings = dir('Z:\ITPC\PROCESSED\SurfaceLapacian\*mixrepeat_ISPC_MF.mat');
listings = dir('F:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\*mixrepeat_ITPC_bl_arcorrected.mat');

NAMES = {length(listings)};
for file_i =1:length(listings)
    NAMES{file_i} = listings(file_i).name(1:6);
end

SAVEDIR = 'F:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\';
LOADDIR = 'F:\FNL_EEG_TOOLBOX\ReferenceOutput\SurfaceLapacian\'; 

%%
%storing data in grand average

grand_average = zeros(length(CONDITIONS), length(labels), length(times));

for cond_i=1:length(CONDITIONS)
    fprintf('\n%s',CONDITIONS{cond_i})
    for name_i=1:length(NAMES)
        fprintf('\n%s',NAMES{name_i})
        tic;
        filename = [LOADDIR NAMES{name_i} CONDITIONS{cond_i} '.mat'];
        load (filename);
        temp_average = zeros(length(labels), length(times));
        for trial_i = 1:size(refdat.trial,2);
            temp_average = temp_average+refdat.trial{1,trial_i}(1:64,:);
        end
        
        temp_average = temp_average./size(refdat.trial,2);
        grand_average(cond_i, :, :) = squeeze (grand_average(cond_i, :, :))+temp_average;
        clear temp_average refdat
        t=toc;
        fprintf('\t%3.2f%s',t,'seconds')
    end
end

grand_average = grand_average./length(NAMES);

filename =[LOADDIR 'grand_average.mat'];
save(filename,'grand_average');

%%
filename =[LOADDIR 'grand_average.mat'];
load(filename)

%Baseline for cue
baseline_start = -300;
baseline_end = -100;
%sometimes rounding errors occur - try all three approaches below
baseline_start_ind = find(ceil(times)==baseline_start);
if isempty(baseline_start_ind)==1;
    baseline_start_ind = find(round(times)==baseline_start);
end
if isempty(baseline_start_ind)==1;
    baseline_start_ind = find(floor(times)==baseline_start);
end
baseline_end_ind = find(ceil(times)==baseline_end)
if isempty(baseline_end_ind)==1;
    baseline_end_ind = find(round(times)==baseline_end);
end
if isempty(baseline_end_ind)==1;
    baseline_end_ind = find(floor(times)==baseline_end);
end
baseline_time=baseline_start_ind:baseline_end_ind;%359:462;
bl_grand_average = zeros (size(grand_average));
for cond_i = 1:length(CONDITIONS)
    for chan_i = 1:length(CHANNELS)
    bl_grand_average (cond_i, chan_i, :)    = (bsxfun(@minus,(squeeze(grand_average(cond_i,chan_i,:))),(mean(squeeze(grand_average(cond_i,chan_i,baseline_time)),1))));
    end%chan_i loop
end
%%
%Baseline after target
baseline_start = 600;
baseline_end = 900;
%sometimes rounding errors occur - try all three approaches below
baseline_start_ind = find(ceil(times)==baseline_start);
if isempty(baseline_start_ind)==1;
    baseline_start_ind = find(round(times)==baseline_start);
end
if isempty(baseline_start_ind)==1;
    baseline_start_ind = find(floor(times)==baseline_start);
end
baseline_end_ind = find(ceil(times)==baseline_end)
if isempty(baseline_end_ind)==1;
    baseline_end_ind = find(round(times)==baseline_end);
end
if isempty(baseline_end_ind)==1;
    baseline_end_ind = find(floor(times)==baseline_end);
end
baseline_time=baseline_start_ind:baseline_end_ind;%359:462;
bl_grand_average = zeros (size(grand_average));
for cond_i = 1:length(CONDITIONS)
    for chan_i = 1:length(CHANNELS)
    bl_grand_average (cond_i, chan_i, :)    = (bsxfun(@minus,(squeeze(grand_average(cond_i,chan_i,:))),(mean(squeeze(grand_average(cond_i,chan_i,baseline_time)),1))));
    end%chan_i loop
end

%%
%Plot
line_colour={'-m','--m','--b','-g','-c'};
cluster = {'POz','Pz','PO3','PO4','P1','P2'};
cluster_ind = find(ismember(labels,cluster));
figure();
for cond_i=1:length(CONDITIONS);
    subplot (2,1,1)
    plot (times, squeeze (mean(bl_grand_average(cond_i, cluster_ind, :),2)),line_colour{cond_i});hold on
    set (gca,'YDir','reverse')
end

for cond_i=1:length(CONDITIONS);
    subplot (2,1,2)
    data = squeeze(mean(bl_grand_average(cond_i, cluster_ind, :),2))-squeeze(mean(bl_grand_average(5,cluster_ind,:),2));
    plot (times,data,line_colour{cond_i});hold on
    set (gca,'YDir','reverse')
end
saveas(gcf,'ERP_centroparietal.tif','tif')
close

cluster = {'Fz','FCz','FC1','FC2','F1','F2'};
cluster_ind = find(ismember(labels,cluster));
figure();
for cond_i=1:length(CONDITIONS);
    subplot (2,1,1)
    plot (times, squeeze (mean(bl_grand_average(cond_i, cluster_ind, :),2)),line_colour{cond_i});hold on
    set (gca,'YDir','reverse')
end

for cond_i=1:length(CONDITIONS);
    subplot (2,1,2)
    data = squeeze(mean(bl_grand_average(cond_i, cluster_ind, :),2))-squeeze(mean(bl_grand_average(5,cluster_ind,:),2));
    plot (times,data,line_colour{cond_i});hold on
    set (gca,'YDir','reverse')
end

saveas(gcf,'ERP_frontal.tif','tif')
close
%%
%Topoplot
addpath(genpath('F:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip-20150902\fieldtrip-20150902'));
addpath(genpath('F:\FNL_EEG_TOOLBOX\PACKAGES\tightsubplot'));
bl_grand_average2 = [bl_grand_average(4,:,:);bl_grand_average(1,:,:);...
    bl_grand_average(2,:,:);bl_grand_average(3,:,:);...
    bl_grand_average(5,:,:)];

times_start = -100:10:1600;
times_end = times_start+50;
figure(); set (gcf, 'Position',[0 0 1920 540], 'Color',[1 1 1]);
count=1;
figtitle = {'Mixed Repeat','Switch-to', 'Switch-away', 'Noninformative'};

for time_i = 1:length(times_start)

    temp_times = times-times_start(time_i);
    [~,basetimestart_index] = min(abs(temp_times));
    temp_times = times-times_end(time_i);
    [~,basetimeend_index] = min(abs(temp_times));
    times_of_interest = basetimestart_index:basetimeend_index;
    
    for cond_i = 1:length(CONDITIONS)-1
      
        fprintf('Condition: %s\n',CONDITIONS{cond_i})
        cfg = [];
        cfg.zlim        = [-10 10];
        cfg.colormap    = 'jet';
        cfg.parameter   = 'avg';
        cfg.layout      = 'biosemi64.lay';
        cfg.comment     = 'no';
        cfg.marker      = '';
        cfg.channel     = labels;
        %cfg.marker = 'labels';
        %cfg.contournum = 1;
        cfg.contournum  = 4;
        cfg.gridsize    = 600;
        data =[];
        data.avg = zeros(72,1);
        %eval(['currentfile = ' datastruct_cue{(contrasts_i)} ';']);
        %eval(['currentpval = ' datastruct_pval{(contrasts_i)} ';']);
        %eval(['currentpval_corrected = ' datastruct_pval_corrected{(contrasts_i)} ';']);
        % threshold = 0.05;
        % data4plot_ind = find(currentpval<threshold);
        % labels_ind = find(currentpval_corrected<threshold);
        %cfg.highlight = 'on';
        %cfg.highlightchannel = labels(labels_ind);
        %cfg.highlightcolor = [0 0 1]; %blue
        
        %cfg.highlightfontsize = 1; %no label writing
        %cfg.highlightsymbol = 'o';
        data_temp  = squeeze(mean(bl_grand_average2(cond_i,:,times_of_interest),3))-squeeze(mean(bl_grand_average2(5,:,times_of_interest),3));
      
        data.avg = [data_temp';0;0;0;0;0;0;0;0];
        %         for i = 1:length(data.avg)
        %             if (data.avg(i) >0)
        %                 data.avg(i) = 0;
        %             end
        %         end
        data.avg = (data.avg);
        clear pval_i
        data.var = zeros(72,1);
        data.time = 1;
        data.label = labels;
        data.dimord = 'chan_time';
        data.cov = zeros(72,72);
        
        ind = cond_i;
        
       %figure();
        set (gcf, 'Position',[0 0 1920 540], 'Color',[1 1 1]);
        subplot(5,5,[ind,ind+5,ind+10]); ft_topoplotER(cfg, data);
        %if contrasts_i ==1
            title (figtitle{cond_i},'FontSize',16);
    end
    ind = ind+1;
    set (gcf, 'Position',[0 0 1920 540], 'Color',[1 1 1]);
    subplot(5,5,[ind,ind+5,ind+10]); colorbar('east','FontSize',16); caxis ([-10 10]); axis square; axis off;  title ('Amplitude (\mu V^2)','FontSize',16,'HorizontalAlignment','left');
    %adding sliding timebar
    set (gcf, 'Position',[0 0 1920 540], 'Color',[1 1 1]);
    subplot(5,5,[ind+15:ind+20]); 
    plot(times,ones(1,length(times)).*-1,'k');
    ylim([-1 1]);
    set(gca,'xtick',-1000:500:3500);
    set(gca,'XTickLabels',-1000:500:3500);
    hold on;
    set(gca,'ytick',[]);
    set(gca,'ycolor',[1 1 1])
    fprintf('START: %3.5f, END: %3.5f\n',times(times_of_interest(1)),times(times_of_interest(end)));
    fill([times(times_of_interest(1)) times(times_of_interest(1)) times(times_of_interest(end)) times(times_of_interest(end))],[-1 0 0 -1],'k');
    box off;
    xlabel('Time (ms)');set(gca,'FontSize',16);
 
    
    pause(0.1);
    if count == 1
        colorbar;
    end
    f = getframe(gcf);
    if count == 1
        [im,map] = rgb2ind(f.cdata,256,'nodither');
        im(1,1,1,21) = 0;
    end
    if count ~=1
        im(:,:,1,count-1) = rgb2ind(f.cdata,map,'nodither');
    end
    count = count +1;
    close all;
end
filename = [LOADDIR 'ERP_movie.gif'];
imwrite(im,map,filename,'gif','DelayTime',0.3,'LoopCount',inf);
