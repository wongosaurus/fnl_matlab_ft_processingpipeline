function fnl_extractConditions_full(wpms,name_i)
mkdir([wpms.dirs.CWD wpms.dirs.REPAIRED_DATA]);
load([wpms.dirs.CWD wpms.dirs.preproc wpms.names{name_i} '_RepairedData.mat'])
fprintf('%s\t%s\n', 'Working on:', wpms.names{name_i});

%% AllRepeat
fprintf('\t\t\t%s\n', 'Searching for AllRepeat');
arv = [ 131 132 141 142 151 152 65411 65421 65431 65412 65422 65432];
indices = zeros(length(refdat.trial),1);
cnt = 0;
for k = 1:length(refdat.trialinfo(:,1));
    for j = 1:size(arv,2);
        if arv(1,j) == refdat.trialinfo(k,1);
            indices(k,1) = 1;
            cnt = cnt+1;
        end
    end
end
ardat = [];
artrinf = [];
arsmpinf =[];
tlen = length(refdat.time{1,1});
j = 1;
for k = 1:length(indices)
    if indices(k,1) == 1;
        x = refdat.trial{1,k}(1:72,1:tlen);
        ardat{1,j} = x;
        clear x
        x = refdat.trialinfo(k,1);
        artrinf(j,1) = x;
        clear x
        x = refdat.sampleinfo(k,1:2);
        arsmpinf(j,1:2) = x;
        clear x
        j = j +1;
    end
end
ardata = [];
ardata = refdat;
ardata.trial = ardat;
ardata.trialinfo = artrinf;
ardata.sampleinfo = arsmpinf;
y = length(ardata.trial);
ardata.time = refdat.time(1:y);
save([wpms.dirs.CWD wpms.dirs.REPAIRED_DATA wpms.names{name_i} '_RepairedData_allrepeat.mat'],'ardata');
clear y ardata ardat artrinf arsmpinf

%% Mixed Repeat:
fprintf('\t\t\t%s\n', 'Searching for MixRepeat');
mrv = [161 162 171 172 181 182 65441 65442 65451 65452 65461 65462];
indices = zeros(length(refdat.trial),1);
cnt = 0;
for k = 1:length(refdat.trialinfo(:,1));
    for j = 1:size(mrv,2);
        if mrv(1,j) == refdat.trialinfo(k,1);
            indices(k,1) = 1;
            cnt = cnt+1;
        end
    end
end
mrdat = [];
mrtrinf=[];
mrsmpinf=[];
tlen = length(refdat.time{1,1});
j = 1;
for k = 1:length(indices)
    if indices(k,1) == 1;
        x = refdat.trial{1,k}(1:72,1:tlen);
        mrdat{1,j} = x;
        clear x
        x = refdat.trialinfo(k,1);
        mrtrinf(j,1) = x;
        clear x
        x = refdat.sampleinfo(k,1:2);
        mrsmpinf(j,1:2) = x;
        clear x
        j = j +1;
    end
end
mrdata = refdat;
mrdata.trial = mrdat;
mrdata.trialinfo = mrtrinf;
mrdata.sampleinfo = mrsmpinf;
trial_length = length(mrdata.trial);
mrdata.time = refdat.time(1:trial_length);

save([wpms.dirs.CWD wpms.dirs.REPAIRED_DATA wpms.names{name_i} '_RepairedData_mixrepeat.mat'],'mrdata');
%% SwitchTo
fprintf('\t\t\t%s\n', 'Searching for SwitchTo');
stv = [163 164 173 174 183 184 65443 65444 65453 65454 65463 65464];
indices = zeros(length(refdat.trial),1);
cnt = 0;
for k = 1:length(refdat.trialinfo(:,1));
    for j = 1:size(stv,2);
        if stv(1,j) == refdat.trialinfo(k,1);
            indices(k,1) = 1;
            cnt = cnt+1;
        end
    end
end
stdat = [];
sttrinf =[];
stsmpinf=[];
tlen = length(refdat.time{1,1});
j = 1;
for k = 1:length(indices)
    if  indices(k,1) == 1;
        x = refdat.trial{1,k}(1:72,1:tlen);
        stdat{1,j} = x;
        clear x
        x = refdat.trialinfo(k,1);
        sttrinf(j,1) = x;
        clear x
        x = refdat.sampleinfo(k,1:2);
        stsmpinf(j,1:2) = x;
        clear x
        j = j +1;
    end
end

stdata = refdat;
stdata.trial = stdat;
stdata.trialinfo = sttrinf;
stdata.sampleinfo = stsmpinf;
trial_length = length(stdata.trial);
stdata.time = refdat.time(1:trial_length);
save([wpms.dirs.CWD wpms.dirs.REPAIRED_DATA wpms.names{name_i} '_RepairedData_switchto.mat'],'stdata');


%% SwitchAway
fprintf('\t\t\t%s\n', 'Searching for SwitchAway');
sav = [165 166 175 176 185 186 65445 65446 65455 65456 65465 65466];
indices = zeros(length(refdat.trial),1);
cnt = 0;
for k = 1:length(refdat.trialinfo(:,1));
    for j = 1:size(sav,2);
        if sav(1,j) == refdat.trialinfo(k,1);
            indices(k,1) = 1;
            cnt = cnt+1;
        end
    end
end
sadat = [];
satrinf=[];
sasmpinf=[];
tlen = length(refdat.time{1,1});
j = 1;
for k = 1:length(indices)
    if  indices(k,1) == 1;
        x = refdat.trial{1,k}(1:72,1:tlen);
        sadat{1,j} = x;
        clear x
        x = refdat.trialinfo(k,1);
        satrinf(j,1) = x;
        clear x
        x = refdat.sampleinfo(k,1:2);
        sasmpinf(j,1:2) = x;
        clear x
        j = j +1;
    end
end
sadata = [];
sadata = refdat;
sadata.trial = sadat;
sadata.trialinfo = satrinf;
sadata.sampleinfo = sasmpinf;
y = length(sadata.trial);
sadata.time = refdat.time(1:y);
save([wpms.dirs.CWD wpms.dirs.REPAIRED_DATA wpms.names{name_i} '_RepairedData_switchaway.mat'],'sadata');
clear y sadata sadat satrinf sasmpinf

%% NonInf
fprintf('\t\t\t%s\n', 'Searching for NonInformative');
niv = [167 168 169 170 177 178 179 180 187 188 189 190 65447 65448 65449 65450 65457 65458 65459 65460 65467 65468 65469 65470];
indices = zeros(length(refdat.trial),1);
cnt = 0;
for k = 1:length(refdat.trialinfo(:,1));
    for j = 1:size(niv,2);
        if niv(1,j) == refdat.trialinfo(k,1);
            indices(k,1) = 1;
            cnt = cnt+1;
        end
    end
end
nidat = [];
nitrinf =[];
nismpinf=[];
tlen = length(refdat.time{1,1});
j = 1;
for k = 1:length(indices)
    if  indices(k,1) == 1;
        x = refdat.trial{1,k}(1:72,1:tlen);
        nidat{1,j} = x;
        clear x
        x = refdat.trialinfo(k,1);
        nitrinf(j,1) = x;
        clear x
        x = refdat.sampleinfo(k,1:2);
        nismpinf(j,1:2) = x;
        clear x
        j = j +1;
    end
end
nidata = [];
nidata = refdat;
nidata.trial = nidat;
nidata.trialinfo = nitrinf;
nidata.sampleinfo = nismpinf;
y = length(nidata.trial);
nidata.time = refdat.time(1:y);
save([wpms.dirs.CWD wpms.dirs.REPAIRED_DATA wpms.names{name_i} '_RepairedData_noninf.mat'],'nidata');
clear y nidata nidat nitrinf nismpinf
%% NonInfRepeat
nrv = [167 168  177 178  187 188 65447 65448 65457 65458 65467 65468];   %[169 170 179 180 189 190]
indices = zeros(length(refdat.trial),1);
cnt = 0;
for k = 1:length(refdat.trialinfo(:,1));
    for j = 1:size(nrv,2);
        if nrv(1,j) == refdat.trialinfo(k,1);
            indices(k,1) = 1;
            cnt = cnt+1;
        end
    end
end
nrdat = [];
nrtrinf =[];
nrsmpinf=[];
tlen = length(refdat.time{1,1});
j = 1;
for k = 1:length(indices)
    if  indices(k,1) == 1;
        x = refdat.trial{1,k}(1:72,1:tlen);
        nrdat{1,j} = x;
        clear x
        x = refdat.trialinfo(k,1);
        nrtrinf(j,1) = x;
        clear x
        x = refdat.sampleinfo(k,1:2);
        nrsmpinf(j,1:2) = x;
        clear x
        j = j +1;
    end
end
nrdata = [];
nrdata = refdat;
nrdata.trial = nrdat;
nrdata.trialinfo = nrtrinf;
nrdata.sampleinfo = nrsmpinf;
y = length(nrdata.trial);
nrdata.time = refdat.time(1:y);
save([wpms.dirs.CWD wpms.dirs.REPAIRED_DATA wpms.names{name_i} '_RepairedData_noninfrepeat.mat'],'nrdata');
clear y nrdata nrdat nrtrinf nrsmpinf

%% NonInfSwitch
nsv = [169 170 179 180 189 190 65449 65450 65459 65460  65469 65470];
indices = zeros(length(refdat.trial),1);
cnt = 0;
for k = 1:length(refdat.trialinfo(:,1));
    for j = 1:size(nsv,2);
        if nsv(1,j) == refdat.trialinfo(k,1);
            indices(k,1) = 1;
            cnt = cnt+1;
        end
    end
end
nsdat = [];
nstrinf =[];
nssmpinf=[];
tlen = length(refdat.time{1,1});
j = 1;
for k = 1:length(indices)
    if  indices(k,1) == 1;
        x = refdat.trial{1,k}(1:72,1:tlen);
        nsdat{1,j} = x;
        clear x
        x = refdat.trialinfo(k,1);
        nstrinf(j,1) = x;
        clear x
        x = refdat.sampleinfo(k,1:2);
        nssmpinf(j,1:2) = x;
        clear x
        j = j +1;
    end
end
nsdata = [];
nsdata = refdat;
nsdata.trial = nsdat;
nsdata.trialinfo = nstrinf;
nsdata.sampleinfo = nssmpinf;
y = length(nsdata.trial);
nsdata.time = refdat.time(1:y);
save([wpms.dirs.CWD wpms.dirs.REPAIRED_DATA wpms.names{name_i} '_RepairedData_noninfswitch.mat'],'nsdata');
clear y nsdata nsdat nstrinf nssmpinf