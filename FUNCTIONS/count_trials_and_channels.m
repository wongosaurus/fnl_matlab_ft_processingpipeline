%% Get counts for bad channels and trials that go into analyses
% Set up working environment
clear all;close all;clc
CWD = 'G:\FNL_EEG_TOOLBOX\';
fftdatdir = [CWD 'WAVELET_OUTPUT_DIR'];
preprocdir = [CWD 'PREPROC_OUTPUT\'];
subjectidentifer = 'AGE';%project specific identifier to ensure names are curent study subjects
cd(fftdatdir)
listing = dir;
names ={};
for list_i = 1:length(listing);names{1,list_i}=listing(list_i,1).name;end
names2={};%temp
for name_i = 1:length(names)
    if true(length(names{1,name_i})>2);
        if strcmp(names{1,name_i}(1:3),subjectidentifer)==1;
            names2 = [names2 names{name_i}];
        end
    end
end
names = names2;
clear names2 list_i listing name_i
conditions = {'mixrepeat','switchto','switchaway','noninf'};
%% preallocate
badchannels{length(names),1} = [];
trial_counts = zeros(length(names),length(conditions));
header = {'Participant','nBadChann'};
for cond_i =1:length(conditions)
    header = [header strcat('N_',conditions{cond_i})];
end
%% main counting loop
for name_i = 1:length(names)
    fprintf('%s\t%s\n','Working on subject:',names{name_i});
    chandat= load([preprocdir names{name_i} '_badchannellist.mat']);
    chandatnam = fieldnames(chandat);
    badchannels{name_i,1} = length(chandat.(chandatnam{1}));
    clear dat
    for cond_i =1:length(conditions)
        fprintf('\t%s\t%s','Counting trials for condition:',conditions{cond_i});
        datafile = [preprocdir names{name_i} '_CSD_' conditions{cond_i}];
        try
            data = load(datafile);
        catch err
            altdatafile = [preprocdir names{name_i} '_' conditions{cond_i}];%not CSD labelled
            data = load(altdatafile);
        end
        datanam = fieldnames(data);
        trial_counts(name_i,cond_i) = length(data.(datanam{1}).trial);
        fprintf('\n');
    end
end
%% write data to text file
filename = [preprocdir 'EEGDataDetails.txt'];
fid = fopen(filename,'w');
fprintf(fid,'%s\t',header{:});
fprintf(fid,'\n');
for name_i = 1:length(names)
    for header_i = 1:length(header)
        switch header_i
            case 1
                fprintf(fid,'%s\t',names{name_i});
            case 2
                fprintf(fid,'%s\t',num2str(badchannels{name_i}));
            case 3
                fprintf(fid,'%s\t',num2str(trial_counts(name_i,header_i-2)));
            case 4
                fprintf(fid,'%s\t',num2str(trial_counts(name_i,header_i-2)));
            case 5
                fprintf(fid,'%s\t',num2str(trial_counts(name_i,header_i-2)));
            case 6
                fprintf(fid,'%s\n',num2str(trial_counts(name_i,header_i-2)));
        end
    end
end
fclose(fid);