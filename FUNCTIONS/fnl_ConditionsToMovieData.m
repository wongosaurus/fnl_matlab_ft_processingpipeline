%% Convert to Movie from Pipeline Condition Structure:

% Check DATA:
for cond_i = 1:length(wpms.conditions)
    listings= dir([wpms.dirs.CWD wpms.dirs.REF_OUT 'SurfaceLapacian' filesep '*' wpms.conditions{cond_i} '.mat']);
    fprintf('%20s: \t%i\n',wpms.conditions{cond_i},length(listings));
end
filenames = {listings(:).name};
names = cell(1,length(filenames));
for name_i = 1:length(filenames)
    names{name_i} = filenames{1,name_i}(1:6);
end
wpms.names_all = names;

channels = 1:64;
DATA = load([wpms.dirs.CWD wpms.dirs.REF_OUT 'SurfaceLapacian' filesep wpms.names_all{1} wpms.conditions{1} '.mat']);
fnames = fieldnames(DATA);
time = DATA.(fnames{1}).time{1,1};

erpdata = zeros(length(wpms.names_all),length(channels),length(wpms.conditions),length(time),'single');

for name_i = 1:length(wpms.names_all)
    hasNan = false;
    fprintf('Working on: %s',wpms.names_all{name_i});
    
    for cond_i = 1:length(wpms.conditions)
        DATA = load([wpms.dirs.CWD wpms.dirs.REF_OUT 'SurfaceLapacian' filesep wpms.names_all{name_i}  wpms.conditions{cond_i} '.mat']);
        fprintf('. ');
        fnames = fieldnames(DATA);
        trialData = zeros(length(DATA.(fnames{1}).trial),64,length(time));
        %Trial x Channel x Time
        for trial_i = 1:length(DATA.(fnames{1}).trial)
            trialData(trial_i,:,:) = DATA.(fnames{1}).trial{1,trial_i}(1:64,:);
        end
        A = squeeze(mean(trialData,1));
        B = A;
        B(A==0)=NaN;
        BNan = isnan(B);
        nan_index = find(sum(BNan,2)==length(time));
        if ~isempty(nan_index)
            for i = 1:length(nan_index)
                
                A(nan_index(i),:) = nan(1,length(time));
                hasNan = true;
            end
        end
        erpdata(name_i,:,cond_i,:) = single(A);
    end
    
    if hasNan==true
        fid = fopen([wpms.dirs.CWD wpms.dirs.REF_OUT 'SurfaceLapacian' filesep 'MovieChannelsWithNaN.txt'],'a');
        fprintf(fid,'%s: \t',wpms.names_all{name_i});
        for i = nan_index
            fprintf(fid,'%i, ',i);
        end
        fprintf(fid,'\n');
        fclose(fid);
    end
    fprintf('\n');
end



save([wpms.dirs.CWD wpms.dirs.REF_OUT 'SurfaceLapacian' filesep  'AGEILITY_T2_ERP_MovieData.mat'],'erpdata','-v7.3');


% Make Particicpant List:
fid = fopen([wpms.dirs.CWD wpms.dirs.REF_OUT 'SurfaceLapacian' filesep 'Participants.txt'],'w');
for name_i = 1:length(wpms.names_all)
    fprintf(fid,'%s\n',wpms.names_all{name_i});
end
fclose(fid);

% Make Channel List:
labels = DATA.(fnames{1}).label;
fid = fopen([wpms.dirs.CWD wpms.dirs.REF_OUT 'SurfaceLapacian' filesep 'ElectrodeNames.txt'],'w');
for chan_i = 1:length(channels)
    fprintf(fid,'%s\n',labels{chan_i});
end
fclose(fid);

% Make Condition List:
fid = fopen([wpms.dirs.CWD wpms.dirs.REF_OUT 'SurfaceLapacian' filesep 'ConditionNames.txt'],'w');
for cond_i = 1:length(wpms.conditions)
    fprintf(fid,'%s\n',wpms.conditions{cond_i});
end
fclose(fid);


clear DATA