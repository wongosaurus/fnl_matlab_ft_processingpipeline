clear data

data = zeros(2,length(conditions),64,80,2305);
for cond_i = 1:length(conditions)
    fprintf('.');
    power_filename = [wpms.dirs.CWD filesep conditions{cond_i} '_powerdata.mat'];
    itpc_filename = [wpms.dirs.CWD filesep conditions{cond_i} '_itpcdata.mat'];
    load(power_filename);
    data(1,cond_i,:,:,:)=powerdata;
    load(itpc_filename);
    data(2,cond_i,:,:,:)=itpcdata;
end
%data2=cell2mat(data);
%data2 = reshape(2,length(conditions),numel(data2)/(2*length(conditions)));
av_data = mean(data,2);
av_data = squeeze(av_data);

basetime_start = -0.25;
basetime_end = -0.05;
temp_times = times-basetime_start;
[~,basetimestart_index] = min(abs(temp_times));
temp_times = times-basetime_end;
[~,basetimeend_index] = min(abs(temp_times));

frequencies = logspace(log10(2),log10(30),80);
times = -1:0.001953125000000:3.5;
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','Afz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
figure();set(gcf,'Position',[0 0 1920 1080]);
for channel = 1:64;
    power_data_plot = zeros(80,2305);
    power_data_plot(:,:) = bsxfun(@minus,10*log10(mean(squeeze(av_data(1,channel,:,basetimestart_index:basetimeend_index)),2)),10*log10(squeeze(av_data(1,channel,:,:))));
    subplot(1,2,1,'replace');contourf(times,frequencies,(power_data_plot),50,'linecolor','none');caxis([-5 5]);colormap 'jet'
    title([labels(channel);'Average POWER for All Conditions']);axis square;
    
    itpc_data_plot = zeros(80,2305);
    itpc_data_plot(:,:) = bsxfun(@minus,(mean(squeeze(av_data(2,channel,:,basetimestart_index:basetimeend_index)),2)),(squeeze(av_data(2,channel,:,:))));
    subplot(1,2,2,'replace');contourf(times,frequencies,(itpc_data_plot),50,'linecolor','none');caxis([-0.2 0.2]);colormap 'jet'
    title([labels(channel);'Average ITPC for All Conditions']);axis square;
    pause(0.01);
end

%3.5-9Hz 0-400ms
cue_theta = zeros(length(labels),length(conditions));
cond_short = {'RA','RM','NI','NR','NS','SA','ST'};
figure();
for channel=1:length(labels)
    fprintf('\n%s\t',labels{channel});
    for cond_i = 1:length(conditions)
        fprintf('.');
        x=bsxfun(@minus,10*log10(mean(squeeze(data(1,cond_i,channel,:,basetimestart_index:basetimeend_index)),2)),10*log10(squeeze(data(1,cond_i,channel,:,:))));
        cue_theta(channel,cond_i) = mean(mean(x(18:45,528:718),1),2);
    end
    bar(squeeze(cue_theta(channel,:)));title(['THETA Power @: ' labels{channel}]);axis square;
    ylim([-6 6]);set(gca,'XTickLabel',cond_short);pause(0.3);
end

%%

CWD = 'F:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL';
conditions = {'allrepeat';'mixrepeat';'noninfrepeat';'noninfswitch';'switchaway';'switchto'};
cd(CWD);
listings = dir(['AGE*_',conditions{1},'_ITPC.mat']);
names = {listings(:).name};
for name_i = 1:length(names)
    names{name_i} = names{name_i}(1:6);
end

conditions = {'allrepeat';'mixrepeat';'noninfrepeat';'noninfswitch';'switchaway';'switchto'};
POWER_DATA = zeros(length(names),length(labels),80,2305,'single');
for name_i = 1:length(names)  
    fprintf('\nWorking on %s \t',names{name_i});
    for cond_i = 1:length(conditions) 
        fprintf('.');
        load([names{name_i} '_' conditions{cond_i} '_ITPC.mat'],'eegpower_all');
        %load eegpower_all ispc_all and itpc_all for name_i and Cond_i
        POWER_DATA(name_i,:,:,:) = squeeze(POWER_DATA(name_i,:,:,:))+eegpower_all;
    end
    
     POWER_DATA(name_i,:,:,:) = POWER_DATA(name_i,:,:,:)./length(conditions);
end
save('AverageCondition_Power_ALL.mat','POWER_DATA','-v7.3');

conditions = {'allrepeat';'mixrepeat';'noninfrepeat';'noninfswitch';'switchaway';'switchto'};
ITPC_DATA = zeros(length(names),length(labels),80,2305,'single');
for name_i = 1:length(names)  
    fprintf('\nWorking on %s \t',names{name_i});
    for cond_i = 1:length(conditions) 
        fprintf('.');
        load([names{name_i} '_' conditions{cond_i} '_ITPC.mat'],'itpc_all');
        %load eegpower_all ispc_all and itpc_all for name_i and Cond_i
        ITPC_DATA(name_i,:,:,:) = squeeze(ITPC_DATA(name_i,:,:,:))+itpc_all;
    end
    
     ITPC_DATA(name_i,:,:,:) = ITPC_DATA(name_i,:,:,:)./length(conditions);
end
save('AverageCondition_ITPC_ALL.mat','ITPC_DATA','-v7.3');

%% TTEST on Matrix:


frequencies = logspace(log10(2),log10(30),80);
times = -1:0.001953125000000:3.5;
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','Afz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
POWER_DATA2 = zeros(length(names),64,80,2305,'single');
%POWER_DATA = matfile('AverageCondition_Power_ALL.mat');
load('AverageCondition_Power_ALL.mat');
for window = 0.2:0.1:0.5;
    for basetime_end = 0:-0.05:-0.5
        chan_i = 47;
        basetime_start = basetime_end - window;
        %basetime_end = basetime_start + window;
        temp_times = times-basetime_start;
        [~,basetimestart_index] = min(abs(temp_times));
        temp_times = times-basetime_end;
        [~,basetimeend_index] = min(abs(temp_times));
        fprintf('Working on: %s %s window: %s:\n',num2str(basetime_start),num2str(basetime_end),num2str(window))
        
        % baseline:
        fprintf('Baseline: ');
        for name_i=1:length(names)
            fprintf('.');
            %POWER_DATA2(name_i,:,:,:)=bsxfun(@minus,10*log10(mean(squeeze(POWER_DATA(name_i,:,:,basetimestart_index:basetimeend_index)),3)),10*log10(squeeze(POWER_DATA(name_i,:,:,:))));
            POWER_DATA2(name_i,chan_i,:,:)=bsxfun(@minus,10*log10(mean(squeeze(POWER_DATA(name_i,chan_i,:,basetimestart_index:basetimeend_index)),2)),10*log10(squeeze(POWER_DATA(name_i,chan_i,:,:))));
        end
        fprintf('\n')
        p = zeros(64,80,2305);
        % ttest:
        fprintf('Ttest: ')
        %for chan_i = 1:64
            fprintf('.');
            [~,p(chan_i,:,:),~,~] = ttest(squeeze(POWER_DATA2(:,chan_i,:,:))); % output size: 64x80x2305
        %end
        fprintf('\n');
        POWER_DATA_sig= squeeze(mean(POWER_DATA2,1));
        % mask:
        POWER_DATA_sig(p>0.001) = 0;
        % plot:
        contourf(times,frequencies,squeeze(POWER_DATA_sig(chan_i,:,:)),50,'linecolor','none');caxis([-5 5]);colormap 'jet'
        title(['Baseline: Power: Time: ',num2str(basetime_start),' to ',num2str(basetime_end),' Window: ',num2str(window)]);
        saveas(gcf,['baseline_power_',num2str(basetime_start),num2str(basetime_end),'_window_',num2str(window),'.jpg'],'jpeg');
    end
end
%% FDR CORRECTION
addpath(genpath('F:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox'));
adj_p = zeros(64,80,2305);
for chan_i = 1:64
    fprintf('.');
    [~,~,adj_p(chan_i,:,:)]=fdr_bh(squeeze(p(chan_i,:,:)),0.001,'pdep');
end
fprintf('\n');
POWER_DATA_sig= squeeze(mean(POWER_DATA,1));
POWER_DATA_sig(adj_p>0.001) = 0;
contourf(times,frequencies,squeeze(POWER_DATA_sig(30,:,:)),50,'linecolor','none');caxis([-5 5]);colormap 'jet'


cutoff_p = 15:20;
figure();
for thresh=1:length(cutoff_p)
% POWER_DATA_sig= squeeze(mean(POWER_DATA,1));
% POWER_DATA_sig(p>10^-cutoff_p(thresh)) = 0;
%

%title(['threshold = ' num2str(10^-cutoff_p(thresh))]);
pause(0.5)
end

%%
Temp_Power_Data = squeeze(POWER_DATA(:,:,1,:));
Temp_Power_Data = permute(Temp_Power_Data,[2,3,1]);
max_dist = 6*56/(2*pi);

chan_hood=spatial_neighbors(ALLEEG.chanlocs,max_dist);
[pval, t_orig, clust_info, seed_state, est_alpha]=clust_perm1(Temp_Power_Data,chan_hood)


%% Run Afer Meeting:For All conditions: 
%  Take current condition, and subtract All Repeat
%  Apply baseline -300 to -100 ms before of difference power.
%   -300 to -100ms before target.
%  Appy ttest;
%  
%  Run cluster over all the 

frequencies = logspace(log10(2),log10(30),80);
times = -1:0.001953125000000:3.5;
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','Afz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';

CWD = 'F:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL';
conditions = {'allrepeat';'mixrepeat';'noninfrepeat';'noninfswitch';'switchaway';'switchto'};
cd(CWD);
listings = dir(['AGE*_',conditions{1},'_ITPC.mat']);
names = {listings(:).name};
for name_i = 1:length(names)
    names{name_i} = names{name_i}(1:6);
end

basetime_start = -0.3;
basetime_end = -0.1;

temp_times = times-basetime_start;
[~,basetimestart_index] = min(abs(temp_times));
temp_times = times-basetime_end;
[~,basetimeend_index] = min(abs(temp_times));


conditions = {'allrepeat';'mixrepeat';'noninfrepeat';'noninfswitch';'switchaway';'switchto'};
chan_i = 47;
for cond_i = 2:length(conditions) 
    fprintf('\nWorking on %s \t',conditions{cond_i});
    POWER_DATA = zeros(length(names),length(labels),80,2305,'single');
    for name_i = 1:length(names)  
        fprintf('\nWorking on %s \t',names{name_i});
        allrepeat_power =load([names{name_i} '_' conditions{1} '_ITPC.mat'],'eegpower_all');
        fprintf('.');
        condition_power = load([names{name_i} '_' conditions{cond_i} '_ITPC.mat'],'eegpower_all');
        %load eegpower_all ispc_all and itpc_all for name_i and Cond_i
        %POWER_DATA(name_i,:,:,:) = squeeze(POWER_DATA(name_i,:,:,:))+eegpower_all;
        eegpower_all = 10*log10(condition_power.eegpower_all) - 10*log10(allrepeat_power.eegpower_all);
        
        %baseline:
        POWER_DATA(name_i,:,:,:)=bsxfun(@minus,mean(squeeze(eegpower_all(:,:,basetimestart_index:basetimeend_index)),3),(squeeze(eegpower_all(:,:,:))));
    end
    save([conditions{cond_i},'vAllRepeat_ALL.mat'],'POWER_DATA','-v7.3');
    fprintf('\n')
    p = zeros(64,80,2305);
    % ttest:
    fprintf('Ttest: ')
    %for chan_i = 1:64
    fprintf('.');
    [~,p(chan_i,:,:),~,~] = ttest(squeeze(POWER_DATA(:,chan_i,:,:))); % output size: 64x80x2305
    %end
    fprintf('\n');
    POWER_DATA_sig= squeeze(mean(POWER_DATA,1));
    % mask:
    POWER_DATA_sig(p>0.001) = 0;
    % plot:
    contourf(times,frequencies,squeeze(POWER_DATA_sig(chan_i,:,:)),50,'linecolor','none');caxis([-5 5]);colormap 'jet'
    title([conditions{cond_i},'vAllRepeat_baseline_power Baseline: Power: Time: ',num2str(basetime_start),' to ',num2str(basetime_end)]);
    saveas(gcf,[conditions{cond_i},'vAllRepeat_baseline_power_',num2str(basetime_start),num2str(basetime_end),'.jpg'],'jpeg');
end

%% Testing Re-baseline:

basetime_start = 0.7;
basetime_end = 0.9;

temp_times = times-basetime_start;
[~,basetimestart_index] = min(abs(temp_times));
temp_times = times-basetime_end;
[~,basetimeend_index] = min(abs(temp_times));

for cond_i = 2:length(conditions) 
   
    load([conditions{cond_i},'vAllRepeat_ALL.mat']);
    %baseline:
    for name_i = 1:length(names)
        fprintf('\nWorking on %s \t',names{name_i});
        POWER_DATA(name_i,:,:,:)=bsxfun(@minus,mean(squeeze(POWER_DATA(name_i,:,:,basetimestart_index:basetimeend_index)),3),(squeeze(POWER_DATA(name_i,:,:,:))));
        fprintf('.');
    end
    fprintf('\n')
        fprintf('\n')
    p = zeros(64,80,2305);
    % ttest:
    fprintf('Ttest: ')
    %for chan_i = 1:64
    fprintf('.');
    [~,p(chan_i,:,:),~,~] = ttest(squeeze(POWER_DATA(:,chan_i,:,:))); % output size: 64x80x2305
    %end
    fprintf('\n');
    POWER_DATA_sig= squeeze(mean(POWER_DATA,1));
    % mask:
    POWER_DATA_sig(p>0.001) = 0;
    % plot:
    contourf(times,frequencies,squeeze(POWER_DATA_sig(chan_i,:,:)),50,'linecolor','none');caxis([-5 5]);colormap 'jet'
    title([conditions{cond_i},'vAllRepeat_baseline_power Baseline: Power: Time: ',num2str(basetime_start),' to ',num2str(basetime_end)]);
    saveas(gcf,[conditions{cond_i},'vAllRepeat_baseline_power_',num2str(basetime_start),num2str(basetime_end),'.jpg'],'jpeg');

end

        
    
