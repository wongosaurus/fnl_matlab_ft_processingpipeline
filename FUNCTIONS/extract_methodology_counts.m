%% 1. Determine study specific working parameters.
clc; clear all; close all;
disp('1. Determine study specific working parameters.');
% set up data directories
workingvolume = 'E:\'; %this would vary per operating system.
wpms          = [];       %working parameters
wpms.dirs     = struct('CWD',[workingvolume 'FNL_EEG_TOOLBOX' filesep]);
wpms.dirs     = struct('CWD',[workingvolume 'FNL_EEG_TOOLBOX' filesep], ...
    'PACKAGES',[wpms.dirs.CWD 'PACKAGES' filesep], ...
    'FUNCTIONS',[wpms.dirs.CWD 'FUNCTIONS' filesep], ...
    'REPAIRED_DATA',[wpms.dirs.CWD 'REPAIRED_DATA' filesep],...
    'LOGS',[wpms.dirs.CWD 'FUNCTIONS\Kylie_HONOURS\LOGS' filesep]);
mkdir(wpms.dirs.LOGS);    
addpath(genpath(wpms.dirs.FUNCTIONS));%functions
% set up study conditions

wpms.conditions = {'allrepeat','mixrepeat','switchto'}; %{'allrepeat','mixrepeat','noninfrepeat','switchto','switchaway','noninfswitch','noninf'};


wpms.names = {'AGE002','AGE003','AGE004','AGE005','AGE008','AGE009','AGE012','AGE013','AGE014','AGE015','AGE017','AGE018','AGE019','AGE020','AGE021','AGE022','AGE023','AGE024','AGE026','AGE027','AGE028','AGE030','AGE032','AGE033','AGE034','AGE035','AGE036','AGE038','AGE043','AGE046','AGE050','AGE051','AGE052','AGE053','AGE058','AGE059','AGE061','AGE062','AGE063','AGE064','AGE066','AGE067','AGE068','AGE069','AGE070','AGE072','AGE073','AGE075','AGE077','AGE079','AGE081','AGE083','AGE084','AGE085','AGE086','AGE088','AGE089','AGE090','AGE092','AGE093','AGE094','AGE095','AGE096','AGE097','AGE098','AGE100','AGE102','AGE103','AGE104','AGE106','AGE107','AGE108','AGE109','AGE111','AGE114','AGE115','AGE116','AGE117','AGE118','AGE119','AGE120','AGE121','AGE122','AGE123','AGE124','AGE127','AGE128','AGE129','AGE130','AGE131','AGE133','AGE135','AGE136','AGE138','AGE141','AGE145','AGE146','AGE147','AGE148','AGE149','AGE150','AGE151','AGE152','AGE153','AGE155','AGE156','AGE158','AGE159','AGE160','AGE161','AGE162','AGE163','AGE164','AGE165','AGE166','AGE167','AGE168','AGE169','AGE170','AGE172','AGE175','AGE176','AGE177','AGE178','AGE179','AGE180','AGE181','AGE182','AGE183','AGE184','AGE185','AGE186','AGE187','AGE189','AGE190','AGE192','AGE195','AGE197','AGE198','AGE199','AGE200','AGE201','AGE202','AGE203','AGE204','AGE205','AGE206','AGE207','AGE208','AGE209','AGE210','AGE211','AGE216','AGE217','AGE218','AGE219','AGE220','AGE221','AGE222','AGE224','AGE225','AGE226','AGE227','AGE228','AGE229','AGE230','AGE231','AGE232','AGE233','AGE234','AGE236','AGE237','AGE238','AGE239','AGE240','AGE241','AGE243','AGE244','AGE245','AGE246','AGE247','AGE248','AGE249','AGE251','AGE252','AGE253','AGE254','AGE255','AGE256','AGE257','AGE258','AGE259','AGE260','AGE261','AGE262','AGE264','AGE265','AGE266','AGE267','AGE268','AGE269','AGE270','AGE273','AGE275','AGE276','AGE277','AGE278','AGE279'};
clear names listing

%wpms.names = {'AGE002','AGE003','AGE004','AGE005','AGE007','AGE008','AGE009','AGE012','AGE013','AGE014','AGE015','AGE017','AGE018','AGE019','AGE020','AGE021','AGE022','AGE023','AGE024','AGE026','AGE027','AGE028','AGE030','AGE032','AGE033','AGE034','AGE035','AGE036','AGE038','AGE043','AGE046','AGE047','AGE050','AGE051','AGE052','AGE053','AGE058','AGE059','AGE061','AGE062','AGE063','AGE064','AGE066','AGE067','AGE068','AGE069','AGE070','AGE072','AGE073','AGE075','AGE077','AGE079','AGE081','AGE083','AGE084','AGE085','AGE086','AGE088','AGE089','AGE090','AGE092','AGE093','AGE094','AGE095','AGE096','AGE097','AGE098','AGE100','AGE102','AGE103','AGE104','AGE106','AGE107','AGE108','AGE109','AGE111','AGE114','AGE115','AGE116','AGE117','AGE118','AGE119','AGE120','AGE121','AGE122','AGE123','AGE124','AGE127','AGE128','AGE129','AGE130','AGE131','AGE133','AGE134','AGE135','AGE136','AGE138','AGE141','AGE145','AGE146','AGE147','AGE148','AGE149','AGE150','AGE151','AGE152','AGE153','AGE155','AGE156','AGE158','AGE159','AGE160','AGE161','AGE162','AGE163','AGE164','AGE165','AGE166','AGE167','AGE168','AGE169','AGE170','AGE172','AGE175','AGE176','AGE177','AGE178','AGE179','AGE180','AGE181','AGE182','AGE183','AGE184','AGE185','AGE186','AGE187','AGE189','AGE190','AGE192','AGE195','AGE197','AGE198','AGE199','AGE200','AGE201','AGE202','AGE203','AGE204','AGE205','AGE206','AGE207','AGE208','AGE209','AGE210','AGE211','AGE216','AGE217','AGE218','AGE219','AGE220','AGE221','AGE222','AGE224','AGE225','AGE226','AGE227','AGE228','AGE229','AGE230','AGE231','AGE232','AGE233','AGE234','AGE236','AGE237','AGE238','AGE239','AGE240','AGE241','AGE243','AGE244','AGE245','AGE246','AGE247','AGE248','AGE249','AGE251','AGE252','AGE253','AGE254','AGE255','AGE256','AGE257','AGE258','AGE259','AGE260','AGE261','AGE262','AGE264','AGE265','AGE266','AGE267','AGE268','AGE269','AGE270','AGE273','AGE275','AGE276','AGE277','AGE278','AGE279'};
% set up participant list


clear workingvolume listing temp_names list_i participant_index space_index file_i good_inds NAMES listings %tidy as we travel

%%
disp('2. Bad channel counting')
badchannel_count = zeros(1,length(wpms.names));
for name_i=1:length(wpms.names)
    fprintf('\n%s\t',wpms.names{name_i});
    filename = [wpms.dirs.REPAIRED_DATA wpms.names{name_i} '_RepairedData_allrepeat.mat'];
    load(filename);
    badchannel_count(1,name_i)=length(ardata.cfg.badchannel);
    fprintf('%i\t%s',length(ardata.cfg.badchannel),'bad channels');
end%name_i loop
savename = [wpms.dirs.LOGS 'badchannel_count'];
save(savename,'badchannel_count');
save(savename,'badchannel_count','-ascii','-tabs');
%Write to a CSV: AW_20171003
fid = fopen('badchannel_count.csv','w');
fprintf(fid,'Subject,NumberOfBadChannelsRejected\n');
for i = 1:length(wpms.names)
    fprintf(fid,'%s,%i\n',wpms.names{i},badchannel_count(i));
end
fclose(fid);

fprintf('\n');
%%
disp('3. Counting number of ICA components removed AND determining valid trial counts')
component_count = zeros(1,length(wpms.names));
valid_trials = zeros(1,length(wpms.names));
for name_i=1:length(wpms.names)
    tic;
    try
        fprintf('\n%s\t',wpms.names{name_i});
        filename = [wpms.dirs.REPAIRED_DATA wpms.names{name_i} '_RepairedData_allrepeat.mat'];
        load(filename);
        component_count(1,name_i)=length(ardata.cfg.previous.previous.previous.previous.component);
        fprintf('%i\t%s\t\t',length(ardata.cfg.previous.previous.previous.previous.component),'components channels');
        valid_trials(1,name_i)=length(ardata.cfg.previous.previous.previous.previous.previous.previous.trl);
        fprintf('%i\t%s',length(ardata.cfg.previous.previous.previous.previous.previous.previous.trl),'valid trials');
    catch exception
        disp(['failed for ' wpms.names{name_i}]);
    end%try/catch loop
    t=toc;
    estimated_time = time_remaining(name_i,length(wpms.names),t);
    fprintf('\t%3.2f %s',estimated_time,'s remaining')
end%name_i loop
%ica components
savename = [wpms.dirs.LOGS 'icacomponent_count'];
save(savename,'component_count');
save(savename,'component_count','-ascii','-tabs');
%Write to a CSV: AW_20171003
fid = fopen('component_count.csv','w'); %Number of ICAComponents Rejected: Rejection Criterium: Mostly Eye blink, or eye related artefact, such as saccades.
fprintf(fid,'Subject,NumberOfBadICAComponentsRejected\n');
for i = 1:length(wpms.names)
    fprintf(fid,'%s,%i\n',wpms.names{i},component_count(i));
end
fclose(fid);
%valid trials
savename = [wpms.dirs.LOGS 'validtrial_count'];
save(savename,'valid_trials');
save(savename,'valid_trials','-ascii','-tabs');
%Write to a CSV: AW_20171003
fid = fopen('valid_trials.csv','w');
fprintf(fid,'Subject,NumberOfValidTrials\n'); %Number of ValidTrials: Accepted in TRL Stage: i.e. 1) < +-3SD of Mean, 2) faster 200ms, 3) not error and not trial preceeding error trial.
for i = 1:length(wpms.names)
    fprintf(fid,'%s,%i\n',wpms.names{i},valid_trials(i));
end
fclose(fid);

%% 
disp('4. Determining each trial''s count and RTs');
trial_count = zeros(length(wpms.conditions),length(wpms.names));
rts_mean = zeros(length(wpms.conditions),length(wpms.names));
rolling_average = zeros(1,length(wpms.names));
for name_i=1:length(wpms.names)
    fprintf('\n%s\t',wpms.names{name_i});
    ts = zeros(1,length(wpms.conditions));
    for cond_i = 1:length(wpms.conditions)
        tic;
        try
            fprintf('.');
            filename = [wpms.dirs.REPAIRED_DATA wpms.names{name_i} '_RepairedData_' wpms.conditions{cond_i} '.mat'];
            if exist(filename,'file')==2;
                A=load(filename);
                datname = fieldnames(A);
                trial_count(cond_i,name_i) = length(A.(datname{1}).trial);
                %meanRTs:
                originalTRL = A.(datname{1}).cfg.previous.previous.previous.previous.previous.previous.trl;
                indices = find(ismember(originalTRL(:,1),A.(datname{1}).sampleinfo(:,1)));
                rts_mean(cond_i,name_i) = mean(originalTRL(indices,5));
%             elseif exist(filename2,'file')==2;
%                 A=load(filename2);
%                 datname = fieldnames(A);
%                 trial_count(cond_i,name_i) = length(A.(datname{1}).trial);
%             elseif exist(filename3,'file')==2;
%                 A=load(filename3);
%                 datname = fieldnames(A);
%                 trial_count(cond_i,name_i) = length(A.(datname{1}).trial);
            else
                trial_count(cond_i,name_i) = NaN;
            end
        catch exception
            disp(['failed for ' wpms.names{name_i}]);
        end
        ts(1,cond_i) = toc;
    end%try/catch loop
    t=mean(ts);
    estimated_time = time_remaining(name_i,length(wpms.names),t);
    rolling_average(1,name_i) = estimated_time;
    fprintf('\t%3.2f %s',mean(rolling_average(1,1:name_i)),'s remaining')
end%name_i loop
savename = [wpms.dirs.LOGS 'trial_count'];
save(savename,'trial_count');
save(savename,'trial_count','-ascii','-tabs');
%Write to a CSV: AW_20171003
fid = fopen('trial_count.csv','w');
fprintf(fid,'Subject,'); %Trial Count per each condition, after +-120uv Rejection.
for cond_i = 1:length(wpms.conditions)
    fprintf(fid,'%s,',wpms.conditions{cond_i});
end
for i = 1:length(wpms.names)
    fprintf(fid,'%s,%i\n',wpms.names{i},trial_count(i));
end
fprintf(fid,'\n');
fclose(fid);

savename = [wpms.dirs.LOGS 'rts_mean'];
save(savename,'rts_mean');
save(savename,'rts_mean','-ascii','-tabs');

%WRite to a CSV:
fid = fopen([savename ,'.csv'],'w');
%Header:
fprintf(fid,'SUBJECT,');
for cond_i = 1:length(wpms.conditions)
    fprintf(fid,'%s,',wpms.conditions{cond_i});
end
fprintf(fid,'\n');

%main data:
for name_i = 1:length(wpms.names)
    fprintf(fid,'%s,',wpms.names{name_i});
    for cond_i = 1:length(wpms.conditions)
        fprintf(fid,'%4.6f,',rts_mean(cond_i,name_i));
    end
    fprintf(fid,'\n');
end
fclose(fid);

bar(mean(rts_mean,2));
set(gca,'XTickLabel',wpms.conditions);

%% 
disp('5. finding bad cases');
bad_cases = find(isnan(trial_count(1,:)));
for bad_i = 1:length(bad_cases)
    fprintf('\n%s %s',wpms.names{bad_cases(bad_i)},'did not have a trial file found');
end
fprintf('\n\n');
% these cases are probably located on another directory
% thus, we will try and find those there.
wpms.dirs.altdir = ['E:' filesep 'FNL_EEG_TOOLBOX' filesep 'PREPROC_OUTPUT' filesep];
rolling_average = zeros(1,length(bad_cases));
for name_i=1:length(bad_cases)
    fprintf('\n%s\t',wpms.names{bad_cases(name_i)});
    ts = zeros(1,length(wpms.conditions));
    for cond_i = 1:length(wpms.conditions)
        tic;
        try
            fprintf('.');
            filename = [wpms.dirs.altdir wpms.names{bad_cases(name_i)} '_ERP_' wpms.conditions{cond_i} '.mat'];
            filename2 = [wpms.dirs.altdir wpms.names{bad_cases(name_i)} '_CSD_' wpms.conditions{cond_i} '.mat'];
            filename3 = [wpms.dirs.altdir wpms.names{bad_cases(name_i)} '_' wpms.conditions{cond_i} '.mat'];
            if exist(filename,'file')==2;
                A=load(filename);
                datname = fieldnames(A);
                trial_count(cond_i,bad_cases(name_i)) = length(A.(datname{1}).trial);
            elseif exist(filename2,'file')==2;
                A=load(filename2);
                datname = fieldnames(A);
                trial_count(cond_i,bad_cases(name_i)) = length(A.(datname{1}).trial);
            elseif exist(filename3,'file')==2;
                A=load(filename3);
                datname = fieldnames(A);
                trial_count(cond_i,bad_cases(name_i)) = length(A.(datname{1}).trial);
            else
                trial_count(cond_i,bad_cases(name_i)) = NaN;
            end
        catch exception
            disp(['failed for ' wpms.names{bad_cases(name_i)}]);
        end
        ts(1,cond_i) = toc;
    end%try/catch loop
    t=mean(ts);
    estimated_time = time_remaining(name_i,length(bad_cases),t);
    rolling_average(1,name_i) = estimated_time;
    fprintf('\t%3.2f %s',mean(rolling_average(1,1:name_i)),'s remaining')
end%name_i loop

disp('5b. determining if any remaining bad cases');
bad_cases = find(isnan(trial_count(1,:)));
if isempty(bad_cases)==1
    fprintf('No bad cases remaining')
else
    for bad_i = 1:length(bad_cases)
        fprintf('\n%s %s',wpms.names{bad_cases(bad_i)},'did not have a trial file found');
    end
end
fprintf('\n\n');
savename = [wpms.dirs.LOGS 'trial_count'];
save(savename,'trial_count');
save(savename,'trial_count','-ascii','-tabs');

%% Save Names used in this calculation:
%Convert to  Character format:
name_temp = {};
for name_i = 1:length(names)
    name_temp{name_i} = names{name_i}(1:6);
end
names_used_count = name_temp;
savename = [wpms.dirs.LOGS 'names_used'];
save(savename,'names_used_count');