%% Extract ERP waveforms and save off as ascii files for fusion ICA
% Documentation here.
% Patrick Cooper, 2015
% University of Newcastle, Australia
%% 1. Set up working parameters.

clear all;close all;clc;
warning off;
disp('1. Set up working parameters.');
fprintf('\t%s','Pointing pipeline to data directories.');
% set up data directories
wpms.dirs.CWD     = ['F:' filesep 'FNL_EEG_TOOLBOX' filesep];
wpms.dirs.dataList  = [wpms.dirs.CWD 'ReferenceOutput' filesep 'SurfaceLapacian' filesep 'ERPtimelock' filesep];
wpms.dirs.dataIN  = [wpms.dirs.CWD 'ReferenceOutput' filesep 'SurfaceLapacian' filesep 'ERPtimelock' filesep];
wpms.dirs.dataOUT = ['F:' filesep 'ICA FIT' filesep 'ERP_ASCII_CSD' filesep ];
wpms.dirs.FUNCTIONS = [wpms.dirs.CWD 'FUNCTIONS' filesep];
addpath(genpath(wpms.dirs.FUNCTIONS));
mkdir(wpms.dirs.dataOUT);
fprintf('\n\t%s\n','Extracting participant codes.');
%extract participant codes
wpms.conditions = {'mixrepeat','switchto'};
wpms.timelocking = {'Cue','Target'};
wpms.baselines = {[-0.2 0], [0.8 1]};
wpms.savetimes = {[0 1], [1 2]};
cd([wpms.dirs.dataList])
listing = dir('*switchto*.mat');%relies on allrepeat being a filename
wpms.names = {listing(1:(end-1)).name}';
%extract participant specific part of filename (i.e., first six characters)
for name_i = 1:length(wpms.names)
    wpms.names{name_i,1} = wpms.names{name_i,1}(1:6);
end
clear listing name_i%tidy as we go

%% Load filename:
Label_Of_Interest = 'POz';
for cond_i = 1:length(wpms.conditions)
    for name_i = 1:length(wpms.names)
        filename = [wpms.dirs.dataIN wpms.names{name_i} '_' wpms.conditions{cond_i} '_timelock.mat']; 
        load(filename); 
        %% Find Label of interest: 
        
        % Extract the Channel:
        label_index = -1;
        for label_i = 1:length(timelock.label)
            if strcmpi(Label_Of_Interest,timelock.label{label_i});
                label_index = label_i;
            end
        end
        
        for timelocking_i = 1:length(wpms.timelocking)
            %% Baseline and Extract Channel of Interest:
            data_temp = timelock.avg(label_index,:);
            %Find Corresponding time indices:
            time_start = wpms.baselines{timelocking_i}(1);
            time_end = wpms.baselines{timelocking_i}(2);
            temp_times = timelock.time-time_start;
            [~,base_timestart_index] = min(abs(temp_times));
            temp_times = timelock.time-time_end;
            [~,base_timeend_index] = min(abs(temp_times));
            data_temp = data_temp - mean(data_temp(base_timestart_index:base_timeend_index));
    
            %% Times for extraction:
            time_start = wpms.savetimes{timelocking_i}(1);
            time_end = wpms.savetimes{timelocking_i}(2);
            temp_times = timelock.time-time_start;
            [~,timestart_index] = min(abs(temp_times));
            temp_times = timelock.time-time_end;
            [~,timeend_index] = min(abs(temp_times));
            fid =fopen([wpms.dirs.dataOUT wpms.names{name_i} '_' wpms.conditions{cond_i} '_' Label_Of_Interest '_' wpms.timelocking{timelocking_i} '.asc'],'w');
            for time_i = timestart_index:timeend_index
               fprintf(fid, '%d\t%d\n', timelock.time(time_i), data_temp(time_i));
            end
            fclose(fid);
                
        end
    end
end