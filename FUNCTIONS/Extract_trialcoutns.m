clear all;close all;clc;
datadir = ['E:' filesep 'FNL_EEG_TOOLBOX' filesep 'PREPROC_OUTPUT' filesep];
altdatadir = ['F:' filesep 'FNL_EEG_TOOLBOX' filesep 'PREPROC_OUTPUT' filesep];
cd(datadir);
% listing = dir('*_CSD_mixrepeat.mat');
% names = {listing(1:125).name};
% for name_i = 1:length(names)
%     names{1,name_i} = names{1,name_i}(1:6);
% end
%
names = {'AGE007',	'AGE008',	'AGE012',	'AGE013',	'AGE014',	...
    'AGE017',	'AGE018',	'AGE019',	'AGE021',	'AGE022',	...
    'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	...
    'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE047',	...
    'AGE050',	'AGE051',	'AGE052',	'AGE053',	'AGE058',	...
    'AGE059',	'AGE061',	'AGE062',	'AGE067',	'AGE070',	...
    'AGE072',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	...
    'AGE084',	'AGE086',	'AGE088',	'AGE089',	'AGE092',	...
    'AGE093',	'AGE100',	'AGE104',	'AGE109',	'AGE114',	...
    'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	...
    'AGE121',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	...
    'AGE130',	'AGE131',	'AGE133',	'AGE134',	'AGE135',	...
    'AGE138',	'AGE145',	'AGE146',	'AGE149',	'AGE150',	...
    'AGE151',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	...
    'AGE161',	'AGE162',	'AGE164',	'AGE165',	'AGE166',	...
    'AGE167',	'AGE168',	'AGE169',	'AGE172',	'AGE175',	...
    'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	...
    'AGE183',	'AGE184',	'AGE186',	'AGE187',	'AGE190',	...
    'AGE195',	'AGE197',	'AGE198',	'AGE199',	'AGE201',	...
    'AGE203',	'AGE205',	'AGE207',	'AGE208',	'AGE210',	...
    'AGE216',	'AGE217',	'AGE218',	'AGE219',	'AGE222',	...
    'AGE224',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	...
    'AGE230',	'AGE233',	'AGE234',	'AGE236',	'AGE237',	...
    'AGE239',	'AGE240',	'AGE241',	'AGE243',	'AGE244',	...
    'AGE246',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	...
    'AGE253',	'AGE254',	'AGE255',	'AGE259',	'AGE260',	...
    'AGE266',	'AGE267',	'AGE268',	'AGE273',	'AGE276',	'AGE277'};
trialcounts = zeros(length(names),1);
for name_i = 1:length(names)
    fprintf('\n%s\t%s','Working on subject:',names{name_i});
    tic;
    %     filename = [datadir names{name_i} '_EOGCORRECTED.mat'];
    filename = [datadir names{name_i} '_ARTFREEDATA.mat'];
    fprintf('\n\t\t%s','Loading file . . .');
    if exist(filename,'file')==2;
        A=load(filename);
        fprintf('\t%s','Complete.');
        datnames = fieldnames(A);
        if isempty(datnames)~=1;
            fprintf('\t%s','Determining valid trial count . . .');
            %             trialcounts(name_i,1)= length(A.(datnames{1}).trial);
            trialcounts(name_i,1)= length(A.(datnames{1}).cfg.trlold);
            fprintf('\t%s','Complete.');
            clear A datnames filename
            t=toc;
            if name_i == 1;
                timeremaining = (t*length(names))-t;
            end
            fprintf('\n\t\t%s %3.3f %s','Time taken ',t,'s');
            previoustime = timeremaining;
            timeremaining = (previoustime)-(t*name_i);
            if timeremaining <0;
                %recalculate time based on updated values
                timeremaining = (t*length(names))-(t*name_i);
            end
            fprintf('\n\t\t%s %3.3f %s','Estimated time remaining ',timeremaining,'s');
        else
            t=toc;
            fprintf('\n\t\t%s %3.3f %s','Time taken ',t,'s');
            previoustime = timeremaining;
            timeremaining = (previoustime)-(t*name_i);
            if timeremaining <0;
                %recalculate time based on updated values
                timeremaining = (t*length(names))-(t*name_i);
            end
            fprintf('\n\t\t%s %3.3f %s','Estimated time remaining ',timeremaining,'s');
        end
    elseif exist([altdatadir names{name_i} '_ARTREFFREE.mat'],'file')==2;
        A=load(filename);
        fprintf('\t%s','Complete.');
        datnames = fieldnames(A);
        if isempty(datnames)~=1;
            fprintf('\t%s','Determining valid trial count . . .');
            %             trialcounts(name_i,1)= length(A.(datnames{1}).trial);
            trialcounts(name_i,1)= length(A.(datnames{1}).cfg.trlold);
            fprintf('\t%s','Complete.');
            clear A datnames filename
            t=toc;
            if name_i == 1;
                timeremaining = (t*length(names))-t;
            end
            fprintf('\n\t\t%s %3.3f %s','Time taken ',t,'s');
            previoustime = timeremaining;
            timeremaining = (previoustime)-(t*name_i);
            if timeremaining <0;
                %recalculate time based on updated values
                timeremaining = (t*length(names))-(t*name_i);
            end
            fprintf('\n\t\t%s %3.3f %s','Estimated time remaining ',timeremaining,'s');
        else
            t=toc;
            fprintf('\n\t\t%s %3.3f %s','Time taken ',t,'s');
            previoustime = timeremaining;
            timeremaining = (previoustime)-(t*name_i);
            if timeremaining <0;
                %recalculate time based on updated values
                timeremaining = (t*length(names))-(t*name_i);
            end
            fprintf('\n\t\t%s %3.3f %s','Estimated time remaining ',timeremaining,'s');
        end
    else
        t=toc;
        fprintf('\n\t\t%s %3.3f %s','Time taken ',t,'s');
        previoustime = timeremaining;
        timeremaining = (previoustime)-(t*name_i);
        if timeremaining <0;
            %recalculate time based on updated values
            timeremaining = (t*length(names))-(t*name_i);
        end
        fprintf('\n\t\t%s %3.3f %s','Estimated time remaining ',timeremaining,'s');
    end
end

conditions = {'allrepeat','mixrepeat','noninfrepeat','switchto','switchaway','noninfswitch'};
conditioncount = zeros(length(names),length(conditions));

for name_i = 1:length(names)
    tic;
    fprintf('\n%s\t%s\t','Working on subject:',names{name_i});
    for cond_i = 1:length(conditions)
        fprintf('.');
        filename = [datadir names{name_i} '_CSD_' conditions{cond_i} '.mat'];
        A=load(filename);
        datnames = fieldnames(A);
        conditioncount(name_i,cond_i)= length(A.(datnames{1}).trial);
        clear A datnames
    end
    t=toc;
    fprintf('\t%s %3.3f %s','Completed in ',t,'s');
end

badchanncount = zeros(length(names),1);

for name_i = 1:length(names)
    tic;
    fprintf('\n%s\t%s\t','Working on subject:',names{name_i});
    fprintf('.');
    filename = [datadir names{name_i} '_badchannellist.mat'];
    A=load(filename);
    datnames = fieldnames(A);
    badchanncount(name_i,1)= length(A.(datnames{1}));
    clear A datnames
    t=toc;
    fprintf('\t%s %3.3f %s','Completed in ',t,'s');
end