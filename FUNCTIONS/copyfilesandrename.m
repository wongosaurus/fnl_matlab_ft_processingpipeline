conditions = {'mixrepeat','switchto','noninfrepeat','noninfswitch'};
for name_i = 1:length(wpms.names)
    fprintf('\n%s\t%s\t','COPYING FILES FOR:',wpms.names{name_i});
    tic;
    for cond_i = 1:length(conditions)
        fprintf('.');
        src = ['E:\FNL_EEG_TOOLBOX\FORGRID\'  wpms.names{name_i} '_' conditions{cond_i} '.mat'];
        dest = ['Z:\singleTrialRegression\datain\'  wpms.names{name_i} conditions{cond_i} '.mat'];
        copyfile(src,dest);
    end
    t=toc;
    fprintf('\t%2.2f %s',t,'seconds');
end
