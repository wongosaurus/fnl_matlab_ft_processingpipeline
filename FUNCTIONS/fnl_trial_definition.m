function [trdat] = fnl_trial_definition(wpms,name_i,trialfunction,pre_trial,post_trial)

    fprintf('%s %s %s \n','----- Begin Trial Definition -----','Participant:',wpms.names{name_i});
    %cd([CWD,PREPROC_OUTPUT]);
    load([wpms.dirs.CWD wpms.dirs.preproc wpms.names{name_i} '_BadChannRemoved']);
    %cd([CWD,RAW]);
    cfg=[];
    
    listings = dir([wpms.dirs.CWD wpms.dirs.RAW]);
    filenames = {listings(3:end).name};
    count = 0;
    for name_j = 1:length(filenames)
        if strcmpi(wpms.names{name_i},filenames{name_j}(1:6));
            count = count+1;
            fprintf('Multiple Files Found for %s: %s\n',wpms.names{name_i},filenames{name_j});
        end
    end
    if count == 1
        cfg.hdr                   = ft_read_header([wpms.dirs.CWD wpms.dirs.RAW wpms.names{name_i} '_TSWT.bdf']);
        cfg.event                 = ft_read_event ([wpms.dirs.CWD wpms.dirs.RAW wpms.names{name_i} '_TSWT.bdf']);
        cfg.trialfun              = trialfunction;
        cfg.trialdef.pre          = pre_trial; % latency in seconds
        cfg.trialdef.post         = post_trial;   % latency in seconds
        cfg.filename_original     = [wpms.dirs.CWD wpms.dirs.preproc wpms.names{name_i} '_TSWT.bdf'];


    else
        segment_names = {'_A','_B','_C'};
        event = [];
        filenames = [];
        for count_i = 1:count
            filename = [wpms.dirs.CWD wpms.dirs.RAW wpms.names{name_i} '_TSWT',segment_names{count_i},'.bdf'];
            hdr(count_i)          = ft_read_header(filename);
            filenames = [filenames;{filenames}];
        end
        for count_i = 1:count
            event_temp = ft_read_event([wpms.dirs.CWD wpms.dirs.RAW wpms.names{name_i} '_TSWT',segment_names{count_i},'.bdf']);
            %for each event, append to the event_temp, and add the last
            %time into the event.
            for event_i = 1:length(event_temp)
                if strcmpi(event_temp(event_i).type,'STATUS')
                    if count_i > 1
                        event_temp(event_i).sample = event_temp(event_i).sample+hdr(count_i-1).nSamples;
                    end
                end
            end
            event = [event,event_temp(4:end)];
        end
        
        %cfg.event                 = ft_read_event ([wpms.dirs.CWD wpms.dirs.RAW wpms.names{name_i} '_TSWT.bdf']);
        cfg.event                 = event;
        cfg.hdr                   = hdr(1);
        cfg.trialfun              = trialfunction;
        cfg.trialdef.pre          = pre_trial; % latency in seconds
        cfg.trialdef.post         = post_trial;   % latency in seconds
        cfg.filename_original     = filenames;%[wpms.dirs.CWD wpms.dirs.preproc wpms.names{name_i} '_TSWT.bdf'];
    end
    
    eval(['[trl] = ft_trialfun_',cfg.trialfun,'(cfg,name_i);']); % for data recorded on new biosemi - else use normal function
    if isempty(trl)
        fprintf('%s\n','Using biosemi2 function')%debugging PC
        eval(['[trl] = ft_trialfun_',cfg.trialfun,'_biosemi2(cfg,name_i);'])
    end
    cfg.trl = trl;
    for trl_i = 1:length(cfg.trl)
    if cfg.hdr.Fs ~= data.fsample
        cfg.trl(trl_i,1) = round(cfg.trl(trl_i,1)/(cfg.hdr.Fs/data.fsample));
        cfg.trl(trl_i,2) = round(cfg.trl(trl_i,2)/(cfg.hdr.Fs/data.fsample));
        cfg.trl(trl_i,3) = round(cfg.trl(trl_i,3)/(cfg.hdr.Fs/data.fsample));
    end
    end
    trdat = ft_redefinetrial(cfg,data);
    trdat.trialinfo  = trdat.trialinfo(:,1);
    clear data tdat%tidying
    