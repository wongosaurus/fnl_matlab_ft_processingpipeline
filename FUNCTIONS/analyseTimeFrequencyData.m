%% 1. Set up working environment
clear all;close all;clc;
disp('1. Set up working environment');
h             = msgbox('Select directory where data is located',...
    'Directory Locator','help','modal');
uiwait(h);clear h;
wpms.dirs.CWD = uigetdir();
fprintf('\t\t%s\n','Detecting participants');
cd(wpms.dirs.CWD);
listing       = dir('*.mat');
%show user what particpant name is estimated as
fprintf('\t\t%s\n','Determining participant identifier');
estimatedname = listing(1).name(1:6);
prompt        = sprintf('%s\n\n%s','Program detected participant name as:',...
    'If incorrect, please modify to correct format');
dlg_title     = 'Participant name detection';
num_lines     = 1;
def           = {estimatedname};
fprintf('\t\t%s\n','Displaying predicted participant identifier');
estimatedname = inputdlg(prompt,dlg_title,num_lines,def);
clear prompt dlg_title num_lines def %tidy
%find where estimatedname occurs in file names for extraction
fprintf('\t\t%s\n','Finding all participant codes');
namelocations = ismember(listing(1).name,estimatedname{1});
names{length(listing),1} = [];
for name_i    = 1:length(listing)
    names{name_i,1} = listing(name_i).name(namelocations);
end
prompt        = 'Detected participant codes. Please select all valid participants';
names         = unique(names);
fprintf('\t\t%s\n','Verifying participant codes');
[namesinds,~] = listdlg('ListString',names,'Name','Participants',...
    'fus',20,'uh',35,'ffs',20,'ListSize',[500 500],...
    'PromptString',prompt);
names         = {names(namesinds)};
fprintf('\t\t%s\n','Completed participant code generation');
clear prompt namesinds namelocations estimatedname listing name_i %tidy

listing       = dir([names{1}{1} '*']);
fprintf('\t\t%s\n','Determining conditions');
estimatedconditions{length(listing),1}=[]; %preallocate
for cond_i = 1:length(estimatedconditions)
    underlocations               = find(ismember(listing(cond_i).name,'_'));
    estimatedconditions{cond_i,1}= listing(cond_i).name(underlocations(1)+1:underlocations(2)-1);
end
prompt{length(estimatedconditions),1} = [];
for cond_i = 1:length(estimatedconditions)
    if cond_i ==1;
        prompt{cond_i,1}        = sprintf('%s\n\n%s\n','Program detected conditions as:',...
            'If incorrect, please modify to correct format');
    else
        prompt{cond_i,1} = '';%just leave blank lines - save room
    end
end
dlg_title     = 'Conditions detection';
num_lines     = 1;
def           = estimatedconditions;
fprintf('\t\t%s\n','Displaying predicted conditions');
conditions = inputdlg(prompt,dlg_title,num_lines,def);
clear prompt def dlg_title num_lines underlocations estimatedconditions listing cond_i %tidy
%% 2. Prepare the data structures
disp('2. Prepare the data structures');
h           = msgbox('Select data file',...
    'Data File Locator','help','modal');
uiwait(h);clear h;
fprintf('\t\t%s\n','Load example data for preallocation');
exampledata = uigetfile('*.mat');
data        = load([wpms.dirs.CWD filesep exampledata]);
datnames    = fieldnames(data);
%determine what parameters to extract (i.e., power, ITPC and ISPC)
prompt        = 'What measures are desired: power, inter-trial phase coherence, inter-site phase clustering?';
measures         = {'POWER','ITPC','ISPC'};
fprintf('\t\t%s\n','Verifying desired measures');
[namesinds,~] = listdlg('ListString',measures,'Name','Participants',...
    'fus',20,'uh',35,'ffs',20,'ListSize',[500 500],...
    'PromptString',prompt);
measures         = {measures(namesinds)};
fprintf('\t\t%s\t%s\t%s\t%s\n','Selected:',measures{1}{1,:});
clear prompt namesinds namelocations %tidy
fprintf('\t\t%s\n','Preallocating data structures for desired measures');
if find(ismember(measures{1},'POWER'))~=0;
    fprintf('\t\t%s\n','POWER was requested');
    prompt        = 'What structure corresponds to power?';
    [namesinds,~] = listdlg('ListString',datnames,'Name','Participants',...
        'fus',20,'uh',35,'ffs',20,'ListSize',[500 500],...
        'PromptString',prompt);
    powername = datnames{namesinds};
    powerdata = zeros(size(data.(datnames{namesinds})));
    clear prompt namesinds
end
if find(ismember(measures{1},'ITPC'))~=0;
    fprintf('\t\t%s\n','ITPC was requested');
    prompt        = 'What structure corresponds to ITPC?';
    [namesinds,~] = listdlg('ListString',datnames,'Name','Participants',...
        'fus',20,'uh',35,'ffs',20,'ListSize',[500 500],...
        'PromptString',prompt);
    itpcname = datnames{namesinds};
    itpcdata = zeros(size(data.(datnames{namesinds})));
    clear prompt namesinds
end
if find(ismember(measures{1},'ISPC'))~=0;
    fprintf('\t\t%s\n','ISPC was requested');
    prompt        = 'What structure corresponds to ISPC?';
    [namesinds,~] = listdlg('ListString',datnames,'Name','Participants',...
        'fus',20,'uh',35,'ffs',20,'ListSize',[500 500],...
        'PromptString',prompt);
    ispcname = datnames{namesinds};
    ispcdata = zeros(size(data.(datnames{namesinds})));
    clear prompt namesinds
end
clear data datnames exampledata measures %tidy
names = names{1};%fix cell array issue from above now that we are done with set up
%% 3. Load in all data to create averages
disp('3. Load in all data to create averages');
waitmsg = 'Preparing to load data';
h = waitbar(0,waitmsg);
for cond_i = 1:length(conditions)
    waitmsg = ['Working on condition: ',conditions{cond_i}];
    waitbar(0,h,waitmsg);
    fprintf('\t\t%s\t%s\n','Working on condition: ',conditions{cond_i});
    tic;
    for name_i = 1:length(names) %AGE220 doesn't work -name_i=160
        waitmsg = sprintf('%s %s %s\n %s','Working on participant: ',names{name_i},' ',conditions{cond_i});
        waitbar(name_i/length(names),h,waitmsg);
        filename = [wpms.dirs.CWD filesep names{name_i} '_' conditions{cond_i} '_ITPC.mat'];
        fprintf('\t\t\t%s\t%s\n','Loading data: ',filename);
        data=load(filename);
        if exist('powerdata','var') ==1;%power
            if ~any(any(any(data.(powername))))
                beep;
                disp([name_i,': ',names{name_i}, ' ',conditions{cond_i}] );
                pause;
            end
            powerdata(:,:,:) = powerdata(:,:,:)+data.(powername);
        end
        if exist('itpcdata','var') ==1;%itpc
            itpcdata(:,:,:) = itpcdata(:,:,:)+data.(itpcname);
        end
        if exist('ispcdata','var') ==1;%ispc
            ispcdata(:,:,:) = ispcdata(:,:,:)+data.(ispcname);
        end
    end%name_i loop
    fprintf('\t\t%s\n','Computing means and saving off data');
    if exist('powerdata','var') ==1;%power
        powerdata_t = powerdata./length(names);
        powerdata = powerdata_t;
        save([wpms.dirs.CWD filesep conditions{cond_i} '_powerdata.mat'],'powerdata','-v7.3');
        powerdata = zeros(size(powerdata));
    end
    if exist('itpcdata','var') ==1;%itpc
        itpcdata = itpcdata./length(names);
        save([wpms.dirs.CWD filesep conditions{cond_i} '_itpcdata.mat'],'itpcdata','-v7.3');
        itpcdata = zeros(size(itpcdata));
    end
    if exist('ispcdata','var') ==1;%ispc
        ispcdata=ispcdata./length(names);
        save([wpms.dirs.CWD filesep conditions{cond_i} '_ispcdata.mat'],'ispcdata','-v7.3');
        ispcdata = zeros(size(ispcdata));
    end
    t=toc;
    fprintf('\t\t%3.3f\t%s\n',t, 's to complete');
end%cond_i loop
delete(h);
%% 4. Visualise data
disp('4. Visualise data');
frequencies = logspace(log10(2),log10(30),80);
times = -1:0.001953125000000:3.5;
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','Afz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
% for cond_i = 1:length(conditions)
%     load([wpms.dirs.CWD filesep conditions{cond_i} '_itpcdata.mat']);
%     lines= 500;
%     figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);
%     channel = [find(strcmpi('AFz',labels));find(strcmpi('Fz',labels));find(strcmpi('FCz',labels))];
%     contourf(times(1:1537)*1000,frequencies,squeeze(mean(itpcdata(channel,:,1:1537),1)),lines,'linecolor','none');caxis([0 0.25]);
%     ylabel('Frequency (Hz)','FontSize',18);xlabel('Time (ms)','FontSize',18);
%     axis square;colorbar;    set(gca,'FontSize',18);
%     saveas(gcf,[conditions{cond_i} '_ITPC.fig'],'fig');close;
% end
    
addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\eeglab12_0_1_0b\'));
datavector = zeros(72,1);
for cond_i = 1:length(conditions)
    figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);
    fprintf('\t\t%s\n','Checking if power data exists');
    if exist([wpms.dirs.CWD filesep conditions{cond_i} '_powerdata.mat'],'file')~=0;
        fprintf('\t\t%s\n','Power data exists');
        load([wpms.dirs.CWD filesep conditions{cond_i} '_powerdata.mat']);
    end
    fprintf('\t\t%s\n','Checking if ITPC data exists');
    if exist([wpms.dirs.CWD filesep conditions{cond_i} '_itpcdata.mat'],'file')~=0;
        fprintf('\t\t%s\n','ITPC data exists');
        load([wpms.dirs.CWD filesep conditions{cond_i} '_itpcdata.mat']);
    end
    fprintf('\t\t%s\n','Checking if ISPC data exists');
    if exist([wpms.dirs.CWD filesep conditions{cond_i} '_ispcdata.mat'],'file')~=0;
        fprintf('\t\t%s\n','ISPC data exists');
        load([wpms.dirs.CWD filesep conditions{cond_i} '_ispcdata.mat']);
    end
    for channel = [38,30]%:length(labels)
        time_start = 1.1;
        time_end = 1.3;
        temp_times = times-time_start;
        [~,timestart_index] = min(abs(temp_times));
        temp_times = times-time_end;
        [~,timeend_index] = min(abs(temp_times));
        
        freq_start = 4;
        freq_end = 7;
        temp_freq = frequencies-freq_start;
        [~,freqstart_index] = min(abs(temp_freq));
        temp_freq = frequencies-freq_end;
        [~,freqend_index] = min(abs(temp_freq));

        if exist('powerdata','var')~=0;
            subplot(2,4,1);contourf(times,frequencies,squeeze(10*log10(powerdata(channel,:,:))),20,'linecolor','none');caxis([-20 20]);
            title([conditions{cond_i} ' ' labels{channel}]);axis square
        end
        if exist('itpcdata','var')~=0;
            subplot(2,4,2);contourf(times,frequencies,squeeze(itpcdata(channel,:,:)),20,'linecolor','none');caxis([0 0.3]);
            title([conditions{cond_i} ' ' labels{channel}]);axis square
        end
        if exist('ispcdata','var')~=0;
            subplot(2,4,3);contourf(times,frequencies,squeeze(ispcdata(channel,:,:)),20,'linecolor','none');caxis([0 0.3]);
            title([conditions{cond_i} ' ' labels{channel}]);axis square
            subplot(2,4,4,'replace');
            topoplot(datavector, 'E:\FNL_EEG_TOOLBOX\FUNCTIONS\fnl_BioSemi72.loc',...
                'electrodes','on','plotchans',[47 channel],'emarker',{'s','r',72,1},...
                'whitebk','on','style','blank');hold off;
        end
        if exist('powerdata','var')~=0;
            subplot(2,4,5);
            datavector= mean(mean(squeeze(10*log10(powerdata(:,freqstart_index:freqend_index,timestart_index:timeend_index))),3),2);
            datavector = [datavector;0;0;0;0;0;0;0;0];
            topoplot(datavector, 'E:\FNL_EEG_TOOLBOX\FUNCTIONS\fnl_BioSemi72.loc',...
                'electrodes','on','maplimits',[-50 50]);hold off;
            title([conditions{cond_i} ' ' labels{channel}]);axis square
        end
        if exist('itpcdata','var')~=0;
            subplot(2,4,6);
            datavector= mean(mean(squeeze((itpcdata(:,freqstart_index:freqend_index,timestart_index:timeend_index))),3),2);
            datavector = [datavector;0;0;0;0;0;0;0;0];
            topoplot(datavector, 'E:\FNL_EEG_TOOLBOX\FUNCTIONS\fnl_BioSemi72.loc',...
                'electrodes','on','maplimits',[0 0.3]);hold off;
            title([conditions{cond_i} ' ' labels{channel}]);axis square
        end
        
        pause(0.2);
    end%channel loop
end%cond_i loop
%% checking allrepeat
disp('4. Visualise data');
frequencies = logspace(log10(2),log10(30),80);
times = -1:0.001953125000000:3.5;
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','Afz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
% for cond_i = 1:length(conditions)
%     load([wpms.dirs.CWD filesep conditions{cond_i} '_itpcdata.mat']);
%     lines= 500;
%     figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);
%     channel = [find(strcmpi('AFz',labels));find(strcmpi('Fz',labels));find(strcmpi('FCz',labels))];
%     contourf(times(1:1537)*1000,frequencies,squeeze(mean(itpcdata(channel,:,1:1537),1)),lines,'linecolor','none');caxis([0 0.25]);
%     ylabel('Frequency (Hz)','FontSize',18);xlabel('Time (ms)','FontSize',18);
%     axis square;colorbar;    set(gca,'FontSize',18);
%     saveas(gcf,[conditions{cond_i} '_ITPC.fig'],'fig');close;
% end
    
addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\eeglab12_0_1_0b\'));
datavector = zeros(72,1);
for cond_i = 1:length(conditions)
    figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);
    fprintf('\t\t%s\n','Checking if power data exists');
    if exist([wpms.dirs.CWD filesep conditions{cond_i} '_powerdata.mat'],'file')~=0;
        fprintf('\t\t%s\n','Power data exists');
        load([wpms.dirs.CWD filesep conditions{cond_i} '_powerdata.mat']);
    end
    fprintf('\t\t%s\n','Checking if ITPC data exists');
    if exist([wpms.dirs.CWD filesep conditions{cond_i} '_itpcdata.mat'],'file')~=0;
        fprintf('\t\t%s\n','ITPC data exists');
        load([wpms.dirs.CWD filesep conditions{cond_i} '_itpcdata.mat']);
    end
    fprintf('\t\t%s\n','Checking if ISPC data exists');
    if exist([wpms.dirs.CWD filesep conditions{cond_i} '_ispcdata.mat'],'file')~=0;
        fprintf('\t\t%s\n','ISPC data exists');
        load([wpms.dirs.CWD filesep conditions{cond_i} '_ispcdata.mat']);
    end
    for channel = 57:62;%57:62;%20:26%1:length(labels)
        if exist('powerdata','var')~=0;
            dat=squeeze(power.(conditions{cond_i}).powerdata(channel,:,:)-power.(conditions{1}).powerdata(channel,:,:));
            subplot(1,4,1);contourf(times,frequencies,squeeze(10*log10(dat)),20,'linecolor','none');caxis([-50 50]);
            title([conditions{cond_i} ' ' labels{channel}]);axis square
        end
        if exist('itpcdata','var')~=0;
            dat=squeeze(itpc.(conditions{cond_i}).itpcdata(channel,:,:)-itpc.(conditions{1}).itpcdata(channel,:,:));
            subplot(1,4,2);contourf(times,frequencies,squeeze(dat),20,'linecolor','none');caxis([-0.1 0.1]);
            title([conditions{cond_i} ' ' labels{channel}]);axis square
        end
        if exist('ispcdata','var')~=0;
            dat=squeeze(ispc.(conditions{cond_i}).ispcdata(channel,:,:)-ispc.(conditions{1}).ispcdata(channel,:,:));
            subplot(1,4,3);contourf(times,frequencies,squeeze(dat),20,'linecolor','none');caxis([-0.1 0.1]);
            title([conditions{cond_i} ' ' labels{channel}]);axis square
            subplot(1,4,4,'replace');
            topoplot(datavector, 'E:\FNL_EEG_TOOLBOX\FUNCTIONS\fnl_BioSemi72.loc',...
                'electrodes','on','plotchans',[47 channel],'emarker',{'s','r',72,1},...
                'whitebk','on','style','blank');hold off;
        end
        pause(0.2);
    end%channel loop
end%cond_i loop
%% 4b. Visualise data as headplot movies
datavector = zeros(72,1);
for cond_i = 1:length(conditions)
    fprintf('\t\t%s\n','Checking if power data exists');
    if exist([wpms.dirs.CWD filesep conditions{cond_i} '_powerdata.mat'],'file')~=0;
        fprintf('\t\t%s\n','Power data exists');
        power.(conditions{cond_i}) = load([wpms.dirs.CWD filesep conditions{cond_i} '_powerdata.mat']);
    end
    fprintf('\t\t%s\n','Checking if ITPC data exists');
    if exist([wpms.dirs.CWD filesep conditions{cond_i} '_itpcdata.mat'],'file')~=0;
        fprintf('\t\t%s\n','ITPC data exists');
        itpc.(conditions{cond_i}) = load([wpms.dirs.CWD filesep conditions{cond_i} '_itpcdata.mat']);
    end
    fprintf('\t\t%s\n','Checking if ISPC data exists');
    if exist([wpms.dirs.CWD filesep conditions{cond_i} '_ispcdata.mat'],'file')~=0;
        fprintf('\t\t%s\n','ISPC data exists');
        ispc.(conditions{cond_i}) = load([wpms.dirs.CWD filesep conditions{cond_i} '_ispcdata.mat']);
    end
end%cond_i loop
freq_of_interest = 1:50;
figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);
for time_i = 1:20:length(times)
    for cond_i=1:length(conditions)
        if exist('power','var')~=0;
            datavector = squeeze(10*log10(mean(mean(power.(conditions{cond_i}).powerdata(:,freq_of_interest,time_i:time_i+20),3),2)));
            datavector = [datavector;0;0;0;0;0;0;0;0];
            subplot(1,length(conditions),cond_i,'replace');
            topoplot(datavector, 'E:\FNL_EEG_TOOLBOX\FUNCTIONS\fnl_BioSemi72.loc',...
                'electrodes','on','whitebk','on','numcontour',2,'maplimits',[-100 100],...
                'verbose','off','shading','interp');
            title([conditions{cond_i} ' ' num2str(times(time_i)*1000+100) ' ms']);axis square
        end
    end
    pause(0.1);
end
freq_of_interest = 1:50;
figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);
for time_i = 461:20:length(times)
    for cond_i=1:length(conditions)
        if exist('itpc','var')~=0;
            datavector = squeeze(mean(mean(itpc.(conditions{cond_i}).itpcdata(:,freq_of_interest,time_i:time_i+20),3),2));
            datavector = [datavector;0;0;0;0;0;0;0;0];
            subplot(1,length(conditions),cond_i,'replace');
            topoplot(datavector, 'E:\FNL_EEG_TOOLBOX\FUNCTIONS\fnl_BioSemi72.loc',...
                'electrodes','on','whitebk','on','numcontour',2,'maplimits',[0 .5],...
                'verbose','off','shading','interp');
            title([conditions{cond_i} ' ' num2str(times(time_i)*1000+100) ' ms']);axis square
        end
    end
    pause(0.1);
end
%ispc
freq_of_interest = 1:50;
figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);
for time_i = 461:20:length(times)
    for cond_i=1:length(conditions)
        if exist('itpc','var')~=0;
            datavector = squeeze(mean(mean(ispc.(conditions{cond_i}).ispcdata(:,freq_of_interest,time_i:time_i+20),3),2));
            datavector = [datavector;0;0;0;0;0;0;0;0];
            subplot(1,length(conditions),cond_i,'replace');
            topoplot(datavector, 'E:\FNL_EEG_TOOLBOX\FUNCTIONS\fnl_BioSemi72.loc',...
                'electrodes','on','whitebk','on','numcontour',2,'maplimits',[0 .5],...
                'verbose','off','shading','interp');
            title([conditions{cond_i} ' ' num2str(times(time_i)*1000+100) ' ms']);axis square
        end
    end
    pause(0.1);
end

%% control for allrepeat
%power
delta_f = 1:12;
theta_f = 13:41;
alpha_f = 42:59;
beta_f  = 60:80;
freq_1 = 28:38;%33 is centre
freq_2 = 45:51;%48 is centre
freq_of_interest = {[freq_1];[freq_2]};%{[13:37];[42:59]};
figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);
for time_i = 461:20:length(times)-20
    for cond_i=1:length(conditions)
        if exist('ispc','var')~=0;
            datavector = [abs(squeeze(mean(mean(itpc.(conditions{cond_i}).itpcdata(:,freq_of_interest{1},time_i:time_i+20),3),2)-...
                mean(mean(itpc.(conditions{1}).itpcdata(:,freq_of_interest{1},time_i:time_i+20),3),2)));0;0;0;0;0;0;0;0];
            %             datavector = squeeze(mean(mean(ispc.(conditions{cond_i}).ispcdata(:,freq_of_interest,time_i:time_i+20),3),2));
            subplot(2,length(conditions),cond_i,'replace');
            topoplot(datavector, 'E:\FNL_EEG_TOOLBOX\FUNCTIONS\fnl_BioSemi72.loc',...
                'electrodes','on','whitebk','on','numcontour',2,'maplimits',[0 0.2],...
                'verbose','off','shading','interp','colormap','hot');
            title(['6 Hz ' conditions{cond_i} ' ' num2str(times(time_i)*1000+100) ' ms']);axis square
            datavector = [abs(squeeze(mean(mean(itpc.(conditions{cond_i}).itpcdata(:,freq_of_interest{2},time_i:time_i+20),3),2)-...
                mean(mean(itpc.(conditions{1}).itpcdata(:,freq_of_interest{2},time_i:time_i+20),3),2)));0;0;0;0;0;0;0;0];
            %             datavector = squeeze(mean(mean(ispc.(conditions{cond_i}).ispcdata(:,freq_of_interest,time_i:time_i+20),3),2));
            subplot(2,length(conditions),(cond_i+length(conditions)),'replace');
            topoplot(datavector, 'E:\FNL_EEG_TOOLBOX\FUNCTIONS\fnl_BioSemi72.loc',...
                'electrodes','on','whitebk','on','numcontour',2,'maplimits',[0 0.2],...
                'verbose','off','shading','interp','colormap','hot');
            title(['10 Hz' conditions{cond_i} ' ' num2str(times(time_i)*1000+100) ' ms']);axis square
        end
    end
    pause(0.01);
end

%itpc
delta_f = 1:12;
theta_f = 13:41;
alpha_f = 42:59;
beta_f  = 60:80;
freq_of_interest = {[freq_1];[freq_2]};%{[13:37];[42:59]};
figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);
for time_i = 461:20:length(times)-20
    for cond_i=1:length(conditions)
        if exist('ispc','var')~=0;
            datavector = [abs(squeeze(mean(mean(itpc.(conditions{cond_i}).itpcdata(:,freq_of_interest{1},time_i:time_i+20),3),2)-...
                mean(mean(itpc.(conditions{1}).itpcdata(:,freq_of_interest{1},time_i:time_i+20),3),2)));0;0;0;0;0;0;0;0];
            %             datavector = squeeze(mean(mean(ispc.(conditions{cond_i}).ispcdata(:,freq_of_interest,time_i:time_i+20),3),2));
            subplot(2,length(conditions),cond_i,'replace');
            topoplot(datavector, 'E:\FNL_EEG_TOOLBOX\FUNCTIONS\fnl_BioSemi72.loc',...
                'electrodes','on','whitebk','on','numcontour',2,'maplimits',[0 0.2],...
                'verbose','off','shading','interp','colormap','hot');
            title(['6 Hz ' conditions{cond_i} ' ' num2str(times(time_i)*1000+100) ' ms']);axis square
            datavector = [abs(squeeze(mean(mean(itpc.(conditions{cond_i}).itpcdata(:,freq_of_interest{2},time_i:time_i+20),3),2)-...
                mean(mean(itpc.(conditions{1}).itpcdata(:,freq_of_interest{2},time_i:time_i+20),3),2)));0;0;0;0;0;0;0;0];
            %             datavector = squeeze(mean(mean(ispc.(conditions{cond_i}).ispcdata(:,freq_of_interest,time_i:time_i+20),3),2));
            subplot(2,length(conditions),(cond_i+length(conditions)),'replace');
            topoplot(datavector, 'E:\FNL_EEG_TOOLBOX\FUNCTIONS\fnl_BioSemi72.loc',...
                'electrodes','on','whitebk','on','numcontour',2,'maplimits',[0 0.2],...
                'verbose','off','shading','interp','colormap','hot');
            title(['10 Hz ' conditions{cond_i} ' ' num2str(times(time_i)*1000+100) ' ms']);axis square
        end
    end
    pause(0.01);
end

%ispc
delta_f = 1:12;
theta_f = 13:41;
alpha_f = 42:59;
beta_f  = 60:80;
freq_of_interest = {[freq_1];[freq_2]};%{[13:37];[42:59]};
figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);
for time_i = 461:20:length(times)-20
    for cond_i=1:length(conditions)
        if exist('ispc','var')~=0;
            datavector = [abs(squeeze(mean(mean(ispc.(conditions{cond_i}).ispcdata(:,freq_of_interest{1},time_i:time_i+20),3),2)-...
                mean(mean(ispc.(conditions{1}).ispcdata(:,freq_of_interest{1},time_i:time_i+20),3),2)));0;0;0;0;0;0;0;0];
            %             datavector = squeeze(mean(mean(ispc.(conditions{cond_i}).ispcdata(:,freq_of_interest,time_i:time_i+20),3),2));
            subplot(2,length(conditions),cond_i,'replace');
            topoplot(datavector, 'E:\FNL_EEG_TOOLBOX\FUNCTIONS\fnl_BioSemi72.loc',...
                'electrodes','on','whitebk','on','numcontour',2,'maplimits',[0 0.05],...
                'verbose','off','shading','interp','colormap','hot');
            title(['6 Hz ' conditions{cond_i} ' ' num2str(times(time_i)*1000+100) ' ms']);axis square
            datavector = [abs(squeeze(mean(mean(ispc.(conditions{cond_i}).ispcdata(:,freq_of_interest{2},time_i:time_i+20),3),2)-...
                mean(mean(ispc.(conditions{1}).ispcdata(:,freq_of_interest{2},time_i:time_i+20),3),2)));0;0;0;0;0;0;0;0];
            %             datavector = squeeze(mean(mean(ispc.(conditions{cond_i}).ispcdata(:,freq_of_interest,time_i:time_i+20),3),2));
            subplot(2,length(conditions),(cond_i+length(conditions)),'replace');
            topoplot(datavector, 'E:\FNL_EEG_TOOLBOX\FUNCTIONS\fnl_BioSemi72.loc',...
                'electrodes','on','whitebk','on','numcontour',2,'maplimits',[0 0.05],...
                'verbose','off','shading','interp','colormap','hot');
            title(['10 Hz ' conditions{cond_i} ' ' num2str(times(time_i)*1000+100) ' ms']);axis square
        end
    end
    pause(0.01);
end
%% 5. Extract data
disp('5. Extract data');
frequencies = logspace(log10(2),log10(30),80);
times = -1:0.001953125000000:3.5;
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','Afz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
MIDFRONT_channelToCompare = [{'Fz'},{'FCz'},{'AFz'}];
P_LchannelToCompare = [{'P5'},{'PO7'},{'P7'}];
P_RchannelToCompare = [{'P6'},{'PO8'},{'P8'}];
channelToCompare = [P_RchannelToCompare,P_LchannelToCompare,MIDFRONT_channelToCompare];
freq_s = 3.0;           % Start Frequency in Hertz
freq_e = 10.0;           % End Frequency in Hertz
[~,startfreq_ind]   = min(abs(frequencies - freq_s));
[~,endfreq_ind]     = min(abs(frequencies - freq_e));
Cluster_Names = [{'MIDFRONT'},{'P_Left'},{'P_Right'}];
Cluster.(Cluster_Names{1}) = MIDFRONT_channelToCompare;
Cluster.(Cluster_Names{2}) = P_LchannelToCompare;
Cluster.(Cluster_Names{3}) = P_RchannelToCompare;
Time_Intervals = [{[0.1 0.300]},{[1.100 1.300]}];
DATA_EXTRACTED = zeros(length(names),length(Cluster_Names),length(conditions),length(Time_Intervals));
baseline_t = [-0.7 -0.5];
[~,basestarttime_ind] = min(abs(times*1000-baseline_t(1)*1000));
[~,baseendtime_ind]   = min(abs(times*1000-baseline_t(2)*1000));
for name_i = 1:length(names)%113 114 115 116 133 160
    for cond_i = 1%:length(conditions)
        fprintf('Working on %s %s \t',names{name_i},conditions{cond_i});
        load([wpms.dirs.CWD filesep names{name_i} '_' conditions{cond_i} '_ITPC.mat'],itpcname);
        for cluster_i = 1:length(Cluster_Names)
            data_temp = zeros(size(itpc_all,3),1);
            for chan_i = 1:length(Cluster.(Cluster_Names{cluster_i}));
                channelToCompare_clust = Cluster.(Cluster_Names{cluster_i}){chan_i};
                fprintf('%s ',channelToCompare_clust);
                %basline
                data = squeeze(mean(itpc_all(strcmpi(channelToCompare_clust,labels),startfreq_ind:endfreq_ind,:),2));
                baseline = mean(mean(itpc_all(strcmpi(channelToCompare_clust,labels),startfreq_ind:endfreq_ind,basestarttime_ind:baseendtime_ind),2));
                data_t = data-baseline;
                data_temp = data_temp+data_t;
                %plot(data_t);
                
            end
            data_temp = data_temp./length(Cluster.(Cluster_Names{cluster_i}));
            %extract time interval:
            for time_i = 1:length(Time_Intervals)
                [~,starttime_ind] = min(abs(times*1000-Time_Intervals{time_i}(1)*1000));
                [~,endtime_ind]   = min(abs(times*1000-Time_Intervals{time_i}(2)*1000));
                fprintf('%3.5f ',mean(data_temp(starttime_ind:endtime_ind)));
                DATA_EXTRACTED(name_i,cluster_i,cond_i,time_i) = mean(data_temp(starttime_ind:endtime_ind));%Time_Intervals{time_i}(1);
            end
            
        end
        fprintf('\n');
    end
end
save([wpms.dirs.CWD filesep 'EXTRACTED_MEASURES.mat'],'DATA_EXTRACTED','-v7.3');
time_interval_names = [{'Cue'},{'Target'}];
fid = fopen([wpms.dirs.CWD filesep 'EXTRACT_DATA.txt'],'w');
fprintf(fid,'%s\t','SUBJECT_ID');
for cond_i = 1:length(conditions)
    
    for cluster_i = 1:length(Cluster_Names)
        for time_i = 1:length(Time_Intervals)
            fprintf(fid,'%s\t',[conditions{cond_i},'_',Cluster_Names{cluster_i},'_',time_interval_names{time_i}]);
        end
    end
end
fprintf(fid,'\n');
for name_i = 1:length(names)
    fprintf(fid,'%s\t',names{name_i});
    for cond_i = 1:length(conditions)
        for cluster_i = 1:length(Cluster_Names)
            for time_i = 1:length(Time_Intervals)
                fprintf(fid,'%3.5f\t',DATA_EXTRACTED(name_i,cluster_i,cond_i,time_i));
            end
        end
    end
    fprintf(fid,'\n');
end
fclose(fid);