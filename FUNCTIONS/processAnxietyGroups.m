%% Generate topology plots of theta power over time for anxiety groups
% Patrick Cooper, 2015
% Functional Neuroimaging Laboratory,
% University of Newcastle, Australia
%% Set up workspace
clc;clear all;close all;
DATAIN  = 'F:\FNL_EEG_TOOLBOX\WAVELET_OUTPUT_DIR\';
DATAOUT = [DATAIN 'AnxietyData\'];
%groups: 1=LCLA,2=LCHA,3=HCLA,4=HCHA
groups_labs = {{'LCLA'},{'LCHA'},{'HCLA'},{'HCHA'}};
groups = {{'AGE023','AGE026','AGE032','AGE043','AGE047','AGE050','AGE051', ...
'AGE059','AGE081','AGE092','AGE095','AGE102','AGE104','AGE109','AGE116', ...
'AGE117','AGE118','AGE119','AGE123','AGE124','AGE134','AGE145','AGE155', ...
'AGE161','AGE162','AGE166','AGE167','AGE170','AGE172','AGE177','AGE178', ...
'AGE179','AGE184','AGE187','AGE195','AGE198','AGE199','AGE202','AGE204', ...
'AGE226','AGE231','AGE233','AGE236','AGE241','AGE244','AGE254','AGE257','AGE267','AGE276'},...
{'AGE027','AGE033','AGE038','AGE052','AGE053','AGE061','AGE072','AGE088', ...
'AGE093','AGE108','AGE114','AGE127','AGE128','AGE131','AGE133','AGE143', ...
'AGE150','AGE158','AGE164','AGE165','AGE180','AGE181','AGE182','AGE189', ...
'AGE190','AGE197','AGE205','AGE222','AGE248','AGE261','AGE264'},...
{'AGE012','AGE013','AGE014','AGE017','AGE018','AGE019','AGE021', ...
'AGE024','AGE028','AGE030','AGE036','AGE058','AGE067','AGE069', ...
'AGE070','AGE075','AGE077','AGE083','AGE085','AGE086','AGE089', ...
'AGE090','AGE094','AGE100','AGE120','AGE129','AGE135','AGE138', ...
'AGE147','AGE149','AGE151','AGE153','AGE159','AGE168','AGE169', ...
'AGE175','AGE183','AGE185','AGE186','AGE200','AGE201','AGE203', ...
'AGE208','AGE209','AGE210','AGE217','AGE218','AGE219','AGE220', ...
'AGE224','AGE225','AGE227','AGE228','AGE229','AGE230','AGE243', ...
'AGE245','AGE246','AGE251','AGE252','AGE253','AGE258','AGE266', ...
'AGE268','AGE273','AGE279'}, ...
{'AGE008','AGE062','AGE068','AGE130','AGE156','AGE176','AGE192', ...
'AGE237','AGE238','AGE247','AGE249','AGE259','AGE277'}};
%AGE275 didn't have wavelet data...
package_dir = 'F:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip\';
addpath(genpath('F:\FNL_EEG_TOOLBOX\FUNCTIONS\'));
addpath(genpath(package_dir));
conditions = {'mixrepeat' 'switchto' 'switchaway' 'noninf'};
channels = 1:64;
%% create data structures for each group
for group_i = 4:length(groups)
    fprintf('%s %s','Processing Group:',groups_labs{group_i}{1});
    sub = 1;
    filename = strcat(DATAIN,groups{group_i}(sub),'\',conditions{1},'\',groups{group_i}(sub),'_',conditions{1},'_1_imagcoh_mwtf.mat');
    A= load(filename{1,1});%to determine space requirements
    Anam = fieldnames(A);
    freq = 19:32;%theta logspace
    time = size(A.(Anam{1}),2);
    %preallocate data structure first
    data = zeros(length(groups{group_i}),length(conditions),length(channels),time);
    clear A Anam sub filename time
    for name_i = 1:length(groups{group_i})
        tic;
        fprintf('\n%s\t%s\t','Subject:',groups{group_i}{name_i});
        for cond_i = 1:length(conditions)
            for chann = channels
                filename = strcat(DATAIN,groups{group_i}(name_i),'\',conditions{cond_i},'\',groups{group_i}(name_i),'_',conditions{cond_i},'_',num2str(chann),'_imagcoh_mwtf.mat');
                load(filename{1});
                thetadata = squeeze(mean(mw_tf(freq,:),1));
                data(name_i,cond_i,chann,:) = thetadata;
            end%channel loop
        end%conditions loop
        t = toc;
        fprintf('%s%s',num2str(t),'s');
    end%name_i loop
    savename = strcat(DATAOUT,groups_labs{group_i},'_ERSP_Theta_data.mat');
    save(savename{1},'data','-v7.3');
    clear data savename chann cond_i name_i
end%group_i loop
%% now load data in and show as headplot
% layout: each quadrant is one group, clock wise from top left =
% repeat,switchto,switchaway,noninf
% x x x x
% x x x x
% x x x x
% x x x x
channs   = 1:64;
%total time range = -1000 to 3500ms
%i.e., 5201/4500  = 1.15ms time steps
warning off;
cue_onset        = (1.15*1000);
target_onset     = cue_onset+(1.15*1000);
post_target      = target_onset+(1.15*1000);
times = (cue_onset-200):10:post_target;
cfg      = [];
cfg.comment = 'no';
cfg.label = {'FP1';'AF7';'AF3';'F1';'F3';'F5';'F7';'FT7';...
    'FC5';'FC3';'FC1';'C1';'C3';'C5';'T7';'TP7';'CP5';'CP3';...
    'CP1';'P1';'P3';'P5';'P7';'P9';'PO7';'PO3';'O1';'Iz';'Oz';...
    'POz';'Pz';'CPz';'Fpz';'Fp2';'AF8';'AF4';'AFz';'Fz';'F2';...
    'F4';'F6';'F8';'FT8';'FC6';'FC4';'FC2';'FCz';'Cz';'C2';'C4';...
    'C6';'T8';'TP8';'CP6';'CP4';'CP2';'P2';'P4';'P6';'P8';'P10';'PO8';'PO4';'O2';};
cfg.layout = 'biosemi64.lay';
cfg.zlim = [-1 1];
dat = [];
dat.label = cfg.label;
dat.dimord = 'chan_time';
dat.var  = zeros(length(channs),1);
dat.time = 1;
dat.dof  = zeros(length(channs),1);
dat.cov  = zeros(length(channs),length(channs));
sinds = [1 2 5 6 3 4 7 8 9 10 13 14 11 12 15 16];%subplot indices
frames.cdata = zeros(length(times),420,560,3);
frames.colormap = [];
for time_i = 1:length(times)
    count = 0;
    figure();set(gcf,'Color',[1 1 1]);title(['Time:',num2str(times(time_i))]);
    for group_i = 1:length(groups)
        load(strcat(DATAOUT,groups_labs{group_i}{1},'_ERSP_Theta_data.mat'));
        for cond_i = 1:length(conditions)
            count = count+1;
            avdata = squeeze(mean(data(:,cond_i,:,times(time_i)),1));
            dat.avg = avdata;
            if sinds(count)==2;
                subplot(4,4,sinds(count));ft_topoplotER(cfg,dat);title([num2str(times(time_i)-1000) 'ms'],'FontSize',18);
            elseif sinds(count)==9||sinds(count)==10||sinds(count)==13||sinds(count)==14;
                subplot(4,4,sinds(count));ft_topoplotER(cfg,dat);title(conditions{cond_i},'FontSize',16)
            else
                subplot(4,4,sinds(count));ft_topoplotER(cfg,dat);
            end
        end%conditions loop
    end%group_i loop

  
    f = getframe(gcf);
    if time_i == 1
        [im,map] = rgb2ind(f.cdata,256,'nodither');
        im(1,1,1,287) = 0;
    end
    im(:,:,1,time_i) = rgb2ind(f.cdata,map,'nodither');
    

    close;
end
filename = 'F:\FNL_EEG_TOOLBOX\WAVELET_OUTPUT_DIR\AnxietyData\AnxietyMovie.gif';
imwrite(im,map,filename,'gif','DelayTime',0.5,'LoopCount',inf);
%% plot average time courses
%set up blank headplot that will highlight vals of electrodes
channs = 1:64;
cols = {'-b','-b','-b','-b',...
    '-r','-r','-r','-r',...
    '-g','-g','-g','-g',...
    '-c','-c','-c','-c'};
cfg      = [];
cfg.comment = 'no';
cfg.label = {'Fp1';'AF7';'AF3';'F1';'F3';'F5';'F7';'FT7';...
    'FC5';'FC3';'FC1';'C1';'C3';'C5';'T7';'TP7';'CP5';'CP3';...
    'CP1';'P1';'P3';'P5';'P7';'P9';'PO7';'PO3';'O1';'Iz';'Oz';...
    'POz';'Pz';'CPz';'Fpz';'Fp2';'AF8';'AF4';'AFz';'Fz';'F2';...
    'F4';'F6';'F8';'FT8';'FC6';'FC4';'FC2';'FCz';'Cz';'C2';'C4';...
    'C6';'T8';'TP8';'CP6';'CP4';'CP2';'P2';'P4';'P6';'P8';'P10';'PO8';'PO4';'O2';};
cfg.layout = 'biosemi64.lay';
cfg.zlim = [-1 1];
cfg.marker = 'off';
cfg.highlight = 'on';
cfg.highlightsymbol = 'o';
cfg.highlightcolor = [1 0 0];
cfg.highlightsize = 16;
locdat = [];
locdat.label = cfg.label;
locdat.dimord = 'chan_time';
locdat.var  = zeros(length(channs),1);
locdat.time = 1;
locdat.dof  = zeros(length(channs),1);
locdat.cov  = zeros(length(channs),length(channs));
locdat.avg = zeros(64,1);

condtitles = {'Repeat','Switch-To','Switch-Away','Non-Inf'};
sites = {[1,3,33,34,36,37],[2,5:7],[35,40:42],...
    [4,11,38,39,46,47],[8:10],[43:45],...
    [12,48,49],[13:15],[50:52],...
    [19,32,56],[16:18],[53:55],...
    [20,31,57],[21:24],[58:61],...
    [28:30],[25:27],[62:64],...
    [2,5:10,28:30,62:64]};
sitelabs = {'Front_mid_','Front_left_','Front_right_'...
            'Frontcent_mid_','Frontcent_left','Frontcent_right_',...
            'Centre_mid_','Centre_left_','Centre_right_',...
            'Centroparietal_mid_','Centroparietal_left_','Centroparietal_right_',...
            'Parietal_mid_','Parietal_left_','Parietal_right_',...
            'Occipital_mid_','Occipital_left_','Occipital_right_',...
            'HotSpots_'};
% sitelabs = {'Frontal_','FrontoCentral_','Central_','CentroParietal_','Parietal_'};
sinds = [1 3 4 6];
for site_i = 1:length(sites)
    figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);ha = tight_subplot(2,3,[.1 .1],[.1 .1],[.1 .1]);
    count = 0;
    for group_i = 1:length(groups)
        load(strcat(DATAOUT,groups_labs{group_i}{1},'_ERSP_Theta_data.mat'));
        for cond_i =1:length(conditions)    
            count = count+1;
            dat = squeeze(nanmean(data(:,cond_i,sites{site_i},:),3));
            datstd = std(dat,1)/sqrt(size(dat,1));
            dat = squeeze(nanmean(dat,1));
            axes(ha(sinds(cond_i)));
            errorbar(dat,datstd,cols{count});hold on;
%             plot(dat,cols{count});hold on;
%             subplot(2,3,sinds(cond_i));plot(dat,cols{count});hold on;
            title(condtitles{cond_i});xlim([1 5200]);set(gca,'XTick',1:1000:5200,...
                'XTickLabel',{round((-1:1000:5200)-1000+1.15)},'FontSize',8);
            ylim([-2.5 2.5]);xlabel('Time (ms)');ylabel('Power (dB)');
            plot(ones(6,1)*1000,-2.5:2.5,'--k','LineWidth',2);
            plot(ones(6,1)*2000,-2.5:2.5,'--k','LineWidth',2);
        end
    end
%     subplot(2,3,5);
    axes(ha(5));plot(1:length(dat),ones(1:length(dat),1)*NaN);hold on;axis off;
    legend LCLA LCHA HCLA HCHA
    cfg.highlightchannel = cfg.label(sites{site_i});
    axes(ha(2));axis off;ft_topoplotER(cfg,locdat);
    saveas(gcf,[DATAOUT num2str(site_i) '.' sitelabs{site_i} 'theta_timecourse.pdf'],'pdf');
    close;
%     subplot(2,3,2);
end
%% test pairs for sig diffs
pair_cols = {'-r','--r','-.r','--b','-.b','-.g'};
t = [1 2 1 3 1 4 2 3 2 4 3 4];
count = 0;    
p=zeros(6,5201);
site_i = 18;
for j = 1:2:length(t)
    fprintf('.');
    count = count+1;
    load(strcat(DATAOUT,groups_labs{t(j)}{1},'_ERSP_Theta_data.mat'));
    x = squeeze(nanmean(data(:,1,sites{site_i},:),3));
    load(strcat(DATAOUT,groups_labs{t(j+1)}{1},'_ERSP_Theta_data.mat'));
    y = squeeze(nanmean(data(:,1,sites{site_i},:),3));
    ym = mean(y,1);
    for i = 1:5201;
        [~,p(count,i),~,~]=ttest(x(:,i),ym(i));
    end

end
ps = p;
ps(p>0.05) = NaN;
plot(1:5201,ps);