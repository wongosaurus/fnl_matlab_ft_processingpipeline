%create theta movies
%set up globals
clear all;close all;clc;
datain = 'F:\FNL_EEG_TOOLBOX\WAVELET_OUTPUT\';
dataout = 'F:\FNL_EEG_TOOLBOX\WAVELET_OUTPUT\Movies\';
conditions = {'mixrepeat','switchto','switchaway','noninfrepeat','noninfswitch'};
theta = 19:32;%1:18%36:49;
times = -400:0.5:2200;
%% first get list of participants
cd(datain);listing = dir();
names = {};
for list_i = 1:length(listing)
    if ~ismember('.',listing(list_i).name) 
        if ~ismember('Movies',listing(list_i).name)%don't want directory
            names = [names listing(list_i).name];
        end
    end
end

%% now create average power matrix
power = zeros(length(names),length(conditions),64,length(times));
for name_i = [1:150, 152:length(names)]
    fprintf('%s\t%s','Working on subject:',names{name_i});
    tic;
    for cond_i = 1:length(conditions)
        fprintf('\n%s\t',conditions{cond_i});
        for chan_i = 1:64
            fprintf('.');
            filename = [datain names{name_i} filesep conditions{cond_i} filesep names{name_i} '_' conditions{cond_i} '_' num2str(chan_i) '_imagcoh_mwtf.mat'];
            load(filename,'mw_tf');
            power(name_i,cond_i,chan_i,:) = squeeze(mean(mw_tf(theta,:),1));
        end
    end
    toc
end

%%
startime = find(times==1400);
endtime  = find(times==1500);
times2show = startime:endtime;
MFLABS = [4,11,12,38,39,46:48];
data = zeros(length(names),length(conditions));
for name_i = [1:150, 152:length(names)];
    for cond_i = 1:length(conditions)
        data(name_i,cond_i) = squeeze(mean(mean(power(name_i,cond_i,MFLABS,times2show),4),3));
    end
end

save('MFPower_N3.txt','data','-ascii','-tabs');

labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','Afz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
%%
HCHA={'AGE062'	'AGE068'	'AGE130'	'AGE156'	'AGE176'	'AGE189'	'AGE192'	'AGE237'	'AGE238'	'AGE247'	'AGE249'	'AGE259'};																																																										
LCHA={'AGE008'	'AGE027'	'AGE033'	'AGE038'	'AGE052'	'AGE053'	'AGE061'	'AGE072'	'AGE088'	'AGE093'	'AGE108'	'AGE114'	'AGE127'	'AGE128'	'AGE131'	'AGE133'	'AGE143'	'AGE150'	'AGE158'	'AGE164'	'AGE165'	'AGE180'	'AGE181'	'AGE182'	'AGE190'	'AGE197'	'AGE205'	'AGE222'	'AGE248'	'AGE261'	'AGE264'	'AGE277'};																																						
HCLA={'AGE012'	'AGE013'	'AGE014'	'AGE017'	'AGE018'	'AGE019'	'AGE021'	'AGE023'	'AGE024'	'AGE028'	'AGE030'	'AGE036'	'AGE043'	'AGE050'	'AGE058'	'AGE067'	'AGE069'	'AGE070'	'AGE075'	'AGE077'	'AGE083'	'AGE085'	'AGE086'	'AGE089'	'AGE090'	'AGE094'	'AGE100'	'AGE120'	'AGE129'	'AGE135'	'AGE138'	'AGE147'	'AGE149'	'AGE151'	'AGE153'	'AGE159'	'AGE168'	'AGE169'	'AGE175'	'AGE183'	'AGE185'	'AGE186'	'AGE200'	'AGE201'	'AGE203'	'AGE208'	'AGE209'	'AGE210'	'AGE217'	'AGE218'	'AGE219'	'AGE220'	'AGE224'	'AGE225'	'AGE227'	'AGE228'	'AGE229'	'AGE230'	'AGE243'	'AGE245'	'AGE246'	'AGE251'	'AGE252'	'AGE253'	'AGE258'	'AGE266'	'AGE268'	'AGE273'	'AGE276'	'AGE279'};
LCLA={'AGE026'	'AGE032'	'AGE047'	'AGE051'	'AGE059'	'AGE081'	'AGE092'	'AGE095'	'AGE102'	'AGE104'	'AGE109'	'AGE116'	'AGE117'	'AGE118'	'AGE119'	'AGE123'	'AGE124'	'AGE134'	'AGE145'	'AGE155'	'AGE161'	'AGE162'	'AGE166'	'AGE167'	'AGE170'	'AGE172'	'AGE177'	'AGE178'	'AGE179'	'AGE184'	'AGE187'	'AGE195'	'AGE198'	'AGE199'	'AGE202'	'AGE204'	'AGE226'	'AGE231'	'AGE233'	'AGE236'	'AGE241'	'AGE244'	'AGE254'	'AGE257'	'AGE267'	'AGE275'};																								

inds = find(ismember(names,LCLA));
avpower = squeeze(nanmean(power(inds,:,:,:),1));
%clear power;
%% make movie
startime = find(times==-100);
endtime  = find(times==2000);
times2show = startime:endtime;
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','Afz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
%for plotting
cfg =[];
cfg.layout = 'biosemi64.lay';
cfg.marker = 'off';
cfg.parameter = 'avg';
cfg.comment = 'no';
cfg.contournum = 2;
%cfg.gridscale = 400;
%cfg.shading = 'interp';
cfg.interactive = 'no';
dat.var = zeros(64,1);
dat.label = labels(:,1);
dat.time = 1;
dat.dimord = 'chan_time';
cfg.zlim = [-2.5 2.5];
timelabels = -100:1600;
%cfg.highlightcolor = [0 0 0];
%cfg.highlight = 'on';
condshort = {'R','ST','SA','NIR','NIS'};    

count = 1;
for time_i = [floor(length(times2show)/2),1:20:length(times2show)]
    figure();set(gcf,'Position',[100 100 500 500],'Color',[1 1 1]);
    for cond_i =1:length(conditions)
        dat.avg = squeeze(avpower(cond_i,:,times2show(time_i)))';
        subplot(2,3,cond_i,'replace');ft_topoplotER(cfg,dat);
        title(condshort{cond_i},'FontSize',16);
    end
    subplot(2,3,6);title([num2str(times(times2show(time_i))) ' ms'],'FontSize',16);
    axis off;pause(0.1);
    if count == 1
        colorbar;
    end
    f = getframe(gcf);
    if count == 1
       [im,map] = rgb2ind(f.cdata,256,'nodither');
       im(1,1,1,21) = 0;
    end
    if count ~=1
        im(:,:,1,count-1) = rgb2ind(f.cdata,map,'nodither');
    end
    count = count +1;
    close all;
end
mkdir(dataout)
filename = [dataout 'LCLA_THETA_Movie.gif'];
imwrite(im,map,filename,'gif','DelayTime',0.3,'LoopCount',inf);
%% plot differences head plots
%% create plots at 100 ms intervals for 4 anxiety/control groups
%first set up details for fieldtrip
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','Afz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
%for plotting
cfg =[];
cfg.layout = 'biosemi64.lay';
cfg.marker = 'off';
cfg.parameter = 'avg';
cfg.comment = 'no';
cfg.contournum = 2;
%cfg.gridscale = 400;
%cfg.shading = 'interp';
cfg.interactive = 'no';
dat.var = zeros(64,1);
dat.label = labels(:,1);
dat.time = 1;
dat.dimord = 'chan_time';
cfg.zlim = [-2.5 2.5];
%% and group membership
% HCHA={'AGE062'	'AGE068'	'AGE130'	'AGE156'	'AGE176'	'AGE189'	'AGE192'	'AGE237'	'AGE238'	'AGE247'	'AGE249'	'AGE259'};
% LCHA={'AGE008'	'AGE027'	'AGE033'	'AGE038'	'AGE052'	'AGE053'	'AGE061'	'AGE072'	'AGE088'	'AGE093'	'AGE108'	'AGE114'	'AGE127'	'AGE128'	'AGE131'	'AGE133'	'AGE143'	'AGE150'	'AGE158'	'AGE164'	'AGE165'	'AGE180'	'AGE181'	'AGE182'	'AGE190'	'AGE197'	'AGE205'	'AGE222'	'AGE248'	'AGE261'	'AGE264'	'AGE277'};
% HCLA={'AGE012'	'AGE013'	'AGE014'	'AGE017'	'AGE018'	'AGE019'	'AGE021'	'AGE023'	'AGE024'	'AGE028'	'AGE030'	'AGE036'	'AGE043'	'AGE050'	'AGE058'	'AGE067'	'AGE069'	'AGE070'	'AGE075'	'AGE077'	'AGE083'	'AGE085'	'AGE086'	'AGE089'	'AGE090'	'AGE094'	'AGE100'	'AGE120'	'AGE129'	'AGE135'	'AGE138'	'AGE147'	'AGE149'	'AGE151'	'AGE153'	'AGE159'	'AGE168'	'AGE169'	'AGE175'	'AGE183'	'AGE185'	'AGE186'	'AGE200'	'AGE201'	'AGE203'	'AGE208'	'AGE209'	'AGE210'	'AGE217'	'AGE218'	'AGE219'	'AGE220'	'AGE224'	'AGE225'	'AGE227'	'AGE228'	'AGE229'	'AGE230'	'AGE243'	'AGE245'	'AGE246'	'AGE251'	'AGE252'	'AGE253'	'AGE258'	'AGE266'	'AGE268'	'AGE273'	'AGE276'	'AGE279'};
% LCLA={'AGE026'	'AGE032'	'AGE047'	'AGE051'	'AGE059'	'AGE081'	'AGE092'	'AGE095'	'AGE102'	'AGE104'	'AGE109'	'AGE116'	'AGE117'	'AGE118'	'AGE119'	'AGE123'	'AGE124'	'AGE134'	'AGE145'	'AGE155'	'AGE161'	'AGE162'	'AGE166'	'AGE167'	'AGE170'	'AGE172'	'AGE177'	'AGE178'	'AGE179'	'AGE184'	'AGE187'	'AGE195'	'AGE198'	'AGE199'	'AGE202'	'AGE204'	'AGE226'	'AGE231'	'AGE233'	'AGE236'	'AGE241'	'AGE244'	'AGE254'	'AGE257'	'AGE267'	'AGE275'};
HCHA={	'AGE062',	'AGE068',	'AGE130',	'AGE156',	'AGE176',	'AGE189',	'AGE192',	'AGE237',	'AGE238',	'AGE247',	'AGE249',	'AGE259'	};
LCHA={	'AGE008',	'AGE027',	'AGE033',	'AGE061',	'AGE072',	'AGE108',	'AGE127',	'AGE128',	'AGE150',	'AGE158',	'AGE164',	'AGE165'	};
HCLA={	'AGE138',	'AGE149',	'AGE159',	'AGE077',	'AGE201',	'AGE210',	'AGE216',	'AGE219',	'AGE228',	'AGE245',	'AGE246',	'AGE253'	};
LCLA={	'AGE022',	'AGE032',	'AGE081',	'AGE116',	'AGE118',	'AGE124',	'AGE155',	'AGE170',	'AGE187',	'AGE204',	'AGE233',	'AGE257'	};
groups = {HCHA;LCHA;HCLA;LCLA};
group_names = {'HCHA','LCHA','HCLA','LCLA'};
%% now find time bins corresponding to 100ms intervals
startime = find(times==1000);%start at cue onset
inctime  = find(times==100)-find(times==0);%sample every 100 ms
endtime = find(times==2000);%end at target onset
times2plot = startime:inctime:endtime;
timelabels = 0:100:1000;
addpath(genpath('F:\FNL_EEG_TOOLBOX\FUNCTIONS\'));%functions including tight_subplot
addpath(genpath('F:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));%fieldtrip
cfg.highlight = 'off';
for cond_i=1:length(conditions)
    figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);ha = tight_subplot(length(groups),length(times2plot),.01);
    count=0;
    for group_i = 1:length(groups)
        x_inds = find(ismember(names,groups{group_i}));
        for time_i = 1:length(times2plot)
            count =count + 1;
            dat.avg = squeeze(nanmean(power(x_inds,cond_i,:,times2plot(time_i)),1));
            axes(ha(count));ft_topoplotER(cfg,dat);
            if time_i == 1;
                title([group_names{group_i} ' ' num2str(timelabels(time_i)) ' ms'],'FontSize',14);
            else
                title([num2str(timelabels(time_i)) ' ms'],'FontSize',14);
            end
        end%time_i loop
    end%group_i loop
    %savename = ['F:\FNL_EEG_TOOLBOX\WAVELET_OUTPUT\ThetaTimeSeries\' conditions{cond_i} '_TARGET_SMALLERSAMPLE.pdf'];
    %saveas(gcf,savename,'pdf');close all;
end%cond_i loop
%% look for differences between groups
cfg.highlight = 'on';
cfg.highlightsymbol = '.';
cfg.highlightsize = 18;
addpath(genpath('F:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'));%fdr_toolbox
for cond_i=1:length(conditions)
%     figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);ha = tight_subplot(length(groups),length(times2plot),.01);
%     count=0;
    for group_i = 1:length(groups)
        figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);ha = tight_subplot(length(groups),length(times2plot),.01);
        count =0;
        x_inds = find(ismember(names,groups{group_i}));
        for group_j = 1:length(groups)
            y_inds = find(ismember(names,groups{group_j}));
            for time_i = 1:length(times2plot)
                count =count + 1;
                dat.avg = squeeze(nanmean(power(y_inds,cond_i,:,times2plot(time_i)),1));
                difflabels = {};
                p = zeros(1,size(power,3));
                for channel = 1:size(power,3)
                    x = squeeze(power(x_inds,cond_i,channel,times2plot(time_i)));
                    y = squeeze(power(y_inds,cond_i,channel,times2plot(time_i)));
                    [~,p(1,channel),~,~] = ttest2(x,y);
%                     [p,h] = ranksum(x,y);
                end
                [~,~,adj_p]=fdr_bh(p,0.05,'pdep');
                for channel = 1:size(power,3)
                    if p(1,channel) < 0.05;
                        difflabels = [difflabels labels{channel}];
                    end
                end
                cfg.highlightchannel = difflabels;
                axes(ha(count));ft_topoplotER(cfg,dat);
                if time_i == 1;
                    title([group_names{group_j} ' ' num2str(timelabels(time_i)) ' ms'],'FontSize',14);
                else
                    title([num2str(timelabels(time_i)) ' ms'],'FontSize',14);
                end
            end%time_i loop
        end
    savename = ['F:\FNL_EEG_TOOLBOX\WAVELET_OUTPUT\ThetaTimeSeries\' group_names{group_i} 'vs_all_' conditions{cond_i} '_TARGET_SMALLERSAMPLE.pdf'];
    saveas(gcf,savename,'pdf');close all;
    end
end%cond_i loop
%%
clc;
for cond_i = 1:length(conditions)
    for time_i = 1:length(times2plot)
        for channel = 1:64
            xinds = find(ismember(names,groups{1}));
            yinds = find(ismember(names,groups{2}));
            x =squeeze(power(xinds,cond_i,channel,times2plot(time_i)));
            y=squeeze(power(yinds,cond_i,channel,times2plot(time_i)));
            
            [p,h,stats]=ranksum(x,y);
            if h ==1;
                fprintf('\n%s %s %s %s\t','Sig difference found for timebin | condition | electrode:',num2str(time_i),conditions{cond_i}, labels{channel});
                fprintf('%1.3f',p);
            end
        end
    end
end