clear all;close all;clc;
datadir = 'F:\FNL_EEG_TOOLBOX\WAVELET_OUTPUT\';
names = {'AGE002','AGE003','AGE005','AGE007','AGE008','AGE012',...
    'AGE013','AGE014','AGE015','AGE017','AGE018','AGE019',...
    'AGE020','AGE021','AGE022','AGE023','AGE024','AGE026',...
    'AGE027','AGE028','AGE030','AGE032','AGE033','AGE035',...
    'AGE036','AGE038','AGE043','AGE046','AGE047','AGE048',...
    'AGE050','AGE051','AGE052','AGE053','AGE058','AGE059',...
    'AGE061','AGE062','AGE063','AGE064','AGE066','AGE067',...
    'AGE068','AGE069','AGE070','AGE072','AGE075','AGE077',...
    'AGE079','AGE081','AGE083','AGE084','AGE085','AGE086',...
    'AGE088','AGE089','AGE090','AGE092','AGE093','AGE094',...
    'AGE095','AGE096','AGE097','AGE098','AGE100','AGE102',...
    'AGE103','AGE104','AGE106','AGE107','AGE108','AGE109',...
    'AGE114','AGE115','AGE116','AGE117','AGE118','AGE119',...
    'AGE120','AGE121','AGE122','AGE123','AGE124','AGE127',...
    'AGE128','AGE129','AGE130','AGE131','AGE133','AGE134',...
    'AGE135','AGE136','AGE138','AGE141','AGE143','AGE145',...
    'AGE146','AGE147','AGE148','AGE149','AGE150','AGE151',...
    'AGE152','AGE153','AGE155','AGE156','AGE158','AGE159',...
    'AGE161','AGE162','AGE163','AGE164','AGE165','AGE166',...
    'AGE167','AGE168','AGE169','AGE170','AGE172','AGE175',...
    'AGE176','AGE177','AGE178','AGE179','AGE180','AGE181',...
    'AGE182','AGE183','AGE184','AGE185','AGE186','AGE187',...
    'AGE189','AGE190','AGE192','AGE195','AGE197','AGE198',...
    'AGE199','AGE200','AGE201','AGE202','AGE203','AGE204',...
    'AGE205','AGE206','AGE207','AGE208','AGE209','AGE210',...
    'AGE216','AGE217','AGE218','AGE219','AGE220','AGE222',...
    'AGE224','AGE225','AGE226','AGE227','AGE228','AGE229',...
    'AGE230','AGE231','AGE233','AGE234','AGE236','AGE237',...
    'AGE238','AGE239','AGE240','AGE241','AGE243','AGE244',...
    'AGE245','AGE246','AGE247','AGE248','AGE249','AGE251',...
    'AGE252','AGE253','AGE254','AGE255','AGE256','AGE257',...
    'AGE258','AGE259','AGE260','AGE261','AGE264','AGE265',...
    'AGE266','AGE267','AGE268','AGE273','AGE276','AGE277','AGE279'};

HCHA={'AGE062'	'AGE068'	'AGE130'	'AGE156'	'AGE176'	'AGE189'	'AGE192'	'AGE237'	'AGE238'	'AGE247'	'AGE249'	'AGE259'};																																																										
LCHA={'AGE008'	'AGE027'	'AGE033'	'AGE038'	'AGE052'	'AGE053'	'AGE061'	'AGE072'	'AGE088'	'AGE093'	'AGE108'	'AGE114'	'AGE127'	'AGE128'	'AGE131'	'AGE133'	'AGE143'	'AGE150'	'AGE158'	'AGE164'	'AGE165'	'AGE180'	'AGE181'	'AGE182'	'AGE190'	'AGE197'	'AGE205'	'AGE222'	'AGE248'	'AGE261'	'AGE264'	'AGE277'};																																						
HCLA={'AGE012'	'AGE013'	'AGE014'	'AGE017'	'AGE018'	'AGE019'	'AGE021'	'AGE023'	'AGE024'	'AGE028'	'AGE030'	'AGE036'	'AGE043'	'AGE050'	'AGE058'	'AGE067'	'AGE069'	'AGE070'	'AGE075'	'AGE077'	'AGE083'	'AGE085'	'AGE086'	'AGE089'	'AGE090'	'AGE094'	'AGE100'	'AGE120'	'AGE129'	'AGE135'	'AGE138'	'AGE147'	'AGE149'	'AGE151'	'AGE153'	'AGE159'	'AGE168'	'AGE169'	'AGE175'	'AGE183'	'AGE185'	'AGE186'	'AGE200'	'AGE201'	'AGE203'	'AGE208'	'AGE209'	'AGE210'	'AGE217'	'AGE218'	'AGE219'	'AGE220'	'AGE224'	'AGE225'	'AGE227'	'AGE228'	'AGE229'	'AGE230'	'AGE243'	'AGE245'	'AGE246'	'AGE251'	'AGE252'	'AGE253'	'AGE258'	'AGE266'	'AGE268'	'AGE273'	'AGE276'	'AGE279'};
LCLA={'AGE026'	'AGE032'	'AGE047'	'AGE051'	'AGE059'	'AGE081'	'AGE092'	'AGE095'	'AGE102'	'AGE104'	'AGE109'	'AGE116'	'AGE117'	'AGE118'	'AGE119'	'AGE123'	'AGE124'	'AGE134'	'AGE145'	'AGE155'	'AGE161'	'AGE162'	'AGE166'	'AGE167'	'AGE170'	'AGE172'	'AGE177'	'AGE178'	'AGE179'	'AGE184'	'AGE187'	'AGE195'	'AGE198'	'AGE199'	'AGE202'	'AGE204'	'AGE226'	'AGE231'	'AGE233'	'AGE236'	'AGE241'	'AGE244'	'AGE254'	'AGE257'	'AGE267'	'AGE275'};																								
groups = {HCHA;LCHA;HCLA;LCLA};
theta = 22:38;
conditions = {'mixrepeat','switchto','switchaway','noninfrepeat','noninfswitch'};
nchan = 64;
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','Afz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
for chan_i = 1:nchan;
    data = zeros(length(names),length(conditions),80,5201);
    tic;
    for name_i = 1:length(names)
        fprintf('\n%s\t%s\t','Subject:',names{name_i});
        for cond_i = 1:length(conditions)
            fprintf('.');
            filename = [datadir names{name_i} filesep conditions{cond_i} filesep names{name_i} '_' conditions{cond_i} '_' num2str(chan_i) '_imagcoh_mwtf.mat'];
            load(filename,'mw_tf');
            data(name_i,cond_i,:,:) = mw_tf;
            clear mw_tf
        end
    end
    toc
    %make plots
    %first make average theta plot
    figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);
    x = 1:size(data,4);
    y = squeeze(nanmean(nanmean(data(:,:,theta,:),1),3));
    for cond_i = 1:length(conditions)
        e = squeeze(std(nanmean(data(:,cond_i,theta,:),3)))./sqrt(size(data,1));
        subplot(2,3,cond_i);errorbar(x,y(cond_i,:)',e);set(gca,'YLim',[-2 2],'XLim',[-1000 5000]);
        title(['Theta at electrode:', labels(chan_i) , ' condition: ', conditions{cond_i}],'FontSize',14); 
    end
    savename = [datadir 'ThetaTimeSeries' filesep 'GroupLevelTheta_' labels{chan_i} '.pdf'];
    saveas(gcf,savename,'pdf');close all;
    % group differences
    figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);
    x = 1:size(data,4);
    linecolors = {'r','b','g','k'};
    for cond_i = 1:length(conditions)
        for group_i = 1:length(groups)
            inds = find(ismember(names,groups{group_i}));
            y = squeeze(nanmean(nanmean(data(inds,:,theta,:),1),3));
            e = squeeze(std(nanmean(data(inds,cond_i,theta,:),3)))./sqrt(size(data(inds,:,:,:,:),1));
            subplot(2,3,cond_i);errorbar(x,y(cond_i,:)',e,linecolors{group_i});set(gca,'YLim',[-2 2],'XLim',[-1000 5000]);
            title(['Theta at electrode:', labels(chan_i) , ' condition: ', conditions{cond_i}],'FontSize',14);hold on;
        end
        if cond_i == length(conditions);
            for group_i = 1:length(groups)
                y = squeeze(nanmean(nanmean(data(inds,:,theta,:),1),3));
                e = squeeze(std(nanmean(data(inds,cond_i,theta,:),3)))./sqrt(size(data(inds,:,:,:,:),1));
                subplot(2,3,6);errorbar(x,y(cond_i,:)',e,linecolors{group_i});set(gca,'YLim',[-2 2],'XLim',[-1000 5000]);hold on;
                legend('HCHA','LCHA','HCLA','LCLA','FontSize',14);
            end
        end
    end
    savename = [datadir 'ThetaTimeSeries' filesep 'GroupDifferencesTheta_' labels{chan_i} '.pdf'];
    saveas(gcf,savename,'pdf');close all;
end
        
