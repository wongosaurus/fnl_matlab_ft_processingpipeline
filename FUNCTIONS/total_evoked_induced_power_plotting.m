clearvars; close all; clc;
%Data Structure Time/Frequency definition
frequencies = logspace(log10(2),log10(30),80);
times = (-1:0.001953125000000:3.5)*1000;
%Electrode Labels:
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
CHANNELS = 1:length(labels);
CONDITIONS  = {'switchto','switchaway','noninf','mixrepeat','allrepeat'};
% Find the names of participants:
listings = dir('F:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat');
NAMES = {length(listings)};
for file_i =1:length(listings)
    NAMES{file_i} = listings(file_i).name(1:6);
end

SAVEDIR = 'F:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\';
LOADDIR = 'F:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\';
%%
% Baseline: -300ms = 359index, -100ms = 462index :CHECKED -> OK
baseline_time=359:462;
filename=[LOADDIR NAMES{1} '_' CONDITIONS{1} '_ALL_POWER3.mat'];
load(filename);
%incorporate into an average
condition_average_eegpower = zeros(length(CONDITIONS)-1,size(eegpower_all,1),size(eegpower_all,2),size(eegpower_all,3));
%condition_average_evoked = condition_average_eegpower;
condition_average_induced = condition_average_eegpower;

clear eegpower_all evokedpower_all inducedpower_all

for name_i=1:length(NAMES)
    disp(NAMES{name_i});
    tic;
    % Process All Repeat First: Keep This in memory:
    condition_i = 5; % For All Repeat:
    filename=[LOADDIR NAMES{name_i} '_' CONDITIONS{condition_i} '_ALL_POWER3.mat'];
    load(filename);
    eegpower_allRepeat_bl = zeros(size(eegpower_all));
    %evoked_allRepeat_bl     = zeros(size(evokedpower_all));
    induced_allRepeat_bl     = zeros(size(inducedpower_all));
    for chan_i = 1:length(CHANNELS)
        eegpower_allRepeat_bl(chan_i,:,:) = bsxfun(@minus,10*log10(squeeze(eegpower_all(chan_i,:,:))),10*log10(mean(squeeze(eegpower_all(chan_i,:,baseline_time)),2)));
        %evoked_allRepeat_bl(chan_i,:,:) = bsxfun(@minus,real(10*log10(squeeze(evokedpower_all(chan_i,:,:)))),real(10*log10(mean(squeeze(evokedpower_all(chan_i,:,baseline_time)),2))));
        induced_allRepeat_bl(chan_i,:,:) = bsxfun(@minus,10*log10(squeeze(inducedpower_all(chan_i,:,:))),10*log10(mean(squeeze(inducedpower_all(chan_i,:,baseline_time)),2)));
        clear temp temp2 bl
    end%chan_i loop
    
    %Remove Variables just incase something goes wrong..
    clear eegpower_all evokedpower_all inducedpower_all;
    
    for condition_i=1:length(CONDITIONS)-1
        fprintf('.');
        filename=[LOADDIR NAMES{name_i} '_' CONDITIONS{condition_i} '_ALL_POWER3.mat'];
        load(filename);
        %preallocate baseline structure
        eegpower_all_bl = zeros(size(eegpower_all));
        %evokedpower_all_bl     = zeros(size(evokedpower_all));
        inducedpower_all_bl     = zeros(size(inducedpower_all));
        % baseline
        for chan_i = 1:length(CHANNELS)
            eegpower_all_bl(chan_i,:,:) = bsxfun(@minus,10*log10(squeeze(eegpower_all(chan_i,:,:))),10*log10(mean(squeeze(eegpower_all(chan_i,:,baseline_time)),2)));
            %evokedpower_all_bl(chan_i,:,:) = bsxfun(@minus,real(10*log10(squeeze(evokedpower_all(chan_i,:,:)))),real(10*log10(mean(squeeze(evokedpower_all(chan_i,:,baseline_time)),2))));
            inducedpower_all_bl(chan_i,:,:) = bsxfun(@minus,10*log10(squeeze(inducedpower_all(chan_i,:,:))),10*log10(mean(squeeze(inducedpower_all(chan_i,:,baseline_time)),2)));
        end%chan_i loop
        
        eegpower_bl_arcorrected         = eegpower_all_bl - eegpower_allRepeat_bl;
        %evokedpower_bl_arcorrected      = evokedpower_all_bl - evoked_allRepeat_bl;
        inducedpower_bl_arcorrected     = inducedpower_all_bl - induced_allRepeat_bl;
        
        
        condition_average_eegpower(condition_i,:,:,:) = squeeze(condition_average_eegpower(condition_i,:,:,:)) + eegpower_bl_arcorrected;
        %condition_average_evoked(condition_i,:,:,:)   = squeeze(condition_average_evoked(condition_i,:,:,:)) + evokedpower_bl_arcorrected;
        condition_average_induced(condition_i,:,:,:)  = squeeze(condition_average_induced(condition_i,:,:,:)) + inducedpower_bl_arcorrected;
        
%      savename=[SAVEDIR NAMES{name_i} '_' CONDITIONS{condition_i} '_ITPC_bl_arcorrected.mat'];
% %      save(savename, 'eegpower_all_bl', 'itpc_all_bl');
%      save(savename, 'eegpower_bl_arcorrected', 'itpc_bl_arcorrected', 'ispc_bl_arcorrected');
   
    end %condition_i loop
    t=toc;
    fprintf('\t%3.2f %s\n',t,'seconds to complete');
end %name_i loop 

condition_average_eegpower = condition_average_eegpower./length(NAMES);
%condition_average_evoked   = condition_average_evoked./length(NAMES);
condition_average_induced  = condition_average_induced./length(NAMES);
%%
%% View the plots:
% EEG POWER
CONDITIONS_NOAR  = {'mixrepeat','switchto','switchaway','noninf'};
%chan_i = find(ismember(labels,{'CP1','CPz','CP2','P1','Pz','P2','POz'}));
chan_i = find(ismember(labels,{'C1','Cz','C2','FC1','FCz','FC2'}));
%chan_i = find(ismember(labels,{'Pz','POz'}));
%chan_i = find(ismember(labels,{'FCz','Fz'}));
figure();
plot_times = 359:1333;
count=0;

erp = squeeze(mean(EEG.data(sensoridx,:,:),3));

% scale ERP for plotting
erpt = (erp-min(erp))./max(erp-min(erp));
erpt = erpt*(frequencies(end)-frequencies(1))+frequencies(1);


%total
for cond_i = 1:length(CONDITIONS_NOAR)
    subplot(3,length(CONDITIONS_NOAR),count+cond_i);
    contourf(times(plot_times),frequencies,squeeze(mean(condition_average_eegpower(cond_i,chan_i,:,plot_times),2)),50,'linecolor','none');
    caxis([-5 5]);
    colormap 'jet';
    title(CONDITIONS_NOAR{cond_i});
end
%evoked
count=count+length(CONDITIONS_NOAR);
for cond_i = 1:length(CONDITIONS_NOAR)
    subplot(3,length(CONDITIONS_NOAR),count+cond_i);
    %contourf(times,frequencies,squeeze(mean(condition_average_evoked(cond_i,chan_i,:,:),2)),50,'linecolor','none');
    contourf(times(plot_times),frequencies,squeeze(mean(condition_average_eegpower(cond_i,chan_i,:,plot_times),2))-squeeze(mean(condition_average_induced(cond_i,chan_i,:,plot_times),2)),50,'linecolor','none');
    caxis([-5 5]);
    colormap 'jet';
    title(CONDITIONS_NOAR{cond_i});
    
end

% plot ERP on top
hold on
plot(eegpower_all_bl,erpt,'k')

%sanity check
% count=count+length(CONDITIONS_NOAR);
% for cond_i = 1:length(CONDITIONS_NOAR)
%     subplot(3,length(CONDITIONS_NOAR),count+cond_i);
%     contourf(times,frequencies,squeeze(mean(condition_average_eegpower(cond_i,chan_i,:,:),2))-squeeze(mean(real(condition_average_evoked(cond_i,chan_i,:,:)),2)),50,'linecolor','none');
%     %caxis([-5 5]);
%     colormap 'jet';
%     title(CONDITIONS_NOAR{cond_i});
% end

%induced
count=count+length(CONDITIONS_NOAR);
for cond_i = 1:length(CONDITIONS_NOAR)
    subplot(3,length(CONDITIONS_NOAR),count+cond_i);
    contourf(times(plot_times),frequencies,squeeze(mean(condition_average_induced(cond_i,chan_i,:,plot_times),2)),50,'linecolor','none');
    caxis([-1 1]);
    colormap 'jet';
    title(CONDITIONS_NOAR{cond_i});
end

% count=count+length(CONDITIONS_NOAR);
% for cond_i = 1:length(CONDITIONS_NOAR)
%     subplot(4,length(CONDITIONS_NOAR),count+cond_i);
%     %contourf(times,frequencies,squeeze(mean(condition_average_evoked(cond_i,chan_i,:,:),2)),50,'linecolor','none');
%     contourf(times,frequencies,squeeze(mean(condition_average_eegpower(cond_i,chan_i,:,:),2)) - (squeeze(mean(condition_average_eegpower(cond_i,chan_i,:,:),2))-squeeze(mean(condition_average_induced(cond_i,chan_i,:,:),2))),50,'linecolor','none');
%     %caxis([-5 5]);
%     colormap 'jet';
%     title(CONDITIONS_NOAR{cond_i});
% end
%%

CONDITIONS_NOAR  = {'mixrepeat','switchto','switchaway','noninf'};
for condition_i=1:length(CONDITIONS_NOAR)
    tic;
    disp(CONDITIONS_NOAR{condition_i});
    load_name=[SAVEDIR NAMES{1} '_' CONDITIONS_NOAR{1} '_ITPC_bl_arcorrected.mat'];
    temp_participant=load(load_name);
    condition_average_eegpower = zeros(size(temp_participant.eegpower_bl_arcorrected));
    condition_average_itpc = zeros(size(temp_participant.itpc_bl_arcorrected));
    condition_average_ispc = zeros(size(temp_participant.ispc_bl_arcorrected));
    for name_i=1:length(NAMES)
        fprintf('.');
        load_name=[SAVEDIR NAMES{name_i} '_' CONDITIONS_NOAR{condition_i} '_ITPC_bl_arcorrected.mat'];
        temp_participant=load(load_name);
        condition_average_eegpower = condition_average_eegpower + temp_participant.eegpower_bl_arcorrected;
        condition_average_itpc=condition_average_itpc + temp_participant.itpc_bl_arcorrected;
        condition_average_ispc=condition_average_ispc + temp_participant.ispc_bl_arcorrected;
    end
    condition_average_eegpower = condition_average_eegpower/length(NAMES);
    condition_average_itpc = condition_average_itpc/length(NAMES);
    condition_average_ispc = condition_average_ispc/length(NAMES);
    savename=[SAVEDIR CONDITIONS_NOAR{condition_i} '_Average_ITPC_bl_arcorrected.mat'];
    save(savename, 'condition_average_ispc','condition_average_eegpower','condition_average_itpc');
    
     t=toc;
    fprintf('\t%3.2f %s\n',t,'seconds to complete');
end


%% View the plots:
% EEG POWER
chan_i = find(ismember(labels,{'CP1','CPz','CP2','P1','Pz','P2','POz'}));
%chan_i = find(ismember(labels,{'C1','Cz','C2','FC1','FCz','FC2'}));
figure();
for cond_i = 1:length(CONDITIONS_NOAR)
    subplot(2,2,cond_i);
    load_name=[SAVEDIR CONDITIONS_NOAR{cond_i} '_Average_ITPC_bl_arcorrected.mat'];
    load(load_name);
    contourf(times,frequencies,squeeze(mean(condition_average_eegpower(chan_i,:,:),1)),50,'linecolor','none');
    caxis([-5 5]);
    colormap 'jet';
    title(CONDITIONS_NOAR{cond_i});
end

% ITPC POWER
figure();
for cond_i = 1:length(CONDITIONS_NOAR)
    subplot(2,2,cond_i);
    load_name=[SAVEDIR CONDITIONS_NOAR{cond_i} '_Average_ITPC_bl_arcorrected.mat'];
    load(load_name);
    contourf(times,frequencies,squeeze(mean(condition_average_itpc(chan_i,:,:),1)),50,'linecolor','none');
    caxis([-0.1 0.1]);
    colormap 'jet';
    title(CONDITIONS_NOAR{cond_i});
end
name_i=1;
figure();
for chan_i = 1:64
    for cond_i = 1:length(CONDITIONS_NOAR)
        subplot(2,2,cond_i);
        load_name=[SAVEDIR CONDITIONS_NOAR{cond_i} '_Average_ITPC_bl_arcorrected.mat'];
%         load_name=[SAVEDIR NAMES{name_i} '_' CONDITIONS{cond_i} '_ISPC_bl_arcorrected.mat'];
        load(load_name);
        contourf(times,frequencies,squeeze(condition_average_ispc(chan_i,:,:)),50,'linecolor','none');
%         contourf(times,frequencies,abs(squeeze(mean(ispc_bl_arcorrected(chan_i,:,:),1))),50,'linecolor','none');
        caxis([0 100]);
        colormap 'hot';
        title([CONDITIONS{cond_i} ' Connectivity With ' labels{chan_i}]);
    end
    pause(0.1);
end



%% Topology Plots:
addpath(genpath('F:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip-20150902\fieldtrip-20150902'));
addpath(genpath('F:\FNL_EEG_TOOLBOX\PACKAGES\tightsubplot'));
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
%close all;
%0-500MS
times_start = 0;%1400
times_end = 450;%1600
figure();
temp_times = times-times_start;
[~,basetimestart_index] = min(abs(temp_times));
temp_times = times-times_end;
[~,basetimeend_index] = min(abs(temp_times));
times_of_interest = basetimestart_index:basetimeend_index;
%4-7HZ
low_freq=4;%5
high_freq = 8;%7
Frequencybin_Theta = find((floor(frequencies-low_freq)==0),1):find((floor(frequencies-high_freq)==0),1); % 4-7Hz
for cond_i = 1:length(CONDITIONS_NOAR)
    load_name=[SAVEDIR CONDITIONS_NOAR{cond_i} '_Average_ITPC_bl_arcorrected.mat'];
    load(load_name);

    fprintf('Condition: %s\n',CONDITIONS_NOAR{cond_i})
    cfg = [];
    cfg.zlim        = [-5 5];
    cfg.colormap    = 'jet';
    cfg.parameter   = 'avg';
    cfg.layout      = 'biosemi64.lay';
    cfg.comment     = 'no';
    cfg.marker      = '';
    cfg.channel     = labels;
    cfg.contournum  = 4;
    cfg.gridsize    = 600;
    data =[];
    data.avg = zeros(72,1);
    data_temp  = squeeze(mean(mean(condition_average_eegpower(:,Frequencybin_Theta,times_of_interest),3),2));

    data.avg = [data_temp;0;0;0;0;0;0;0;0];
    data.avg = (data.avg);
    clear pval_i
    data.var = zeros(72,1);
    data.time = 1;
    data.label = labels;
    data.dimord = 'chan_time';
    data.cov = zeros(72,72);
    subplot(2,2,cond_i); ft_topoplotER(cfg, data);
    title(CONDITIONS_NOAR{cond_i});
    set(gcf,'Color',[1 1 1]);
end

%ITPC
figure();
for cond_i = 1:length(CONDITIONS_NOAR)
    load_name=[SAVEDIR CONDITIONS_NOAR{cond_i} '_Average_ITPC_bl_arcorrected.mat'];
    load(load_name);

    fprintf('Condition: %s\n',CONDITIONS_NOAR{cond_i})
    cfg = [];
    cfg.zlim        = [0 0.2];
    cfg.colormap    = 'hot';
    cfg.parameter   = 'avg';
    cfg.layout      = 'biosemi64.lay';
    cfg.comment     = 'no';
    cfg.marker      = '';
    cfg.channel     = labels;
    cfg.contournum  = 4;
    cfg.gridsize    = 600;
    data =[];
    data.avg = zeros(72,1);
    data_temp  = squeeze(mean(mean(condition_average_itpc(:,Frequencybin_Theta,times_of_interest),3),2));

    data.avg = [data_temp;0;0;0;0;0;0;0;0];
    data.avg = (data.avg);
    clear pval_i
    data.var = zeros(72,1);
    data.time = 1;
    data.label = labels;
    data.dimord = 'chan_time';
    data.cov = zeros(72,72);
    subplot(2,2,cond_i); ft_topoplotER(cfg, data);
    title(CONDITIONS_NOAR{cond_i});
    set(gcf,'Color',[1 1 1]);
end


%%
times_start = -100:10:1600;
times_end = times_start+50;
figure(); set (gcf, 'Position',[0 0 1920 540], 'Color',[1 1 1]);
count=1;
figtitle = {'Mixed Repeat', 'Switch-to', 'Switch-away', 'Noninformative'};

for time_i = 1:length(times_start)
    
    temp_times = times-times_start(time_i);
    [~,basetimestart_index] = min(abs(temp_times));
    temp_times = times-times_end(time_i);
    [~,basetimeend_index] = min(abs(temp_times));
    times_of_interest = basetimestart_index:basetimeend_index;
    
    %4-8HZ
    low_freq=4;
    high_freq = 8;
    Frequencybin_Theta = find((floor(frequencies-low_freq)==0),1):find((floor(frequencies-high_freq)==0),1); % 4-8Hz
    for cond_i = 1:length(CONDITIONS_NOAR);
        
        load_name=[SAVEDIR CONDITIONS_NOAR{cond_i} '_Average_ITPC_bl_arcorrected.mat'];
        load(load_name);
        
        fprintf('Condition: %s\n',CONDITIONS_NOAR{cond_i})
        cfg = [];
        cfg.zlim        = [-5 5];
        cfg.colormap    = 'jet';
        cfg.parameter   = 'avg';
        cfg.layout      = 'biosemi64.lay';
        cfg.comment     = 'no';
        cfg.marker      = '';
        cfg.channel     = labels;
        %cfg.marker = 'labels';
        %cfg.contournum = 1;
        cfg.contournum  = 4;
        cfg.gridsize    = 600;
        data =[];
        data.avg = zeros(72,1);
        %eval(['currentfile = ' datastruct_cue{(contrasts_i)} ';']);
        %eval(['currentpval = ' datastruct_pval{(contrasts_i)} ';']);
        %eval(['currentpval_corrected = ' datastruct_pval_corrected{(contrasts_i)} ';']);
        % threshold = 0.05;
        % data4plot_ind = find(currentpval<threshold);
        % labels_ind = find(currentpval_corrected<threshold);
        %cfg.highlight = 'on';
        %cfg.highlightchannel = labels(labels_ind);
        %cfg.highlightcolor = [0 0 1]; %blue
        
        %cfg.highlightfontsize = 1; %no label writing
        %cfg.highlightsymbol = 'o';
        data_temp  = squeeze(mean(mean(condition_average_eegpower(:,Frequencybin_Theta,times_of_interest),3),2));
        
        data.avg = [data_temp;0;0;0;0;0;0;0;0];
        %         for i = 1:length(data.avg)
        %             if (data.avg(i) >0)
        %                 data.avg(i) = 0;
        %             end
        %         end
        data.avg = (data.avg);
        clear pval_i
        data.var = zeros(72,1);
        data.time = 1;
        data.label = labels;
        data.dimord = 'chan_time';
        data.cov = zeros(72,72);
        
        ind = cond_i;
        
       %figure();
        set (gcf, 'Position',[0 0 1920 540], 'Color',[1 1 1]);
        subplot(5,5,[ind,ind+5,ind+10]); ft_topoplotER(cfg, data);
        %if contrasts_i ==1
            title (figtitle{cond_i},'FontSize',16);
    end
    ind = ind+1;
    set (gcf, 'Position',[0 0 1920 540], 'Color',[1 1 1]);
    subplot(5,5,[ind,ind+5,ind+10]); colorbar('east','FontSize',16); caxis ([-5 5]); axis square; axis off;  title ('Power(dB)','FontSize',16,'HorizontalAlignment','left');
    %adding sliding timebar
    set (gcf, 'Position',[0 0 1920 540], 'Color',[1 1 1]);
    subplot(5,5,[ind+15:ind+20]); 
    plot(times,ones(1,length(times)).*-1,'k');
    ylim([-1 1]);
    set(gca,'xtick',-1000:500:3500);
    set(gca,'XTickLabels',-1000:500:3500);
    hold on;
    set(gca,'ytick',[]);
    set(gca,'ycolor',[1 1 1])
    fprintf('START: %3.5f, END: %3.5f\n',times(times_of_interest(1)),times(times_of_interest(end)));
    fill([times(times_of_interest(1)) times(times_of_interest(1)) times(times_of_interest(end)) times(times_of_interest(end))],[-1 0 0 -1],'k');
    box off;
    xlabel('Time (ms)');set(gca,'FontSize',16);
 
    
    pause(0.1);
    if count == 1
        colorbar;
    end
    f = getframe(gcf);
    if count == 1
        [im,map] = rgb2ind(f.cdata,256,'nodither');
        im(1,1,1,21) = 0;
    end
    if count ~=1
        im(:,:,1,count-1) = rgb2ind(f.cdata,map,'nodither');
    end
    count = count +1;
    close all;
end
filename = [SAVEDIR 'Theta_Power_Movie_v4.gif'];
imwrite(im,map,filename,'gif','DelayTime',0.3,'LoopCount',inf);

%%

figure();
for cond_i = 1:length(CONDITIONS_NOAR);

load_name=[SAVEDIR CONDITIONS_NOAR{cond_i} '_Average_ITPC_bl_arcorrected.mat'];
load(load_name);

fprintf('Condition: %s\n',CONDITIONS_NOAR{cond_i})
cfg = [];
cfg.zlim        = [0 .1];
cfg.colormap    = 'hot';
cfg.parameter   = 'avg';
cfg.layout      = 'biosemi64.lay';
cfg.comment     = 'no';
cfg.marker      = '';
%cfg.marker = 'labels';
%cfg.contournum = 1;
cfg.contournum  = 4;
cfg.gridsize    = 600;
data =[];
data.avg = zeros(72,1);
%eval(['currentfile = ' datastruct_cue{(contrasts_i)} ';']);
%eval(['currentpval = ' datastruct_pval{(contrasts_i)} ';']);
%eval(['currentpval_corrected = ' datastruct_pval_corrected{(contrasts_i)} ';']);
% threshold = 0.05;
% data4plot_ind = find(currentpval<threshold);
% labels_ind = find(currentpval_corrected<threshold);
%cfg.highlight = 'on';
%cfg.highlightchannel = labels(labels_ind);
%cfg.highlightcolor = [0 0 1]; %blue

%cfg.highlightfontsize = 1; %no label writing
%cfg.highlightsymbol = 'o';
data_temp  = squeeze(mean(mean(condition_average_itpc(:,Frequencybin_Theta,times_of_interest),3),2));

data.avg = [data_temp;0;0;0;0;0;0;0;0];
%         for i = 1:length(data.avg)
%             if (data.avg(i) >0)
%                 data.avg(i) = 0;
%             end
%         end
data.avg = (data.avg);
clear pval_i
data.var = zeros(72,1);
data.time = 1;
data.label = labels;
data.dimord = 'chan_time';
data.cov = zeros(72,72);

ind = cond_i;

subplot(2,2,ind); ft_topoplotER(cfg, data);
set(gcf,'Color',[1 1 1]);
%if contrasts_i ==1

title(['Condition: ',CONDITIONS_NOAR{cond_i}],'FontSize',12);
%end

end
CONDITIONS_NOAR  = {'switchto','switchaway','noninf','mixrepeat'};
figure();
for cond_i = 1:length(CONDITIONS_NOAR);
    
    load_name=[SAVEDIR NAMES{name_i} '_' CONDITIONS{cond_i} '_ISPC_bl_arcorrected.mat'];
    load(load_name);
    
    fprintf('Condition: %s\n',CONDITIONS_NOAR{cond_i})
    cfg = [];
    cfg.zlim        = [0 .1];
    cfg.colormap    = 'hot';
    cfg.parameter   = 'avg';
    cfg.layout      = 'biosemi64.lay';
    cfg.comment     = 'no';
    cfg.marker      = '';
    %cfg.marker = 'labels';
    %cfg.contournum = 1;
    cfg.contournum  = 4;
    cfg.gridsize    = 600;
    data =[];
    data.avg = zeros(72,1);
    %eval(['currentfile = ' datastruct_cue{(contrasts_i)} ';']);
    %eval(['currentpval = ' datastruct_pval{(contrasts_i)} ';']);
    %eval(['currentpval_corrected = ' datastruct_pval_corrected{(contrasts_i)} ';']);
    % threshold = 0.05;
    % data4plot_ind = find(currentpval<threshold);
    % labels_ind = find(currentpval_corrected<threshold);
    %cfg.highlight = 'on';
    %cfg.highlightchannel = labels(labels_ind);
    %cfg.highlightcolor = [0 0 1]; %blue
    
    %cfg.highlightfontsize = 1; %no label writing
    %cfg.highlightsymbol = 'o';
    data_temp  = abs(squeeze(mean(mean(ispc_bl_arcorrected(:,Frequencybin_Theta,times_of_interest),3),2)));
    
    data.avg = [data_temp;0;0;0;0;0;0;0;0];
    %         for i = 1:length(data.avg)
    %             if (data.avg(i) >0)
    %                 data.avg(i) = 0;
    %             end
    %         end
    data.avg = (data.avg);
    clear pval_i
    data.var = zeros(72,1);
    data.time = 1;
    data.label = labels;
    data.dimord = 'chan_time';
    data.cov = zeros(72,72);
    
    ind = cond_i;
    
    subplot(2,2,ind); ft_topoplotER(cfg, data);
    set(gcf,'Color',[1 1 1]);
    %if contrasts_i ==1
    
    title(['Condition: ',CONDITIONS_NOAR{cond_i}],'FontSize',12);
    %end
    
end