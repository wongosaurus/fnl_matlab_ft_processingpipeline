function [rho,p]= runCorrelations(wpms,ivData,dvData)

rho = zeros(1,length(wpms.conditions));
p   = zeros(1,length(wpms.conditions));
for cond_i = 1:length(wpms.conditions)
    [rho(1,cond_i),p(1,cond_i)] = corr(ivData(:,cond_i),dvData(:,cond_i));
end