function cDat = catData(data,n,nDim,squeezeDims,nonsqueezeDims)

% data           = input data that needs to be concatenated
% n              = number of groups
% nDim           = dimension corresponding to groups
% squeezeDims    = [] dimensions that can be 'squeezed over' (i.e., singleton
%                 dimensions)
% nonsqueezeDims = [] dimensions to not squeeze over (i.e., observations),
%                  include groups here

if nargin < 5
    error('Too few inputs, include both data and dimensions to concatenate');
end

%make sure group dimension is set first in the nonsqueezeDims
if nonsqueezeDims(1)~=nDim;
    nDimLoc  =find(nonsqueezeDims == nDim);
    remainLoc=find(nonsqueezeDims ~= nDim);
    nonsqueezeDims = permute(nonsqueezeDims,[nDimLoc remainLoc]);
end
if isempty(squeezeDims)
    orderedData = squeeze(permute(data,[nonsqueezeDims]));
    cDat = [];
else
    orderedData = squeeze(permute(data,[squeezeDims,nonsqueezeDims]));
    cDat = [];
end

for group_i = 1:n
    cDat = [cDat squeeze(orderedData(group_i,:))];
end
