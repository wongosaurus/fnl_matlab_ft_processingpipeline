%% run pixel-wise anovas and correlations
DATADIR = 'F:\FNL_EEG_TOOLBOX\EEGLAB_FORMAT\';
wpms.dirs.PACKAGES = 'E:\FNL_EEG_TOOLBOX\PACKAGES\';
CONDITIONS  = {'switchto','switchaway','noninfrepeat','noninfswitch','mixrepeat','allrepeat'};
 
listings = dir('E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\*mixrepeat_ITPC.mat');
NAMES = {length(listings)};
for file_i =1:length(listings)
    NAMES{file_i} = listings(file_i).name(1:6);
end

addpath(genpath('E:\FNL_EEG_TOOLBOX\FUNCTIONS\'))
datain = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\FrequencyMatrices\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
% load in RT variability data
filenameAV = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\meansITC.mat';
filenameSD = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\stdevITC.mat';
load('E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\good_inds_v8_170117.mat')
[control_sdevs]=createCVmeasure(NAMES,filenameAV,filenameSD,good_inds);
%collapse across noninfs
load('E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\FrequencyMatrices\Behaviour\Behavioural_Data.mat')
% dvData for correlations
dvData = [CVdata(:,1),control_sdevs(:,2),...
                 control_sdevs(:,5),[control_sdevs(:,3)+control_sdevs(:,4)]./2];
%frequencies and times
frequencies = logspace(log10(2),log10(30),80);
times = (-1:0.001953125000000:3.5)*1000;

wpms.conditions    ={'switchto','switchaway','mixrepeat','noninf'};
%%
frontal_cluster = {'FCz','FC1','FC2','Fz','F1','F2'};%frontal
parietal_cluster = {'CPz','CP1','CP2','Pz','P1','P2'};%parietal
clusters = {frontal_cluster,parietal_cluster};
cluster_names = {'frontal','parietal'};
%begin by first averaging across electrodes of interest
for cluster_i = 1:length(clusters)
    cluster= clusters{cluster_i};
    cluster_name = cluster_names{cluster_i};
    
    electrodes_of_interest = find(ismember(labels,cluster));
    ERSPfilename = [datain 'ERSP_' num2str(1) '.mat'];
    load(ERSPfilename);%for preallocation purposes
    %storage for anova p and f values
    pfStat_ERSP = zeros(length(frequencies),size(ERSP_F_t,2));
    fStat_ERSP = pfStat_ERSP;
    pfStat_ITPC = pfStat_ERSP;
    fStat_ITPC = fStat_ERSP;
    %storage for correlation p and r values
    prStat_ERSP = zeros(length(frequencies),size(ERSP_F_t,2),length(wpms.conditions));
    rStat_ERSP = prStat_ERSP;
    prStat_ITPC = prStat_ERSP;
    rStat_ITPC = rStat_ERSP;

    for freq_i = 1:length(frequencies)
        fprintf('\n%s\t%s','Frequency:',num2str(freq_i));
        tic;
        %average across clusters storage variable
        ERSPfilename = [datain 'ERSP_' num2str(freq_i) '.mat'];
        load(ERSPfilename);
        ITPCfilename = [datain 'ITPC_' num2str(freq_i) '.mat'];
        load(ITPCfilename);
        %dimension for order of data for input to anova
        n    = length(wpms.conditions);%number of conditions
        nDim = 5;%where conditions are stored in the multidimensional matrix
        squeezeDims = [1 2 3];%singleton dimensions that we ignore
        nonsqueezeDims = [5 4];%non-singleton dimensions that we care about

        %average across cluster
        ERSP_cluster = mean(ERSP_F_t(:,:,electrodes_of_interest,:,1:4),3);
        ITPC_cluster = mean(ITPC_F_t(:,:,electrodes_of_interest,:,1:4),3);
        %now control for all-repeat by removing that condition
        %ERSP_cluster = bsxfun(@minus,ERSP_cluster,ERSP_cluster(:,:,:,:,5));
        %ITPC_cluster = bsxfun(@minus,ITPC_cluster,ITPC_cluster(:,:,:,:,5));
        for time_i = 1:size(ERSP_cluster,2)
            if ~mod(time_i,100)
                fprintf('.');
            end
            %organise data and run through one-way anova
            data_ERSP = ERSP_cluster(:,time_i,:,:,:);
            data_ITPC = ITPC_cluster(:,time_i,:,:,:);
            cDat_ERSP = catData(data_ERSP,n,nDim,squeezeDims,nonsqueezeDims);%reorder ERSP data for anova
            cDat_ITPC = catData(data_ITPC,n,nDim,squeezeDims,nonsqueezeDims);%reorder ITPC data for anova
            conditionLabels = cell(size(cDat_ERSP))';
            conditionLabels=makeLabels(wpms,conditionLabels);%labels for anova
            [pfStat_ERSP(freq_i,time_i),tbl,~] = anova1(cDat_ERSP,conditionLabels,'off');%run anova
            fStat_ERSP(freq_i,time_i)= double(tbl{2,5});clear tbl;
            [pfStat_ITPC(freq_i,time_i),tbl,~] = anova1(cDat_ITPC,conditionLabels,'off');%run anova
            fStat_ITPC(freq_i,time_i)= double(tbl{2,5});clear tbl;
            %organise data and run through correlation
            corrData_ERSP = squeeze(data_ERSP);
            corrData_ITPC = squeeze(data_ITPC);
            [rStat_ERSP(freq_i,time_i,:),prStat_ERSP(freq_i,time_i,:)]= runCorrelations(wpms,corrData_ERSP,dvData);
            [rStat_ITPC(freq_i,time_i,:),prStat_ITPC(freq_i,time_i,:)]= runCorrelations(wpms,corrData_ITPC,dvData);
            clear data_ERSP data_ITPC cDat* conditionLabels corrData*
        end
        t=toc;fprintf('\t%2.2f %s',t,'seconds to complete');
    end%freq_i loop

    %save ERSP
    savename_fstat  = [datain 'fstat_ERSP_noAR_' cluster_name '_20170216.mat'];save(savename_fstat,'fStat_ERSP','-v7.3');
    savename_pfstat = [datain 'pfstat_ERSP_noAR_' cluster_name '_20170216.mat'];save(savename_pfstat,'pfStat_ERSP','-v7.3');
    savename_rstat  = [datain 'rstat_ERSP_noAR_' cluster_name '_20170216.mat'];save(savename_rstat,'rStat_ERSP','-v7.3');
    savename_prstat = [datain 'prstat_ERSP_noAR_' cluster_name '_20170216.mat'];save(savename_prstat,'prStat_ERSP','-v7.3');
    %save ITPC
    savename_fstat  = [datain 'fstat_ITPC_noAR_' cluster_name '_20170216.mat'];save(savename_fstat,'fStat_ITPC','-v7.3');
    savename_pfstat = [datain 'pfstat_ITPC_noAR_' cluster_name '_20170216.mat'];save(savename_pfstat,'pfStat_ITPC','-v7.3');
    savename_rstat  = [datain 'rstat_ITPC_noAR_' cluster_name '_20170216.mat'];save(savename_rstat,'rStat_ITPC','-v7.3');
    savename_prstat = [datain 'prstat_ITPC_noAR_' cluster_name '_20170216.mat'];save(savename_prstat,'prStat_ITPC','-v7.3');
end
%% visualise effects
%load statistical spaces in
cluster_names = {'frontal','parietal'};
figure();set(gcf,'Color',[1 1 1],'Position',[0 0 1920 1080]);
count=0;
for cluster_i = 1:length(cluster_names)
    
    %load ERSP
    savename_fstat  = [datain 'fstat_ERSP_noAR_' cluster_names{cluster_i} '_20170216.mat'];load(savename_fstat);
    savename_pfstat = [datain 'pfstat_ERSP_noAR_' cluster_names{cluster_i} '_20170216.mat'];load(savename_pfstat);
    savename_rstat  = [datain 'rstat_ERSP_noAR_' cluster_names{cluster_i} '_20170216.mat'];load(savename_rstat);
    savename_prstat = [datain 'prstat_ERSP_noAR_' cluster_names{cluster_i} '_20170216.mat'];load(savename_prstat);
    %load ITPC
    savename_fstat  = [datain 'fstat_ITPC_noAR_' cluster_names{cluster_i} '_20170216.mat'];load(savename_fstat);
    savename_pfstat = [datain 'pfstat_ITPC_noAR_' cluster_names{cluster_i} '_20170216.mat'];load(savename_pfstat);
    savename_rstat  = [datain 'rstat_ITPC_noAR_' cluster_names{cluster_i} '_20170216.mat'];load(savename_rstat);
    savename_prstat = [datain 'prstat_ITPC_noAR_' cluster_names{cluster_i} '_20170216.mat'];load(savename_prstat);
    % set up plot times - no need to show or correct for additional times
    plottime_start = -300;%start of plot
    plottime_end = 1100;%end of plot
    %find values
    temp_times = times-plottime_start;
    [~,plottimestart_index] = min(abs(temp_times));
    temp_times = times-plottime_end;
    [~,plottimeend_index] = min(abs(temp_times));
    %store in a single variable
    plot_times = plottimestart_index:plottimeend_index;
    % apply fdr correction
    addpath(genpath([wpms.dirs.PACKAGES 'mass_uni_toolbox']))
    alpha = .05;
    %ERSP
    [pfStatCorr_ERSP,~]=fdr_bky(pfStat_ERSP(:,plot_times),alpha);%anova p
    [prStatCorr_ERSP,~]=fdr_bky(prStat_ERSP(:,plot_times,:),alpha);%corr p
    %ITPC
    [pfStatCorr_ITPC,~]=fdr_bky(pfStat_ITPC(:,plot_times),alpha);%anova p
    [prStatCorr_ITPC,~]=fdr_bky(prStat_ITPC(:,plot_times,:),alpha);%corr p
    

    %anova
    %ERSP
    count=count+1;
    subplot(2,2,count);
    contourf(times(plot_times),frequencies,fStat_ERSP(:,plot_times),50,'linecolor','none');colormap jet;caxis([-10 10]);
    hold on;
    contour(times(plot_times),frequencies,pfStatCorr_ERSP,1,'k');axis square;
    title([cluster_names{cluster_i} ' F-Map ERSP']);
    count=count+1;
    %ITPC
    subplot(2,2,count);
    contourf(times(plot_times),frequencies,fStat_ITPC(:,plot_times),50,'linecolor','none');colormap jet;caxis([-10 10]);
    hold on;
    contour(times(plot_times),frequencies,pfStatCorr_ITPC,1,'k');axis square;
    title([cluster_names{cluster_i} ' F-Map ITPC']);
    
    %%%%%
    count = count-1;
    %find significant clusters
    width = 60;  %Time Bins
    height = 6; %Frequency Bins:
    ERSP_clusters.(cluster_names{cluster_i})=bwconncomp(pfStatCorr_ERSP);%find significant clusters
    ITPC_clusters.(cluster_names{cluster_i})=bwconncomp(pfStatCorr_ITPC);%find significant clusters
    for pixel_i = 1:length(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxList)
        reduced_data = fStat_ERSP(:,plot_times);
        [m,ind]=max(reduced_data(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxList{pixel_i}));
        [freq_max,time_max] =find (reduced_data==m);
        if times(plot_times(time_max))> 1000 || frequencies(freq_max)< 4|| frequencies(freq_max)>20
            ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
            continue;
        end
        if numel(reduced_data(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxList{pixel_i})) < 100 %(width+1)*(height+1)
            ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
            continue;
        end
        subplot(2,2,count);
        scatter(times(plot_times(time_max)),frequencies(freq_max),10,'k','filled');
        x = times(plot_times(time_max - width/2));
        width_t = times(plot_times(time_max + width/2)) - x;
        y = frequencies(freq_max - height/2);
        height_f = frequencies(freq_max + height/2) - y;
        hold on;
        rectangle('Position',[x,y,width_t,height_f]);
        
        ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [freq_max,time_max];
    end
    count = count +1;
    for pixel_i = 1:length(ITPC_clusters.(cluster_names{cluster_i}).PixelIdxList)
        reduced_data = fStat_ITPC(:,plot_times);
        [m,ind]=max(reduced_data(ITPC_clusters.(cluster_names{cluster_i}).PixelIdxList{pixel_i}));
        [freq_max,time_max] =find (reduced_data==m);
        if times(plot_times(time_max))> 1000 || frequencies(freq_max)< 4 || frequencies(freq_max)>20
            ITPC_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
            continue;
        end
        if numel(reduced_data(ITPC_clusters.(cluster_names{cluster_i}).PixelIdxList{pixel_i})) < 100%(width+1)*(height+1)
            ITPC_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
            continue;
        end
        
        subplot(2,2,count);
        scatter(times(plot_times(time_max)),frequencies(freq_max),10,'k','filled');
        x = times(plot_times(time_max - width/2));
        width_t = times(plot_times(time_max + width/2)) - x;
        y = frequencies(freq_max - height/2);
        height_f = frequencies(freq_max + height/2) - y;
        hold on;
        rectangle('Position',[x,y,width_t,height_f]);
        
        ITPC_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [freq_max,time_max];
    end
    
end
SAVEDIR = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\';
LOADDIR = [SAVEDIR 'FrequencyMatrices' filesep];
save([LOADDIR 'CLUSTER_MaxPixelLoc_noAR.mat'],'*_clusters','-v7.3');

%% LOAD DATA FOR TOPOPLOT: FMAP:

%ERSP
width = 60;  %Time Bins
height = 6; %Frequency Bins:
ERSP_DATA = zeros(height+1,2305,64,185,5,'single');
ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND = zeros(length(cluster_names),...
    length(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxList),...
                                        64,185,5,'single');
for cluster_i = 1:length(cluster_names)
    fprintf('ERSP Working on %s\n',cluster_names{cluster_i} );
    for pixel_i = 1:length(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxList)
        fprintf('\tWorking on Pixel Index: %i\n',pixel_i );
        Location = ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i};
        if isempty(Location)
            continue;
        end
        freq_max = Location(1);
        time_max = Location(2);
        count = 1;
        for freq_i = freq_max-height/2:1:freq_max+height/2
            fprintf('\t\tWorking on Frequency Index: %i\n',freq_i );
            load([LOADDIR 'ERSP_' num2str(freq_i) '.mat']);
            ERSP_DATA(count,:,:,:,:) = ERSP_F_t;
            count = count+1;
        end
        fprintf('\tAveraging over Frequencing and Time\n' );
        timewindows = plot_times(time_max-width/2):1:plot_times(time_max+width/2);
        ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,:,:,:) = squeeze(mean(squeeze(mean(ERSP_DATA(:,timewindows,:,:,:),1)),1));
    end
    fprintf('ERSP Finnished %s\n',cluster_names{cluster_i} );
end
clear ERSP_DATA;
ITPC_DATA = zeros(height+1,2305,64,185,5,'single');
ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND = zeros(length(cluster_names),...
    length(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxList),...
                                        64,185,5,'single');
for cluster_i = 1:length(cluster_names)
    fprintf('ITPC Working on %s\n',cluster_names{cluster_i} );
    for pixel_i = 1:length(ITPC_clusters.(cluster_names{cluster_i}).PixelIdxList)
        fprintf('\tWorking on Pixel Index: %i\n',pixel_i );
        Location = ITPC_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i};
        if isempty(Location)
            continue;
        end
        freq_max = Location(1);
        time_max = Location(2);
        count = 1;
        for freq_i = freq_max-height/2:1:freq_max+height/2
            fprintf('\t\tWorking on Frequency Index: %i\n',freq_i );
            load([LOADDIR 'ITPC_' num2str(freq_i) '.mat']);
            ITPC_DATA(count,:,:,:,:) = ITPC_F_t;
            count = count+1;
        end
        fprintf('\tAveraging over Frequencing and Time\n' );
        timewindows = plot_times(time_max-width/2):1:plot_times(time_max+width/2);
        ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,:,:,:) = squeeze(mean(squeeze(mean(ITPC_DATA(:,timewindows,:,:,:),1)),1));
    end
    fprintf('ITPC Finnished %s\n',cluster_names{cluster_i} );
end
clear ITPC_DATA;                                  
save([LOADDIR 'CLUSTER_PIXEL_CHANNEL_SUB_COND_noAR.mat'],'*_CLUSTER_PIXEL_CHANNEL_SUB_COND','-v7.3');

%% ANOVA:
% Perform an ANOVA(SUB,CHAN) for CLUSTER_PIXEL_COND on the
% CLUSTER_PIXEL_CHANNEL_SUB_COND, after AR Baseline: COND(5)

%dimension for order of data for input to anova
    n    = length(wpms.conditions);%number of conditions
    nDim = 5;%where conditions are stored in the multidimensional matrix
    squeezeDims = [1 2 3];%singleton dimensions that we ignore
    nonsqueezeDims = [5 4];%non-singleton dimensions that we care about
%ERSP:

%Set up output variables:
cluster_pfStat_ERSP = zeros(size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,1),...
                            size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,2),...
                            size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,3));  
cluster_fStat_ERSP = cluster_pfStat_ERSP;
cluster_rStat_ERSP = zeros(size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,1),...
                            size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,2),...
                            size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,3),...
                            size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,5)-1) ;
cluster_prStat_ERSP = cluster_rStat_ERSP;
%dimension for order of data for input to anova
n    = length(wpms.conditions);%number of conditions
nDim = 5;%where conditions are stored in the multidimensional matrix
squeezeDims = [1 2 3];%singleton dimensions that we ignore
nonsqueezeDims = [5 4];%non-singleton dimensions that we care about

for cluster_i = 1:size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,1)
    for pixel_i = 1:size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,2)
        %Check if Pixel is valid:
        if ~any(any(any(squeeze(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,:,:,:))))) %3Dimensional Check to see if There exists any data.
            fprintf('Data at Cluster: %i Pixel: %i, is not valid, Moving on...\n',cluster_i,pixel_i);
            continue;
        end
        fprintf('Working on Data at Cluster: %i Pixel: %i, ...\n',cluster_i,pixel_i);
        fprintf('Pixel Location: FreqBin %i, TimeBin %i, Freq %3.5f Hz, Time %3.5f ms \n',...
            ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i}(1),...
            ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i}(2),...
            frequencies(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i}(1)),...
            times(plot_times(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i}(2))));
        
        for chan_i = 1:size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,3)
%             ANOVA_DATA = bsxfun(@minus,...
%                                 ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
%                                 ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
            ANOVA_DATA = ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4);
            ANOVA_DATA_t = catData(ANOVA_DATA,n,nDim,squeezeDims,nonsqueezeDims);%reorder ERSP data for anova

            conditionLabels = cell(size(ANOVA_DATA_t))';
            conditionLabels=makeLabels(wpms,conditionLabels);%labels for anova
            [cluster_pfStat_ERSP(cluster_i,pixel_i,chan_i),tbl,~] = anova1(ANOVA_DATA_t,conditionLabels,'off');%run anova
            cluster_fStat_ERSP(cluster_i,pixel_i,chan_i)= double(tbl{2,5});clear tbl;

            %organise data and run through correlation
            corrANOVA_DATA = squeeze(ANOVA_DATA);
        
            [cluster_rStat_ERSP(cluster_i,pixel_i,chan_i,:),cluster_prStat_ERSP(cluster_i,pixel_i,chan_i,:)]= runCorrelations(wpms,corrANOVA_DATA,dvData);
        end
        %clear data_ERSP data_ITPC cDat* conditionLabels corrData*
    end
end

%ITPC:

%Set up output variables:
cluster_pfStat_ITPC = zeros(size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,1),...
                            size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,2),...
                            size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,3));  
cluster_fStat_ITPC = cluster_pfStat_ITPC;
cluster_rStat_ITPC = zeros(size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,1),...
                            size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,2),...
                            size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,3),...
                            size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,5)-1) ;
cluster_prStat_ITPC = cluster_rStat_ITPC;
%dimension for order of data for input to anova
n    = length(wpms.conditions);%number of conditions
nDim = 5;%where conditions are stored in the multidimensional matrix
squeezeDims = [1 2];%singleton dimensions that we ignore
nonsqueezeDims = [5 3 4];%non-singleton dimensions that we care about

for cluster_i = 1:size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,1)
    for pixel_i = 1:size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,2)
        %Check if Pixel is valid:
        if ~any(any(any(squeeze(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,:,:,:))))) %3Dimensional Check to see if There exists any data.
            fprintf('Data at Cluster: %i Pixel: %i, is not valid, Moving on...\n',cluster_i,pixel_i);
            continue;
        end
        fprintf('Working on Data at Cluster: %i Pixel: %i, ...\n',cluster_i,pixel_i);
        fprintf('Pixel Location: FreqBin %i, TimeBin %i, Freq %3.5f Hz, Time %3.5f ms \n',...
            ITPC_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i}(1),...
            ITPC_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i}(2),...
            frequencies(ITPC_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i}(1)),...
            times(plot_times(ITPC_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i}(2))));
        for chan_i = 1:size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,3)
%             ANOVA_DATA = bsxfun(@minus,...
%                                 ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
%                                 ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
            ANOVA_DATA = ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4);
            ANOVA_DATA_t = catData(ANOVA_DATA,n,nDim,squeezeDims,nonsqueezeDims);%reorder ERSP data for anova

            conditionLabels = cell(size(ANOVA_DATA_t))';
            conditionLabels=makeLabels(wpms,conditionLabels);%labels for anova
            [cluster_pfStat_ITPC(cluster_i,pixel_i,chan_i),tbl,~] = anova1(ANOVA_DATA_t,conditionLabels,'off');%run anova
            cluster_fStat_ITPC(cluster_i,pixel_i,chan_i)= double(tbl{2,5});clear tbl;

            %organise data and run through correlation
            corrANOVA_DATA = squeeze(ANOVA_DATA);
        
            [cluster_rStat_ITPC(cluster_i,pixel_i,chan_i,:),cluster_prStat_ITPC(cluster_i,pixel_i,chan_i,:)]= runCorrelations(wpms,corrANOVA_DATA,dvData);
        end
        %clear data_ERSP data_ITPC cDat* conditionLabels corrData*
    end
end
save([LOADDIR 'CLUSTER_PIXEL_CHAN_COND_STATS_noAR.mat'],'cluster_*','-v7.3');

%% Plot Topology: For each Cluster Pixel:
close all;

addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));

labels = {'FP1';'AF7';'AF3';'F1';'F3';'F5';'F7';'FT7';'FC5';'FC3'; ...
              'FC1';'C1';'C3';'C5';'T7';'TP7';'CP5';'CP3';'CP1';'P1';'P3'; ...
              'P5';'P7';'P9';'PO7';'PO3';'O1';'Iz';'Oz';'POz';'Pz';'CPz'; ...
              'Fpz';'Fp2';'AF8';'AF4';'AFz';'Fz';'F2';'F4';'F6';'F8';'FT8'; ...
              'FC6';'FC4';'FC2';'FCz';'Cz';'C2';'C4';'C6';'T8';'TP8';'CP6'; ...
              'CP4';'CP2';'P2';'P4';'P6';'P8';'P10';'PO8';'PO4';'O2';'M1'; ...
              'M2';'LO1';'LO2';'SO1';'SO2';'IO1';'IO2';};

cfg = [];
cfg.zlim = [-10.0 10.0];
cfg.colormap = 'jet';
cfg.parameter   = 'avg';
cfg.layout      = 'biosemi64.lay';
cfg.comment   = 'no';
cfg.marker = 'off';
cfg.markersize = 4;
cfg.contournum = 1;
cfg.gridsize = 600;
cfg.highlight = 'on';
cfg.highlightfontsize = 1; %no label writing
cfg.highlightsymbol = '*';
cfg.highlightcolor = [0 0 1]; %blue
data =[];


%ERSP:
for cluster_i = 1:size(cluster_fStat_ERSP,1)
    for pixel_i = 1:size(cluster_fStat_ERSP,2)
        if ~any(any(any(squeeze(cluster_fStat_ERSP(cluster_i,pixel_i,:,:,:))))) %3Dimensional Check to see if There exists any data.
            fprintf('Data at Cluster: %i Pixel: %i, is not valid, Moving on...\n',cluster_i,pixel_i);
            continue;
        end
        title_str=sprintf('Topology at ERSP Peak Location: \n Frequency %3.3f Hz, Time %3.3f ms',...
                    frequencies(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i}(1)),...
                    times(plot_times(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i}(2))) );
        figure();set(gcf,'Color',[1 1 1],'Position',[0 0 500 500]);        
        suptitle(title_str); %super title
        %for cond_i = 1:size(cluster_rStat_ERSP,4)
        %    h=subplot(2,2,cond_i);
        
        plot_data = double(squeeze(cluster_fStat_ERSP(cluster_i,pixel_i,:)));
        plot_p_data = double(squeeze(cluster_pfStat_ERSP(cluster_i,pixel_i,:)));
        data.avg = zeros(72,1);
        data.avg = [plot_data;0;0;0;0;0;0;0;0];
        clear pval_i
        data.var = zeros(72,1);
        data.time = 1;
        data.label = labels;
        data.dimord = 'chan_time';
        data.cov = zeros(72,72);

        threshold = 0.05;
        plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
        plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
%         data4plot_ind = find(plot_p_data<threshold);
%         cfg.highlightchannel = labels(data4plot_ind);
        cfg.highlightchannel = labels(plot_p_data_corr==1);

        ft_topoplotER(cfg, data);

        saveas(gcf,['ERSP_cluster' num2str(cluster_i) '_pixel' num2str(pixel_i) 'Ftopo_noAR.jpg'],'jpeg');
        close;
    end
end

%ITPC:
for cluster_i = 1:size(cluster_fStat_ITPC,1)
    for pixel_i = 1:size(cluster_fStat_ITPC,2)
        if ~any(any(any(squeeze(cluster_fStat_ITPC(cluster_i,pixel_i,:,:,:))))) %3Dimensional Check to see if There exists any data.
            fprintf('Data at Cluster: %i Pixel: %i, is not valid, Moving on...\n',cluster_i,pixel_i);
            continue;
        end
        title_str=sprintf('Topology at ITPC Peak Location: \n Frequency %3.3f Hz, Time %3.3f ms',...
                    frequencies(ITPC_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i}(1)),...
                    times(plot_times(ITPC_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i}(2))) );
        figure();set(gcf,'Color',[1 1 1],'Position',[0 0 500 500]);        
        suptitle(title_str); %super title
        %for cond_i = 1:size(cluster_rStat_ITPC,4)
        %    h=subplot(2,2,cond_i);
        
        plot_data = double(squeeze(cluster_fStat_ITPC(cluster_i,pixel_i,:)));
        plot_p_data = double(squeeze(cluster_pfStat_ITPC(cluster_i,pixel_i,:)));
        data.avg = zeros(72,1);
        data.avg = [plot_data;0;0;0;0;0;0;0;0];
        clear pval_i
        data.var = zeros(72,1);
        data.time = 1;
        data.label = labels;
        data.dimord = 'chan_time';
        data.cov = zeros(72,72);

        threshold = 0.05;
        plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
        plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
%         data4plot_ind = find(plot_p_data<threshold);
%         cfg.highlightchannel = labels(data4plot_ind);
        cfg.highlightchannel = labels(plot_p_data_corr==1);

        ft_topoplotER(cfg, data);

        saveas(gcf,['ITPC_cluster' num2str(cluster_i)  '_pixel' num2str(pixel_i) 'Ftopo_noAR.jpg'],'jpeg');
        close;
    end
end

%% Plot Topology of Rstat (correlation with behaviour) per each cluster,
% Mark Significant Electrodes (prstat)
% State whether a significant main effect: cluster_pixel (cluster_fstat*)
% In a subplot(2,2,i), of Conditions,
close all;

addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));

labels = {'FP1';'AF7';'AF3';'F1';'F3';'F5';'F7';'FT7';'FC5';'FC3'; ...
              'FC1';'C1';'C3';'C5';'T7';'TP7';'CP5';'CP3';'CP1';'P1';'P3'; ...
              'P5';'P7';'P9';'PO7';'PO3';'O1';'Iz';'Oz';'POz';'Pz';'CPz'; ...
              'Fpz';'Fp2';'AF8';'AF4';'AFz';'Fz';'F2';'F4';'F6';'F8';'FT8'; ...
              'FC6';'FC4';'FC2';'FCz';'Cz';'C2';'C4';'C6';'T8';'TP8';'CP6'; ...
              'CP4';'CP2';'P2';'P4';'P6';'P8';'P10';'PO8';'PO4';'O2';'M1'; ...
              'M2';'LO1';'LO2';'SO1';'SO2';'IO1';'IO2';};

cfg = [];
cfg.zlim = [0 0.4];
cfg.colormap = 'hot';
cfg.parameter   = 'avg';
cfg.layout      = 'biosemi64.lay';
cfg.comment   = 'no';
cfg.marker = 'off';
cfg.contournum = 1;
cfg.gridsize = 600;
cfg.highlight = 'on';
cfg.highlightfontsize = 1; %no label writing
cfg.highlightsymbol = 'o';
cfg.highlightcolor = [0 0 1]; %blue
data =[];

%ERSP:
for cluster_i = 1:size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,1)
    for pixel_i = 1:size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,2)
        if ~any(any(any(squeeze(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,:,:,:))))) %3Dimensional Check to see if There exists any data.
            fprintf('Data at Cluster: %i Pixel: %i, is not valid, Moving on...\n',cluster_i,pixel_i);
            continue;
        end
        title_str=sprintf('Topology at ERSP vs RT Peak Location: \n Frequency %3.3f Hz, Time %3.3f ms \n F-Stat: %3.3f P-value: %3.3f',...
                    frequencies(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i}(1)),...
                    times(plot_times(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i}(2))),...
                    cluster_fStat_ERSP(cluster_i,pixel_i),...
                    cluster_pfStat_ERSP(cluster_i,pixel_i));
        figure();set(gcf,'Color',[1 1 1],'Position',[0 0 1920 1080]);        
        suptitle(title_str); %super title
        for cond_i = 1:size(cluster_rStat_ERSP,4)
            h=subplot(2,2,cond_i);
            plot_data = squeeze(cluster_rStat_ERSP(cluster_i,pixel_i,:,cond_i));
            plot_p_data = squeeze(cluster_prStat_ERSP(cluster_i,pixel_i,:,cond_i));
            %topoplot: 
            data.avg = zeros(72,1);
            %eval(['currentfile = ' datastruct_cue{(contrasts_i)} ';']);
            %eval(['currentpval = ' datastruct_pval{(contrasts_i)} ';']);
            %eval(['currentpval_corrected = ' datastruct_pval_corrected{(contrasts_i)} ';']);
            threshold = 0.05;
            data4plot_ind = find(plot_p_data<threshold);
            %labels_ind = find(currentpval_corrected<threshold);

            cfg.highlightchannel = labels(data4plot_ind);
            data4plot = zeros(1,64);
            data4plot(data4plot_ind)=plot_data(data4plot_ind);
            data.avg = abs([data4plot';0;0;0;0;0;0;0;0]);
            clear pval_i
            data.var = zeros(72,1);
            data.time = 1;
            data.label = labels;
            data.dimord = 'chan_time';
            data.cov = zeros(72,72);
        
            ft_topoplotER(cfg, data);
            title([wpms.conditions{cond_i}]);
            %title(h,[wpms.conditions{cond_i}]);
        end
        
    end
end

%ITPC:
for cluster_i = 1:size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,1)
    for pixel_i = 1:size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,2)
        if ~any(any(any(squeeze(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,:,:,:))))) %3Dimensional Check to see if There exists any data.
            fprintf('Data at Cluster: %i Pixel: %i, is not valid, Moving on...\n',cluster_i,pixel_i);
            continue;
        end
        title_str=sprintf('Topology at ITPC vs RT Peak Location: \n Frequency %3.3f Hz, Time %3.3f ms \n F-Stat: %3.3f P-value: %3.3f',...
                    frequencies(ITPC_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i}(1)),...
                    times(plot_times(ITPC_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i}(2))),...
                    cluster_fStat_ITPC(cluster_i,pixel_i),...
                    cluster_pfStat_ITPC(cluster_i,pixel_i));
        figure();set(gcf,'Color',[1 1 1],'Position',[0 0 1920 1080]);        
        suptitle(title_str); %super title
        for cond_i = 1:size(cluster_rStat_ITPC,4)
            h=subplot(2,2,cond_i);
            plot_data = squeeze(cluster_rStat_ITPC(cluster_i,pixel_i,:,cond_i));
            plot_p_data = squeeze(cluster_prStat_ITPC(cluster_i,pixel_i,:,cond_i));
            %topoplot: 
            data.avg = zeros(72,1);
            %eval(['currentfile = ' datastruct_cue{(contrasts_i)} ';']);
            %eval(['currentpval = ' datastruct_pval{(contrasts_i)} ';']);
            %eval(['currentpval_corrected = ' datastruct_pval_corrected{(contrasts_i)} ';']);
            threshold = 0.05;
            data4plot_ind = find(plot_p_data<threshold);
            %labels_ind = find(currentpval_corrected<threshold);

            cfg.highlightchannel = labels(data4plot_ind);
            data4plot = zeros(1,64);
            data4plot(data4plot_ind)=plot_data(data4plot_ind);
            data.avg = abs([data4plot';0;0;0;0;0;0;0;0]);
            clear pval_i
            data.var = zeros(72,1);
            data.time = 1;
            data.label = labels;
            data.dimord = 'chan_time';
            data.cov = zeros(72,72);
        
            ft_topoplotER(cfg, data);
            title([wpms.conditions{cond_i}]);
            %title(h,[wpms.conditions{cond_i}]);
        end
        
    end
end


%% show significant clusters
figure();set(gcf,'Color',[1 1 1],'Position',[0 0 1920 1080]);
subplot(2,2,1);
contourf(times(plot_times),frequencies,labelmatrix(ERSP_clusters.frontal))
axis square;
subplot(2,2,2);
contourf(times(plot_times),frequencies,labelmatrix(ITPC_clusters.frontal))
axis square;
subplot(2,2,3);
contourf(times(plot_times),frequencies,labelmatrix(ERSP_clusters.parietal))
axis square;
subplot(2,2,4);
contourf(times(plot_times),frequencies,labelmatrix(ITPC_clusters.parietal))
axis square;
%% create topography plots for clusters
%first find common significant pixels
figure();
ERSP_pixels = [];
for cluster_i = 1:length(ERSP_clusters.frontal.PixelIdxList)
    ERSP_pixels = [ERSP_pixels ; cell2mat(ERSP_clusters.frontal.PixelIdxList(cluster_i))];
end
for cluster_i = 1:length(ERSP_clusters.parietal.PixelIdxList);
    ERSP_pixels = [ERSP_pixels ; cell2mat(ERSP_clusters.parietal.PixelIdxList(cluster_i))];
end

pixels=unique(ERSP_pixels);
repeat_pixels_ERSP = [];
for pixel_i =1:length(pixels)
    if length(find(ERSP_pixels==pixels(pixel_i)))>1
        repeat_pixels_ERSP = [repeat_pixels_ERSP pixels(pixel_i)];
    end
end
%ITPC
ITPC_pixels = [];
for cluster_i = 1:length(ITPC_clusters.frontal.PixelIdxList)
    ITPC_pixels = [ITPC_pixels ; cell2mat(ITPC_clusters.frontal.PixelIdxList(cluster_i))];
end
for cluster_i = 1:length(ITPC_clusters.parietal.PixelIdxList);
    ITPC_pixels = [ITPC_pixels ; cell2mat(ITPC_clusters.parietal.PixelIdxList(cluster_i))];
end

pixels=unique(ITPC_pixels);
repeat_pixels_ITPC = [];
for pixel_i =1:length(pixels)
    if length(find(ITPC_pixels==pixels(pixel_i)))>1
        repeat_pixels_ITPC = [repeat_pixels_ITPC pixels(pixel_i)];
    end
end
% show common pixels (debugging)
blank_data = zeros(ERSP_clusters.frontal.ImageSize);
blank_data(repeat_pixels_ERSP)=1;
contourf(times(plot_times),frequencies,fStat_ERSP(:,plot_times),50,'linecolor','none');hold on;
contour(times(plot_times),frequencies,blank_data,1,'linecolor','k');
%%
%correlations
%ERSP
for cond_i = 1:length(wpms.conditions)
    subplot(2,length(wpms.conditions),cond_i);
    contourf(times(plot_times),frequencies,squeeze(rStat_ERSP(:,plot_times,cond_i)),50,'linecolor','none');colormap jet;caxis([-.4 .4]);
    hold on;
    contour(times(plot_times),frequencies,squeeze(prStatCorr_ERSP(:,:,cond_i)),1,'k');axis square
    title(['ERSP ' wpms.conditions{cond_i}])
end
%ITPC
for cond_i = 1:length(wpms.conditions)
    subplot(2,length(wpms.conditions),cond_i+length(wpms.conditions));
    contourf(times(plot_times),frequencies,squeeze(rStat_ITPC(:,plot_times,cond_i)),50,'linecolor','none');colormap jet;caxis([-.4 .4]);
    hold on;
    contour(times(plot_times),frequencies,squeeze(prStatCorr_ITPC(:,:,cond_i)),1,'k');axis square;
    title(['ITPC ' wpms.conditions{cond_i}])
end
%% for data exploration purposes - uncorrected p
figure();
prStatUnCorr_ERSP = zeros(size(prStat_ERSP));
prStatUnCorr_ERSP(prStat_ERSP<.05) = 1;
prStatUnCorr_ITPC = zeros(size(prStat_ITPC));
prStatUnCorr_ITPC(prStat_ITPC<.05) = 1;
%correlations
%ERSP
for cond_i = 1:length(wpms.conditions)
    subplot(2,length(wpms.conditions),cond_i);
    contourf(times(plot_times),frequencies,squeeze(rStat_ERSP(:,plot_times,cond_i)),50,'linecolor','none');colormap jet;caxis([-.4 .4]);
    hold on;
    contour(times(plot_times),frequencies,squeeze(prStatUnCorr_ERSP(:,plot_times,cond_i)),1,'k');axis square
    title(['ERSP ' wpms.conditions{cond_i}])
end
%ITPC
for cond_i = 1:length(wpms.conditions)
    subplot(2,length(wpms.conditions),cond_i+length(wpms.conditions));
    contourf(times(plot_times),frequencies,squeeze(rStat_ITPC(:,plot_times,cond_i)),50,'linecolor','none');colormap jet;caxis([-.4 .4]);
    hold on;
    contour(times(plot_times),frequencies,squeeze(prStatUnCorr_ITPC(:,plot_times,cond_i)),1,'k');axis square;
    title(['ITPC ' wpms.conditions{cond_i}])
end