load('stdevITC.mat');
load('meansITC.mat');
means = means( [1:length(NAMES)] , : );
sdevs = sdevs( [1:length(NAMES)] , : );
avg_means = mean(means,2);
std_means = std(means,1);
control_sdevs(:,1) = sdevs(:,1)./avg_means;
control_sdevs(:,2) = sdevs(:,2)./avg_means;
control_sdevs(:,3) = sdevs(:,3)./avg_means;
control_sdevs(:,4) = sdevs(:,4)./avg_means;
control_sdevs(:,5) = sdevs(:,5)./avg_means;
control_sdevs(:,6) = sdevs(:,6)./avg_means;
control_sdevs = sqrt(control_sdevs);

WEWANT = good_inds;

control_sdevs = control_sdevs(WEWANT,:);