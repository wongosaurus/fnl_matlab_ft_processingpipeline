function [control_sdevs]=createCVmeasure(allnames,filenameAV,filenameSD,good_inds)

averageRT =load(filenameAV);
sdevRT    =load(filenameSD);

fName=fieldnames(averageRT);
averageRT = averageRT.(fName{1});

fName=fieldnames(sdevRT);
sdevRT = sdevRT.(fName{1});

averageRT = averageRT( [1:length(allnames)] , : );
sdevRT = sdevRT( [1:length(allnames)] , : );
avg_means = mean(averageRT,2);
std_means = std(averageRT,1);

control_sdevs(:,1) = sdevRT(:,1)./avg_means;
control_sdevs(:,2) = sdevRT(:,2)./avg_means;
control_sdevs(:,3) = sdevRT(:,3)./avg_means;
control_sdevs(:,4) = sdevRT(:,4)./avg_means;
control_sdevs(:,5) = sdevRT(:,5)./avg_means;
control_sdevs(:,6) = sdevRT(:,6)./avg_means;
control_sdevs = sqrt(control_sdevs);

WEWANT = good_inds;

control_sdevs = control_sdevs(WEWANT,:);