function conditionLabels=makeLabels(wpms,conditionLabels)

count=0;
for cond_i = 1:length(wpms.conditions)
    for sub_i = 1:length(conditionLabels)/length(wpms.conditions)
        count=count+1;
        conditionLabels{count,1} = wpms.conditions{cond_i};
    end
end