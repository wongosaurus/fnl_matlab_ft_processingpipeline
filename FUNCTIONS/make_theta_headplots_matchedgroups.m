%create theta movies
%set up globals
clear all;close all;clc;
datain = 'F:\FNL_EEG_TOOLBOX\WAVELET_OUTPUT\';%make sure this is correct Montana (F: or E:)
dataout = 'F:\FNL_EEG_TOOLBOX\WAVELET_OUTPUT\Movies\';%same here
conditions = {'mixrepeat','switchto','switchaway','noninfrepeat', 'noninfswitch'};
theta = 19:32;%1:18%36:49;
times = -400:0.5:2200;
%change these to the new groups
HCHA={	'AGE008',	'AGE052',	'AGE068',	'AGE072',	'AGE128',	'AGE165',	'AGE176',	'AGE192',	'AGE237',	'AGE238',	'AGE247',	'AGE259',	'AGE264'};
LCHA={	'AGE027',	'AGE033',	'AGE062',	'AGE150',	'AGE156',	'AGE158',	'AGE164',	'AGE189',	'AGE190',	'AGE197',	'AGE205',	'AGE222',	'AGE249'};
HCLA={	'AGE018',	'AGE019',	'AGE023',	'AGE024',	'AGE026',	'AGE050',	'AGE059',	'AGE070',	'AGE077',	'AGE083',	'AGE092',	'AGE094',	'AGE134'};
LCLA={	'AGE022',	'AGE032',	'AGE051',	'AGE067',	'AGE069',	'AGE081',	'AGE084',	'AGE086',	'AGE095',	'AGE129',	'AGE145',	'AGE149',	'AGE167'};
groups = {HCHA;LCHA;HCLA;LCLA};
group_names = {'HCHA','LCHA','HCLA','LCLA'};
%% first get list of participants
cd(datain);listing = dir();
names = {};
for list_i = 1:length(listing)
    if ~ismember('.',listing(list_i).name) 
        if ~ismember('Movies',listing(list_i).name)%don't want directory
            names = [names listing(list_i).name];
        end
    end
end

%% now create average power matrix
power = zeros(length(names),length(conditions),64,length(times));
for name_i = [1:150, 152:length(names)]
    fprintf('%s\t%s','Working on subject:',names{name_i});
    tic;
    for cond_i = 1:length(conditions)
        fprintf('\n%s\t',conditions{cond_i});
        for chan_i = 1:64
            fprintf('.');
            filename = [datain names{name_i} filesep conditions{cond_i} filesep names{name_i} '_' conditions{cond_i} '_' num2str(chan_i) '_imagcoh_mwtf.mat'];
            load(filename,'mw_tf');
            power(name_i,cond_i,chan_i,:) = squeeze(mean(mw_tf(theta,:),1));
        end
    end
    toc
end
%% plot differences head plots
%% create plots at 100 ms intervals for 4 anxiety/control groups
%first set up details for fieldtrip
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','Afz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
%for plotting
cfg =[];
cfg.layout = 'biosemi64.lay';
cfg.marker = 'off';
cfg.parameter = 'avg';
cfg.comment = 'no';
cfg.contournum = 2;
%cfg.gridscale = 400;
%cfg.shading = 'interp';
cfg.interactive = 'no';
dat.var = zeros(64,1);
dat.label = labels(:,1);
dat.time = 1;
dat.dimord = 'chan_time';
cfg.zlim = [-2.5 2.5];
%% now find time bins corresponding to 100ms intervals
startime = find(times==1000);%start at cue onset
inctime  = find(times==100)-find(times==0);%sample every 100 ms
endtime = find(times==2000);%end at target onset
times2plot = startime:inctime:endtime;
timelabels = 0:100:1000;
addpath(genpath('E:\FNL_EEG_TOOLBOX\FUNCTIONS\'));%functions including tight_subplot
addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));%fieldtrip
cfg.highlight = 'off';
%% look for differences between groups
cfg.highlight = 'on';
cfg.highlightsymbol = '.';
cfg.highlightsize = 18;
addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'));%fdr_toolbox
for cond_i=1:length(conditions)
%     figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);ha = tight_subplot(length(groups),length(times2plot),.01);
%     count=0;
    for group_i = 1:length(groups)
        figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);ha = tight_subplot(length(groups),length(times2plot),.01);
        count =0;
        x_inds = find(ismember(names,groups{group_i}));
        for group_j = 1:length(groups)
            y_inds = find(ismember(names,groups{group_j}));
            for time_i = 1:length(times2plot)
                count =count + 1;
                dat.avg = squeeze(nanmean(power(y_inds,cond_i,:,times2plot(time_i)),1));
                difflabels = {};
                p = zeros(1,size(power,3));
                for channel = 1:size(power,3)
                    x = squeeze(power(x_inds,cond_i,channel,times2plot(time_i)));
                    y = squeeze(power(y_inds,cond_i,channel,times2plot(time_i)));
                    [~,p(1,channel),~,~] = ttest2(x,y);
%                     [p,h] = ranksum(x,y);
                end
                [~,~,adj_p]=fdr_bh(p,0.05,'pdep');
                for channel = 1:size(power,3)
                    if p(1,channel) < 0.05;
                        difflabels = [difflabels labels{channel}];
                    end
                end
                cfg.highlightchannel = difflabels;
                axes(ha(count));ft_topoplotER(cfg,dat);
                if time_i == 1;
                    title([group_names{group_j} ' ' num2str(timelabels(time_i)) ' ms'],'FontSize',14);
                else
                    title([num2str(timelabels(time_i)) ' ms'],'FontSize',14);
                end
            end%time_i loop
        end
    savename = ['F:\FNL_EEG_TOOLBOX\WAVELET_OUTPUT\ThetaTimeSeries\' group_names{group_i} 'vs_all_' conditions{cond_i} '_TARGET_BRIEFSAMPLE.jpg'];
    saveas(gcf,savename,'jpg');pause(1);close all;
    end
end%cond_i loop
%%
startime = find(times==0);%start at cue onset
inctime  = find(times==100)-find(times==0);%sample every 100 ms
endtime = find(times==1000);%end at target onset
times2plot = startime:inctime:endtime;
timelabels = 0:100:1000;
addpath(genpath('E:\FNL_EEG_TOOLBOX\FUNCTIONS\'));%functions including tight_subplot
addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));%fieldtrip
cfg.highlight = 'off';
cfg.highlight = 'on';
cfg.highlightsymbol = '.';
cfg.highlightsize = 18;
addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'));%fdr_toolbox
for cond_i=1:length(conditions)
%     figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);ha = tight_subplot(length(groups),length(times2plot),.01);
%     count=0;
    for group_i = 1:length(groups)
        figure();set(gcf,'Position',[0 0 1920 1080],'Color',[1 1 1]);ha = tight_subplot(length(groups),length(times2plot),.01);
        count =0;
        x_inds = find(ismember(names,groups{group_i}));
        for group_j = 1:length(groups)
            y_inds = find(ismember(names,groups{group_j}));
            for time_i = 1:length(times2plot)
                count =count + 1;
                dat.avg = squeeze(nanmean(power(y_inds,cond_i,:,times2plot(time_i)),1));
                difflabels = {};
                p = zeros(1,size(power,3));
                for channel = 1:size(power,3)
                    x = squeeze(power(x_inds,cond_i,channel,times2plot(time_i)));
                    y = squeeze(power(y_inds,cond_i,channel,times2plot(time_i)));
                    [~,p(1,channel),~,~] = ttest2(x,y);
%                     [p,h] = ranksum(x,y);
                end
                [~,~,adj_p]=fdr_bh(p,0.05,'pdep');
                for channel = 1:size(power,3)
                    if p(1,channel) < 0.05;
                        difflabels = [difflabels labels{channel}];
                    end
                end
                cfg.highlightchannel = difflabels;
                axes(ha(count));ft_topoplotER(cfg,dat);
                if time_i == 1;
                    title([group_names{group_j} ' ' num2str(timelabels(time_i)) ' ms'],'FontSize',14);
                else
                    title([num2str(timelabels(time_i)) ' ms'],'FontSize',14);
                end
            end%time_i loop
        end
    savename = ['F:\FNL_EEG_TOOLBOX\WAVELET_OUTPUT\ThetaTimeSeries\' group_names{group_i} 'vs_all_' conditions{cond_i} '_CUE_BRIEFSAMPLE.jpg'];
    saveas(gcf,savename,'jpg');pause(1);close all;
    end
end%cond_i loop