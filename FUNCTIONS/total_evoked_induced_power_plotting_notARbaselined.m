%% [AW: 2017/03/22] Set up the variables required for the analysis:

clearvars; close all; clc;
%Data Structure Time/Frequency definition
frequencies = logspace(log10(2),log10(30),80);
times = (-1:0.001953125000000:3.5)*1000;
%Electrode Labels:
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
CHANNELS = 1:length(labels);
%CONDITIONS  = {'switchto','switchaway','noninf','mixrepeat','allrepeat'};

% Montana Conditions: 
CONDITIONS  = {'allrepeat','mixrepeat','switchto','switchaway','noninfrepeat','noninfswitch'};

% Find the names of participants:
listings = dir('E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat');
NAMES = {'AGE002',	'AGE003',	'AGE004',	'AGE005',	'AGE007',	'AGE008',	'AGE012',	'AGE013',	'AGE014',	'AGE015',	'AGE017',	'AGE018',	'AGE019',	'AGE020',	'AGE021',	'AGE022',	'AGE023',	'AGE024',	'AGE026',	'AGE027',	'AGE028',	'AGE030',	'AGE032',	'AGE033',	'AGE034',	'AGE035',	'AGE036',	'AGE038',	'AGE043',	'AGE046',	'AGE047',	'AGE050',	'AGE051',	'AGE053',	'AGE058',	'AGE059',	'AGE061',	'AGE062',	'AGE063',	'AGE066',	'AGE067',	'AGE068',	'AGE069',	'AGE070',	'AGE072',	'AGE073',	'AGE075',	'AGE077',	'AGE081',	'AGE083',	'AGE084',	'AGE085',	'AGE086',	'AGE088',	'AGE089',	'AGE090',	'AGE092',	'AGE093',	'AGE094',	'AGE095',	'AGE096',	'AGE097',	'AGE098',	'AGE100',	'AGE102',	'AGE103',	'AGE104',	'AGE107',	'AGE108',	'AGE109',	'AGE111',	'AGE114',	'AGE115',	'AGE116',	'AGE117',	'AGE118',	'AGE119',	'AGE120',	'AGE121',	'AGE122',	'AGE123',	'AGE124',	'AGE127',	'AGE128',	'AGE129',	'AGE130',	'AGE131',	'AGE133',	'AGE134',	'AGE135',	'AGE136',	'AGE138',	'AGE141',	'AGE145',	'AGE146',	'AGE147',	'AGE148',	'AGE149',	'AGE150',	'AGE151',	'AGE152',	'AGE153',	'AGE155',	'AGE156',	'AGE158',	'AGE159',	'AGE160',	'AGE161',	'AGE162',	'AGE163',	'AGE164',	'AGE165',	'AGE166',	'AGE167',	'AGE168',	'AGE169',	'AGE170',	'AGE172',	'AGE175',	'AGE176',	'AGE177',	'AGE178',	'AGE179',	'AGE180',	'AGE181',	'AGE182',	'AGE183',	'AGE184',	'AGE185',	'AGE186',	'AGE187',	'AGE189',	'AGE190',	'AGE195',	'AGE197',	'AGE198',	'AGE199',	'AGE201',	'AGE202',	'AGE203',	'AGE205',	'AGE206',	'AGE207',	'AGE208',	'AGE209',	'AGE210',	'AGE211',	'AGE217',	'AGE218',	'AGE219',	'AGE220',	'AGE221',	'AGE222',	'AGE225',	'AGE226',	'AGE227',	'AGE228',	'AGE229',	'AGE230',	'AGE231',	'AGE232',	'AGE233',	'AGE236',	'AGE237',	'AGE238',	'AGE239',	'AGE241',	'AGE243',	'AGE244',	'AGE245',	'AGE246',	'AGE247',	'AGE248',	'AGE249',	'AGE251',	'AGE252',	'AGE253',	'AGE254',	'AGE255',	'AGE256',	'AGE257',	'AGE258',	'AGE259',	'AGE260',	'AGE261',	'AGE264',	'AGE265',	'AGE266',	'AGE267',	'AGE268',	'AGE269',	'AGE270',	'AGE273',	'AGE275',	'AGE276',	'AGE278',	'AGE279'};


SAVEDIR = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\';
LOADDIR = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\';
%% [AW: 2017/03/22] TASK AVERAGE Time-Frequency Response: This section applies a time baseline for the data and averages the data together.

% Precue Baseline: -300ms = 359index, -100ms = 462index :CHECKED -> OK
baseline_time=359:462;

% Pretarget Baseline: 700ms = 871index, 900ms = 974index :CHECKED -> OK
% baseline_time=871:974;

%[AW: 2017/03/22] Load first file to assist with seting up of storage variables: 
filename=[LOADDIR NAMES{1} '_' CONDITIONS{1} '_ALL_POWER3.mat'];
load(filename);
%incorporate into an average
%[AW: 2017/03/22] Set up variables for Data Processing and Storage: 
%AR Removed:
% condition_average_eegpower = zeros(length(CONDITIONS)-1,size(eegpower_all,1),size(eegpower_all,2),size(eegpower_all,3));

%[AW: 2017/03/22] Set up variables for Data Processing and Storage: 
%AR included:
condition_average_eegpower = zeros(length(NAMES),size(eegpower_all,1),size(eegpower_all,2),size(eegpower_all,3));

condition_average_evoked = condition_average_eegpower;
condition_average_induced = condition_average_eegpower;

clear eegpower_all evokedpower_all inducedpower_all


%[AW: 2017/03/22] This Section Runs the Time Baseline for the data:
for name_i=1:length(NAMES)
    disp(NAMES{name_i});
    tic;
    
    %[AW: 2017/03/22] This Section Works on All Repeat: 
%     % Process All Repeat First: Keep This in memory:
%     condition_i = 5; % For All Repeat:
%     filename=[LOADDIR NAMES{name_i} '_' CONDITIONS{condition_i} '_ALL_POWER3.mat'];
%     load(filename);
%     eegpower_allRepeat_bl = zeros(size(eegpower_all));
%     %evoked_allRepeat_bl     = zeros(size(evokedpower_all));
%     induced_allRepeat_bl     = zeros(size(inducedpower_all));
%     for chan_i = 1:length(CHANNELS)
%         eegpower_allRepeat_bl(chan_i,:,:) = bsxfun(@minus,10*log10(squeeze(eegpower_all(chan_i,:,:))),10*log10(mean(squeeze(eegpower_all(chan_i,:,baseline_time)),2)));
%         %evoked_allRepeat_bl(chan_i,:,:) = bsxfun(@minus,real(10*log10(squeeze(evokedpower_all(chan_i,:,:)))),real(10*log10(mean(squeeze(evokedpower_all(chan_i,:,baseline_time)),2))));
%         induced_allRepeat_bl(chan_i,:,:) = bsxfun(@minus,10*log10(squeeze(inducedpower_all(chan_i,:,:))),10*log10(mean(squeeze(inducedpower_all(chan_i,:,baseline_time)),2)));
%         clear temp temp2 bl
%     end%chan_i loop
    
    %Remove Variables just incase something goes wrong..
    clear eegpower_all evokedpower_all inducedpower_all;
    
    %[AW: 2017/03/22] This Section Works on all Conditions, listed in CONDITION variable : 
    for condition_i=1:length(CONDITIONS)
        fprintf('.');
        filename=[LOADDIR NAMES{name_i} '_' CONDITIONS{condition_i} '_ALL_POWER3.mat'];
        load(filename);
        
        %preallocate baseline structure
        eegpower_all_bl = zeros(size(eegpower_all));
        inducedpower_all_bl     = zeros(size(inducedpower_all));
        evokedpower_all     = eegpower_all - inducedpower_all;
        evokedpower_all_bl = zeros(size(evokedpower_all));
       
        %[AW: 2017/03/22] This Section Works applies the Time baseline: 
        % Time domain baseline:
        for chan_i = 1:length(CHANNELS)
            eegpower_all_bl(chan_i,:,:) = bsxfun(@minus,10*log10(squeeze(eegpower_all(chan_i,:,:))),10*log10(mean(squeeze(eegpower_all(chan_i,:,baseline_time)),2)));
            evokedpower_all_bl(chan_i,:,:) = bsxfun(@minus,real(10*log10(squeeze(evokedpower_all(chan_i,:,:)))),real(10*log10(mean(squeeze(evokedpower_all(chan_i,:,baseline_time)),2))));
            inducedpower_all_bl(chan_i,:,:) = bsxfun(@minus,10*log10(squeeze(inducedpower_all(chan_i,:,:))),10*log10(mean(squeeze(inducedpower_all(chan_i,:,baseline_time)),2)));
        end%chan_i loop
        
        %[AW: 2017/03/22] This Section calculates the AllRepeat Baseline,: 
        % eegpower_bl_arcorrected         = eegpower_all_bl - eegpower_allRepeat_bl;
        %%evokedpower_bl_arcorrected      = evokedpower_all_bl - evoked_allRepeat_bl;
        % inducedpower_bl_arcorrected     = inducedpower_all_bl - induced_allRepeat_bl;
        
        %[AW: 2017/03/22] This section appends to the sum for the average
        %calculation, AR Baselined Data: Comment out if the AR baseline is not required.
        % condition_average_eegpower(condition_i,:,:,:) = squeeze(condition_average_eegpower(condition_i,:,:,:)) + eegpower_bl_arcorrected;
        %%condition_average_evoked(condition_i,:,:,:)   = squeeze(condition_average_evoked(condition_i,:,:,:)) + evokedpower_bl_arcorrected;
        % condition_average_induced(condition_i,:,:,:)  = squeeze(condition_average_induced(condition_i,:,:,:)) + inducedpower_bl_arcorrected;
        
        %[AW: 2017/03/22] This section appends the sum for the average
        %calculation, FOR NON-ARbaseline: Comment out if the AR baseline is not required.
        condition_average_eegpower(:,:,:) = squeeze(condition_average_eegpower(2,:,:,:)) + eegpower_all_bl;
        condition_average_evoked(condition_i,:,:,:)   = squeeze(condition_average_evoked(condition_i,:,:,:)) + evokedpower_all_bl;
        condition_average_induced(condition_i,:,:,:)  = squeeze(condition_average_induced(condition_i,:,:,:)) + inducedpower_all_bl;
        
%      savename=[SAVEDIR NAMES{name_i} '_' CONDITIONS{condition_i} '_ITPC_bl_arcorrected.mat'];
% %      save(savename, 'eegpower_all_bl', 'itpc_all_bl');
%      save(savename, 'eegpower_bl_arcorrected', 'itpc_bl_arcorrected', 'ispc_bl_arcorrected');
   
    end %condition_i loop
    t=toc;
    fprintf('\t%3.2f %s\n',t,'seconds to complete');
end %name_i loop 


%[AW: 2017/03/22] This section calculates the average, by dividing the sum
%by number of participants.
condition_average_eegpower = condition_average_eegpower./length(NAMES);
condition_average_evoked   = condition_average_evoked./length(NAMES);
condition_average_induced  = condition_average_induced./length(NAMES);

%[AW: 2017/03/22] SAVE the Average Data:
savename=[SAVEDIR 'Condition_Average_EEGPOWER_CUE_NOARBASELINE_301117.mat'];
save(savename, 'condition_average_eegpower','condition_average_induced');
   
%% View the plots:
% EEG POWER
% CONDITIONS_NOAR  = {'switchto','switchaway','noninf','mixrepeat'};%{'mixrepeat','switchto','switchaway','noninf'};
% montana - 
%CONDITIONS = {'allrepeat','mixrepeat','switchto','switchaway','noninfrepeat','noninfswitch'};

CONDITIONS_NOAR = CONDITIONS;

%NEW
%chan_i = find(ismember(labels,{'POz','PO1','PO2','P1','Pz','P2','POz'}));
%chan_i = find(ismember(labels,{'F1','Fz','F2','FC1','FCz','FC2'}));
chan_i = find(ismember(labels,{'C1','Cz','C2','CP1','CPz','CP2'}));

%OLD
%chan_i = find(ismember(labels,{'CPz','CP1','CP2','P1','Pz','P2','POz'}));
%chan_i = find(ismember(labels,{'C1','Cz','C2','FC1','FCz','FC2'}));

%chan_i = find(ismember(labels,{'Pz','POz'}));
%chan_i = find(ismember(labels,{'FCz','Fz'}));
figure();

wpms.times = times;
baseline_start = -300;
baseline_end = 2000;

plot_times = setbaseline(wpms, baseline_start, baseline_end);

count=0;

erp = squeeze(mean(condition_average_eegpower,3));

% % scale ERP for plotting

% change CONDITIONS_NOAR to CONDITIONS

clims = [-2 2];
%total
for cond_i = 1:2%length(CONDITIONS_NOAR)
    subplot(3,length(CONDITIONS_NOAR),count+cond_i);
    if cond_i == 1
    data = squeeze(mean(condition_average_eegpower(2,chan_i,:,plot_times),2)) - squeeze(mean(condition_average_eegpower(1,chan_i,:,plot_times),2));
    elseif cond_i == 2
    data = squeeze(mean(condition_average_eegpower(3,chan_i,:,plot_times),2)) - squeeze(mean(condition_average_eegpower(2,chan_i,:,plot_times),2));
    end
    contourf(times(plot_times),frequencies,data,50,'linecolor','none');
    caxis(clims);
    colormap 'jet';
    title(CONDITIONS_NOAR{cond_i});
    %hold on;
    %wave=squeeze(mean(erp(cond_i,chan_i,plot_times),2)).*-1';
    %erpt = (wave-min(wave))./max(wave-min(wave));
    %erpt = erpt*(frequencies(end)-frequencies(1))+frequencies(1)';
    %plot (times(plot_times),erpt,'k','linewidth',2);
end
%colorbar
%evoked
count=count+length(CONDITIONS_NOAR);
for cond_i = 1:2%:length(CONDITIONS_NOAR)
    subplot(3,length(CONDITIONS_NOAR),count+cond_i);
    if cond_i == 1
    data = (squeeze(mean(condition_average_eegpower(2,chan_i,:,plot_times),2))-(squeeze(mean(condition_average_induced(2,chan_i,:,plot_times),2))) - (squeeze(mean(condition_average_eegpower(1,chan_i,:,plot_times),2))-(squeeze(mean(condition_average_induced(1,chan_i,:,plot_times),2)))));
    elseif cond_i == 2
    data = (squeeze(mean(condition_average_eegpower(3,chan_i,:,plot_times),2))-(squeeze(mean(condition_average_induced(3,chan_i,:,plot_times),2))) - (squeeze(mean(condition_average_eegpower(2,chan_i,:,plot_times),2))-(squeeze(mean(condition_average_induced(2,chan_i,:,plot_times),2)))));
    end
    contourf(times(plot_times),frequencies,data,50,'linecolor','none');
    caxis(clims);
    colormap 'jet';
    title(CONDITIONS_NOAR{cond_i});
    %hold on;
    %wave=squeeze(mean(erp(cond_i,chan_i,plot_times),2)).*-1';
    %erpt = (wave-min(wave))./max(wave-min(wave));
    %erpt = erpt*(frequencies(end)-frequencies(1))+frequencies(1)';
    %plot (times(plot_times),erpt,'k','linewidth',2);
end
%colorbar
    

%sanity check
% count=count+length(CONDITIONS_NOAR);
% for cond_i = 1:length(CONDITIONS_NOAR)
%     subplot(3,length(CONDITIONS_NOAR),count+cond_i);
%     contourf(times,frequencies,squeeze(mean(condition_average_eegpower(cond_i,chan_i,:,:),2))-squeeze(mean(real(condition_average_evoked(cond_i,chan_i,:,:)),2)),50,'linecolor','none');
%     %caxis([-5 5]);
%     colormap 'jet';
%     title(CONDITIONS_NOAR{cond_i});
% end

%induced
clims_induced = [-1 1];
count=count+length(CONDITIONS_NOAR);
for cond_i = 1:2%length(CONDITIONS_NOAR)
    subplot(3,length(CONDITIONS_NOAR),count+cond_i);
    if cond_i == 1
    data = squeeze(mean(condition_average_induced(2,chan_i,:,plot_times),2)) - squeeze(mean(condition_average_induced(1,chan_i,:,plot_times),2));
    elseif cond_i == 2
    data = squeeze(mean(condition_average_induced(3,chan_i,:,plot_times),2)) - squeeze(mean(condition_average_induced(2,chan_i,:,plot_times),2));
    end
    contourf(times(plot_times),frequencies,data,50,'linecolor','none');
    caxis(clims_induced);
    colormap 'jet';
    title(CONDITIONS_NOAR{cond_i});
    %hold on;
    %wave=squeeze(mean(erp(cond_i,chan_i,plot_times),2)).*-1';
    %erpt = (wave-min(wave))./max(wave-min(wave));
    %erpt = erpt*(frequencies(end)-frequencies(1))+frequencies(1)';
    %plot (times(plot_times),erpt,'k','linewidth',2);
end
% colorbar
% count=count+length(CONDITIONS_NOAR);
% for cond_i = 1:length(CONDITIONS_NOAR)
%     subplot(4,length(CONDITIONS_NOAR),count+cond_i);
%     %contourf(times,frequencies,squeeze(mean(condition_average_evoked(cond_i,chan_i,:,:),2)),50,'linecolor','none');
%     contourf(times,frequencies,squeeze(mean(condition_average_eegpower(cond_i,chan_i,:,:),2)) - (squeeze(mean(condition_average_eegpower(cond_i,chan_i,:,:),2))-squeeze(mean(condition_average_induced(cond_i,chan_i,:,:),2))),50,'linecolor','none');
%     %caxis([-5 5]);
%     colormap 'jet';
%     title(CONDITIONS_NOAR{cond_i});
% end
%%
% 
% CONDITIONS_NOAR  = {'mixrepeat','switchto','switchaway','noninf'};
% for condition_i=1:length(CONDITIONS_NOAR)
%     tic;
%     disp(CONDITIONS_NOAR{condition_i});
%     load_name=[SAVEDIR NAMES{1} '_' CONDITIONS_NOAR{1} '_ITPC_bl_arcorrected.mat'];
%     temp_participant=load(load_name);
%     condition_average_eegpower = zeros(size(temp_participant.eegpower_bl_arcorrected));
%     condition_average_itpc = zeros(size(temp_participant.itpc_bl_arcorrected));
%     condition_average_ispc = zeros(size(temp_participant.ispc_bl_arcorrected));
%     for name_i=1:length(NAMES)
%         fprintf('.');
%         load_name=[SAVEDIR NAMES{name_i} '_' CONDITIONS_NOAR{condition_i} '_ITPC_bl_arcorrected.mat'];
%         temp_participant=load(load_name);
%         condition_average_eegpower = condition_average_eegpower + temp_participant.eegpower_bl_arcorrected;
%         condition_average_itpc=condition_average_itpc + temp_participant.itpc_bl_arcorrected;
%         condition_average_ispc=condition_average_ispc + temp_participant.ispc_bl_arcorrected;
%     end
%     condition_average_eegpower = condition_average_eegpower/length(NAMES);
%     condition_average_itpc = condition_average_itpc/length(NAMES);
%     condition_average_ispc = condition_average_ispc/length(NAMES);
%     savename=[SAVEDIR CONDITIONS_NOAR{condition_i} '_Average_ITPC_bl_arcorrected.mat'];
%     save(savename, 'condition_average_ispc','condition_average_eegpower','condition_average_itpc');
%     
%      t=toc;
%     fprintf('\t%3.2f %s\n',t,'seconds to complete');
% end
% 
% 
%% View the plots:
% % EEG POWER
% chan_i = find(ismember(labels,{'CP1','CPz','CP2','P1','Pz','P2','POz'}));
% %chan_i = find(ismember(labels,{'C1','Cz','C2','FC1','FCz','FC2'}));
% figure();
% for cond_i = 1:length(CONDITIONS_NOAR)
%     subplot(2,2,cond_i);
%     load_name=[SAVEDIR CONDITIONS_NOAR{cond_i} '_Average_ITPC_bl_arcorrected.mat'];
%     load(load_name);
%     contourf(times,frequencies,squeeze(mean(condition_average_eegpower(chan_i,:,:),1)),50,'linecolor','none');
%     caxis([-5 5]);
%     colormap 'jet';
%     title(CONDITIONS_NOAR{cond_i});
% end
% 
% % ITPC POWER
% figure();
% for cond_i = 1:length(CONDITIONS_NOAR)
%     subplot(2,2,cond_i);
%     load_name=[SAVEDIR CONDITIONS_NOAR{cond_i} '_Average_ITPC_bl_arcorrected.mat'];
%     load(load_name);
%     contourf(times,frequencies,squeeze(mean(condition_average_itpc(chan_i,:,:),1)),50,'linecolor','none');
%     caxis([-0.1 0.1]);
%     colormap 'jet';
%     title(CONDITIONS_NOAR{cond_i});
% end
% name_i=1;
% figure();
% for chan_i = 1:64
%     for cond_i = 1:length(CONDITIONS_NOAR)
%         subplot(2,2,cond_i);
%         load_name=[SAVEDIR CONDITIONS_NOAR{cond_i} '_Average_ITPC_bl_arcorrected.mat'];
% %         load_name=[SAVEDIR NAMES{name_i} '_' CONDITIONS{cond_i} '_ISPC_bl_arcorrected.mat'];
%         load(load_name);
%         contourf(times,frequencies,squeeze(condition_average_ispc(chan_i,:,:)),50,'linecolor','none');
% %         contourf(times,frequencies,abs(squeeze(mean(ispc_bl_arcorrected(chan_i,:,:),1))),50,'linecolor','none');
%         caxis([0 100]);
%         colormap 'hot';
%         title([CONDITIONS{cond_i} ' Connectivity With ' labels{chan_i}]);
%     end
%     pause(0.1);
% end
% 
% 
% 
% %% Topology Plots:
% addpath(genpath('F:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip-20150902\fieldtrip-20150902'));
% addpath(genpath('F:\FNL_EEG_TOOLBOX\PACKAGES\tightsubplot'));
% labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
%     'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
%     'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
%     'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
%     'P8','P10','PO8','PO4','O2'}';
% %close all;
% %0-500MS
% times_start = 0;%1400
% times_end = 450;%1600
% figure();
% temp_times = times-times_start;
% [~,basetimestart_index] = min(abs(temp_times));
% temp_times = times-times_end;
% [~,basetimeend_index] = min(abs(temp_times));
% times_of_interest = basetimestart_index:basetimeend_index;
% %4-7HZ
% low_freq=4;%5
% high_freq = 8;%7
% Frequencybin_Theta = find((floor(frequencies-low_freq)==0),1):find((floor(frequencies-high_freq)==0),1); % 4-7Hz
% for cond_i = 1:length(CONDITIONS_NOAR)
%     load_name=[SAVEDIR CONDITIONS_NOAR{cond_i} '_Average_ITPC_bl_arcorrected.mat'];
%     load(load_name);
% 
%     fprintf('Condition: %s\n',CONDITIONS_NOAR{cond_i})
%     cfg = [];
%     cfg.zlim        = [-5 5];
%     cfg.colormap    = 'jet';
%     cfg.parameter   = 'avg';
%     cfg.layout      = 'biosemi64.lay';
%     cfg.comment     = 'no';
%     cfg.marker      = '';
%     cfg.channel     = labels;
%     cfg.contournum  = 4;
%     cfg.gridsize    = 600;
%     data =[];
%     data.avg = zeros(72,1);
%     data_temp  = squeeze(mean(mean(condition_average_eegpower(:,Frequencybin_Theta,times_of_interest),3),2));
% 
%     data.avg = [data_temp;0;0;0;0;0;0;0;0];
%     data.avg = (data.avg);
%     clear pval_i
%     data.var = zeros(72,1);
%     data.time = 1;
%     data.label = labels;
%     data.dimord = 'chan_time';
%     data.cov = zeros(72,72);
%     subplot(2,2,cond_i); ft_topoplotER(cfg, data);
%     title(CONDITIONS_NOAR{cond_i});
%     set(gcf,'Color',[1 1 1]);
% end
% 
% %ITPC
% figure();
% for cond_i = 1:length(CONDITIONS_NOAR)
%     load_name=[SAVEDIR CONDITIONS_NOAR{cond_i} '_Average_ITPC_bl_arcorrected.mat'];
%     load(load_name);
% 
%     fprintf('Condition: %s\n',CONDITIONS_NOAR{cond_i})
%     cfg = [];
%     cfg.zlim        = [0 0.2];
%     cfg.colormap    = 'hot';
%     cfg.parameter   = 'avg';
%     cfg.layout      = 'biosemi64.lay';
%     cfg.comment     = 'no';
%     cfg.marker      = '';
%     cfg.channel     = labels;
%     cfg.contournum  = 4;
%     cfg.gridsize    = 600;
%     data =[];
%     data.avg = zeros(72,1);
%     data_temp  = squeeze(mean(mean(condition_average_itpc(:,Frequencybin_Theta,times_of_interest),3),2));
% 
%     data.avg = [data_temp;0;0;0;0;0;0;0;0];
%     data.avg = (data.avg);
%     clear pval_i
%     data.var = zeros(72,1);
%     data.time = 1;
%     data.label = labels;
%     data.dimord = 'chan_time';
%     data.cov = zeros(72,72);
%     subplot(2,2,cond_i); ft_topoplotER(cfg, data);
%     title(CONDITIONS_NOAR{cond_i});
%     set(gcf,'Color',[1 1 1]);
% end
% 
% 
% %%
% times_start = -100:10:1600;
% times_end = times_start+50;
% figure(); set (gcf, 'Position',[0 0 1920 540], 'Color',[1 1 1]);
% count=1;
% figtitle = {'Mixed Repeat', 'Switch-to', 'Switch-away', 'Noninformative'};
% 
% for time_i = 1:length(times_start)
%     
%     temp_times = times-times_start(time_i);
%     [~,basetimestart_index] = min(abs(temp_times));
%     temp_times = times-times_end(time_i);
%     [~,basetimeend_index] = min(abs(temp_times));
%     times_of_interest = basetimestart_index:basetimeend_index;
%     
%     %4-8HZ
%     low_freq=4;
%     high_freq = 8;
%     Frequencybin_Theta = find((floor(frequencies-low_freq)==0),1):find((floor(frequencies-high_freq)==0),1); % 4-8Hz
%     for cond_i = 1:length(CONDITIONS_NOAR);
%         
%         load_name=[SAVEDIR CONDITIONS_NOAR{cond_i} '_Average_ITPC_bl_arcorrected.mat'];
%         load(load_name);
%         
%         fprintf('Condition: %s\n',CONDITIONS_NOAR{cond_i})
%         cfg = [];
%         cfg.zlim        = [-5 5];
%         cfg.colormap    = 'jet';
%         cfg.parameter   = 'avg';
%         cfg.layout      = 'biosemi64.lay';
%         cfg.comment     = 'no';
%         cfg.marker      = '';
%         cfg.channel     = labels;
%         %cfg.marker = 'labels';
%         %cfg.contournum = 1;
%         cfg.contournum  = 4;
%         cfg.gridsize    = 600;
%         data =[];
%         data.avg = zeros(72,1);
%         %eval(['currentfile = ' datastruct_cue{(contrasts_i)} ';']);
%         %eval(['currentpval = ' datastruct_pval{(contrasts_i)} ';']);
%         %eval(['currentpval_corrected = ' datastruct_pval_corrected{(contrasts_i)} ';']);
%         % threshold = 0.05;
%         % data4plot_ind = find(currentpval<threshold);
%         % labels_ind = find(currentpval_corrected<threshold);
%         %cfg.highlight = 'on';
%         %cfg.highlightchannel = labels(labels_ind);
%         %cfg.highlightcolor = [0 0 1]; %blue
%         
%         %cfg.highlightfontsize = 1; %no label writing
%         %cfg.highlightsymbol = 'o';
%         data_temp  = squeeze(mean(mean(condition_average_eegpower(:,Frequencybin_Theta,times_of_interest),3),2));
%         
%         data.avg = [data_temp;0;0;0;0;0;0;0;0];
%         %         for i = 1:length(data.avg)
%         %             if (data.avg(i) >0)
%         %                 data.avg(i) = 0;
%         %             end
%         %         end
%         data.avg = (data.avg);
%         clear pval_i
%         data.var = zeros(72,1);
%         data.time = 1;
%         data.label = labels;
%         data.dimord = 'chan_time';
%         data.cov = zeros(72,72);
%         
%         ind = cond_i;
%         
%        %figure();
%         set (gcf, 'Position',[0 0 1920 540], 'Color',[1 1 1]);
%         subplot(5,5,[ind,ind+5,ind+10]); ft_topoplotER(cfg, data);
%         %if contrasts_i ==1
%             title (figtitle{cond_i},'FontSize',16);
%     end
%     ind = ind+1;
%     set (gcf, 'Position',[0 0 1920 540], 'Color',[1 1 1]);
%     subplot(5,5,[ind,ind+5,ind+10]); colorbar('east','FontSize',16); caxis ([-5 5]); axis square; axis off;  title ('Power(dB)','FontSize',16,'HorizontalAlignment','left');
%     %adding sliding timebar
%     set (gcf, 'Position',[0 0 1920 540], 'Color',[1 1 1]);
%     subplot(5,5,[ind+15:ind+20]); 
%     plot(times,ones(1,length(times)).*-1,'k');
%     ylim([-1 1]);
%     set(gca,'xtick',-1000:500:3500);
%     set(gca,'XTickLabels',-1000:500:3500);
%     hold on;
%     set(gca,'ytick',[]);
%     set(gca,'ycolor',[1 1 1])
%     fprintf('START: %3.5f, END: %3.5f\n',times(times_of_interest(1)),times(times_of_interest(end)));
%     fill([times(times_of_interest(1)) times(times_of_interest(1)) times(times_of_interest(end)) times(times_of_interest(end))],[-1 0 0 -1],'k');
%     box off;
%     xlabel('Time (ms)');set(gca,'FontSize',16);
%  
%     
%     pause(0.1);
%     if count == 1
%         colorbar;
%     end
%     f = getframe(gcf);
%     if count == 1
%         [im,map] = rgb2ind(f.cdata,256,'nodither');
%         im(1,1,1,21) = 0;
%     end
%     if count ~=1
%         im(:,:,1,count-1) = rgb2ind(f.cdata,map,'nodither');
%     end
%     count = count +1;
%     close all;
% end
% filename = [SAVEDIR 'Theta_Power_Movie_v4.gif'];
% imwrite(im,map,filename,'gif','DelayTime',0.3,'LoopCount',inf);
% 
% %%
% 
% figure();
% for cond_i = 1:length(CONDITIONS_NOAR);
% 
% load_name=[SAVEDIR CONDITIONS_NOAR{cond_i} '_Average_ITPC_bl_arcorrected.mat'];
% load(load_name);
% 
% fprintf('Condition: %s\n',CONDITIONS_NOAR{cond_i})
% cfg = [];
% cfg.zlim        = [0 .1];
% cfg.colormap    = 'hot';
% cfg.parameter   = 'avg';
% cfg.layout      = 'biosemi64.lay';
% cfg.comment     = 'no';
% cfg.marker      = '';
% %cfg.marker = 'labels';
% %cfg.contournum = 1;
% cfg.contournum  = 4;
% cfg.gridsize    = 600;
% data =[];
% data.avg = zeros(72,1);
% %eval(['currentfile = ' datastruct_cue{(contrasts_i)} ';']);
% %eval(['currentpval = ' datastruct_pval{(contrasts_i)} ';']);
% %eval(['currentpval_corrected = ' datastruct_pval_corrected{(contrasts_i)} ';']);
% % threshold = 0.05;
% % data4plot_ind = find(currentpval<threshold);
% % labels_ind = find(currentpval_corrected<threshold);
% %cfg.highlight = 'on';
% %cfg.highlightchannel = labels(labels_ind);
% %cfg.highlightcolor = [0 0 1]; %blue
% 
% %cfg.highlightfontsize = 1; %no label writing
% %cfg.highlightsymbol = 'o';
% data_temp  = squeeze(mean(mean(condition_average_itpc(:,Frequencybin_Theta,times_of_interest),3),2));
% 
% data.avg = [data_temp;0;0;0;0;0;0;0;0];
% %         for i = 1:length(data.avg)
% %             if (data.avg(i) >0)
% %                 data.avg(i) = 0;
% %             end
% %         end
% data.avg = (data.avg);
% clear pval_i
% data.var = zeros(72,1);
% data.time = 1;
% data.label = labels;
% data.dimord = 'chan_time';
% data.cov = zeros(72,72);
% 
% ind = cond_i;
% 
% subplot(2,2,ind); ft_topoplotER(cfg, data);
% set(gcf,'Color',[1 1 1]);
% %if contrasts_i ==1
% 
% title(['Condition: ',CONDITIONS_NOAR{cond_i}],'FontSize',12);
% %end
% 
% end
% CONDITIONS_NOAR  = {'switchto','switchaway','noninf','mixrepeat'};
% figure();
% for cond_i = 1:length(CONDITIONS_NOAR);
%     
%     load_name=[SAVEDIR NAMES{name_i} '_' CONDITIONS{cond_i} '_ISPC_bl_arcorrected.mat'];
%     load(load_name);
%     
%     fprintf('Condition: %s\n',CONDITIONS_NOAR{cond_i})
%     cfg = [];
%     cfg.zlim        = [0 .1];
%     cfg.colormap    = 'hot';
%     cfg.parameter   = 'avg';
%     cfg.layout      = 'biosemi64.lay';
%     cfg.comment     = 'no';
%     cfg.marker      = '';
%     %cfg.marker = 'labels';
%     %cfg.contournum = 1;
%     cfg.contournum  = 4;
%     cfg.gridsize    = 600;
%     data =[];
%     data.avg = zeros(72,1);
%     %eval(['currentfile = ' datastruct_cue{(contrasts_i)} ';']);
%     %eval(['currentpval = ' datastruct_pval{(contrasts_i)} ';']);
%     %eval(['currentpval_corrected = ' datastruct_pval_corrected{(contrasts_i)} ';']);
%     % threshold = 0.05;
%     % data4plot_ind = find(currentpval<threshold);
%     % labels_ind = find(currentpval_corrected<threshold);
%     %cfg.highlight = 'on';
%     %cfg.highlightchannel = labels(labels_ind);
%     %cfg.highlightcolor = [0 0 1]; %blue
%     
%     %cfg.highlightfontsize = 1; %no label writing
%     %cfg.highlightsymbol = 'o';
%     data_temp  = abs(squeeze(mean(mean(ispc_bl_arcorrected(:,Frequencybin_Theta,times_of_interest),3),2)));
%     
%     data.avg = [data_temp;0;0;0;0;0;0;0;0];
%     %         for i = 1:length(data.avg)
%     %             if (data.avg(i) >0)
%     %                 data.avg(i) = 0;
%     %             end
%     %         end
%     data.avg = (data.avg);
%     clear pval_i
%     data.var = zeros(72,1);
%     data.time = 1;
%     data.label = labels;
%     data.dimord = 'chan_time';
%     data.cov = zeros(72,72);
%     
%     ind = cond_i;
%     
%     subplot(2,2,ind); ft_topoplotER(cfg, data);
%     set(gcf,'Color',[1 1 1]);
%     %if contrasts_i ==1
%     
%     title(['Condition: ',CONDITIONS_NOAR{cond_i}],'FontSize',12);
%     %end
%     
% end