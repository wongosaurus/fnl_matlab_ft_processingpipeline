%% 1. Determine study specific working parameters.
clc; clear all; close all;
disp('1. Determine study specific working parameters.');
% set up data directories
workingvolume = 'E:\'; %this would vary per operating system.
wpms          = [];       %working parameters
wpms.dirs     = struct('CWD',[workingvolume 'FNL_EEG_TOOLBOX' filesep]);
wpms.dirs     = struct('CWD',[workingvolume 'FNL_EEG_TOOLBOX' filesep], ...
    'PACKAGES',[wpms.dirs.CWD 'PACKAGES' filesep], ...
    'FUNCTIONS',[wpms.dirs.CWD 'FUNCTIONS' filesep], ...
    'REPAIRED_DATA',[wpms.dirs.CWD 'REPAIRED_DATA' filesep],...
    'REPAIRED_DATA2',[wpms.dirs.CWD 'REPAIRED_DATA2' filesep],...
    'LOGS',[wpms.dirs.CWD 'FUNCTIONS\Sam_HONOURS\LOGS' filesep]);
mkdir(wpms.dirs.LOGS);    
addpath(genpath(wpms.dirs.FUNCTIONS));%functions
% set up study conditions

wpms.conditions = {'mixrepeat','switchto','noninfrepeat','noninfswitch'}; %{'allrepeat','mixrepeat','noninfrepeat','switchto','switchaway','noninfswitch','noninf'};


wpms.names ={'AGE003','AGE004','AGE005',...
    'AGE012','AGE022','AGE026','AGE030','AGE032',...
    'AGE035','AGE038','AGE046','AGE050','AGE058',...
    'AGE062','AGE064','AGE069','AGE077','AGE081',...
    'AGE089','AGE094','AGE100','AGE107','AGE108',...
    'AGE109','AGE114','AGE117','AGE118','AGE119',...
    'AGE129','AGE134','AGE138','AGE155',...
    'AGE162','AGE165','AGE166','AGE168',...
    'AGE169','AGE179','AGE182','AGE183','AGE184',...
    'AGE187','AGE192','AGE200','AGE205',...
    'AGE224','AGE225','AGE226','AGE230',...
    'AGE237','AGE238','AGE245','AGE247','AGE252',...
    'AGE260','AGE267','AGE268','AGE269','AGE270','AGE277',...
    'BGE003','BGE004','BGE005',...
    'BGE012','BGE022','BGE026','BGE030','BGE032',...
    'BGE035','BGE038','BGE046','BGE050','BGE058',...
    'BGE062','BGE064','BGE069','BGE077','BGE081',...
    'BGE089','BGE094','BGE100','BGE107','BGE108',...
    'BGE109','BGE114','BGE117','BGE118','BGE119',...
    'BGE129','BGE134','BGE138','BGE155',...
    'BGE162','BGE165','BGE166','BGE168',...
    'BGE169','BGE179','BGE182','BGE183','BGE184',...
    'BGE187','BGE192','BGE200','BGE205',...
    'BGE224','BGE225','BGE226','BGE230',...
    'BGE237','BGE238','BGE245','BGE247','BGE252',...
    'BGE260','BGE267','BGE268','BGE269','BGE270','BGE277'};
clear names listing


clear workingvolume listing temp_names list_i participant_index space_index file_i good_inds NAMES listings %tidy as we travel
cd(wpms.dirs.LOGS)
%%
disp('2. Bad channel counting')
badchannel_count = zeros(1,length(wpms.names));
for name_i=1:length(wpms.names)
    fprintf('\n%s\t',wpms.names{name_i});
    try
        filename = [wpms.dirs.REPAIRED_DATA wpms.names{name_i} '_RepairedData_allrepeat.mat'];
        load(filename);
    catch
        filename = [wpms.dirs.REPAIRED_DATA2 wpms.names{name_i} '_RepairedData_allrepeat.mat'];
        load(filename);
    end
    badchannel_count(1,name_i)=length(ardata.cfg.badchannel);
    fprintf('%i\t%s',length(ardata.cfg.badchannel),'bad channels');
end%name_i loop
savename = [wpms.dirs.LOGS 'badchannel_count'];
save(savename,'badchannel_count');
save(savename,'badchannel_count','-ascii','-tabs');
%Write to a CSV: AW_20171003
fid = fopen('badchannel_count.csv','w');
fprintf(fid,'Subject,NumberOfBadChannelsRejected\n');
for i = 1:length(wpms.names)
    fprintf(fid,'%s,%i\n',wpms.names{i},badchannel_count(i));
end
fclose(fid);

fprintf('\n');
%%
disp('3. Counting number of ICA components removed AND determining valid trial counts')
component_count = zeros(1,length(wpms.names));
valid_trials = zeros(1,length(wpms.names));
for name_i=1:length(wpms.names)
    tic;
    try
        fprintf('\n%s\t',wpms.names{name_i});
        try
            filename = [wpms.dirs.REPAIRED_DATA wpms.names{name_i} '_RepairedData_allrepeat.mat'];
            load(filename);
        catch
            filename = [wpms.dirs.REPAIRED_DATA2 wpms.names{name_i} '_RepairedData_allrepeat.mat'];
            load(filename);
        end
        component_count(1,name_i)=length(ardata.cfg.previous.previous.previous.previous.component);
        fprintf('%i\t%s\t\t',length(ardata.cfg.previous.previous.previous.previous.component),'components channels');
        valid_trials(1,name_i)=length(ardata.cfg.previous.previous.previous.previous.previous.previous.trl);
        fprintf('%i\t%s',length(ardata.cfg.previous.previous.previous.previous.previous.previous.trl),'valid trials');
    catch exception
        disp(['failed for ' wpms.names{name_i}]);
    end%try/catch loop
    t=toc;
    estimated_time = time_remaining(name_i,length(wpms.names),t);
    fprintf('\t%3.2f %s',estimated_time,'s remaining')
end%name_i loop
%ica components
savename = [wpms.dirs.LOGS 'icacomponent_count'];
save(savename,'component_count');
save(savename,'component_count','-ascii','-tabs');
%Write to a CSV: AW_20171003
fid = fopen('component_count.csv','w'); %Number of ICAComponents Rejected: Rejection Criterium: Mostly Eye blink, or eye related artefact, such as saccades.
fprintf(fid,'Subject,NumberOfBadICAComponentsRejected\n');
for i = 1:length(wpms.names)
    fprintf(fid,'%s,%i\n',wpms.names{i},component_count(i));
end
fclose(fid);
%valid trials
savename = [wpms.dirs.LOGS 'validtrial_count'];
save(savename,'valid_trials');
save(savename,'valid_trials','-ascii','-tabs');
%Write to a CSV: AW_20171003
fid = fopen('valid_trials.csv','w');
fprintf(fid,'Subject,NumberOfValidTrials\n'); %Number of ValidTrials: Accepted in TRL Stage: i.e. 1) < +-3SD of Mean, 2) faster 200ms, 3) not error and not trial preceeding error trial.
for i = 1:length(wpms.names)
    fprintf(fid,'%s,%i\n',wpms.names{i},valid_trials(i));
end
fclose(fid);

%% 
disp('4. Determining each trial''s count and RTs');
trial_count = zeros(length(wpms.conditions),length(wpms.names));
rts_mean = zeros(length(wpms.conditions),length(wpms.names));
rolling_average = zeros(1,length(wpms.names));
for name_i=1:length(wpms.names)
    fprintf('\n%s\t',wpms.names{name_i});
    ts = zeros(1,length(wpms.conditions));
    for cond_i = 1:length(wpms.conditions)
        tic;
        try
            fprintf('.');
            filename = [wpms.dirs.REPAIRED_DATA wpms.names{name_i} '_RepairedData_' wpms.conditions{cond_i} '.mat'];
            if exist(filename,'file')==2;
                A=load(filename);
                datname = fieldnames(A);
                trial_count(cond_i,name_i) = length(A.(datname{1}).trial);
                %meanRTs:
                originalTRL = A.(datname{1}).cfg.previous.previous.previous.previous.previous.previous.trl;
                indices = find(ismember(originalTRL(:,1),A.(datname{1}).sampleinfo(:,1)));
                rts_mean(cond_i,name_i) = mean(originalTRL(indices,5));
%             elseif exist(filename2,'file')==2;
%                 A=load(filename2);
%                 datname = fieldnames(A);
%                 trial_count(cond_i,name_i) = length(A.(datname{1}).trial);
%             elseif exist(filename3,'file')==2;
%                 A=load(filename3);
%                 datname = fieldnames(A);
%                 trial_count(cond_i,name_i) = length(A.(datname{1}).trial);
            elseif exist([wpms.dirs.REPAIRED_DATA2 wpms.names{name_i} '_RepairedData_' wpms.conditions{cond_i} '.mat'],'file')==2;
                filename=[wpms.dirs.REPAIRED_DATA2 wpms.names{name_i} '_RepairedData_' wpms.conditions{cond_i} '.mat'];
                A=load(filename);
                datname = fieldnames(A);
                trial_count(cond_i,name_i) = length(A.(datname{1}).trial);
                %meanRTs:
                originalTRL = A.(datname{1}).cfg.previous.previous.previous.previous.previous.previous.trl;
                indices = find(ismember(originalTRL(:,1),A.(datname{1}).sampleinfo(:,1)));
                rts_mean(cond_i,name_i) = mean(originalTRL(indices,5));
            else
                trial_count(cond_i,name_i) = NaN;
            end
        catch exception
            disp(['failed for ' wpms.names{name_i}]);
        end
        ts(1,cond_i) = toc;
    end%try/catch loop
    t=mean(ts);
    estimated_time = time_remaining(name_i,length(wpms.names),t);
    rolling_average(1,name_i) = estimated_time;
    fprintf('\t%3.2f %s',mean(rolling_average(1,1:name_i)),'s remaining')
end%name_i loop
savename = [wpms.dirs.LOGS 'trial_count'];
save(savename,'trial_count');
save(savename,'trial_count','-ascii','-tabs');
%Write to a CSV: AW_20171003
fid = fopen('trial_count.csv','w');
fprintf(fid,'Subject,'); %Trial Count per each condition, after +-120uv Rejection.
for cond_i = 1:length(wpms.conditions)
    fprintf(fid,'%s,',wpms.conditions{cond_i});
end
for i = 1:length(wpms.names)
    fprintf(fid,'%s,%i\n',wpms.names{i},trial_count(i));
end
fprintf(fid,'\n');
fclose(fid);

savename = [wpms.dirs.LOGS 'rts_mean'];
save(savename,'rts_mean');
save(savename,'rts_mean','-ascii','-tabs');

%WRite to a CSV:
fid = fopen([savename ,'.csv'],'w');
%Header:
fprintf(fid,'SUBJECT,');
for cond_i = 1:length(wpms.conditions)
    fprintf(fid,'%s,',wpms.conditions{cond_i});
end
fprintf(fid,'\n');

%main data:
for name_i = 1:length(wpms.names)
    fprintf(fid,'%s,',wpms.names{name_i});
    for cond_i = 1:length(wpms.conditions)
        fprintf(fid,'%4.6f,',rts_mean(cond_i,name_i));
    end
    fprintf(fid,'\n');
end
fclose(fid);

bar(mean(rts_mean,2));
set(gca,'XTickLabel',wpms.conditions);

%% 
disp('5. finding bad cases');
bad_cases = find(isnan(trial_count(1,:)));
for bad_i = 1:length(bad_cases)
    fprintf('\n%s %s',wpms.names{bad_cases(bad_i)},'did not have a trial file found');
end
fprintf('\n\n');
% these cases are probably located on another directory
% thus, we will try and find those there.
wpms.dirs.altdir = ['E:' filesep 'FNL_EEG_TOOLBOX' filesep 'PREPROC_OUTPUT' filesep];
wpms.dirs.altdir2  = ['E:' filesep 'FNL_EEG_TOOLBOX' filesep 'PREPROC_OUTPUT2' filesep];
rolling_average = zeros(1,length(bad_cases));
for name_i=1:length(bad_cases)
    fprintf('\n%s\t',wpms.names{bad_cases(name_i)});
    ts = zeros(1,length(wpms.conditions));
    for cond_i = 1:length(wpms.conditions)
        tic;
        try
            fprintf('.');
            filename = [wpms.dirs.altdir wpms.names{bad_cases(name_i)} '_ERP_' wpms.conditions{cond_i} '.mat'];
            filename2 = [wpms.dirs.altdir wpms.names{bad_cases(name_i)} '_CSD_' wpms.conditions{cond_i} '.mat'];
            filename3 = [wpms.dirs.altdir wpms.names{bad_cases(name_i)} '_' wpms.conditions{cond_i} '.mat'];
            
            filename4 = [wpms.dirs.altdir2 wpms.names{bad_cases(name_i)} '_ERP_' wpms.conditions{cond_i} '.mat'];
            filename5 = [wpms.dirs.altdir2 wpms.names{bad_cases(name_i)} '_CSD_' wpms.conditions{cond_i} '.mat'];
            filename6 = [wpms.dirs.altdir2 wpms.names{bad_cases(name_i)} '_' wpms.conditions{cond_i} '.mat'];
            if exist(filename,'file')==2;
                A=load(filename);
                datname = fieldnames(A);
                trial_count(cond_i,bad_cases(name_i)) = length(A.(datname{1}).trial);
            elseif exist(filename2,'file')==2;
                A=load(filename2);
                datname = fieldnames(A);
                trial_count(cond_i,bad_cases(name_i)) = length(A.(datname{1}).trial);
            elseif exist(filename3,'file')==2;
                A=load(filename3);
                datname = fieldnames(A);
                trial_count(cond_i,bad_cases(name_i)) = length(A.(datname{1}).trial);
            elseif exist(filename4,'file')==2;
                A=load(filename3);
                datname = fieldnames(A);
                trial_count(cond_i,bad_cases(name_i)) = length(A.(datname{1}).trial);
            elseif exist(filename5,'file')==2;
                A=load(filename3);
                datname = fieldnames(A);
                trial_count(cond_i,bad_cases(name_i)) = length(A.(datname{1}).trial);
            elseif exist(filename6,'file')==2;
                A=load(filename3);
                datname = fieldnames(A);
                trial_count(cond_i,bad_cases(name_i)) = length(A.(datname{1}).trial);
            else
                trial_count(cond_i,bad_cases(name_i)) = NaN;
            end
        catch exception
            disp(['failed for ' wpms.names{bad_cases(name_i)}]);
        end
        ts(1,cond_i) = toc;
    end%try/catch loop
    t=mean(ts);
    estimated_time = time_remaining(name_i,length(bad_cases),t);
    rolling_average(1,name_i) = estimated_time;
    fprintf('\t%3.2f %s',mean(rolling_average(1,1:name_i)),'s remaining')
end%name_i loop

disp('5b. determining if any remaining bad cases');
bad_cases = find(isnan(trial_count(1,:)));
if isempty(bad_cases)==1
    fprintf('No bad cases remaining')
else
    for bad_i = 1:length(bad_cases)
        fprintf('\n%s %s',wpms.names{bad_cases(bad_i)},'did not have a trial file found');
    end
end
fprintf('\n\n');
savename = [wpms.dirs.LOGS 'trial_count'];
save(savename,'trial_count');
save(savename,'trial_count','-ascii','-tabs');

%% Save Names used in this calculation:
%Convert to  Character format:
name_temp = {};
for name_i = 1:length(wpms.names)
    name_temp{name_i} = wpms.names{name_i}(1:6);
end
names_used_count = name_temp;
savename = [wpms.dirs.LOGS 'names_used'];
save(savename,'names_used_count');