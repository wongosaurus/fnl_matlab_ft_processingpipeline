clearvars; close all; clc;
%Data Structure Time/Frequency definition
frequencies = logspace(log10(2),log10(30),80);
times = (-1:0.001953125000000:3.5)*1000;
%Electrode Labels:
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
CHANNELS = 1:length(labels);
%CONDITIONS  = {'switchto','switchaway','noninf','mixrepeat','allrepeat'};
 
CONDITIONS  = {'allrepeat','mixrepeat','switchto','switchaway','noninfrepeat','noninfswitch'};

% Find the names of participants:
listings = dir('E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat');
NAMES = {length(listings)};
for file_i =1:length(listings)
    NAMES{file_i} = listings(file_i).name(1:6);
end

SAVEDIR = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\';
LOADDIR = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\';
%%
%CUE
% set up comparison structures
conditionA = {find(ismember(CONDITIONS,'mixrepeat')) ...
                find(ismember(CONDITIONS,'switchto'))...
                find(ismember(CONDITIONS, 'switchaway'))...
                find(ismember(CONDITIONS, 'noninfrepeat')),...
                find(~ismember(CONDITIONS,'allrepeat'))};
               
conditionB = {find(ismember(CONDITIONS,'allrepeat')) ...
                find(ismember(CONDITIONS,'mixrepeat'))...
                find(ismember(CONDITIONS,'switchto'))...
                find(ismember(CONDITIONS,'mixrepeat')),...
                find(ismember(CONDITIONS,'allrepeat'))}; 

costname   = {'Mix Cost','Switch Cost', 'STSA', 'RMRN','General Switch'};
% 
filename = [SAVEDIR 'Condition_Average_EEGPOWER_CUE_NOARBASELINE_170322.mat'];
load (filename);


%TARGET
% conditionA = [find(ismember(CONDITIONS,'mixrepeat')) ...
%                 find(ismember(CONDITIONS,'switchto'))...
%                 find(ismember(CONDITIONS, 'switchaway'))...
%                 find(ismember(CONDITIONS, 'noninfrepeat'))...
%                 find(ismember(CONDITIONS, 'noninfswitch'))];
%                
% conditionB = [find(ismember(CONDITIONS,'allrepeat')) ...
%                 find(ismember(CONDITIONS,'mixrepeat'))...
%                 find(ismember(CONDITIONS,'switchto'))...
%                 find(ismember(CONDITIONS,'mixrepeat'))...
%                 find(ismember(CONDITIONS,'switchaway'))]; 
% 
% costname   = {'Mix Cost','Switch Cost', 'STSA', 'RMRN', 'SNSA'};
% 
% filename = [SAVEDIR 'Condition_Average_EEGPOWER_TARGET_NOARBASELINE_170322.mat'];
% load (filename);
%% total
figure();
for contrast_i = 1:length(conditionA)
    contrast = squeeze(mean(condition_average_eegpower(conditionA{contrast_i},:,:,:),1)-mean(condition_average_eegpower(conditionB{contrast_i},:,:,:),1));
%chan_i = find(ismember(labels,{'CP1','CPz','CP2','P1','Pz','P2','POz'}));
chan_i = find(ismember(labels,{'C1','Cz','C2','FC1','FCz','FC2'})); 
    subplot(1,length(conditionA),contrast_i);
    contourf(times,frequencies,squeeze(mean(contrast(chan_i,:,:),1)),50,'linecolor','none');
    caxis([-5 5]);
    colormap 'jet';
    axis square
    title(costname{contrast_i});
end

addpath(genpath('F:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip-20150902\fieldtrip-20150902'));
freqs_of_interest = [find(round(frequencies,3)==5.593),...
    find(round(frequencies,3)==9.038),...
    find(round(frequencies,3)==6.199),...
    find(round(frequencies,3)==7.614)];
   
times_of_interest = [find(round(times)==373),...
    find(round(times)==293),...
    find(round(times,1)==384.8),...
    find(round(times,1)==318.4)];
for contrast_i = 1:length(conditionA)
    contrast = squeeze(condition_average_induced(conditionA(contrast_i),:,:,:)-condition_average_induced(conditionB(contrast_i),:,:,:));
    cfg = [];
    cfg.zlim        = [-2 2];
    cfg.colormap    = 'jet';
    cfg.parameter   = 'avg';
    cfg.layout      = 'biosemi64.lay';
    cfg.comment     = 'no';
    cfg.marker      = '';
    cfg.channel     = labels;
    cfg.contournum  = 4;
    cfg.gridsize    = 600;
    data =[];
    data.avg = zeros(72,1);
    data_temp  = squeeze(mean(mean(contrast(:,freqs_of_interest(contrast_i),times_of_interest(contrast_i)),3),2));
    data.avg = [data_temp;0;0;0;0;0;0;0;0];
    data.avg = (data.avg);
    clear pval_i
    data.var = zeros(72,1);
    data.time = 1;
    data.label = labels;
    data.dimord = 'chan_time';
    data.cov = zeros(72,72);
    subplot(1,length(conditionA),contrast_i); ft_topoplotER(cfg, data);
end 

%% quickly look for mftheta
freqs_of_interest = [find(round(frequencies,3)==6.639),...
    find(round(frequencies,3)==6.199),...
    find(round(frequencies,2)==11.89),...
    find(round(frequencies,3)==6.199),...
    find(round(frequencies,3)==6.199)];

times_of_interest = 513:10:2305;
for time_i = 1:length(times_of_interest)
    contrast = squeeze(mean(condition_average_eegpower(conditionA{5},:,:,:),1)-mean(condition_average_eegpower(conditionB{1},:,:,:),1));
    cfg = [];
    cfg.zlim        = [-5 5];
    cfg.colormap    = 'jet';
    cfg.parameter   = 'avg';
    cfg.layout      = 'biosemi64.lay';
    cfg.comment     = 'no';
    cfg.marker      = '';
    cfg.channel     = labels;
    cfg.contournum  = 4;
    cfg.gridsize    = 600;
    data =[];
    data.avg = zeros(72,1);
    data_temp  = squeeze(mean(mean(contrast(:,freqs_of_interest(5),times_of_interest(time_i)),3),2));
    data.avg = [data_temp;0;0;0;0;0;0;0;0];
    data.avg = (data.avg);
    clear pval_i
    data.var = zeros(72,1);
    data.time = 1;
    data.label = labels;
    data.dimord = 'chan_time';
    data.cov = zeros(72,72);
    subplot(1,1,1); ft_topoplotER(cfg, data);
    title(num2str(times(times_of_interest(time_i))));
    pause(0.1);
end 
%%
figure();
for contrast_i = 1:length(conditionA)
    contrast = squeeze(mean(condition_average_eegpower(conditionA{contrast_i},:,:,:),1)-mean(condition_average_eegpower(conditionB{contrast_i},:,:,:),1));
chan_i = find(ismember(labels,{'CP1','CPz','CP2','P1','Pz','P2','POz'}));
% chan_i = find(ismember(labels,{'C1','Cz','C2','FC1','FCz','FC2'})); 
    subplot(1,length(conditionA),contrast_i);
    contourf(times,frequencies,squeeze(mean(contrast(chan_i,:,:),1)),50,'linecolor','none');
    caxis([-5 5]);
    colormap 'jet';
    axis square
    title(costname{contrast_i});
end
%% induced
figure();
for contrast_i = 1:length(conditionA)
    contrast = squeeze(condition_average_induced(conditionA(contrast_i),:,:,:)-condition_average_induced(conditionB(contrast_i),:,:,:));
chan_i = find(ismember(labels,{'CP1','CPz','CP2','P1','Pz','P2','POz'}));
%chan_i = find(ismember(labels,{'C1','Cz','C2','FC1','FCz','FC2'})); 
    subplot(1,length(conditionA),contrast_i);
    contourf(times,frequencies,squeeze(mean(contrast(chan_i,:,:),1)),50,'linecolor','none');
    caxis([-1 1]);
    colormap 'jet';
    axis square
    title(['Non-phase-locked ' costname{contrast_i}]);
end

figure();
for contrast_i = 1:length(conditionA)
    contrast = squeeze(condition_average_induced(conditionA(contrast_i),:,:,:)-condition_average_induced(conditionB(contrast_i),:,:,:));
%chan_i = find(ismember(labels,{'CP1','CPz','CP2','P1','Pz','P2','POz'}));
chan_i = find(ismember(labels,{'C1','Cz','C2','FC1','FCz','FC2'})); 
    subplot(1,length(conditionA),contrast_i);
    contourf(times,frequencies,squeeze(mean(contrast(chan_i,:,:),1)),50,'linecolor','none');
    caxis([-1 1]);
    colormap 'jet';
    axis square
     title(['Non-phase-locked ' costname{contrast_i}]);
end
%% evoked
figure();
for contrast_i = 1:length(conditionA)
    contrast = squeeze((condition_average_eegpower(conditionA(contrast_i),:,:,:)-condition_average_induced(conditionA(contrast_i),:,:,:)-(condition_average_eegpower(conditionB(contrast_i),:,:,:)-condition_average_induced(conditionB(contrast_i),:,:,:))));
chan_i = find(ismember(labels,{'CP1','CPz','CP2','P1','Pz','P2','POz'}));
%chan_i = find(ismember(labels,{'C1','Cz','C2','FC1','FCz','FC2'})); 
    subplot(1,length(conditionA),contrast_i);
    contourf(times,frequencies,squeeze(mean(contrast(chan_i,:,:),1)),50,'linecolor','none');
    caxis([-5 5]);
    colormap 'jet';
    axis square
    title(costname{contrast_i});
end

figure();
for contrast_i = 1:length(conditionA)
    contrast = squeeze((condition_average_eegpower(conditionA(contrast_i),:,:,:)-condition_average_induced(conditionA(contrast_i),:,:,:)-(condition_average_eegpower(conditionB(contrast_i),:,:,:)-condition_average_induced(conditionB(contrast_i),:,:,:))));
%chan_i = find(ismember(labels,{'CP1','CPz','CP2','P1','Pz','P2','POz'}));
chan_i = find(ismember(labels,{'C1','Cz','C2','FC1','FCz','FC2'})); 
    subplot(1,length(conditionA),contrast_i);
    contourf(times,frequencies,squeeze(mean(contrast(chan_i,:,:),1)),50,'linecolor','none');
    caxis([-5 5]);
    colormap 'jet';
    axis square
    title(costname{contrast_i});
end