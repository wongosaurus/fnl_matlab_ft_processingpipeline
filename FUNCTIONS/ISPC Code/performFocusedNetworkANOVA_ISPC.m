clear all;close all;clc

listings = dir('E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat');
NAMES = {length(listings)};
for file_i =1:length(listings) %CHECK: set to 5 to check
    NAMES{file_i} = listings(file_i).name(1:6);
end



addpath(genpath('E:\FNL_EEG_TOOLBOX\FUNCTIONS\'))
datain = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\NoBL\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
frequencies = logspace(log10(2),log10(30),80);
times = (-1:0.001953125000000:3.5)*1000;

wpms.conditions         ={'allrepeat',  'mixrepeat',    'noninfrepeat', 'switchto',    'switchaway',   'noninfswitch'};


Frontal_Labels = {'FCz','FC1','FC2','Cz','C1','C2'};%frontal
Parietal_Labels = {'CPz','CP1','CP2','Pz','P1','P2'};%parietal

CHANNEL_CLUSTER = {Frontal_Labels,Parietal_Labels};

cluster_names = {'frontal','parietal'};

baseline_start = 700;%start of plot
baseline_end = 900;%end of plot
%find values
temp_times = times-baseline_start;
[~,baselinetimestart_index] = min(abs(temp_times));
temp_times = times-baseline_end;
[~,baselinetimeend_index] = min(abs(temp_times));
%store in a single variable
baseline_time = baselinetimestart_index:baselinetimeend_index;

ISPCdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\';

addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'))
%%
% This data is after the ISPC_anovacode_power_target.m
% Located in E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\NoBL\
load('E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\NoBL\CLUSTER_MaxPixelLoc_target_20170502.mat');

plottime_start = 700;%start of plot
plottime_end = 2000;%end of plot
%find values
temp_times = times-plottime_start;
[~,plottimestart_index] = min(abs(temp_times));
temp_times = times-plottime_end;
[~,plottimeend_index] = min(abs(temp_times));
%store in a single variable
plot_times = plottimestart_index:plottimeend_index;

cluster_ends = {find(ismember(labels,Parietal_Labels));find(ismember(labels,Frontal_Labels))};

for cluster_i = 1:length(cluster_names)
    fprintf('Currently Running Cluster %i: \t %s\n\n', cluster_i, cluster_names{cluster_i});
    ispc_means = zeros(length(NAMES),length(wpms.conditions),80,2305);
    for name_i = 1:length(NAMES)
        tic
        fprintf('\n%s',NAMES{name_i})
        for condition_i = 1:length(wpms.conditions)
            fprintf('\t%s',wpms.conditions{condition_i})
            if cluster_i == 1
                filename=['Z:\ITPC\PROCESSED\SurfaceLapacian\' NAMES{name_i} '_' wpms.conditions{condition_i} '_ISPC_MF_TARGET.mat'];
            elseif cluster_i == 2
                filename=['Z:\ITPC\PROCESSED\SurfaceLapacian\' NAMES{name_i} '_' wpms.conditions{condition_i} '_ISPC_CP_TARGET.mat'];
            end
            load (filename,'ispc_all')
            ispc_bl = zeros(size(ispc_all,2),size(ispc_all,3));
            clustAv = squeeze(mean(ispc_all(cluster_ends{cluster_i},:,:),1));
            temp    = bsxfun(@minus,clustAv,squeeze(mean(clustAv(:,baseline_time),2)));
            bl     = (mean(squeeze(clustAv(:,baseline_time)),2));
            temp2 = zeros(size(temp));
            for temp_i = 1:size(temp,1)
                temp2(temp_i,:) = temp(temp_i,:)./bl(temp_i); %Ratio: Change over Baseline
            end
            ispc_bl = 100*(temp2); %Percentage Change over baseline
            clear temp temp2 bl
            ispc_means(name_i,condition_i,:,:) = ispc_bl;
            clear ispc_bl
        end
        fprintf('\t');toc
    end%name_i loop
    comparison_name = cluster_names(~ismember(cluster_names,cluster_names{cluster_i}));
    savename=[ISPCdir cluster_names{cluster_i} '_to_' comparison_name{1} '_ISPC_target_alltfspace.mat'];
    save(savename, 'ispc_means','-v7.3');
end%cluster_i loop
%% perform ANOVA
for cluster_i = 1:length(cluster_names)
    comparison_name = cluster_names(~ismember(cluster_names,cluster_names{cluster_i}));
    filename = [ISPCdir cluster_names{cluster_i} '_to_' comparison_name{1} '_ISPC_target_alltfspace.mat'];
    load(filename);
    cluster_pfStat_ISPC = zeros(length(frequencies),length(plot_times));
    cluster_fStat_ISPC  = cluster_pfStat_ISPC;
    for freq_i = 1:length(frequencies)
        tic;
        fprintf('\n%s\t%i\t','Frequency:',freq_i);
        for time_i = 1:length(plot_times)
%             data= squeeze(ispc_means(:,:,freq_i,plot_times(time_i)));
            data= squeeze(ispc_means(:,:,freq_i,plot_times(time_i)));
            t = table(data(:,1),data(:,2),data(:,3),data(:,4),data(:,5),data(:,6),...
                'VariableNames',{'data1','data2','data3','data4','data5','data6'});
            Meas = dataset([1 2 3 4 5 6]','VarNames',{'Measurements'});

            rm = fitrm(t,'data1-data6~1','WithinDesign',Meas);

            ranovatbl = ranova(rm);
            cluster_pfStat_ISPC(freq_i,time_i)=ranovatbl{1,5};
            cluster_fStat_ISPC(freq_i,time_i)=ranovatbl{1,4};
        end
        toc
    end
    savename = [ISPCdir cluster_names{cluster_i} '_to_' comparison_name{1} '_ISPC_target_alltfspace_ANOVA_results.mat'];
    save(savename,'cluster_pfStat_ISPC','cluster_fStat_ISPC','-v7.3');
end
%% visualise ANOVA results
climits = [-25 25];
plotstimes_start = 700;%start of plot
plotstimes_end = 2000;%end of plot
%find values
temp_times = times-plotstimes_start;
[~,plotstimesstart_index] = min(abs(temp_times));
temp_times = times-plotstimes_end;
[~,plotstimesend_index] = min(abs(temp_times));
%store in a single variable
plot_times = plotstimesstart_index:plotstimesend_index;
%threshold for fdr
alpha = .001;%.05;
addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'));
for cluster_i = 1:length(cluster_names)
    comparison_name = cluster_names(~ismember(cluster_names,cluster_names{cluster_i}));
    filename = [ISPCdir cluster_names{cluster_i} '_to_' comparison_name{1} '_ISPC_target_alltfspace_ANOVA_results.mat'];
    load(filename);
    % apply FDR correction
    %ERSP
    [pfStatCorr_ISPC,~]=fdr_bky(cluster_pfStat_ISPC,alpha);%anova p
    %ERSP
    subplot(2,1,cluster_i);
    contourf(times(plot_times),frequencies,cluster_fStat_ISPC,50,'linecolor','none');colormap jet;caxis(climits);
    hold on;
    contour(times(plot_times),frequencies,pfStatCorr_ISPC,1,'k');axis square;
%     contour(times(plot_times),frequencies,cluster_pfStat_ISPC<.05==1,1,'k');axis square;
    title([cluster_names{cluster_i} ' to ' comparison_name{1} ' F-Map ISPC']);
end

%% Find Locations of these Peaks inside the ISPC Network Plots:
clc;
subplot_height = 2;
subplot_width =1;
%find significant clusters
width = 120;  %Time Bins
height = 6; %Frequency Bins:
frequency_offset = 22; %4HZ: Max from 4Hz to 30Hz;
frequency_index_max = 68;%20Hz
clear ISPC_clusters
count = 1;
for cluster_i = 1:length(cluster_names)
    % Load Cluster Data:
    comparison_name = cluster_names(~ismember(cluster_names,cluster_names{cluster_i}));
    filename = [ISPCdir cluster_names{cluster_i} '_to_' comparison_name{1} '_ISPC_target_alltfspace_ANOVA_results.mat'];
    load(filename);
    [pfStatCorr_ISPC,~]=fdr_bky(cluster_pfStat_ISPC,alpha);
    ISPC_clusters.(cluster_names{cluster_i})=bwconncomp(pfStatCorr_ISPC(frequency_offset:frequency_index_max,:));%find significant clusters
    
    for pixel_i = 1:length(ISPC_clusters.(cluster_names{cluster_i}).PixelIdxList)
        reduced_data = cluster_fStat_ISPC(frequency_offset:frequency_index_max,:);
        [m,ind]=max(reduced_data(ISPC_clusters.(cluster_names{cluster_i}).PixelIdxList{pixel_i}));
        [freq_max,time_max] =find (reduced_data==m);
        freq_max = frequency_offset+freq_max-1;
        if freq_max > frequency_index_max
            ISPC_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
            fprintf('Cluster %i, Pixel %i: Freq: %3.3f Hz, at Time: %3.3f ms. Out of Frequency Range...\n',cluster_i,pixel_i,frequencies(freq_max),times(plot_times(time_max)));
            continue;
        end
        if  times(plot_times(time_max))> plotstimes_end || frequencies(freq_max)< 4 || ...
            time_max - width/2 < 0 || time_max + width/2 > length(plot_times) || ... %out of Sample conditions (Time) 
            freq_max - height/2 < 0 || freq_max + height/2 > length(frequencies)
            %times(plot_times(time_max))< 700 || times(plot_times(time_max))> 2000 || frequencies(freq_max)< 4|| frequencies(freq_max)>20
            ISPC_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
            fprintf('Cluster %i, Pixel %i: Freq: %3.3f Hz, at Time: %3.3f ms. Did not meet testing requirements, Moving on...\n',cluster_i,pixel_i,frequencies(freq_max),times(plot_times(time_max)));
            continue;
        end
        if numel(reduced_data(ISPC_clusters.(cluster_names{cluster_i}).PixelIdxList{pixel_i})) < 100 %(width+1)*(height+1)
            ISPC_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
            fprintf('Cluster %i, Pixel %i: Freq: %3.3f Hz, at Time: %3.3f ms. Cluster too Small, Moving on...\n',cluster_i,pixel_i,frequencies(freq_max),times(plot_times(time_max)));
            continue;
        end
        subplot(subplot_height,subplot_width,count);
        fprintf('Plotting: Cluster %i, Pixel %i: Freq: %3.3f Hz, at Time: %3.3f ms.\n',cluster_i,pixel_i,frequencies(freq_max),times(plot_times(time_max)));
        scatter(times(plot_times(time_max)),frequencies(freq_max),10,'k','filled');
        x = times(plot_times(time_max - width/2));
        width_t = times(plot_times(time_max + width/2)) - x;
        y = frequencies(freq_max - height/2);
        height_f = frequencies(freq_max + height/2) - y;
        hold on;
        rectangle('Position',[x,y,width_t,height_f]);

        ISPC_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [freq_max,time_max];
        
    end
    count = count +1;
end
save([ISPCdir 'ISPC_target_cluster_indx.mat'],'ISPC_clusters');
%% Code from the Cluster_Pixel_Topololgy_Plot:


