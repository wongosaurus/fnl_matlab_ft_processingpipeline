%% create figures for publication
%% ERSP figure
%two time-frequency plots
% 5 topo plots for frontal
% 2 topo plots for parietal
% set up
SAVEDIR = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\';
LOADDIR = SAVEDIR;
datain = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\';
frequencies = logspace(log10(2),log10(30),80);
times = (-1:0.001953125000000:3.5)*1000;
wpms.dirs.PACKAGES = 'E:\FNL_EEG_TOOLBOX\PACKAGES\';
load([LOADDIR 'CLUSTER_PIXEL_CHAN_COND_STATS_20170428.mat']);
addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox'))
close all;
cluster = {{'FCz','FC1','FC2','Fz','F1','F2'};{'CPz','CP1','CP2','Pz','P1','P2'}};
addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));

labels = {'FP1';'AF7';'AF3';'F1';'F3';'F5';'F7';'FT7';'FC5';'FC3'; ...
    'FC1';'C1';'C3';'C5';'T7';'TP7';'CP5';'CP3';'CP1';'P1';'P3'; ...
    'P5';'P7';'P9';'PO7';'PO3';'O1';'Iz';'Oz';'POz';'Pz';'CPz'; ...
    'Fpz';'Fp2';'AF8';'AF4';'AFz';'Fz';'F2';'F4';'F6';'F8';'FT8'; ...
    'FC6';'FC4';'FC2';'FCz';'Cz';'C2';'C4';'C6';'T8';'TP8';'CP6'; ...
    'CP4';'CP2';'P2';'P4';'P6';'P8';'P10';'PO8';'PO4';'O2';'M1'; ...
    'M2';'LO1';'LO2';'SO1';'SO2';'IO1';'IO2';};

cfg = [];
cfg.zlim = [-50.0 50.0];
cfg.colormap = 'jet';
cfg.parameter   = 'avg';
cfg.layout      = 'biosemi64.lay';
cfg.comment   = 'no';
cfg.marker = 'off';
cfg.markersize = 4;
cfg.contournum = 1;
cfg.gridscale = 600;
cfg.highlight = 'on';
cfg.highlightfontsize = 0.5; %no label writing
cfg.highlightsize = 4; %no label writing
cfg.highlightsymbol = '*';
cfg.highlightcolor = [0 0 1]; %blue
height = 1080*0.8;
width = 1920/2;
ylims = [6 13.5]; %[AW 28/3/17: ERSP Plot Limits changed to [-2, 3.5], was [-2,10] ]
xrotation = 90;%rotation for bar plot x tick labels
condname = {'RA','RM','ST','SA','NI'};%reordered and shortened names
% AR vs. RM
%RM vs. ST
%RM vs. SA
%RM vs. NI
%ST vs. SA
%ST vs. NI
%SA vs. NI

x_test_inds = [1 2 2 2 3 3 4];
y_test_inds = [2 3 4 5 4 5 5];

threshold = 0.001;
%????????????????
% set up fileID for ttests
stat_filename = 'ANOVA_cluster_stats_CUE_AW170502.txt';
fid = fopen(stat_filename,'w');
%% plot ERSP topoclusters (frontal)
cd('E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\');
load('CLUSTER_PIXEL_CHAN_COND_STATS_20170428.mat');
load('CLUSTER_PIXEL_CHANNEL_SUB_COND_20170428.mat');
figure();set(gcf,'Color',[1 1 1],'Position',[0 0 width height]);

cluster_i = 1;
headplot_inds = [8  ];
barplot_inds  = [7  ];%headplot_inds+1;
count = 0;
%fprintf(fid,'%s\n%s\n','ERSP RESULTS','FRONTAL');
for pixel_i = [1];
    %fprintf(fid,'%s\t%s\n','Cluster:',num2str(pixel_i));
    count = count+1;
    subplot(5,6,headplot_inds(count));% topoplot ERSP cluster 1, pixel 1
    data =[];
    plot_data = double(squeeze(cluster_fStat_ERSP(cluster_i,pixel_i,:)));
    plot_p_data = double(squeeze(cluster_pfStat_ERSP(cluster_i,pixel_i,:)));
    data.avg = zeros(72,1);
    data.avg = [plot_data;0;0;0;0;0;0;0;0];
    clear pval_i
    data.var = zeros(72,1);
    data.time = 1;
    data.label = labels;
    data.dimord = 'chan_time';
    data.cov = zeros(72,72);
    
    
    plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
    plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
    %         data4plot_ind = find(plot_p_data<threshold);
    %         cfg.highlightchannel = labels(data4plot_ind);
    cfg.highlightchannel = labels(plot_p_data_corr==1);
    
    ft_topoplotER(cfg, data);
    
    %bar plot
    chan_i = find(ismember(labels,cluster{cluster_i}));
    %mean
%     plot_data = bsxfun(@minus,...
%         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
%         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
    plot_data = ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:5);
    plot_data = squeeze(mean(plot_data,3));
    barplot_data  = [plot_data(:,5),plot_data(:,3),plot_data(:,1),plot_data(:,2),plot_data(:,4)];
    errorbar_data = std(barplot_data)./sqrt(length(barplot_data));
    subplot(5,6,barplot_inds(count));
    bar(mean(barplot_data,1));box off;
    if barplot_inds(count) == 7 %[AW: Left Parietal BarPlot Location]
        ylim([-6 6]);
    else
        ylim(ylims);
    end
    xlim([0 6]);
    hold on;
    errorbar(1:size(barplot_data,2),mean(barplot_data),errorbar_data,'k.','MarkerEdgeColor','k');axis square;
    set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14);
    rmpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
    for test_i = 1:length(x_test_inds)
        Statement = [];
        x=squeeze(barplot_data(:,x_test_inds(test_i)));
        y=squeeze(barplot_data(:,y_test_inds(test_i)));
        [h,p,~,stats]=ttest(x,y);
        if (mean(x)>mean(y))
            direction = 'larger';
        else
            direction = 'smaller';
        end
        %if h== 1
            if p < .001
                pval_str = ', p < .001';
            else
                pval_str = [', p = ', sprintf('%3.3f',p)];
            end;
        %end
        
        fprintf(fid,'\tERSP Frontal Pixel: %3i, Head_Plot: %3i',pixel_i,count);
        fprintf(fid,'\t%s\t%s\t%s',condname{x_test_inds(test_i)},'vs',condname{y_test_inds(test_i)});
        fprintf(fid,'\t\t%s%i%s%2.2f%s\n','t(',stats.df,') = ',stats.tstat,pval_str);
     
    end
    addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
        
end

%% plot parietal clusters
cluster_i = 2;
headplot_inds = [20 26 29];
barplot_inds  = [19 25 30];%headplot_inds+1;
count = 0;
for pixel_i = [2];
    count = count+1;
    subplot(5,6,headplot_inds(count));% topoplot ERSP cluster 1, pixel 1
    data =[];
    plot_data = double(squeeze(cluster_fStat_ERSP(cluster_i,pixel_i,:)));
    plot_p_data = double(squeeze(cluster_pfStat_ERSP(cluster_i,pixel_i,:)));
    data.avg = zeros(72,1);
    data.avg = [plot_data;0;0;0;0;0;0;0;0];
    clear pval_i
    data.var = zeros(72,1);
    data.time = 1;
    data.label = labels;
    data.dimord = 'chan_time';
    data.cov = zeros(72,72);
    
    %threshold = 0.05;
    plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
    plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
    %         data4plot_ind = find(plot_p_data<threshold);
    %         cfg.highlightchannel = labels(data4plot_ind);
    cfg.highlightchannel = labels(plot_p_data_corr==1);
    
    ft_topoplotER(cfg, data);
    
    chan_i = find(ismember(labels,cluster{cluster_i}));
    %mean
%     plot_data = bsxfun(@minus,...
%         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
%         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
    plot_data =ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:5);
    plot_data = squeeze(mean(plot_data,3));
    barplot_data  = [plot_data(:,5),plot_data(:,3),plot_data(:,1),plot_data(:,2),plot_data(:,4)];
    errorbar_data = std(barplot_data)./sqrt(length(barplot_data));
    subplot(5,6,barplot_inds(count));
    bar(mean(barplot_data,1));box off;
    xlim([0 6]);
    if barplot_inds(count) == 19 %[AW: Left Parietal BarPlot Location]
        ylim([-6 6]);
    elseif barplot_inds(count) == 16
        ylim([-6 6]); 
    else
        ylim(ylims);
    end
    hold on;
    errorbar(1:size(barplot_data,2),mean(barplot_data),errorbar_data,'k.','MarkerEdgeColor','k');axis square;
    set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14);
    rmpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
    for test_i = 1:length(x_test_inds)
        Statement = [];
        x=squeeze(barplot_data(:,x_test_inds(test_i)));
        y=squeeze(barplot_data(:,y_test_inds(test_i)));
        [h,p,~,stats]=ttest(x,y);
        if (mean(x)>mean(y))
            direction = 'larger';
        else
            direction = 'smaller';
        end
        %if h== 1
            if p < .001
                pval_str = ', p < .001';
            else
                pval_str = [', p = ', sprintf('%3.3f',p)];
            end;
        %end
        fprintf(fid,'\tERSP Parietal Pixel: %3i, Head_Plot: %3i',pixel_i,count);
        fprintf(fid,'\t%s\t%s\t%s',condname{x_test_inds(test_i)},'vs',condname{y_test_inds(test_i)});
        fprintf(fid,'\t\t%s%i%s%2.2f%s\n','t(',stats.df,') = ',stats.tstat,pval_str);
     
    end
    addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
    if count == 1
        ylabel('Power (dB)','FontSize',14);
    end
    
end
%% now plot time-frequency plots
%load ERSP
cluster_name = {'frontal','parietal'};
plot_name    = {sprintf('%s %s','Frontal','Power CUE'),sprintf('%s %s','Parietal', 'Power CUE')};
plot_inds = {[3 4 9 10];[21 22 27 28]};
for cluster_i = 1:length(cluster_name)
    savename_fstat  = [datain 'fstat_ERSP_' cluster_name{cluster_i} '_20170428.mat'];load(savename_fstat);
    savename_pfstat = [datain 'pfstat_ERSP_' cluster_name{cluster_i} '_20170428.mat'];load(savename_pfstat);
    % set up plot times - no need to show or correct for additional times
    plottime_start = -300;%start of plot
    plottime_end =1300;%end of plot
    %find values
    temp_times = times-plottime_start;
    [~,plottimestart_index] = min(abs(temp_times));
    temp_times = times-plottime_end;
    [~,plottimeend_index] = min(abs(temp_times));
    %store in a single variable
    plot_times = plottimestart_index:plottimeend_index;
    % apply fdr correction
    addpath(genpath([wpms.dirs.PACKAGES 'mass_uni_toolbox']))
    alpha = threshold;
    %ERSP
    [pfStatCorr_ERSP,~]=fdr_bky(pfStat_ERSP(:,plot_times),alpha);%anova p
    %[prStatCorr_ERSP,~]=fdr_bky(prStat_ERSP(:,plot_times,:),alpha);%corr p

    subplot(5,6,plot_inds{cluster_i});
    contourf(times(plot_times),frequencies,fStat_ERSP(:,plot_times),50,'linecolor','none');colormap jet;caxis([-50 50]);
    hold on;
    contour(times(plot_times),frequencies,pfStatCorr_ERSP,1,'k');axis square;
    title(plot_name{cluster_i});set(gca,'FontSize',14,'YTick',[2,10,20,29.9],'YTickLabel',[{'2'},{'10'},{'20'},{'30'}]);
end

%% Start the INDUCED:

figure();set(gcf,'Color',[1 1 1],'Position',[0 0 width height]);

cluster_i = 1;
headplot_inds = [8  ];
barplot_inds  = [7  ];%headplot_inds+1;
count = 0;
fprintf(fid,'%s\n%s\n','INDUCED POWER RESULTS','FRONTAL');

for pixel_i = [4];
    %fprintf(fid,'%s\t%s\n','Cluster:',num2str(pixel_i));
    count = count+1;
    subplot(5,6,headplot_inds(count));% topoplot ERSP cluster 1, pixel 1
    data =[];
    plot_data = double(squeeze(cluster_fStat_INDUCEDPOWER(cluster_i,pixel_i,:)));
    plot_p_data = double(squeeze(cluster_pfStat_INDUCEDPOWER(cluster_i,pixel_i,:)));
    data.avg = zeros(72,1);
    data.avg = [plot_data;0;0;0;0;0;0;0;0];
    clear pval_i
    data.var = zeros(72,1);
    data.time = 1;
    data.label = labels;
    data.dimord = 'chan_time';
    data.cov = zeros(72,72);
    
    
    plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
    plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
    %         data4plot_ind = find(plot_p_data<threshold);
    %         cfg.highlightchannel = labels(data4plot_ind);
    cfg.highlightchannel = labels(plot_p_data_corr==1);
    
    ft_topoplotER(cfg, data);
    
    %bar plot
    chan_i = find(ismember(labels,cluster{cluster_i}));
    %mean
%     plot_data = bsxfun(@minus,...
%         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
%         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
    plot_data = INDUCEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:5);
    plot_data = squeeze(mean(plot_data,3));
    barplot_data  = [plot_data(:,5),plot_data(:,3),plot_data(:,1),plot_data(:,2),plot_data(:,4)];
    errorbar_data = std(barplot_data)./sqrt(length(barplot_data));
    subplot(5,6,barplot_inds(count));
    %fprintf('Induced Means:\n');
    %mean(barplot_data,1);
    bar(mean(barplot_data,1));
    box off;
    if barplot_inds(count) == 7 %[AW: Left Parietal BarPlot Location]
        ylim([-1 1]);
    else
        ylim(ylims);
    end
    xlim([0 6]);
    hold on;
    errorbar(1:size(barplot_data,2),mean(barplot_data),errorbar_data,'k.','MarkerEdgeColor','k');axis square;
    set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14);
    rmpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
    for test_i = 1:length(x_test_inds)
        Statement = [];
        x=squeeze(barplot_data(:,x_test_inds(test_i)));
        y=squeeze(barplot_data(:,y_test_inds(test_i)));
        [h,p,~,stats]=ttest(x,y);
        if (mean(x)>mean(y))
            direction = 'larger';
        else
            direction = 'smaller';
        end
        %if h== 1
            if p < .001
                pval_str = ', p < .001';
            else
                pval_str = [', p = ', sprintf('%3.3f',p)];
            end;
        %end
        
        fprintf(fid,'\tInduced Power Frontal Pixel: %3i, Head_Plot: %3i',pixel_i,count);
        fprintf(fid,'\t%s\t%s\t%s',condname{x_test_inds(test_i)},'vs',condname{y_test_inds(test_i)});
        fprintf(fid,'\t\t%s%i%s%2.2f%s\n','t(',stats.df,') = ',stats.tstat,pval_str);
     
    end
    addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
        
end

%% plot parietal clusters
cluster_i = 2;
headplot_inds = [20 26 29];
barplot_inds  = [19 25 30];%headplot_inds+1;
count = 0;
for pixel_i = [4];
    count = count+1;
    subplot(5,6,headplot_inds(count));% topoplot ERSP cluster 1, pixel 1
    data =[];
    plot_data = double(squeeze(cluster_fStat_INDUCEDPOWER(cluster_i,pixel_i,:)));
    plot_p_data = double(squeeze(cluster_pfStat_INDUCEDPOWER(cluster_i,pixel_i,:)));
    data.avg = zeros(72,1);
    data.avg = [plot_data;0;0;0;0;0;0;0;0];
    clear pval_i
    data.var = zeros(72,1);
    data.time = 1;
    data.label = labels;
    data.dimord = 'chan_time';
    data.cov = zeros(72,72);
    
    %threshold = 0.05;
    plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
    plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
    %         data4plot_ind = find(plot_p_data<threshold);
    %         cfg.highlightchannel = labels(data4plot_ind);
    cfg.highlightchannel = labels(plot_p_data_corr==1);
    
    ft_topoplotER(cfg, data);
    
    chan_i = find(ismember(labels,cluster{cluster_i}));
    %mean
%     plot_data = bsxfun(@minus,...
%         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
%         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
    plot_data =INDUCEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:5);
    plot_data = squeeze(mean(plot_data,3));
    barplot_data  = [plot_data(:,5),plot_data(:,3),plot_data(:,1),plot_data(:,2),plot_data(:,4)];
    errorbar_data = std(barplot_data)./sqrt(length(barplot_data));
    subplot(5,6,barplot_inds(count));
    fprintf('Induced Means:\n');
    mean(barplot_data,1)
    bar(mean(barplot_data,1));
    
    box off;
    xlim([0 6]);
    if barplot_inds(count) == 19 %[AW: Left Parietal BarPlot Location]
        ylim([-0.1 1]);
    elseif barplot_inds(count) == 16
        ylim([-4.5 1]); 
    else
        ylim(ylims);
    end
    hold on;
    errorbar(1:size(barplot_data,2),mean(barplot_data),errorbar_data,'k.','MarkerEdgeColor','k');axis square;
    set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14);
    rmpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
    for test_i = 1:length(x_test_inds)
        Statement = [];
        x=squeeze(barplot_data(:,x_test_inds(test_i)));
        y=squeeze(barplot_data(:,y_test_inds(test_i)));
        [h,p,~,stats]=ttest(x,y);
        if (mean(x)>mean(y))
            direction = 'larger';
        else
            direction = 'smaller';
        end
        %if h== 1
            if p < .001
                pval_str = ', p < .001';
            else
                pval_str = [', p = ', sprintf('%3.3f',p)];
            end;
        %end
        fprintf(fid,'\tINDUCED Parietal Pixel: %3i, Head_Plot: %3i',pixel_i,count);
        fprintf(fid,'\t%s\t%s\t%s',condname{x_test_inds(test_i)},'vs',condname{y_test_inds(test_i)});
        fprintf(fid,'\t\t%s%i%s%2.2f%s\n','t(',stats.df,') = ',stats.tstat,pval_str);
     
    end
    addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
    if count == 1
        ylabel('Power (dB)','FontSize',14);
    end
    
end
%% now plot time-frequency plots
%load ERSP
cluster_name = {'frontal','parietal'};
plot_name    = {sprintf('%s %s','Frontal','Induced Power'),sprintf('%s %s','Parietal', 'Induced Power')};
plot_inds = {[3 4 9 10];[21 22 27 28]};
for cluster_i = 1:length(cluster_name)
    savename_fstat  = [datain 'fstat_INDUCEDPOWER_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_fstat);
    savename_pfstat = [datain 'pfstat_INDUCEDPOWER_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_pfstat);
    % set up plot times - no need to show or correct for additional times
    plottime_start = 700;%start of plot
    plottime_end =2000;%end of plot
    %find values
    temp_times = times-plottime_start;
    [~,plottimestart_index] = min(abs(temp_times));
    temp_times = times-plottime_end;
    [~,plottimeend_index] = min(abs(temp_times));
    %store in a single variable
    plot_times = plottimestart_index:plottimeend_index;
    % apply fdr correction
    addpath(genpath([wpms.dirs.PACKAGES 'mass_uni_toolbox']))
    alpha = threshold;
    %ERSP
    [pfStatCorr_INDUCEDPOWER,~]=fdr_bky(pfStat_INDUCEDPOWER(:,plot_times),alpha);%anova p
    %[prStatCorr_ERSP,~]=fdr_bky(prStat_ERSP(:,plot_times,:),alpha);%corr p

    subplot(5,6,plot_inds{cluster_i});
    contourf(times(plot_times),frequencies,fStat_INDUCEDPOWER(:,plot_times),50,'linecolor','none');colormap jet;caxis([-50 50]);
    hold on;
    contour(times(plot_times),frequencies,pfStatCorr_INDUCEDPOWER,1,'k');axis square;
    title(plot_name{cluster_i});set(gca,'FontSize',14,'YTick',[2,10,20,29.9],'YTickLabel',[{'2'},{'10'},{'20'},{'30'}]);
end

%% EVOKED POWER:


figure();set(gcf,'Color',[1 1 1],'Position',[0 0 width height]);

cluster_i = 1;
headplot_inds = [8  ];
barplot_inds  = [7  ];%headplot_inds+1;
count = 0;
fprintf(fid,'%s\n%s\n','EVOKED POWER RESULTS','FRONTAL');

for pixel_i = [3];
    %fprintf(fid,'%s\t%s\n','Cluster:',num2str(pixel_i));
    count = count+1;
    subplot(5,6,headplot_inds(count));% topoplot ERSP cluster 1, pixel 1
    data =[];
    plot_data = double(squeeze(cluster_fStat_EVOKEDPOWER(cluster_i,pixel_i,:)));
    plot_p_data = double(squeeze(cluster_pfStat_EVOKEDPOWER(cluster_i,pixel_i,:)));
    data.avg = zeros(72,1);
    data.avg = [plot_data;0;0;0;0;0;0;0;0];
    clear pval_i
    data.var = zeros(72,1);
    data.time = 1;
    data.label = labels;
    data.dimord = 'chan_time';
    data.cov = zeros(72,72);
    
    
    plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
    plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
    %         data4plot_ind = find(plot_p_data<threshold);
    %         cfg.highlightchannel = labels(data4plot_ind);
    cfg.highlightchannel = labels(plot_p_data_corr==1);
    
    ft_topoplotER(cfg, data);
    
    %bar plot
    chan_i = find(ismember(labels,cluster{cluster_i}));
    %mean
%     plot_data = bsxfun(@minus,...
%         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
%         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
    plot_data = EVOKEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:5);
    plot_data = squeeze(mean(plot_data,3));
    barplot_data  = [plot_data(:,5),plot_data(:,3),plot_data(:,1),plot_data(:,2),plot_data(:,4)];
    errorbar_data = std(barplot_data)./sqrt(length(barplot_data));
    subplot(5,6,barplot_inds(count));
    bar(mean(barplot_data,1));
    box off;
    if barplot_inds(count) == 7 %[AW: Left Parietal BarPlot Location]
        ylim([6 11]);
    else
        ylim(ylims);
    end
    xlim([0 6]);
    hold on;
    errorbar(1:size(barplot_data,2),mean(barplot_data),errorbar_data,'k.','MarkerEdgeColor','k');axis square;
    set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14);
    rmpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
    for test_i = 1:length(x_test_inds)
        Statement = [];
        x=squeeze(barplot_data(:,x_test_inds(test_i)));
        y=squeeze(barplot_data(:,y_test_inds(test_i)));
        [h,p,~,stats]=ttest(x,y);
        if (mean(x)>mean(y))
            direction = 'larger';
        else
            direction = 'smaller';
        end
        %if h== 1
            if p < .001
                pval_str = ', p < .001';
            else
                pval_str = [', p = ', sprintf('%3.3f',p)];
            end;
        %end
        
        fprintf(fid,'\tEVOKED Power Frontal Pixel: %3i, Head_Plot: %3i',pixel_i,count);
        fprintf(fid,'\t%s\t%s\t%s',condname{x_test_inds(test_i)},'vs',condname{y_test_inds(test_i)});
        fprintf(fid,'\t\t%s%i%s%2.2f%s\n','t(',stats.df,') = ',stats.tstat,pval_str);
     
    end
    addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
        
end

%% plot parietal clusters
cluster_i = 2;
headplot_inds = [20 26 29];
barplot_inds  = [19 25 30];%headplot_inds+1;
count = 0;
for pixel_i = [2];
    count = count+1;
    subplot(5,6,headplot_inds(count));% topoplot ERSP cluster 1, pixel 1
    data =[];
    plot_data = double(squeeze(cluster_fStat_EVOKEDPOWER(cluster_i,pixel_i,:)));
    plot_p_data = double(squeeze(cluster_pfStat_EVOKEDPOWER(cluster_i,pixel_i,:)));
    data.avg = zeros(72,1);
    data.avg = [plot_data;0;0;0;0;0;0;0;0];
    clear pval_i
    data.var = zeros(72,1);
    data.time = 1;
    data.label = labels;
    data.dimord = 'chan_time';
    data.cov = zeros(72,72);
    
    %threshold = 0.05;
    plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
    plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
    %         data4plot_ind = find(plot_p_data<threshold);
    %         cfg.highlightchannel = labels(data4plot_ind);
    cfg.highlightchannel = labels(plot_p_data_corr==1);
    
    ft_topoplotER(cfg, data);
    
    chan_i = find(ismember(labels,cluster{cluster_i}));
    %mean
%     plot_data = bsxfun(@minus,...
%         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
%         ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
    plot_data =EVOKEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:5);
    plot_data = squeeze(mean(plot_data,3));
    barplot_data  = [plot_data(:,5),plot_data(:,3),plot_data(:,1),plot_data(:,2),plot_data(:,4)];
    errorbar_data = std(barplot_data)./sqrt(length(barplot_data));
    subplot(5,6,barplot_inds(count));
    bar(mean(barplot_data,1));box off;
    xlim([0 6]);
    if barplot_inds(count) == 19 %[AW: Left Parietal BarPlot Location]
        ylim([10 13.5]);
    elseif barplot_inds(count) == 16
        ylim([-4.5 1]); 
    else
        ylim(ylims);
    end
    hold on;
    errorbar(1:size(barplot_data,2),mean(barplot_data),errorbar_data,'k.','MarkerEdgeColor','k');axis square;
    set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14);
    rmpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
    for test_i = 1:length(x_test_inds)
        Statement = [];
        x=squeeze(barplot_data(:,x_test_inds(test_i)));
        y=squeeze(barplot_data(:,y_test_inds(test_i)));
        [h,p,~,stats]=ttest(x,y);
        if (mean(x)>mean(y))
            direction = 'larger';
        else
            direction = 'smaller';
        end
        %if h== 1
            if p < .001
                pval_str = ', p < .001';
            else
                pval_str = [', p = ', sprintf('%3.3f',p)];
            end;
        %end
        fprintf(fid,'\tEVOKED Parietal Pixel: %3i, Head_Plot: %3i',pixel_i,count);
        fprintf(fid,'\t%s\t%s\t%s',condname{x_test_inds(test_i)},'vs',condname{y_test_inds(test_i)});
        fprintf(fid,'\t\t%s%i%s%2.2f%s\n','t(',stats.df,') = ',stats.tstat,pval_str);
     
    end
    addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
    if count == 1
        ylabel('Power (dB)','FontSize',14);
    end
    
end
%% now plot time-frequency plots
%load ERSP
cluster_name = {'frontal','parietal'};
plot_name    = {sprintf('%s %s','Frontal','EVOKED Power'),sprintf('%s %s','Parietal', 'EVOKED Power')};
plot_inds = {[3 4 9 10];[21 22 27 28]};
for cluster_i = 1:length(cluster_name)
    savename_fstat  = [datain 'fstat_EVOKEDPOWER_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_fstat);
    savename_pfstat = [datain 'pfstat_EVOKEDPOWER_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_pfstat);
    % set up plot times - no need to show or correct for additional times
    plottime_start = 700;%start of plot
    plottime_end =2000;%end of plot
    %find values
    temp_times = times-plottime_start;
    [~,plottimestart_index] = min(abs(temp_times));
    temp_times = times-plottime_end;
    [~,plottimeend_index] = min(abs(temp_times));
    %store in a single variable
    plot_times = plottimestart_index:plottimeend_index;
    % apply fdr correction
    addpath(genpath([wpms.dirs.PACKAGES 'mass_uni_toolbox']))
    alpha = threshold;
    %ERSP
    [pfStatCorr_EVOKEDPOWER,~]=fdr_bky(pfStat_EVOKEDPOWER(:,plot_times),alpha);%anova p
    %[prStatCorr_ERSP,~]=fdr_bky(prStat_ERSP(:,plot_times,:),alpha);%corr p

    subplot(5,6,plot_inds{cluster_i});
    contourf(times(plot_times),frequencies,fStat_EVOKEDPOWER(:,plot_times),50,'linecolor','none');colormap jet;caxis([-50 50]);
    hold on;
    contour(times(plot_times),frequencies,pfStatCorr_EVOKEDPOWER,1,'k');axis square;
    title(plot_name{cluster_i});set(gca,'FontSize',14,'YTick',[2,10,20,29.9],'YTickLabel',[{'2'},{'10'},{'20'},{'30'}]);
end



% %% %% ITPC figure
% %two time-frequency plots
% % 2 topo plots for frontal
% % 2 topo plots for parietal
% % set up
% 
% labels = {'FP1';'AF7';'AF3';'F1';'F3';'F5';'F7';'FT7';'FC5';'FC3'; ...
%     'FC1';'C1';'C3';'C5';'T7';'TP7';'CP5';'CP3';'CP1';'P1';'P3'; ...
%     'P5';'P7';'P9';'PO7';'PO3';'O1';'Iz';'Oz';'POz';'Pz';'CPz'; ...
%     'Fpz';'Fp2';'AF8';'AF4';'AFz';'Fz';'F2';'F4';'F6';'F8';'FT8'; ...
%     'FC6';'FC4';'FC2';'FCz';'Cz';'C2';'C4';'C6';'T8';'TP8';'CP6'; ...
%     'CP4';'CP2';'P2';'P4';'P6';'P8';'P10';'PO8';'PO4';'O2';'M1'; ...
%     'M2';'LO1';'LO2';'SO1';'SO2';'IO1';'IO2';};
% 
% cfg = [];
% cfg.zlim = [-50.0 50.0];
% cfg.colormap = 'jet';
% cfg.parameter   = 'avg';
% cfg.layout      = 'biosemi64.lay';
% cfg.comment   = 'no';
% cfg.marker = 'off';
% cfg.markersize = 4;
% cfg.contournum = 1;
% cfg.gridscale = 600;
% cfg.highlight = 'on';
% cfg.highlightfontsize = 1; %no label writing
% cfg.highlightsymbol = '*';
% cfg.highlightsize = 4; %no label writing
% cfg.highlightcolor = [0 0 1]; %blue
% %% plot ITPC topoclusters (frontal)
% figure();set(gcf,'Color',[1 1 1],'Position',[0 0 width height]);
% ylims = [0 0.15]; %[AW 28/3/2017: ylimits to bar plots changed from -0.025 to 0.01]
% cluster_i = 1;
% headplot_inds = [1 4];
% barplot_inds =  [5 8];%headplot_inds+1;
% count = 0;
% for pixel_i = [9 10];
%     count = count+1;
%     subplot(5,6,headplot_inds(count));% topoplot ITPC cluster 1, pixel 1
%     data =[];
%     plot_data = double(squeeze(cluster_fStat_ITPC(cluster_i,pixel_i,:)));
%     plot_p_data = double(squeeze(cluster_pfStat_ITPC(cluster_i,pixel_i,:)));
%     data.avg = zeros(72,1);
%     data.avg = [plot_data;0;0;0;0;0;0;0;0];
%     clear pval_i
%     data.var = zeros(72,1);
%     data.time = 1;
%     data.label = labels;
%     data.dimord = 'chan_time';
%     data.cov = zeros(72,72);
%     
%     %threshold = 0.05;
%     plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
%     plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
%     %         data4plot_ind = find(plot_p_data<threshold);
%     %         cfg.highlightchannel = labels(data4plot_ind);
%     cfg.highlightchannel = labels(plot_p_data_corr==1);
%     
%     ft_topoplotER(cfg, data);
%     
%     %bar plot
%     chan_i = find(ismember(labels,cluster{cluster_i}));
%     %mean
% %     plot_data = bsxfun(@minus,...
% %         ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
% %         ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
%     plot_data = ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:5);
%     plot_data = squeeze(mean(plot_data,3));
%     barplot_data  = [plot_data(:,5),plot_data(:,3),plot_data(:,1),plot_data(:,2),plot_data(:,4)];
%     errorbar_data = std(barplot_data)./sqrt(length(barplot_data));
%     subplot(5,6,barplot_inds(count));
%     bar(mean(barplot_data,1));box off;ylim(ylims);hold on;
%     xlim([0 6]);
%     errorbar(1:size(barplot_data,2),mean(barplot_data),errorbar_data,'k.','MarkerEdgeColor','k');axis square;
%     %set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14,'YTickLabel',{'0','.05','.10'});
%     set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14);
%     rmpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
%     for test_i = 1:length(x_test_inds)
%         
%         x=squeeze(barplot_data(:,x_test_inds(test_i)));
%         y=squeeze(barplot_data(:,y_test_inds(test_i)));
%         [h,p,~,stats]=ttest(x,y);
%         if (mean(x)>mean(y))
%             direction = 'larger';
%         else
%             direction = 'smaller';
%         end
%         %if h== 1
%             if p < .001
%                 pval_str = ', p < .001';
%             else
%                 pval_str = [', p = ', sprintf('%3.3f',p)];
%             end;
%         %end
%         
%         fprintf(fid,'\tITPC Frontal Pixel: %3i, Head_Plot: %3i',pixel_i,count);
%         fprintf(fid,'\t%s\t%s\t%s',condname{x_test_inds(test_i)},'vs',condname{y_test_inds(test_i)});
%         fprintf(fid,'\t\t%s%i%s%2.2f%s\n','t(',stats.df,') = ',stats.tstat,pval_str);
%      
%     end
%     addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
% end
% 
% %% plot parietal clusters
% cluster_i = 2;
% headplot_inds = [9 12];
% barplot_inds = [13 16];%headplot_inds+1;
% count = 0;
% 
% ylims = [0 0.16];
% 
% for pixel_i = [12 23];
%     count = count+1;
%     subplot(5,6,headplot_inds(count));% topoplot ERSP cluster 1, pixel 1
%     data =[];
%     plot_data = double(squeeze(cluster_fStat_ITPC(cluster_i,pixel_i,:)));
%     plot_p_data = double(squeeze(cluster_pfStat_ITPC(cluster_i,pixel_i,:)));
%     data.avg = zeros(72,1);
%     data.avg = [plot_data;0;0;0;0;0;0;0;0];
%     clear pval_i
%     data.var = zeros(72,1);
%     data.time = 1;
%     data.label = labels;
%     data.dimord = 'chan_time';
%     data.cov = zeros(72,72);
%     
%     %threshold = 0.05;
%     plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
%     plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
%     %         data4plot_ind = find(plot_p_data<threshold);
%     %         cfg.highlightchannel = labels(data4plot_ind);
%     cfg.highlightchannel = labels(plot_p_data_corr==1);
%     
%     ft_topoplotER(cfg, data);
%         %bar plot
%     chan_i = find(ismember(labels,cluster{cluster_i}));
%     %mean
% %     plot_data = bsxfun(@minus,...
% %         ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
% %         ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
%     plot_data = ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:5);
%     plot_data = squeeze(mean(plot_data,3));
%     barplot_data  = [plot_data(:,5),plot_data(:,3),plot_data(:,1),plot_data(:,2),plot_data(:,4)];
%     errorbar_data = std(barplot_data)./sqrt(length(barplot_data));
%     subplot(5,6,barplot_inds(count));
%     bar(mean(barplot_data,1));box off; xlim([0 6])
% %      if barplot_inds(count) == 25 %[AW 28/3/2017: Left Parietal BarImage]
%          ylim(ylims);
%          set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14); %,'YTickLabel',{'0','.05','.10','.15'}
% %      else
% %         ylim(ylims);
% %         set(gca,'XTickLabel',condname,'XTickLabelRotation',xrotation,'FontSize',14,'YTickLabel',{'0','.05','.10'});
% %      end
%     hold on;
%     errorbar(1:size(barplot_data,2),mean(barplot_data),errorbar_data,'k.','MarkerEdgeColor','k');axis square;
%     
%     
%     rmpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
%     for test_i = 1:length(x_test_inds)
%         
%         x=squeeze(barplot_data(:,x_test_inds(test_i)));
%         y=squeeze(barplot_data(:,y_test_inds(test_i)));
%         [h,p,~,stats]=ttest(x,y);
%         if (mean(x)>mean(y))
%             direction = 'larger';
%         else
%             direction = 'smaller';
%         end
%         %if h== 1
%             if p < .001
%                 pval_str = ', p < .001';
%             else
%                 pval_str = [', p = ', sprintf('%3.3f',p)];
%             end;
%         %end
%         
%         fprintf(fid,'\tITPC Parietal Pixel: %3i, Head_Plot: %3i',pixel_i,count);
%         fprintf(fid,'\t%s\t%s\t%s',condname{x_test_inds(test_i)},'vs',condname{y_test_inds(test_i)});
%         fprintf(fid,'\t\t%s%i%s%2.2f%s\n','t(',stats.df,') = ',stats.tstat,pval_str);
%      
%     end
%     addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
%      if count == 1
%         ylabel('ITPC','FontSize',14);
%     end
% end
% fclose(fid);
% %% now plot time-frequency plots
% %load ERSP
% cluster_name = {'frontal','parietal'};
% plot_name    = {sprintf('%s %s','Frontal','ITPC'),sprintf('%s %s','Parietal', 'ITPC')};
% plot_inds = {[2 3 6 7];[10 11 14 15]};
% for cluster_i = 1:length(cluster_name)
%     savename_fstat  = [datain 'fstat_ITPC_noAR_' cluster_name{cluster_i} '_20170426.mat'];load(savename_fstat);
%     savename_pfstat = [datain 'pfstat_ITPC_noAR_' cluster_name{cluster_i} '_20170426.mat'];load(savename_pfstat);
%     % set up plot times - no need to show or correct for additional times
%     plottime_start = -300;%start of plot
%     plottime_end = 1100;%end of plot
%     %find values
%     temp_times = times-plottime_start;
%     [~,plottimestart_index] = min(abs(temp_times));
%     temp_times = times-plottime_end;
%     [~,plottimeend_index] = min(abs(temp_times));
%     %store in a single variable
%     plot_times = plottimestart_index:plottimeend_index;
%     % apply fdr correction
%     addpath(genpath([wpms.dirs.PACKAGES 'mass_uni_toolbox']))
%     alpha = threshold;
%     %ERSP
%     [pfStatCorr_ITPC,~]=fdr_bky(pfStat_ITPC(:,plot_times),alpha);%anova p
%     %[prStatCorr_ITPC,~]=fdr_bky(prStat_ITPC(:,plot_times,:),alpha);%corr p
% 
%     subplot(5,6,plot_inds{cluster_i});
%     contourf(times(plot_times),frequencies,fStat_ITPC(:,plot_times),50,'linecolor','none');colormap jet;caxis([-50 50]);
%     hold on;
%     contour(times(plot_times),frequencies,pfStatCorr_ITPC,1,'k');axis square;
%     title(plot_name{cluster_i});set(gca,'FontSize',14,'YTick',[2,10,20,29.9],'YTickLabel',[{'2'},{'10'},{'20'},{'30'}]);
% %     if cluster_i ==2;
% %         ylabel('             Frequency (Hz)','FontSize',16);
% %         xlabel('Time (ms)','FontSize',16);
% %     end
% end