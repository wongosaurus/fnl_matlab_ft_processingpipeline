%% Plot Setup:
clear all; close all; clc;
%Data Structure Time/Frequency definition
frequencies = logspace(log10(2),log10(30),80);
times = (-1:0.001953125000000:3.5)*1000;

% %Script Specific Area of interest:
% %Cue:
% Timebin_window = 300;
% Timebin_start = [100,700,1100];
% Timebin_end = Timebin_start +Timebin_window;
% component_start_timebin = zeros(size(Timebin_start));
% component_end_timebin = zeros(size(Timebin_start));
% 
% for Timebin_start_i = 1:length(Timebin_start)
%     temp_times = times-Timebin_start(Timebin_start_i);
%     [~,component_start_timebin(Timebin_start_i)] = min(abs(temp_times));
%     %Timebin_end_ind(Timebin_start_i) = find((floor(times-Timebin_end(Timebin_start_i))==0),1);
%     temp_times = times-Timebin_end(Timebin_start_i);
%     [~,component_end_timebin(Timebin_start_i)] = min(abs(temp_times));
% end

wpms.dirs.PACKAGES = 'E:\FNL_EEG_TOOLBOX\PACKAGES\';

labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
CHANNELS = 1:length(labels);

CONDITIONS  = {'allrepeat','mixrepeat','switchto','switchaway','noninf','noninfrepeat','noninfswitch'};

listings = dir('E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat');
NAMES = {length(listings)};
for file_i =1:length(listings)
    NAMES{file_i} = listings(file_i).name(1:6);
end
%load('E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\good_inds_v8_170117.mat');
%NAMES = NAMES(good_inds);

SAVEDIR = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\';
LOADDIR = [SAVEDIR 'FrequencyMatrices' filesep 'NoBL' filesep];
mkdir(LOADDIR);
cd(LOADDIR);
%% Extract Data:
%To make things FASTER: GROUP THEM INTO 
%FREQUENCY_GROUP = 10;
FREQUENCY_GROUP = 1;
ERSP_F         = zeros(FREQUENCY_GROUP,length(times),length(labels),length(NAMES),length(CONDITIONS),'single');
INDUCEDPOWER_F         = ERSP_F;

for freq_i = 12%1:FREQUENCY_GROUP:10%length(frequencies)
    freq_j = freq_i+FREQUENCY_GROUP-1;

    for name_i = 1:length(NAMES);
        fprintf('Processing: %s ', NAMES{name_i})
        tic;
        skip = -1;
        for cond_i = 1:length(CONDITIONS)
            try
                alldata.(CONDITIONS{cond_i})=load([SAVEDIR,NAMES{name_i},'_',CONDITIONS{cond_i},'_ALL_POWER3.mat'],'eegpower_all','inducedpower_all');
            catch
                skip = 1;
                continue;
            end
        end
        if skip ==1
            continue;
        end
        %baseline ersp and INDUCEDPOWER
        for cond_i = 1:length(CONDITIONS)
            %alldata.(CONDITIONS{cond_i}).eegpower_all =      (bsxfun(@minus,10*log10(squeeze(alldata.(CONDITIONS{cond_i}).eegpower_all(:,:,:))),10*log10(mean(squeeze(alldata.(CONDITIONS{cond_i}).eegpower_all(:,:,basetimestart_index:basetimeend_index)),3))));
            %alldata.(CONDITIONS{cond_i}).inducedpower_all   =(bsxfun(@minus,10*log10(squeeze(alldata.(CONDITIONS{cond_i}).inducedpower_all(:,:,:))),10*log10(mean(squeeze(alldata.(CONDITIONS{cond_i}).inducedpower_all(:,:,basetimestart_index:basetimeend_index)),3))));
            
            temp_ERSP           =squeeze(alldata.(CONDITIONS{cond_i}).eegpower_all(:,freq_i:freq_j,:));
            temp_INDUCEDPWOER   =squeeze(alldata.(CONDITIONS{cond_i}).inducedpower_all(:,freq_i:freq_j,:));
            
            temp_ERSP = permute(temp_ERSP,[2,3,1]);%rearrange to fit matrix
            temp_INDUCEDPWOER = permute(temp_INDUCEDPWOER,[2,3,1]);
            
            ERSP_F(:,:,:,name_i,cond_i)         = temp_ERSP;%store ERSP
            INDUCEDPOWER_F(:,:,:,name_i,cond_i)	= temp_INDUCEDPWOER;%store INDUCEDPOWER
            clear temp*
        end
        
        t = toc;
        fprintf('\nTime Taken: %6.2f Minutes \n\n',t/60);
        clearvars alldata
    end
    for freq_group_i = 0:FREQUENCY_GROUP-1

        ERSP_F_t = ERSP_F(freq_group_i+1,:,:,:,:);
        INDUCEDPOWER_F_t = INDUCEDPOWER_F(freq_group_i+1,:,:,:,:);
        savename_ERSP = [LOADDIR filesep 'ERSP_' num2str(freq_i+freq_group_i) '.mat'];
        savename_INDUCEDPOWER = [LOADDIR filesep 'INDUCEDPOWER_' num2str(freq_i+freq_group_i) '.mat'];
        save(savename_ERSP,'ERSP_F_t','-v7.3');
        save(savename_INDUCEDPOWER,'INDUCEDPOWER_F_t','-v7.3');
    end
end%freq_i

%% Perform Data Check:
for i = 1:80
    fprintf('%i\n',i);
    try
        load(['INDUCEDPOWER_',num2str(i),'.mat']);
    catch e
        fprintf('Failed Induced: %i\n',i);
    end
    try
        load(['ERSP_',num2str(i),'.mat']);
    catch e
        fprintf('ERSP: %i\n',i);
    end
end