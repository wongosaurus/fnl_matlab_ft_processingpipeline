%% run pixel-wise anovas and correlations
% continuing from GenerateMontana_TFPlots_forANOVA_DATE.m
% DATADIR = 'F:\FNL_EEG_TOOLBOX\EEGLAB_FORMAT\';
% wpms.dirs.PACKAGES = 'E:\FNL_EEG_TOOLBOX\PACKAGES\';
% CONDITIONS  = {'switchto','switchaway','noninfrepeat','noninfswitch','mixrepeat','allrepeat'};
%  
clear all
listings = dir('E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat');
NAMES = {length(listings)};
for file_i =1:length(listings)
    NAMES{file_i} = listings(file_i).name(1:6);
end

addpath(genpath('E:\FNL_EEG_TOOLBOX\FUNCTIONS\'))
datain = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\NoBL\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
% % load in RT variability data
% filenameAV = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\meansITC.mat';
% filenameSD = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\stdevITC.mat';
% load('E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\good_inds_v8_170117.mat')
% [control_sdevs]=createCVmeasure(NAMES,filenameAV,filenameSD,good_inds);
% %collapse across noninfs
% % dvData for correlations
% dvData = [control_sdevs(:,1),control_sdevs(:,2),...
%                  control_sdevs(:,5),[control_sdevs(:,3)+control_sdevs(:,4)]./2];
%frequencies and times
frequencies = logspace(log10(2),log10(30),80);
times = (-1:0.001953125000000:3.5)*1000;

wpms.conditions    ={'allrepeat','mixrepeat','switchto','switchaway','noninf','noninfrepeat','noninfswitch'};

CONDITIONSTARGET = [1 2 3 4 6 7];

Frontal_Labels = {'FCz','FC1','FC2','Cz','C1','C2'};%frontal
Parietal_Labels = {'CPz','CP1','CP2','Pz','P1','P2'};%parietal

baseline_start = 700;%start of plot
baseline_end = 900;%end of plot
%find values
temp_times = times-baseline_start;
[~,baselinetimestart_index] = min(abs(temp_times));
temp_times = times-baseline_end;
[~,baselinetimeend_index] = min(abs(temp_times));
%store in a single variable
baselineinds = baselinetimestart_index:baselinetimeend_index;

CHANNEL_CLUSTER = {Frontal_Labels,Parietal_Labels};

cluster_names = {'frontal','parietal'};
%% [AW: 17/03/23] Calculate the condition
for cluster_i = 1:length(cluster_names)
    % cluster = {'FCz','FC1','FC2','Fz','F1','F2'};%frontal
    %cluster = {'CPz','CP1','CP2','Pz','P1','P2'};%parietal
    cluster = CHANNEL_CLUSTER{cluster_i};
    cluster_name = cluster_names{cluster_i};%'parietal';
    %begin by first averaging across electrodes of interest
    electrodes_of_interest = find(ismember(labels,cluster));
    ERSPfilename = [datain 'ERSP_' num2str(1) '.mat'];
    load(ERSPfilename);%for preallocation purposes
    %storage for anova p and f values
    pfStat_ERSP = zeros(length(frequencies),size(ERSP_F_t,2));
    fStat_ERSP = pfStat_ERSP;
    pfStat_INDUCEDPOWER = pfStat_ERSP;
    fStat_INDUCEDPOWER = fStat_ERSP;
    pfStat_EVOKEDPOWER = pfStat_ERSP;
    fStat_EVOKEDPOWER = fStat_ERSP;
    %storage for correlation p and r values
    % prStat_ERSP = zeros(length(frequencies),size(ERSP_F_t,2),length(wpms.conditions));
    % rStat_ERSP = prStat_ERSP;
    % prStat_INDUCED = prStat_ERSP;
    % rStat_INDUCED = rStat_ERSP;

    for freq_i = 1:length(frequencies)
        fprintf('\n%s\t%s','Frequency:',num2str(freq_i));
        tic;
        %average across clusters storage variable
        ERSPfilename = [datain 'ERSP_' num2str(freq_i) '.mat'];
        load(ERSPfilename);
        INDUCEDPOWERfilename = [datain 'INDUCEDPOWER_' num2str(freq_i) '.mat'];
        load(INDUCEDPOWERfilename);
        
        %average across cluster
        ERSP_cluster = squeeze(mean(ERSP_F_t(:,:,electrodes_of_interest,:,CONDITIONSTARGET),3));
        INDUCEDPOWER_cluster = squeeze(mean(INDUCEDPOWER_F_t(:,:,electrodes_of_interest,:,CONDITIONSTARGET),3));
        EVOKEDPOWER_cluster = ERSP_cluster - INDUCEDPOWER_cluster;
        %now control for all-repeat by removing that condition
        ERSP_cluster = bsxfun(@minus,ERSP_cluster,mean(ERSP_cluster(baselineinds,:,:),1));
        INDUCEDPOWER_cluster = bsxfun(@minus,INDUCEDPOWER_cluster,mean(INDUCEDPOWER_cluster(baselineinds,:,:),1));
        EVOKEDPOWER_cluster = bsxfun(@minus,EVOKEDPOWER_cluster, mean(EVOKEDPOWER_cluster(baselineinds,:,:),1));
        for time_i = 1:size(ERSP_cluster,1)
            if ~mod(time_i,100)
                fprintf('.');
            end
            %organise data and run through one-way anova
            
            
            %%Run ERSP ANOVA
            data_ERSP = squeeze(ERSP_cluster(time_i,:,:));
            data = data_ERSP;            
            t = table(data(:,1),data(:,2),data(:,3),data(:,4),data(:,5),data(:,6),...
            'VariableNames',{'data1','data2','data3','data4','data5','data6'});
            Meas = dataset([1 2 3 4 5 6]','VarNames',{'Measurements'});

            rm = fitrm(t,'data1-data6~1','WithinDesign',Meas);

            ranovatbl = ranova(rm);
            fStat_ERSP(freq_i,time_i)=ranovatbl{1,4};
            pfStat_ERSP(freq_i,time_i)=ranovatbl{1,5};
            
            %induced
            data_INDUCEDPOWER = squeeze(INDUCEDPOWER_cluster(time_i,:,:));
            data = data_INDUCEDPOWER;            
             t = table(data(:,1),data(:,2),data(:,3),data(:,4),data(:,5),data(:,6),...
            'VariableNames',{'data1','data2','data3','data4','data5','data6'});
            Meas = dataset([1 2 3 4 5 6]','VarNames',{'Measurements'});

            rm = fitrm(t,'data1-data6~1','WithinDesign',Meas);

            ranovatbl = ranova(rm);
            fStat_INDUCEDPOWER(freq_i,time_i)=ranovatbl{1,4};
            pfStat_INDUCEDPOWER(freq_i,time_i)=ranovatbl{1,5};
            
            %evoked
            data_EVOKEDPOWER = squeeze(EVOKEDPOWER_cluster(time_i,:,:));
            data = data_EVOKEDPOWER;            
             t = table(data(:,1),data(:,2),data(:,3),data(:,4),data(:,5),data(:,6),...
            'VariableNames',{'data1','data2','data3','data4','data5','data6'});
            Meas = dataset([1 2 3 4 5 6]','VarNames',{'Measurements'});

            rm = fitrm(t,'data1-data6~1','WithinDesign',Meas);

            ranovatbl = ranova(rm);
            fStat_EVOKEDPOWER(freq_i,time_i)=ranovatbl{1,4};
            pfStat_EVOKEDPOWER(freq_i,time_i)=ranovatbl{1,5};
            
            
            
            %organise data and run through correlation
    %         corrData_ERSP = squeeze(data_ERSP);
    %         corrData_ITPC = squeeze(data_ITPC);
    %         [rStat_ERSP(freq_i,time_i,:),prStat_ERSP(freq_i,time_i,:)]= runCorrelations(wpms,corrData_ERSP,dvData);
    %         [rStat_ITPC(freq_i,time_i,:),prStat_ITPC(freq_i,time_i,:)]= runCorrelations(wpms,corrData_ITPC,dvData);
            clear data_* rm ranovatbl
        end
        t=toc;fprintf('\t%2.2f %s',t,'seconds to complete');
    end%freq_i loop

    
    %save ERSP
    savename_fstat  = [datain 'fstat_ERSP_' cluster_name '_target_20170502.mat'];save(savename_fstat,'fStat_ERSP','-v7.3');
    savename_pfstat = [datain 'pfstat_ERSP_' cluster_name '_target_20170502.mat'];save(savename_pfstat,'pfStat_ERSP','-v7.3');
    %savename_rstat  = [datain 'rstat_ERSP_' cluster_name '_20170216.mat'];save(savename_rstat,'rStat_ERSP','-v7.3');
    %savename_prstat = [datain 'prstat_ERSP_' cluster_name '_20170216.mat'];save(savename_prstat,'prStat_ERSP','-v7.3');
    %save INDUCEDPOWER
    savename_fstat  = [datain 'fstat_INDUCEDPOWER_' cluster_name '_target_20170502.mat'];save(savename_fstat,'fStat_INDUCEDPOWER','-v7.3');
    savename_pfstat = [datain 'pfstat_INDUCEDPOWER_' cluster_name '_target_20170502.mat'];save(savename_pfstat,'pfStat_INDUCEDPOWER','-v7.3');
    %save EVOKEDPOWER
    savename_fstat  = [datain 'fstat_EVOKEDPOWER_' cluster_name '_target_20170502.mat'];save(savename_fstat,'fStat_EVOKEDPOWER','-v7.3');
    savename_pfstat = [datain 'pfstat_EVOKEDPOWER_' cluster_name '_target_20170502.mat'];save(savename_pfstat,'pfStat_EVOKEDPOWER','-v7.3');

end
%savename_rstat  = [datain 'rstat_ITPC_' cluster_name '_20170216.mat'];save(savename_rstat,'rStat_ITPC','-v7.3');
%savename_prstat = [datain 'prstat_ITPC_' cluster_name '_20170216.mat'];save(savename_prstat,'prStat_ITPC','-v7.3');
%% visualise effects [AW: 17/03/23] IM up to here!
%load statistical spaces in
cluster_name = {'frontal','parietal'};
figure();set(gcf,'Color',[1 1 1],'Position',[0 0 1920 1080]);

%%[AW: 17/03/23]: Change Subplot Sizes to include ERSP, INDUCED, EVOKED.
subplot_height = 2; %Number of Clusters
subplot_width = 3; %Number of Images to Show (ERSP, INDUCED, EVOKED);
flims=[-50 50];
count=0;
for cluster_i = 1:length(cluster_name)
    
    %load ERSP
    savename_fstat  = [datain 'fstat_ERSP_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_fstat);
    savename_pfstat = [datain 'pfstat_ERSP_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_pfstat);
    %savename_rstat  = [datain 'rstat_ERSP_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_rstat);
    %savename_prstat = [datain 'prstat_ERSP_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_prstat);
%     %load INDUCED
%     savename_fstat  = [datain 'fstat_INDUCEDPOWER_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_fstat);
%     savename_pfstat = [datain 'pfstat_INDUCEDPOWER_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_pfstat);
%     %load EVOKED
%     savename_fstat  = [datain 'fstat_EVOKEDPOWER_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_fstat);
%     savename_pfstat = [datain 'pfstat_EVOKEDPOWER_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_pfstat);
%     
    %savename_rstat  = [datain 'rstat_ITPC_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_rstat);
    %savename_prstat = [datain 'prstat_ITPC_' cluster_name{cluster_i} '_target_20170502.mat'];load(savename_prstat);
    % set up plot times - no need to show or correct for additional times
    plotstimes_start = 700;%start of plot
    plotstimes_end = 2000;%end of plot
    %find values
    temp_times = times-plotstimes_start;
    [~,plotstimesstart_index] = min(abs(temp_times));
    temp_times = times-plotstimes_end;
    [~,plotstimesend_index] = min(abs(temp_times));
    %store in a single variable
    plot_times = plotstimesstart_index:plotstimesend_index;
    % apply fdr correction
    %addpath(genpath([wpms.dirs.PACKAGES 'mass_uni_toolbox']))
    alpha = .001;%.05;
    %ERSP
    [pfStatCorr_ERSP,~]=fdr_bky(pfStat_ERSP(:,plot_times),alpha);%anova p
    %[prStatCorr_ERSP,~]=fdr_bky(prStat_ERSP(:,plot_times,:),alpha);%corr p
%     %EVOKED
%     [pfStatCorr_INDUCEDPOWER,~]=fdr_bky(pfStat_INDUCEDPOWER(:,plot_times),alpha);%anova p
%     %[prStatCorr_ITPC,~]=fdr_bky(prStat_ITPC(:,plot_times,:),alpha);%corr p
%     %ITPC
%     [pfStatCorr_EVOKEDPOWER,~]=fdr_bky(pfStat_EVOKEDPOWER(:,plot_times),alpha);%anova p

    %anova
    %ERSP
%     count=count+1;
%     subplot(subplot_height,subplot_width,count);
%     contourf(times(plot_times),frequencies,fStat_ERSP(:,plot_times),50,'linecolor','none');colormap jet;caxis(flims);
%     hold on;
%     contour(times(plot_times),frequencies,pfStatCorr_ERSP,1,'k');axis square;
%     title([cluster_name{cluster_i} ' F-Map ERSP']);
    
%     %INDUCEDPOWER
%     count=count+1;
%     subplot(subplot_height,subplot_width,count);
%     contourf(times(plot_times),frequencies,fStat_INDUCEDPOWER(:,plot_times),50,'linecolor','none');colormap jet;caxis(flims);
%     hold on;
%     contour(times(plot_times),frequencies,pfStatCorr_INDUCEDPOWER,1,'k');axis square;
%     title([cluster_name{cluster_i} ' F-Map INDUCEDPOWER']);
%     
%     %EVOKEDPOWER
%     count=count+1;
%     subplot(subplot_height,subplot_width,count);
%     contourf(times(plot_times),frequencies,fStat_EVOKEDPOWER(:,plot_times),50,'linecolor','none');colormap jet;caxis(flims);
%     hold on;
%     contour(times(plot_times),frequencies,pfStatCorr_EVOKEDPOWER,1,'k');axis square;
%     title([cluster_name{cluster_i} ' F-Map EVOKEDPOWER']);

    alpha = .001;%.05;
    %ERSP
    [pfStatCorr_ERSP,~]=fdr_bky(pfStat_ERSP(:,plot_times),alpha);%anova p
    %[prStatCorr_ERSP,~]=fdr_bky(prStat_ERSP(:,plot_times,:),alpha);%corr p
%     %EVOKED
%     [pfStatCorr_INDUCEDPOWER,~]=fdr_bky(pfStat_INDUCEDPOWER(:,plot_times),alpha);%anova p
%     %[prStatCorr_ITPC,~]=fdr_bky(prStat_ITPC(:,plot_times,:),alpha);%corr p
%     %ITPC
%     [pfStatCorr_EVOKEDPOWER,~]=fdr_bky(pfStat_EVOKEDPOWER(:,plot_times),alpha);%anova p
    
%ERSP
theta_min = 4;
[~,theta_min_inds] = min(abs(frequencies-theta_min));
theta_max = 14;
[~,theta_max_inds] = min(abs(frequencies-theta_max));
freq_range = theta_min_inds:theta_max_inds;
count = count-2;
%find significant clusters
width = 60;  %Time Bins
height = 6; %Frequency Bins:
frequency_offset = 22; %4HZ: Max from 4Hz to 30Hz;
ERSP_clusters.(cluster_names{cluster_i})=bwconncomp(pfStatCorr_ERSP(frequency_offset:end,:));%find significant clusters
for pixel_i = 1:length(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxList)
    reduced_data = fStat_ERSP(frequency_offset:end,plot_times);
    [m,ind]=max(reduced_data(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxList{pixel_i}));
    [freq_max,time_max] =find (reduced_data==m);
    freq_max = frequency_offset+freq_max;
    if freq_max >80
        ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
        continue;
    end
    reduced_data = fStat_ERSP(freq_range,plot_times);
    [m,ind]=max(reduced_data(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxList{pixel_i}));
    [freq_max,time_max] =find (reduced_data==m);
    %make sure freq_max isn't in delta
    % should times==2000???
    if times(plot_times(time_max))> plotstimes_end || frequencies(freq_max)< 4 || ...
        time_max - width/2 < 0 || time_max + width/2 > length(plot_times) || ... %out of Sample conditions (Time) 
        freq_max - height/2 < 0 || freq_max + height/2 > length(frequencies)
        %plot_times(time_max) > find(times==2000) || plot_times(time_max) < find(times==1000) %||freq_max <=4
        ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
        fprintf('Condition 1: %i, %i, %i\n', plot_times(time_max) >= find(times==2000),plot_times(time_max),find(times==2000));
        fprintf('Condition 2: %i, %i, %i\n\n', plot_times(time_max) <= find(times==1000), plot_times(time_max), find(times==1000));
        disp('Moving on here 1:');
        continue;
    end
    
    %         if times(plot_times(time_max))< 700 || times(plot_times(time_max))> 2000 || frequencies(freq_max)< 4|| frequencies(freq_max)>20
    %             ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
    %             continue;
    %         end
    %         if numel(reduced_data(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxList{pixel_i})) < 100 %(width+1)*(height+1)
    %             ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
    %             continue;
    %
end

%         subplot(subplot_height,subplot_width,count);
%         scatter(times(plot_times(time_max)),frequencies(freq_max),10,'k','filled');
%         x = times(plot_times(time_max - width/2));
%         width_t = times(plot_times(time_max + width/2)) - x;
%         y = frequencies(freq_max - height/2);
%         height_f = frequencies(freq_max + height/2) - y;
%         hold on;
%         rectangle('Position',[x,y,width_t,height_f]);
%         
%         ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [freq_max,time_max];
%     end
%     
%      %INDUCED
%     count = count+1;
%     %find significant clusters
%     width = 60;  %Time Bins
%     height = 6; %Frequency Bins:
%     frequency_offset = 22; %4HZ: Max from 4Hz to 30Hz;
%     INDUCEDPOWER_clusters.(cluster_names{cluster_i})=bwconncomp(pfStatCorr_INDUCEDPOWER(frequency_offset:end,:));%find significant clusters
%     for pixel_i = 1:length(INDUCEDPOWER_clusters.(cluster_names{cluster_i}).PixelIdxList)
%         reduced_data = fStat_INDUCEDPOWER(frequency_offset:end,plot_times);
%         [m,ind]=max(reduced_data(INDUCEDPOWER_clusters.(cluster_names{cluster_i}).PixelIdxList{pixel_i}));
%         [freq_max,time_max] =find (reduced_data==m);
%         freq_max = frequency_offset+freq_max;
%         if freq_max >80
%             INDUCEDPOWER_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
%             continue;
%         end
%         if times(plot_times(time_max))< 700 || times(plot_times(time_max))> 2000 || frequencies(freq_max)< 4|| frequencies(freq_max)>20
%             INDUCEDPOWER_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
%             continue;
%         end
%         if numel(reduced_data(INDUCEDPOWER_clusters.(cluster_names{cluster_i}).PixelIdxList{pixel_i})) < 100 %(width+1)*(height+1)
%             INDUCEDPOWER_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
%             continue;
%         end
%         subplot(subplot_height,subplot_width,count);
%         scatter(times(plot_times(time_max)),frequencies(freq_max),10,'k','filled');
%         x = times(plot_times(time_max - width/2));
%         width_t = times(plot_times(time_max + width/2)) - x;
%         y = frequencies(freq_max - height/2);
%         height_f = frequencies(freq_max + height/2) - y;
%         hold on;
%         rectangle('Position',[x,y,width_t,height_f]);
%         
%         INDUCEDPOWER_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [freq_max,time_max];
%     end
%     
%    %evoked
%         count = count+1;
%     %find significant clusters
%     width = 60;  %Time Bins
%     height = 6; %Frequency Bins:
%     frequency_offset = 22; %4HZ: Max from 4Hz to 30Hz;
%     EVOKEDPOWER_clusters.(cluster_names{cluster_i})=bwconncomp(pfStatCorr_EVOKEDPOWER(frequency_offset:end,:));%find significant clusters
%     for pixel_i = 1:length(EVOKEDPOWER_clusters.(cluster_names{cluster_i}).PixelIdxList)
%         reduced_data = fStat_EVOKEDPOWER(frequency_offset:end,plot_times);
%         [m,ind]=max(reduced_data(EVOKEDPOWER_clusters.(cluster_names{cluster_i}).PixelIdxList{pixel_i}));
%         [freq_max,time_max] =find (reduced_data==m);
%         freq_max = frequency_offset+freq_max;
%         if freq_max >80
%             EVOKEDPOWER_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
%             continue;
%         end
%         if times(plot_times(time_max))< 700 || times(plot_times(time_max))> 2000 || frequencies(freq_max)< 4|| frequencies(freq_max)>20
%             EVOKEDPOWER_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
%             continue;
%         end
%         if numel(reduced_data(EVOKEDPOWER_clusters.(cluster_names{cluster_i}).PixelIdxList{pixel_i})) < 100 %(width+1)*(height+1)
%             EVOKEDPOWER_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
%             continue;
%         end
%         subplot(subplot_height,subplot_width,count);
%         scatter(times(plot_times(time_max)),frequencies(freq_max),10,'k','filled');
%         x = times(plot_times(time_max - width/2));
%         width_t = times(plot_times(time_max + width/2)) - x;
%         y = frequencies(freq_max - height/2);
%         height_f = frequencies(freq_max + height/2) - y;
%         hold on;
%         rectangle('Position',[x,y,width_t,height_f]);
%         
%         EVOKEDPOWER_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [freq_max,time_max];
%     end
%     
%     
    %%%%%
%     count = count-2;
%     %find significant clusters
%     width = 60;  %Time Bins
%     height = 6; %Frequency Bins:
%     ERSP_clusters.(cluster_name{cluster_i})=bwconncomp(pfStatCorr_ERSP);%find significant clusters
%     INDUCEDPOWER_clusters.(cluster_name{cluster_i})=bwconncomp(pfStatCorr_INDUCEDPOWER);%find significant clusters
%     EVOKEDPOWER_clusters.(cluster_name{cluster_i})=bwconncomp(pfStatCorr_EVOKEDPOWER);%find significant clusters
%     for pixel_i = 1:length(ERSP_clusters.(cluster_name{cluster_i}).PixelIdxList)
%         reduced_data = fStat_ERSP(:,plot_times);
%         %[m,ind]=max(reduced_data(ERSP_clusters.(cluster_name{cluster_i}).PixelIdxList{pixel_i}));
%         %[freq_max,time_max] =find (reduced_data==m);
%         [m,ind]=sort(reduced_data(ERSP_clusters.(cluster_name{cluster_i}).PixelIdxList{pixel_i}),'descend');
%         for subpixel_i = 1:length(m)
%             [freq_max,time_max] =find (reduced_data==m(subpixel_i));
%             if frequencies(freq_max) > 4
%                 break;
%             end
%         end
%         if times(plot_times(time_max))< 0 || ...
%                 times(plot_times(time_max))> 1500 || ...
%                 frequencies(freq_max)< 4 || ...
%                 frequencies(freq_max)> 20 
%             fprintf('Outside Range: %i, %i, %3.5f, %3.5f \n',cluster_i, pixel_i, times(plot_times(time_max)),frequencies(freq_max ))
%             ERSP_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i} = [];
%             continue;
%         end
%         if numel(reduced_data(ERSP_clusters.(cluster_name{cluster_i}).PixelIdxList{pixel_i})) < 10 %(width+1)*(height+1)
%             fprintf('Too Small: %i, %i \n', cluster_i, pixel_i);
%             ERSP_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i} = [];
%             continue;
%         end
%         subplot(subplot_height,subplot_width,count);
%         scatter(times(plot_times(time_max)),frequencies(freq_max),10,'k','filled');
%         %%%%%%%%%%%%%
%         %Issue here: time_max-width/2 can equal negative number
%         %this throws an error when trying to index plot_times with a
%         %negative index value...
%         % same occurs if freq_max+height/2 is greater 80
%         %looks like the box size check is failing to work
%         %
%         %dodgy fix by Patrick below... for display purposes only
%         %we will need to fix the check function...
%         %%%%%%%%%%%%%
%         if time_max-width/2 <=0
%             x = times(plot_times(1));
%             width_t = times(plot_times(1))-x;
%         else
%             x = times(plot_times(time_max - width/2));
%             width_t = times(plot_times(time_max + width/2)) - x;
%         end
%        
%         y = frequencies(freq_max - height/2);
%         if (freq_max + height/2)>80
%             height_f = frequencies(80)-y;
%         else
%             height_f = frequencies(freq_max + height/2) - y;
%         end
%         hold on;
%         rectangle('Position',[x,y,width_t,height_f]);
%         
%         ERSP_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i} = [freq_max,time_max];
%     end
%     count = count +1;
%     for pixel_i = 1:length(INDUCEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxList)
%         reduced_data = fStat_INDUCEDPOWER(:,plot_times);
%         %[m,ind]=max(reduced_data(INDUCEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxList{pixel_i}));
%         %[freq_max,time_max] =find (reduced_data==m);
%         [m,ind]=sort(reduced_data(INDUCEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxList{pixel_i}),'descend');
%         for subpixel_i = 1:length(m)
%             [freq_max,time_max] =find (reduced_data==m(subpixel_i));
%             if frequencies(freq_max) > 4
%                 break;
%             end
%         end
%         if times(plot_times(time_max))< 0 || ...
%                 times(plot_times(time_max))> 1500 || ...
%                 frequencies(freq_max)< 4 || ...
%                 frequencies(freq_max)> 20 
%             fprintf('Outside Range: %i, %i, %3.5f, %3.5f \n',cluster_i, pixel_i, times(plot_times(time_max)),frequencies(freq_max ))
%             INDUCEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i} = [];
%             continue;
%         end
%         if numel(reduced_data(INDUCEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxList{pixel_i})) < 10%(width+1)*(height+1)
%             fprintf('Too Small: %i, %i \n', cluster_i, pixel_i);
%             INDUCEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i} = [];
%             continue;
%         end
%         
%         subplot(subplot_height,subplot_width,count);
%         scatter(times(plot_times(time_max)),frequencies(freq_max),10,'k','filled');
%                 %%%%%%%%%%%%%
%         %Issue here: time_max-width/2 can equal negative number
%         %this throws an error when trying to index plot_times with a
%         %negative index value...
%         % same occurs if freq_max+height/2 is greater 80
%         %looks like the box size check is failing to work
%         %
%         %dodgy fix by Patrick below... for display purposes only
%         %we will need to fix the check function...
%         %%%%%%%%%%%%%
%         if time_max-width/2 <=0
%             x = times(plot_times(1));
%             width_t = times(plot_times(1))-x;
%         else
%             x = times(plot_times(time_max - width/2));
%             width_t = times(plot_times(time_max + width/2)) - x;
%         end
%        
%         y = frequencies(freq_max - height/2);
%         if (freq_max + height/2)>80
%             height_f = frequencies(80)-y;
%         else
%             height_f = frequencies(freq_max + height/2) - y;
%         end
%        % x = times(plot_times(time_max - width/2));
%         %width_t = times(plot_times(time_max + width/2)) - x;
%         %y = frequencies(freq_max - height/2);
%         %height_f = frequencies(freq_max + height/2) - y;
%         hold on;
%         rectangle('Position',[x,y,width_t,height_f]);
%         
%         INDUCEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i} = [freq_max,time_max];
%     end
%     count = count+1;
%     for pixel_i = 1:length(EVOKEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxList)
%         reduced_data = fStat_EVOKEDPOWER(:,plot_times);
%         [m,ind]=sort(reduced_data(EVOKEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxList{pixel_i}),'descend');
%         for subpixel_i = 1:length(m)
%             [freq_max,time_max] =find (reduced_data==m(subpixel_i));
%             if frequencies(freq_max) > 4
%                 break;
%             end
%         end
%         %[freq_max,time_max] =find (reduced_data==m);
%         if      times(plot_times(time_max))< 0 || ...
%                 times(plot_times(time_max))> 1500 || ...
%                 frequencies(freq_max)< 4 || ...
%                 frequencies(freq_max)> 20 
%                 
%             fprintf('Outside Range: %i, %i, %3.5f, %3.5f \n',cluster_i, pixel_i, times(plot_times(time_max)),frequencies(freq_max ))
%             EVOKEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i} = [];
%             continue;
%         end
%         if numel(reduced_data(EVOKEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxList{pixel_i})) < 10%(width+1)*(height+1)
%             fprintf('Too Small: %i, %i \n', cluster_i, pixel_i);
%             EVOKEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i} = [];
%             continue;
%         end
%         
%         subplot(subplot_height,subplot_width,count);
%         scatter(times(plot_times(time_max)),frequencies(freq_max),10,'k','filled');
%                 %%%%%%%%%%%%%
%         %Issue here: time_max-width/2 can equal negative number
%         %this throws an error when trying to index plot_times with a
%         %negative index value...
%         % same occurs if freq_max+height/2 is greater 80
%         %looks like the box size check is failing to work
%         %
%         %dodgy fix by Patrick below... for display purposes only
%         %we will need to fix the check function...
%         %%%%%%%%%%%%%
%         if time_max-width/2 <=0
%             x = times(plot_times(1));
%             width_t = times(plot_times(1))-x;
%         else
%             x = times(plot_times(time_max - width/2));
%             width_t = times(plot_times(time_max + width/2)) - x;
%         end
%        
%         y = frequencies(freq_max - height/2);
%         if (freq_max + height/2)>80
%             height_f = frequencies(80)-y;
%         else
%             height_f = frequencies(freq_max + height/2) - y;
%         end
%         %x = times(plot_times(time_max - width/2));
%         %width_t = times(plot_times(time_max + width/2)) - x;
%         %y = frequencies(freq_max - height/2);
%         %height_f = frequencies(freq_max + height/2) - y;
%         hold on;
%         rectangle('Position',[x,y,width_t,height_f]);
%         
%         EVOKEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i} = [freq_max,time_max];
%     end
%     
end
SAVEDIR = datain;
LOADDIR = datain;
save([LOADDIR 'CLUSTER_MaxPixelLoc_target_20170502.mat'],'*_clusters','-v7.3');

%% LOAD DATA FOR TOPOPLOT: FMAP:
load([LOADDIR 'CLUSTER_MaxPixelLoc_target_20170502.mat']);
cluster_name = cluster_names;
cluster_i=1;
%ERSP
width = 60;  %Time Bins
height = 6; %Frequency Bins:
ERSP_DATA = zeros(height+1,2305,64,211,6,'single');
ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND = zeros(length(cluster_name),...
    length(ERSP_clusters.(cluster_name{cluster_i}).PixelIdxList),...
                                        64,211,6,'single');
for cluster_i = 1:length(cluster_name)
    fprintf('ERSP Working on %s\n',cluster_name{cluster_i} );
    ERSP_clusters.(cluster_names{cluster_i})=bwconncomp(reduced_data_h);%find significant clusters  
    for pixel_i = 3%pixels{cluster_i}
    %for pixel_i = 1:length(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxList)
        reduced_data = fStat_ERSP(freq_range,plot_times);
        [m,ind]=max(reduced_data(ERSP_clusters.(cluster_names{cluster_i}).PixelIdxList{pixel_i}));
        [freq_max,time_max] =find (reduced_data==m);
        %make sure freq_max isn't in delta
        % should times==2000???
        if plot_times(time_max) > find(times==2000) || plot_times(time_max) < find(times==1000) %||freq_max <=4  
            ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [];
            fprintf('Condition 1: %i, %i, %i\n', plot_times(time_max) >= find(times==2000),plot_times(time_max),find(times==2000));
            fprintf('Condition 2: %i, %i, %i\n\n', plot_times(time_max) <= find(times==1000), plot_times(time_max), find(times==1000));
            disp('Moving on here 1:');
            continue;
        end
        ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i} = [freq_max,time_max];
        %Save [F,T]:
        width = 120;  %Time Bins %60
        height = 6; %Frequency Bins:
        Location = ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i};
        if isempty(Location)
            disp('Moving on here 2:');
            continue;
        else
            freq_max = Location(1);
            time_max = Location(2);
            
            freq_i = freq_max-height/2:1:freq_max+height/2;
            %AW CHANGED HERE: 2017/05/02 Pulling incorect times: was
            %plot_times(time_max-width/2) etc..
            timewindows = time_max-width/2:1:time_max+width/2;
            
            ispc_means = zeros(length(NAMES),length(wpms.conditions),length(labels));
        end
    end
  
    
    for pixel_i = 1:length(ERSP_clusters.(cluster_name{cluster_i}).PixelIdxList)
        fprintf('\tWorking on Pixel Index: %i\n',pixel_i );
        Location = ERSP_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i};
        if isempty(Location)
            continue;
        end
        freq_max = Location(1);
        time_max = Location(2);
        count = 1;
        for freq_i = freq_max-height/2:1:freq_max+height/2
            fprintf('\t\tWorking on Frequency Index: %i\n',freq_i );
            load([LOADDIR 'ERSP_' num2str(freq_i) '.mat']);
            ERSP_DATA(count,:,:,:,:) = ERSP_F_t;
            count = count+1;
        end
        fprintf('\tAveraging over Frequencing and Time\n' );
        timewindows = plot_times(time_max-width/2):1:plot_times(time_max+width/2);
        ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,:,:,:) = squeeze(mean(squeeze(mean(ERSP_DATA(:,timewindows,:,:,:),1)),1));
    end
    fprintf('ERSP Finished %s\n',cluster_name{cluster_i} );
end
clear ERSP_DATA;

%INDUCEDPOWER:
INDUCEDPOWER_DATA = zeros(height+1,2305,64,211,6,'single');
INDUCEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND = zeros(length(cluster_name),...
    length(INDUCEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxList),...
                                        64,211,6,'single');
for cluster_i = 1:length(cluster_name)
    fprintf('INDUCEDPOWER Working on %s\n',cluster_name{cluster_i} );
    for pixel_i = 1:length(INDUCEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxList)
        fprintf('\tWorking on Pixel Index: %i\n',pixel_i );
        Location = INDUCEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i};
        if isempty(Location)
            continue;
        end
        freq_max = Location(1);
        time_max = Location(2);
        count = 1;
        for freq_i = freq_max-height/2:1:freq_max+height/2
            fprintf('\t\tWorking on Frequency Index: %i\n',freq_i );
            load([LOADDIR 'INDUCEDPOWER_' num2str(freq_i) '.mat']);
            INDUCEDPOWER_DATA(count,:,:,:,:) = INDUCEDPOWER_F_t;
            count = count+1;
        end
        fprintf('\tAveraging over Frequencing and Time\n' );
        timewindows = plot_times(time_max-width/2):1:plot_times(time_max+width/2);
        INDUCEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,:,:,:) = squeeze(mean(squeeze(mean(INDUCEDPOWER_DATA(:,timewindows,:,:,:),1)),1));
    end
    fprintf('ITPC Finished %s\n',cluster_name{cluster_i} );
end
clear INDUCEDPOWER_DATA;   

%EVOKEDPOWER:
EVOKEDPOWER_DATA = zeros(height+1,2305,64,211,6,'single');
EVOKEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND = zeros(length(cluster_name),...
    length(EVOKEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxList),...
                                        64,211,6,'single');
for cluster_i = 1:length(cluster_name)
    fprintf('EVOKEDPOWER Working on %s\n',cluster_name{cluster_i} );
    for pixel_i = 1:length(EVOKEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxList)
        fprintf('\tWorking on Pixel Index: %i\n',pixel_i );
        Location = EVOKEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i};
        if isempty(Location)
            continue;
        end
        freq_max = Location(1);
        time_max = Location(2);
        count = 1;
        for freq_i = freq_max-height/2:1:freq_max+height/2
            fprintf('\t\tWorking on Frequency Index: %i\n',freq_i );
            load([LOADDIR 'ERSP_' num2str(freq_i) '.mat']);
            load([LOADDIR 'INDUCEDPOWER_' num2str(freq_i) '.mat']);
            EVOKEDPOWER_DATA(count,:,:,:,:) = ERSP_F_t - INDUCEDPOWER_F_t;
            count = count+1;
        end
        fprintf('\tAveraging over Frequencing and Time\n' );
        timewindows = plot_times(time_max-width/2):1:plot_times(time_max+width/2);
        EVOKEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,:,:,:) = squeeze(mean(squeeze(mean(EVOKEDPOWER_DATA(:,timewindows,:,:,:),1)),1));
    end
    fprintf('EVOKED Finished %s\n',cluster_name{cluster_i} );
end
clear EVOKEDPOWER_DATA;   

save([LOADDIR 'CLUSTER_PIXEL_CHANNEL_SUB_COND_target_20170502.mat'],'*_CLUSTER_PIXEL_CHANNEL_SUB_COND','-v7.3');

%% ANOVA:
% Perform an ANOVA(SUB,CHAN) for CLUSTER_PIXEL_COND on the
% CLUSTER_PIXEL_CHANNEL_SUB_COND, after AR Baseline: COND(5)

%dimension for order of data for input to anova
    n    = length(wpms.conditions);%number of conditions
    nDim = 6;%where conditions are stored in the multidimensional matrix
    squeezeDims = [1 2 3];%singleton dimensions that we ignore
    nonsqueezeDims = [5 4];%non-singleton dimensions that we care about
%ERSP:

%Set up output variables:
cluster_pfStat_ERSP = zeros(size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,1),...
                            size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,2),...
                            size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,3));  
cluster_fStat_ERSP = cluster_pfStat_ERSP;
% cluster_rStat_ERSP = zeros(size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,1),...
%                             size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,2),...
%                             size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,3),...
%                             size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,5)-1) ;
% cluster_prStat_ERSP = cluster_rStat_ERSP;


for cluster_i = 1:size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,1)
    for pixel_i = 1:size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,2)
        %Check if Pixel is valid:
        if ~any(any(any(squeeze(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,:,:,:))))) %3Dimensional Check to see if There exists any data.
            fprintf('Data at Cluster: %i Pixel: %i, is not valid, Moving on...\n',cluster_i,pixel_i);
            continue;
        end
        fprintf('Working on Data at Cluster: %i Pixel: %i, ...\n',cluster_i,pixel_i);
        fprintf('Pixel Location: FreqBin %i, TimeBin %i, Freq %3.5f Hz, Time %3.5f ms \n',...
            ERSP_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(1),...
            ERSP_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(2),...
            frequencies(ERSP_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(1)),...
            times(plot_times(ERSP_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(2))));
        
        for chan_i = 1:size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,3)
            %ANOVA_DATA = bsxfun(@minus,...
            %                    ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
            %                    ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
            ANOVA_DATA = ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,:);
            data_ERSP = squeeze(ANOVA_DATA);
            data = data_ERSP;            
            t = table(data(:,1),data(:,2),data(:,3),data(:,4),data(:,5),data(:,6),...
            'VariableNames',{'data1','data2','data3','data4','data5','data6'});
            Meas = dataset([1 2 3 4 5 6]','VarNames',{'Measurements'});

            rm = fitrm(t,'data1-data6~1','WithinDesign',Meas);

            ranovatbl = ranova(rm);
            cluster_fStat_ERSP(cluster_i,pixel_i,chan_i)=ranovatbl{1,4};
            cluster_pfStat_ERSP(cluster_i,pixel_i,chan_i)=ranovatbl{1,5};
        end
        %clear data_ERSP data_ITPC cDat* conditionLabels corrData*
    end
end

%INDUCEDPOWER:

%Set up output variables:
cluster_pfStat_INDUCEDPOWER = zeros(size(INDUCEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND,1),...
                            size(INDUCEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND,2),...
                            size(INDUCEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND,3));  
cluster_fStat_INDUCEDPOWER = cluster_pfStat_INDUCEDPOWER;
% cluster_rStat_ITPC = zeros(size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,1),...
%                             size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,2),...
%                             size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,3),...
%                             size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,5)-1) ;
% cluster_prStat_ITPC = cluster_rStat_ITPC;


for cluster_i = 1:size(INDUCEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND,1)
    for pixel_i = 1:size(INDUCEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND,2)
        %Check if Pixel is valid:
        if ~any(any(any(squeeze(INDUCEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,:,:,:))))) %3Dimensional Check to see if There exists any data.
            fprintf('Data at Cluster: %i Pixel: %i, is not valid, Moving on...\n',cluster_i,pixel_i);
            continue;
        end
        fprintf('Working on Data at Cluster: %i Pixel: %i, ...\n',cluster_i,pixel_i);
        fprintf('Pixel Location: FreqBin %i, TimeBin %i, Freq %3.5f Hz, Time %3.5f ms \n',...
            INDUCEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(1),...
            INDUCEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(2),...
            frequencies(INDUCEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(1)),...
            times(plot_times(INDUCEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(2))));
        
        for chan_i = 1:size(INDUCEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND,3)
%             ANOVA_DATA = bsxfun(@minus,...
%                                 ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
%                                 ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
            ANOVA_DATA = INDUCEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,:);
            data_INDUCEDPOWER = squeeze(ANOVA_DATA);
            data = data_INDUCEDPOWER;            
            t = table(data(:,1),data(:,2),data(:,3),data(:,4),data(:,5),data(:,6),...
            'VariableNames',{'data1','data2','data3','data4','data5','data6'});
            Meas = dataset([1 2 3 4 5 6]','VarNames',{'Measurements'});

            rm = fitrm(t,'data1-data6~1','WithinDesign',Meas);

            ranovatbl = ranova(rm);
            cluster_fStat_INDUCEDPOWER(cluster_i,pixel_i,chan_i)=ranovatbl{1,4};
            cluster_pfStat_INDUCEDPOWER(cluster_i,pixel_i,chan_i)=ranovatbl{1,5};
        end
        %clear data_ERSP data_ITPC cDat* conditionLabels corrData*
    end
end

%EVOKEDPOWER:
%Set up output variables:
cluster_pfStat_EVOKEDPOWER = zeros(size(EVOKEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND,1),...
                            size(EVOKEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND,2),...
                            size(EVOKEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND,3));  
cluster_fStat_EVOKEDPOWER = cluster_pfStat_EVOKEDPOWER;
% cluster_rStat_ITPC = zeros(size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,1),...
%                             size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,2),...
%                             size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,3),...
%                             size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,5)-1) ;
% cluster_prStat_ITPC = cluster_rStat_ITPC;


for cluster_i = 1:size(EVOKEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND,1)
    for pixel_i = 1:size(EVOKEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND,2)
        %Check if Pixel is valid:
        if ~any(any(any(squeeze(EVOKEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,:,:,:))))) %3Dimensional Check to see if There exists any data.
            fprintf('Data at Cluster: %i Pixel: %i, is not valid, Moving on...\n',cluster_i,pixel_i);
            continue;
        end
        fprintf('Working on Data at Cluster: %i Pixel: %i, ...\n',cluster_i,pixel_i);
        fprintf('Pixel Location: FreqBin %i, TimeBin %i, Freq %3.5f Hz, Time %3.5f ms \n',...
            EVOKEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(1),...
            EVOKEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(2),...
            frequencies(EVOKEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(1)),...
            times(plot_times(EVOKEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(2))));
        
        for chan_i = 1:size(EVOKEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND,3)
%             ANOVA_DATA = bsxfun(@minus,...
%                                 ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,1:4),...
%                                 ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,5));
            ANOVA_DATA = EVOKEDPOWER_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,chan_i,:,:);
            data_EVOKEDPOWER = squeeze(ANOVA_DATA);
            data = data_EVOKEDPOWER;            
            t = table(data(:,1),data(:,2),data(:,3),data(:,4),data(:,5),data(:,6),...
            'VariableNames',{'data1','data2','data3','data4','data5','data6'});
            Meas = dataset([1 2 3 4 5 6]','VarNames',{'Measurements'});

            rm = fitrm(t,'data1-data6~1','WithinDesign',Meas);

            ranovatbl = ranova(rm);
            cluster_fStat_EVOKEDPOWER(cluster_i,pixel_i,chan_i)=ranovatbl{1,4};
            cluster_pfStat_EVOKEDPOWER(cluster_i,pixel_i,chan_i)=ranovatbl{1,5};      
        end
        %clear data_ERSP data_ITPC cDat* conditionLabels corrData*
    end
end
save([LOADDIR 'CLUSTER_PIXEL_CHAN_COND_STATS_target_20170502.mat'],'cluster_*','-v7.3');

%% Plot Topology: For each Cluster Pixel:
close all;

addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));

labels = {'FP1';'AF7';'AF3';'F1';'F3';'F5';'F7';'FT7';'FC5';'FC3'; ...
              'FC1';'C1';'C3';'C5';'T7';'TP7';'CP5';'CP3';'CP1';'P1';'P3'; ...
              'P5';'P7';'P9';'PO7';'PO3';'O1';'Iz';'Oz';'POz';'Pz';'CPz'; ...
              'Fpz';'Fp2';'AF8';'AF4';'AFz';'Fz';'F2';'F4';'F6';'F8';'FT8'; ...
              'FC6';'FC4';'FC2';'FCz';'Cz';'C2';'C4';'C6';'T8';'TP8';'CP6'; ...
              'CP4';'CP2';'P2';'P4';'P6';'P8';'P10';'PO8';'PO4';'O2';'M1'; ...
              'M2';'LO1';'LO2';'SO1';'SO2';'IO1';'IO2';};

cfg = [];
cfg.zlim = [-50.0 50.0];
cfg.colormap = 'jet';
cfg.parameter   = 'avg';
cfg.layout      = 'biosemi64.lay';
cfg.comment   = 'no';
cfg.marker = 'off';
cfg.markersize = 4;
cfg.contournum = 1;
cfg.gridscale = 600;
cfg.highlight = 'on';
cfg.highlightfontsize = 1; %no label writing
cfg.highlightsymbol = '*';
cfg.highlightcolor = [0 0 1]; %blue
data =[];
threshold = 0.001;

%ERSP:
for cluster_i = 1:size(cluster_fStat_ERSP,1)
    for pixel_i = 1:size(cluster_fStat_ERSP,2)
        if ~any(any(any(squeeze(cluster_fStat_ERSP(cluster_i,pixel_i,:,:,:))))) %3Dimensional Check to see if There exists any data.
            fprintf('Data at Cluster: %i Pixel: %i, is not valid, Moving on...\n',cluster_i,pixel_i);
            continue;
        end
        title_str=sprintf('Topology at ERSP Peak Location: \n Frequency %3.3f Hz, Time %3.3f ms',...
                    frequencies(ERSP_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(1)),...
                    times(plot_times(ERSP_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(2))) );
        figure();set(gcf,'Color',[1 1 1],'Position',[0 0 500 500]);        
        suptitle(title_str); %super title
        %for cond_i = 1:size(cluster_rStat_ERSP,4)
        %    h=subplot(2,2,cond_i);
        
        plot_data = double(squeeze(cluster_fStat_ERSP(cluster_i,pixel_i,:)));
        plot_p_data = double(squeeze(cluster_pfStat_ERSP(cluster_i,pixel_i,:)));
        data.avg = zeros(72,1);
        data.avg = [plot_data;0;0;0;0;0;0;0;0];
        clear pval_i
        data.var = zeros(72,1);
        data.time = 1;
        data.label = labels;
        data.dimord = 'chan_time';
        data.cov = zeros(72,72);

        plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
        plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
%         data4plot_ind = find(plot_p_data<threshold);
%         cfg.highlightchannel = labels(data4plot_ind);
        cfg.highlightchannel = labels(plot_p_data_corr==1);

        ft_topoplotER(cfg, data);

        saveas(gcf,['ERSP_cluster' num2str(cluster_i) '_pixel' num2str(pixel_i) '_target_Ftopo.jpg'],'jpeg');
        close;
    end
end

%INDUCEDPOWER:
for cluster_i = 1:size(cluster_fStat_INDUCEDPOWER,1)
    for pixel_i = 1:size(cluster_fStat_INDUCEDPOWER,2)
        if ~any(any(any(squeeze(cluster_fStat_INDUCEDPOWER(cluster_i,pixel_i,:,:,:))))) %3Dimensional Check to see if There exists any data.
            fprintf('Data at Cluster: %i Pixel: %i, is not valid, Moving on...\n',cluster_i,pixel_i);
            continue;
        end
        title_str=sprintf('Topology at INDUCEDPOWER Peak Location: \n Frequency %3.3f Hz, Time %3.3f ms',...
                    frequencies(INDUCEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(1)),...
                    times(plot_times(INDUCEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(2))) );
        figure();set(gcf,'Color',[1 1 1],'Position',[0 0 500 500]);        
        suptitle(title_str); %super title
        %for cond_i = 1:size(cluster_rStat_INDUCEDPOWER,4)
        %    h=subplot(2,2,cond_i);
        
        plot_data = double(squeeze(cluster_fStat_INDUCEDPOWER(cluster_i,pixel_i,:)));
        plot_p_data = double(squeeze(cluster_pfStat_INDUCEDPOWER(cluster_i,pixel_i,:)));
        data.avg = zeros(72,1);
        data.avg = [plot_data;0;0;0;0;0;0;0;0];
        clear pval_i
        data.var = zeros(72,1);
        data.time = 1;
        data.label = labels;
        data.dimord = 'chan_time';
        data.cov = zeros(72,72);

        plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
        plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
%         data4plot_ind = find(plot_p_data<threshold);
%         cfg.highlightchannel = labels(data4plot_ind);
        cfg.highlightchannel = labels(plot_p_data_corr==1);

        ft_topoplotER(cfg, data);

        saveas(gcf,['INDUCEDPOWER_cluster' num2str(cluster_i)  '_pixel' num2str(pixel_i) '_target_Ftopo.jpg'],'jpeg');
        close;
    end
end

%EVOKEDPOWER:
for cluster_i = 1:size(cluster_fStat_EVOKEDPOWER,1)
    for pixel_i = 1:size(cluster_fStat_EVOKEDPOWER,2)
        if ~any(any(any(squeeze(cluster_fStat_EVOKEDPOWER(cluster_i,pixel_i,:,:,:))))) %3Dimensional Check to see if There exists any data.
            fprintf('Data at Cluster: %i Pixel: %i, is not valid, Moving on...\n',cluster_i,pixel_i);
            continue;
        end
        title_str=sprintf('Topology at EVOKEDPOWER Peak Location: \n Frequency %3.3f Hz, Time %3.3f ms',...
                    frequencies(EVOKEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(1)),...
                    times(plot_times(EVOKEDPOWER_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(2))) );
        figure();set(gcf,'Color',[1 1 1],'Position',[0 0 500 500]);        
        suptitle(title_str); %super title
        %for cond_i = 1:size(cluster_rStat_EVOKEDPOWER,4)
        %    h=subplot(2,2,cond_i);
        
        plot_data = double(squeeze(cluster_fStat_EVOKEDPOWER(cluster_i,pixel_i,:)));
        plot_p_data = double(squeeze(cluster_pfStat_EVOKEDPOWER(cluster_i,pixel_i,:)));
        data.avg = zeros(72,1);
        data.avg = [plot_data;0;0;0;0;0;0;0;0];
        clear pval_i
        data.var = zeros(72,1);
        data.time = 1;
        data.label = labels;
        data.dimord = 'chan_time';
        data.cov = zeros(72,72);

        plot_p_data_corr = fdr_bky(plot_p_data(1:64),threshold);%apply fdr correction
        plot_p_data_corr = [plot_p_data_corr;0;0;0;0;0;0;0;0];
%         data4plot_ind = find(plot_p_data<threshold);
%         cfg.highlightchannel = labels(data4plot_ind);
        cfg.highlightchannel = labels(plot_p_data_corr==1);

        ft_topoplotER(cfg, data);

        saveas(gcf,['EVOKEDPOWER_cluster' num2str(cluster_i)  '_pixel' num2str(pixel_i) '_target_Ftopo.jpg'],'jpeg');
        close;
    end
end



%%

% %% Plot Topology of Rstat (correlation with behaviour) per each cluster,
% % Mark Significant Electrodes (prstat)
% % State whether a significant main effect: cluster_pixel (cluster_fstat*)
% % In a subplot(2,2,i), of Conditions,
% close all;
% 
% addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\fieldtrip'));
% 
% labels = {'FP1';'AF7';'AF3';'F1';'F3';'F5';'F7';'FT7';'FC5';'FC3'; ...
%               'FC1';'C1';'C3';'C5';'T7';'TP7';'CP5';'CP3';'CP1';'P1';'P3'; ...
%               'P5';'P7';'P9';'PO7';'PO3';'O1';'Iz';'Oz';'POz';'Pz';'CPz'; ...
%               'Fpz';'Fp2';'AF8';'AF4';'AFz';'Fz';'F2';'F4';'F6';'F8';'FT8'; ...
%               'FC6';'FC4';'FC2';'FCz';'Cz';'C2';'C4';'C6';'T8';'TP8';'CP6'; ...
%               'CP4';'CP2';'P2';'P4';'P6';'P8';'P10';'PO8';'PO4';'O2';'M1'; ...
%               'M2';'LO1';'LO2';'SO1';'SO2';'IO1';'IO2';};
% 
% cfg = [];
% cfg.zlim = [0 0.4];
% cfg.colormap = 'hot';
% cfg.parameter   = 'avg';
% cfg.layout      = 'biosemi64.lay';
% cfg.comment   = 'no';
% cfg.marker = 'off';
% cfg.contournum = 1;
% cfg.gridsize = 600;
% cfg.highlight = 'on';
% cfg.highlightfontsize = 1; %no label writing
% cfg.highlightsymbol = 'o';
% cfg.highlightcolor = [0 0 1]; %blue
% data =[];
% 
% %ERSP:
% for cluster_i = 1:size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,1)
%     for pixel_i = 1:size(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND,2)
%         if ~any(any(any(squeeze(ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,:,:,:))))) %3Dimensional Check to see if There exists any data.
%             fprintf('Data at Cluster: %i Pixel: %i, is not valid, Moving on...\n',cluster_i,pixel_i);
%             continue;
%         end
%         title_str=sprintf('Topology at ERSP vs RT Peak Location: \n Frequency %3.3f Hz, Time %3.3f ms \n F-Stat: %3.3f P-value: %3.3f',...
%                     frequencies(ERSP_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(1)),...
%                     times(plot_times(ERSP_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(2))),...
%                     cluster_fStat_ERSP(cluster_i,pixel_i),...
%                     cluster_pfStat_ERSP(cluster_i,pixel_i));
%         figure();set(gcf,'Color',[1 1 1],'Position',[0 0 1920 1080]);        
%         suptitle(title_str); %super title
%         for cond_i = 1:size(cluster_rStat_ERSP,4)
%             h=subplot(2,2,cond_i);
%             plot_data = squeeze(cluster_rStat_ERSP(cluster_i,pixel_i,:,cond_i));
%             plot_p_data = squeeze(cluster_prStat_ERSP(cluster_i,pixel_i,:,cond_i));
%             %topoplot: 
%             data.avg = zeros(72,1);
%             %eval(['currentfile = ' datastruct_cue{(contrasts_i)} ';']);
%             %eval(['currentpval = ' datastruct_pval{(contrasts_i)} ';']);
%             %eval(['currentpval_corrected = ' datastruct_pval_corrected{(contrasts_i)} ';']);
%             threshold = 0.05;
%             data4plot_ind = find(plot_p_data<threshold);
%             %labels_ind = find(currentpval_corrected<threshold);
% 
%             cfg.highlightchannel = labels(data4plot_ind);
%             data4plot = zeros(1,64);
%             data4plot(data4plot_ind)=plot_data(data4plot_ind);
%             data.avg = abs([data4plot';0;0;0;0;0;0;0;0]);
%             clear pval_i
%             data.var = zeros(72,1);
%             data.time = 1;
%             data.label = labels;
%             data.dimord = 'chan_time';
%             data.cov = zeros(72,72);
%         
%             ft_topoplotER(cfg, data);
%             title([wpms.conditions{cond_i}]);
%             %title(h,[wpms.conditions{cond_i}]);
%         end
%         
%     end
% end
% 
% %ITPC:
% for cluster_i = 1:size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,1)
%     for pixel_i = 1:size(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND,2)
%         if ~any(any(any(squeeze(ITPC_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,:,:,:))))) %3Dimensional Check to see if There exists any data.
%             fprintf('Data at Cluster: %i Pixel: %i, is not valid, Moving on...\n',cluster_i,pixel_i);
%             continue;
%         end
%         title_str=sprintf('Topology at ITPC vs RT Peak Location: \n Frequency %3.3f Hz, Time %3.3f ms \n F-Stat: %3.3f P-value: %3.3f',...
%                     frequencies(ITPC_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(1)),...
%                     times(plot_times(ITPC_clusters.(cluster_name{cluster_i}).PixelIdxLocation{pixel_i}(2))),...
%                     cluster_fStat_ITPC(cluster_i,pixel_i),...
%                     cluster_pfStat_ITPC(cluster_i,pixel_i));
%         figure();set(gcf,'Color',[1 1 1],'Position',[0 0 1920 1080]);        
%         suptitle(title_str); %super title
%         for cond_i = 1:size(cluster_rStat_ITPC,4)
%             h=subplot(2,2,cond_i);
%             plot_data = squeeze(cluster_rStat_ITPC(cluster_i,pixel_i,:,cond_i));
%             plot_p_data = squeeze(cluster_prStat_ITPC(cluster_i,pixel_i,:,cond_i));
%             %topoplot: 
%             data.avg = zeros(72,1);
%             %eval(['currentfile = ' datastruct_cue{(contrasts_i)} ';']);
%             %eval(['currentpval = ' datastruct_pval{(contrasts_i)} ';']);
%             %eval(['currentpval_corrected = ' datastruct_pval_corrected{(contrasts_i)} ';']);
%             threshold = 0.05;
%             data4plot_ind = find(plot_p_data<threshold);
%             %labels_ind = find(currentpval_corrected<threshold);
% 
%             cfg.highlightchannel = labels(data4plot_ind);
%             data4plot = zeros(1,64);
%             data4plot(data4plot_ind)=plot_data(data4plot_ind);
%             data.avg = abs([data4plot';0;0;0;0;0;0;0;0]);
%             clear pval_i
%             data.var = zeros(72,1);
%             data.time = 1;
%             data.label = labels;
%             data.dimord = 'chan_time';
%             data.cov = zeros(72,72);
%         
%             ft_topoplotER(cfg, data);
%             title([wpms.conditions{cond_i}]);
%             %title(h,[wpms.conditions{cond_i}]);
%         end
%         
%     end
% end
% 
% 
% %% show significant clusters
% figure();set(gcf,'Color',[1 1 1],'Position',[0 0 1920 1080]);
% subplot(2,2,1);
% contourf(times(plot_times),frequencies,labelmatrix(ERSP_clusters.frontal))
% axis square;
% subplot(2,2,2);
% contourf(times(plot_times),frequencies,labelmatrix(ITPC_clusters.frontal))
% axis square;
% subplot(2,2,3);
% contourf(times(plot_times),frequencies,labelmatrix(ERSP_clusters.parietal))
% axis square;
% subplot(2,2,4);
% contourf(times(plot_times),frequencies,labelmatrix(ITPC_clusters.parietal))
% axis square;
% %% create topography plots for clusters
% %first find common significant pixels
% figure();
% ERSP_pixels = [];
% for cluster_i = 1:length(ERSP_clusters.frontal.PixelIdxList)
%     ERSP_pixels = [ERSP_pixels ; cell2mat(ERSP_clusters.frontal.PixelIdxList(cluster_i))];
% end
% for cluster_i = 1:length(ERSP_clusters.parietal.PixelIdxList);
%     ERSP_pixels = [ERSP_pixels ; cell2mat(ERSP_clusters.parietal.PixelIdxList(cluster_i))];
% end
% 
% pixels=unique(ERSP_pixels);
% repeat_pixels_ERSP = [];
% for pixel_i =1:length(pixels)
%     if length(find(ERSP_pixels==pixels(pixel_i)))>1
%         repeat_pixels_ERSP = [repeat_pixels_ERSP pixels(pixel_i)];
%     end
% end
% %ITPC
% ITPC_pixels = [];
% for cluster_i = 1:length(ITPC_clusters.frontal.PixelIdxList)
%     ITPC_pixels = [ITPC_pixels ; cell2mat(ITPC_clusters.frontal.PixelIdxList(cluster_i))];
% end
% for cluster_i = 1:length(ITPC_clusters.parietal.PixelIdxList);
%     ITPC_pixels = [ITPC_pixels ; cell2mat(ITPC_clusters.parietal.PixelIdxList(cluster_i))];
% end
% 
% pixels=unique(ITPC_pixels);
% repeat_pixels_ITPC = [];
% for pixel_i =1:length(pixels)
%     if length(find(ITPC_pixels==pixels(pixel_i)))>1
%         repeat_pixels_ITPC = [repeat_pixels_ITPC pixels(pixel_i)];
%     end
% end
% % show common pixels (debugging)
% blank_data = zeros(ERSP_clusters.frontal.ImageSize);
% blank_data(repeat_pixels_ERSP)=1;
% contourf(times(plot_times),frequencies,fStat_ERSP(:,plot_times),50,'linecolor','none');hold on;
% contour(times(plot_times),frequencies,blank_data,1,'linecolor','k');
% %%
% %correlations
% %ERSP
% for cond_i = 1:length(wpms.conditions)
%     subplot(2,length(wpms.conditions),cond_i);
%     contourf(times(plot_times),frequencies,squeeze(rStat_ERSP(:,plot_times,cond_i)),50,'linecolor','none');colormap jet;caxis([-.4 .4]);
%     hold on;
%     contour(times(plot_times),frequencies,squeeze(prStatCorr_ERSP(:,:,cond_i)),1,'k');axis square
%     title(['ERSP ' wpms.conditions{cond_i}])
% end
% %ITPC
% for cond_i = 1:length(wpms.conditions)
%     subplot(2,length(wpms.conditions),cond_i+length(wpms.conditions));
%     contourf(times(plot_times),frequencies,squeeze(rStat_ITPC(:,plot_times,cond_i)),50,'linecolor','none');colormap jet;caxis([-.4 .4]);
%     hold on;
%     contour(times(plot_times),frequencies,squeeze(prStatCorr_ITPC(:,:,cond_i)),1,'k');axis square;
%     title(['ITPC ' wpms.conditions{cond_i}])
% end
% %% for data exploration purposes - uncorrected p
% figure();
% prStatUnCorr_ERSP = zeros(size(prStat_ERSP));
% prStatUnCorr_ERSP(prStat_ERSP<.05) = 1;
% prStatUnCorr_ITPC = zeros(size(prStat_ITPC));
% prStatUnCorr_ITPC(prStat_ITPC<.05) = 1;
% %correlations
% %ERSP
% for cond_i = 1:length(wpms.conditions)
%     subplot(2,length(wpms.conditions),cond_i);
%     contourf(times(plot_times),frequencies,squeeze(rStat_ERSP(:,plot_times,cond_i)),50,'linecolor','none');colormap jet;caxis([-.4 .4]);
%     hold on;
%     contour(times(plot_times),frequencies,squeeze(prStatUnCorr_ERSP(:,plot_times,cond_i)),1,'k');axis square
%     title(['ERSP ' wpms.conditions{cond_i}])
% end
% %ITPC
% for cond_i = 1:length(wpms.conditions)
%     subplot(2,length(wpms.conditions),cond_i+length(wpms.conditions));
%     contourf(times(plot_times),frequencies,squeeze(rStat_ITPC(:,plot_times,cond_i)),50,'linecolor','none');colormap jet;caxis([-.4 .4]);
%     hold on;
%     contour(times(plot_times),frequencies,squeeze(prStatUnCorr_ITPC(:,plot_times,cond_i)),1,'k');axis square;
%     title(['ITPC ' wpms.conditions{cond_i}])
% end