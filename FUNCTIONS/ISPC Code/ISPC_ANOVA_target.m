% make sure ISPC is baselined to right time and not baselined to all repeat
% find clusters from power - find peak for each cluster, take window around time & frequency at that cluster
% run anova on ISPC for each electrode
% display plots
%
% Frontal:
% freq = 7.88
% time = 377 (706)
%
% Parietal:
% freq = 5.788
% time = 312.5 (673)


%% run pixel-wise anovas and correlations
% continuing from GenerateMontana_TFPlots_forANOVA_DATE.m
% DATADIR = 'F:\FNL_EEG_TOOLBOX\EEGLAB_FORMAT\';
% wpms.dirs.PACKAGES = 'E:\FNL_EEG_TOOLBOX\PACKAGES\';
% CONDITIONS  = {'switchto','switchaway','noninfrepeat','noninfswitch','mixrepeat','allrepeat'};
%
clear all
close all
clc

listings = dir('E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\*mixrepeat_ALL_POWER3.mat');
NAMES = {length(listings)};
for file_i =1:length(listings) %CHECK: set to 5 to check
    NAMES{file_i} = listings(file_i).name(1:6);
end



addpath(genpath('E:\FNL_EEG_TOOLBOX\FUNCTIONS\'))
datain = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\NoBL\';
labels = {'Fp1','AF7','AF3','F1','F3','F5','F7','FT7','FC5','FC3','FC1','C1','C3','C5',...
    'T7','TP7','CP5','CP3','CP1','P1','P3','P5','P7','P9','PO7','PO3','O1','Iz','Oz',...
    'POz','Pz','CPz','Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4','F6','F8','FT8','FC6',...
    'FC4','FC2','FCz','Cz','C2','C4','C6','T8','TP8','CP6','CP4','CP2','P2','P4','P6',...
    'P8','P10','PO8','PO4','O2'}';
% % load in RT variability data
% filenameAV = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\meansITC.mat';
% filenameSD = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\stdevITC.mat';
% load('E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\good_inds_v8_170117.mat')
% [control_sdevs]=createCVmeasure(NAMES,filenameAV,filenameSD,good_inds);
% %collapse across noninfs
% % dvData for correlations
% dvData = [control_sdevs(:,1),control_sdevs(:,2),...
%                  control_sdevs(:,5),[control_sdevs(:,3)+control_sdevs(:,4)]./2];
%frequencies and times
frequencies = logspace(log10(2),log10(30),80);
times = (-1:0.001953125000000:3.5)*1000;

wpms.conditions         ={'allrepeat',  'mixrepeat',    'noninfrepeat', 'switchto',     'switchaway',   'noninfswitch'};

%wpms.conditions_OLD     ={'allrepeat',  'mixrepeat',    'switchto',     'switchaway',   'noninfrepeat', 'noninfswitch'};

%CONDITIONSTARGET_OLDtoNEW = [1 2 5 3 4 6];

Frontal_Labels = {'FCz','FC1','FC2','Cz','C1','C2'};%frontal
Parietal_Labels = {'CPz','CP1','CP2','Pz','P1','P2'};%parietal

CHANNEL_CLUSTER = {Frontal_Labels,Parietal_Labels};

cluster_names = {'frontal','parietal'};

baseline_start = 700;%start of plot
baseline_end = 900;%end of plot
%find values
temp_times = times-baseline_start;
[~,baselinetimestart_index] = min(abs(temp_times));
temp_times = times-baseline_end;
[~,baselinetimeend_index] = min(abs(temp_times));
%store in a single variable
baseline_time = baselinetimestart_index:baselinetimeend_index;

ISPCdir = 'E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\';

addpath(genpath('E:\FNL_EEG_TOOLBOX\PACKAGES\mass_uni_toolbox\'))
%%
% This data is after the ISPC_anovacode_power_target.m 
% Located in E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\NoBL\
load('E:\FNL_EEG_TOOLBOX\PowerOutput_Processed\SurfaceLapacian_FULL\POWER3\FrequencyMatrices\NoBL\CLUSTER_MaxPixelLoc_target_20170502.mat');

plottime_start = 700;%start of plot
    plottime_end = 2000;%end of plot
    %find values
    temp_times = times-plottime_start;
    [~,plottimestart_index] = min(abs(temp_times));
    temp_times = times-plottime_end;
    [~,plottimeend_index] = min(abs(temp_times));
    %store in a single variable
    plot_times = plottimestart_index:plottimeend_index;

theta_min = 4;
[~,theta_min_inds] = min(abs(frequencies-theta_min));
theta_max = 14;
[~,theta_max_inds] = min(abs(frequencies-theta_max));
freq_range = theta_min_inds:theta_max_inds;

alpha =.001;
% will check this when we know how many clusters & pixels!
pixels = {[3];[3]};

for cluster_i = 1:length(cluster_names)
    fprintf('Currently Running Cluster %i: \t %s\n\n', cluster_i, cluster_names{cluster_i});
    %This has been previously calculated in File: 'ISPC_anovacode_power_target.m' 
     
    for pixel_i = pixels{cluster_i}
    
        %Save [F,T]:
        width = 120;  %Time Bins %60
        height = 6; %Frequency Bins:
        Location = ERSP_clusters.(cluster_names{cluster_i}).PixelIdxLocation{pixel_i};
        if isempty(Location)
            disp('Moving on here 2:');
            continue;
        else
            freq_max = Location(1);
            time_max = Location(2);
            
            freq_i = freq_max-height/2:1:freq_max+height/2;
            %AW CHANGED HERE: 2017/05/02 Pulling incorect times: was
            %plot_times(time_max-width/2) etc..
            timewindows = time_max-width/2:1:time_max+width/2;
            
            ispc_means = zeros(length(NAMES),length(wpms.conditions),length(labels));
            
            for name_i = 1:length(NAMES)
                tic
                fprintf('\n%s',NAMES{name_i})
                for condition_i = 1:length(wpms.conditions)
                    fprintf('\t%s',wpms.conditions{condition_i})
                    
                   
                    if cluster_i == 1
                       filename=['Z:\ITPC\PROCESSED\SurfaceLapacian\' NAMES{name_i} '_' wpms.conditions{condition_i} '_ISPC_MF_TARGET.mat'];
                    elseif cluster_i == 2
                        filename=['Z:\ITPC\PROCESSED\SurfaceLapacian\' NAMES{name_i} '_' wpms.conditions{condition_i} '_ISPC_CP_TARGET.mat'];
                    end
                        load (filename,'ispc_all')
                    ispc_bl = zeros(size(ispc_all));
                    for chan_i = 1:length(labels)
                        % deleted abs
                        temp     = (bsxfun(@minus,(squeeze(ispc_all(chan_i,:,:))),(mean(squeeze(ispc_all(chan_i,:,baseline_time)),2))));
                        bl     = (mean(squeeze(ispc_all(chan_i,:,baseline_time)),2));
                        temp2 = zeros(size(temp));
                        for temp_i = 1:size(temp,1)
                            temp2(temp_i,:) = temp(temp_i,:)./bl(temp_i); %Ratio: Change over Baseline
                        end
                        ispc_bl(chan_i,:,:) = 100*(temp2); %Percentage Change over baseline
                        clear temp temp2 bl
                    end%chan_i loop
                    reduced_ispc = ispc_bl(:,freq_range,plot_times);
                    if min(freq_i)<0
                        shift = abs(min(freq_i))+1;
                        freq_i = freq_i +shift;
                    end
                    if max(freq_i)>length(freq_range)
                        shift = abs(max(freq_i)-length(freq_range));
                        freq_i = freq_i - shift;
                    end
                    ispc_means(name_i,condition_i,:) = squeeze (mean (mean(reduced_ispc(:,freq_i,timewindows),2),3));
                end
                toc
            end
        end
        savename=[ISPCdir cluster_names{cluster_i} '_pixel_' num2str(pixel_i) '_ISPC_noarbaseline_widerwindow_target.mat'];
        save(savename, 'ispc_means','-v7.3');
    end
end
%%
%Set up output variables:

savename=[ISPCdir cluster_names{1} '_pixel_' num2str(pixel_i) '_ISPC_noarbaseline_widerwindow_target.mat'];
load(savename)


cluster_pfStat_ISPC = zeros(1,...
                            size(ispc_means,3));
cluster_fStat_ISPC = cluster_pfStat_ISPC;

%dimension for order of data for input to anova
n    = length(wpms.conditions);%number of conditions
nDim = 2;%where conditions are stored in the multidimensional matrix
squeezeDims = [];%singleton dimensions that we ignore
nonsqueezeDims = [1 2];%non-singleton dimensions that we care about
pixels = {[3];[3]};

for cluster_i = 1:length(cluster_names)
    for pixel_i = pixels{cluster_i}
    savename=[ISPCdir cluster_names{cluster_i} '_pixel_' num2str(pixel_i) '_ISPC_noarbaseline_widerwindow_target.mat'];
    load(savename)
        for chan_i = 1:size(ispc_means,3)
%             ANOVA_DATA = ispc_means(:,:,chan_i);
%             ANOVA_DATA_t = catData(ANOVA_DATA,n,nDim,squeezeDims,nonsqueezeDims);%reorder ERSP data for anova
% 
%             conditionLabels = cell(size(ANOVA_DATA_t))';
%             conditionLabels=makeLabels(wpms,conditionLabels);%labels for anova
            data= squeeze(ispc_means(:,:,chan_i));
            t = table(data(:,1),data(:,2),data(:,3),data(:,4),data(:,5),data(:,6),...
                'VariableNames',{'data1','data2','data3','data4','data5','data6'});
            Meas = dataset([1 2 3 4 5 6]','VarNames',{'Measurements'});

            rm = fitrm(t,'data1-data6~1','WithinDesign',Meas);

            ranovatbl = ranova(rm);
            cluster_pfStat_ISPC(1,chan_i)=ranovatbl{1,5};
            cluster_fStat_ISPC(1,chan_i)=ranovatbl{1,4};
%             [cluster_pfStat_ISPC(1,chan_i),tbl,~] = anova1(ANOVA_DATA_t,conditionLabels,'off');%run anova
%             cluster_fStat_ISPC(1,chan_i)= double(tbl{2,5});clear tbl;
        end
        %clear data_ERSP data_ITPC cDat* conditionLabels corrData*
        save([ISPCdir cluster_names{cluster_i} '_' num2str(pixel_i) '_ISPC_STATS_TARGET_050517.mat'],'cluster_*','-v7.3');
    end
end
%%
warning off
figure();set(gcf,'Color',[1 1 1],'Position',[0 0 1920 1080]);
cluster_labels = {'Frontal','Parietal'};
condition_names = {'RA','RM','RN','ST','SA','SN'};
ylimits = {[0 100];[0 110]};

count=0;
% pixels = {[3];[1:3]};
pixels = {[3];[3]};
for cluster_i = 1:length(cluster_names)
   
    for pixel_i = pixels{cluster_i}
         count=count+1;
    load([ISPCdir cluster_names{cluster_i} '_' num2str(pixel_i) '_ISPC_STATS_TARGET_050517.mat']);
    
    cfg = [];
    cfg.zlim        = [0 10];
    cfg.colormap    = 'hot';
    cfg.parameter   = 'avg';
    cfg.layout      = 'biosemi64.lay';
    cfg.comment     = 'no';
    cfg.marker      = 'off';
    cfg.channel     = labels;
    cfg.contournum  = 4;
    cfg.gridscale    = 600;
    alpha =.001;
    [h, crit_p, adj_p]=fdr_bh(cluster_pfStat_ISPC,alpha,'pdep');
    p = cluster_pfStat_ISPC;
    siglabels = {labels{adj_p<alpha}};
    fprintf(2,'\n%s\t',cluster_names{cluster_i});
    disp(siglabels)
    cfg.highlight = 'on';
    cfg.highlightsymbol = '*';
    cfg.highlightchannel   = siglabels;
    data =[];
    data.avg   = zeros(72,1);
    data_temp  = zeros(64,1);
    data_temp(p<alpha)  = cluster_fStat_ISPC(p<alpha)';

    data.avg = [data_temp;0;0;0;0;0;0;0;0];
    data.avg = (data.avg);
    clear pval_i
    data.var = zeros(72,1);
    data.time = 1;
    data.label = labels;
    data.dimord = 'chan_time';
    data.cov = zeros(72,72);
    subplot(2,6,count); ft_topoplotER(cfg, data);
    title(cluster_labels{cluster_i});
    %now bar plots
    count=count+1;
    
    
    loadname=[ISPCdir cluster_names{cluster_i} '_pixel_' num2str(pixel_i) '_ISPC_noarbaseline_widerwindow_target.mat'];
    load(loadname, 'ispc_means');
    
    temp_data = squeeze(mean(ispc_means(:,:,p<alpha),3));
    temp_data_sd = std(temp_data)./sqrt(size(temp_data,1));
    subplot(2,6,count);bar(mean(temp_data));set(gca,'XTickLabels',condition_names,'XTickLabelRotation',45);
    axis square;box off;ylim(ylimits{cluster_i});xlim([0 size(temp_data,2)+1]);
    hold on;errorbar(mean(temp_data),temp_data_sd,'.k');
    ylabel('ISPC (% change from baseline)');set(gca,'Fontsize',16');
    contrasts=[1 2 1 3 1 4 1 5 2 3 2 4 2 5 3 4 3 5 4 5];
    sigchannels_to_test = find(p<alpha==1);
%     for i=1:2:length(contrasts);
%         A=contrasts(i);
%         B=contrasts(i+1);
%         [h,p,ci,stats]=ttest(temp_data(:,A),temp_data(:,B));
%         fprintf('\n\t%s %s %s %s %2.2f %s %2.2f',condition_names{A}, 'vs', condition_names{B},'t=',stats.tstat,'p=',p);
%     end
    for chann_i = 1:length(sigchannels_to_test)
        fprintf('\n%s',labels{sigchannels_to_test(chann_i)})
        test_data = squeeze(mean(ispc_means(:,:,sigchannels_to_test(chann_i)),3));
        for i=1:2:length(contrasts);
            A=contrasts(i);
            B=contrasts(i+1);
            [h,p,ci,stats]=ttest(test_data(:,A),test_data(:,B));
            fprintf('\n\t%s %s %s %s %2.2f %s %2.2f',condition_names{A}, 'vs', condition_names{B},'t=',stats.tstat,'p=',p);
        end
    end
    end
end
%%
% Set Window Size for Averaging:

% Perform Average:
% for freq_i = freq_max-height/2:1:freq_max+height/2
%     fprintf('\t\tWorking on Frequency Index: %i\n',freq_i );
%     load([LOADDIR 'ERSP_' num2str(freq_i) '.mat']);
%     ERSP_DATA(count,:,:,:,:) = ERSP_F_t;
%     count = count+1;
% end
% fprintf('\tAveraging over Frequencing and Time\n' );
% timewindows = plot_times(time_max-width/2):1:plot_times(time_max+width/2);
% ERSP_CLUSTER_PIXEL_CHANNEL_SUB_COND(cluster_i,pixel_i,:,:,:) = squeeze(mean(squeeze(mean(ERSP_DATA(:,timewindows,:,:,:),1)),1));
% 
